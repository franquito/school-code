#include <stdio.h>

int main(int argc, char *argv[]) {
  int goodlines = 0;
  // A:65 a Z:90
  int tracker = 65;

  char *line = NULL;
  size_t size;
  while (getline(&line, &size, stdin) != -1 &&
         (line[0]-65) == goodlines && 
         goodlines < 91) {
    tracker++;
    goodlines++;
  }
  printf("Good lines: %d\n", goodlines);
  return 0;
}
