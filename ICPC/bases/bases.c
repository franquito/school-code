#include <stdio.h>
#include "bases.h"
#include <string.h>

Polynomial toPolynomial(char *n, int len) {
  Polynomial p;
  p.len = len;
  int i;
  for (i = len-1; i > -1; i--) {
    p.data[i] = *(n+i)-48;
  }
  return p;
}

Polynomial operate(Token op1, Token op, Token *op2, 
                   int lenop2) {
  if (lenop2 == 1) {
    if (op.data[0] == '+')
      return poly_sum(toPolynomial(op1.data, op1.len), toPolynomial(op2.data, op1.len));
    else (op.data[0] == '*')
      return poly_mul(toPolynomial(op1.data, op1.len), toPolynomial(op2.data, op1.len));
  } else {
    if (op.data[0] == '+' && *(op2+1).data[0] == '*') {
      
    }
  }
    
}

int main(int argc, char *argv[]) {
  Token lexed[100];
  int l = 0; /* index of lexed */

  char *line = NULL;
  size_t size;
  while (getline(&line, &size, stdin) != -1) {
    int dindex = 0;
    int num_len = 0;
    while (dindex < strlen(line)) {
      /* @todo: Add search of the maximum digit */
      if (line[dindex] > 48 && line[dindex] < 59) {
        num_len++;
        printf("En dígito: %d\n", line[dindex]);
        dindex++;
      } else if (line[dindex] == '+' || line[dindex] == '*') {
        Token t = {num_len, line+dindex-num_len};
        lexed[l] = t; l++;
        Token r = {1, line+dindex};
        lexed[l] = r; l++;
        num_len = 0;
        dindex++;
      } else if (line[dindex] == '=' || line[dindex] == '\n') {
        Token t = {num_len, line+dindex-num_len};
        lexed[l] = t; l++;
        num_len = 0;
        int j = 0;
        printf("Hola %d\n", lexed[0].len);
        /* Process all lexed information. */
        if (l > 1)
          operate(lexed, lexed[1], lexed[2], l-2);

        if (line[dindex] == '=') {
          // for (; j < *(lexed+4).len; j++)
          //   printf("%c", *(lexed+4).data[j]);
          // printf("\n");
          l = 0;
        }
        dindex++;
      }
    }
  }
  return 0;
}
