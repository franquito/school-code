#include <stdio.h>

int main(int argc, char *argv[]) {
  char *line = NULL;
  size_t size;
  getline(&line, &size, stdin);
  int len;
  sscanf(line, "%d", &len);
  char a = 'z';
  // a: 97, z: 122
  char letters[26];
  int i;
  for (i = 0; i < 26; i++)
    letters[i] = 0;
  for (i = 0; i < len; i++) {
    getline(&line, &size, stdin);
    letters[line[0]-97]++;
  }
  int non_empty = 0;
  for (i = 0; i < 26; i++) {
    if (letters[i] > 4) {
      non_empty = 1;
      printf("%c", i+97);
    }
  }
  if (!non_empty)
    printf("PREDAJA");
  printf("\n");
  return 0;
}
