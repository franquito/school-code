Basado en:
Functional parsing library from chapter 8 of Programming in Haskell,
Graham Hutton, Cambridge University Press, 2007.

Modificado por Mauro Jaskelioff

> module Parsing where
>
> import Data.Char
> import Control.Monad
> import Control.Applicative hiding (many)
> 

The monad of parsers
--------------------

> newtype Parser a              =  P (String -> [(a,String)])
>
> instance Monad Parser where
>    return v                   =  P (\inp -> [(v,inp)])
>    p >>= f                    =  P (\inp -> [ x | (v,out) <- parse p inp, x <- parse (f v) out])
> 
> instance MonadPlus Parser where
>    mzero                      =  P (\_	 -> [])
>    p `mplus` q                =  P (\inp -> case parse p inp of
>                                                []        -> parse q inp
>                                                x         -> x)
> 
> instance Functor Parser where
>   fmap = liftM
>   
> instance Applicative Parser where
>   pure                        = return
>   (<*>)                       = ap
>   

Choice
------

> instance Alternative Parser where
>   empty                       = mzero
>   (<|>)                       = mplus

Basic parsers
-------------

> failure                       :: Parser a
> failure                       =  mzero
>
> item                          :: Parser Char
> item                          =  P (\inp -> case inp of
>                                                []     -> []
>                                                (x:xs) -> [(x,xs)])
> 
> parse                         :: Parser a -> String -> [(a,String)]
> parse (P p) inp               =  p inp

Derived primitives
------------------

> sat                           :: (Char -> Bool) -> Parser Char
> sat p                         =  do x <- item
>                                     if p x then return x else failure
> 
> digit                         :: Parser Char
> digit                         =  sat isDigit
> 
> lower                         :: Parser Char
> lower                         =  sat isLower
> 
> upper                         :: Parser Char
> upper                         =  sat isUpper
> 
> letter                        :: Parser Char
> letter                        =  sat isAlpha
> 
> alphanum                      :: Parser Char
> alphanum                      =  sat isAlphaNum
> 
> char                          :: Char -> Parser Char
> char x                        =  sat (== x)
> 
> string                        :: String -> Parser String
> string []                     =  return []
> string (x:xs)                 =  do char x
>                                     string xs
>                                     return (x:xs)
> 
> many                          :: Parser a -> Parser [a]
> many p                        =  many1 p <|> return []
> 
> many1                         :: Parser a -> Parser [a]
> many1 p                       =  do v  <- p
>                                     vs <- many p
>                                     return (v:vs)
> 
> ident                         :: Parser String
> ident                         =  do x  <- lower
>                                     xs <- many alphanum
>                                     return (x:xs)
> 
> nat                           :: Parser Int
> nat                           =  do xs <- many1 digit
>                                     return (read xs)
>
> int                           :: Parser Int
> int                           =  do char '-'
>                                     n <- nat
>                                     return (-n)
>                                   <|> nat
> 
> space                         :: Parser ()
> space                         =  do many (sat isSpace)
>                                     return ()
>	
> sepBy                         :: Parser a -> Parser sep -> Parser [a]
> sepBy p sep                   =  sepBy1 p sep <|> return []
>
> sepBy1                        :: Parser a -> Parser sep -> Parser [a]
> sepBy1 p sep        		= do{ x <- p
>                         	    ; xs <- many (sep >> p)
>                         	    ; return (x:xs) }	
>
> endBy1                        :: Parser a -> Parser sep -> Parser [a]
> endBy1 p sep                  = many1 (do { x <- p; sep; return x })
>
> endBy                         :: Parser a -> Parser sep -> Parser [a]
> endBy p sep                   	= many (do{ x <- p; sep; return x })
>
>

Ignoring spacing
----------------

> token                         :: Parser a -> Parser a
> token p                       =  do space
>                                     v <- p
>                                     space
>                                     return v
> 
> identifier                    :: Parser String
> identifier                    =  token ident
> 
> natural                       :: Parser Int
> natural                       =  token nat
> 
> integer                       :: Parser Int
> integer                       =  token int
>
> symbol                        :: String -> Parser String
> symbol xs                     =  token (string xs)
>



> p :: Parser String
> p = do char '['
>        d <- digit
>        ds <- many (do char ','
>                       digit)
>        char ']'
>        return (d:ds)

Ejercicio 2.
expr -> term ('+' expr | '-' expr | e)
term -> factor ('*' term | '/' term | e)

> -- expr :: Parser Int
> -- expr = do t <- term
> --           (do char '+'
> --               e <- expr
> --               return (t + e)
> --             <|>
> --            do char '-'
> --               e <- expr
> --               return (t - e)
> --             <|> return t)
> -- 
> -- term :: Parser Int
> -- term = do f <- factor
> --           (do char '*'
> --               t <- term
> --               return (f * t)
> --             <|>
> --            do char '/'
> --               t <- term
> --               return (div f t)
> --             <|> return f)
> -- 
> -- factor :: Parser Int
> -- factor = do d <- digit
> --             return (digitToInt d)
> --           <|>
> --          do char '('
> --             e <- expr
> --             char ')'
> --             return e

Ejercicio 3. Parser parens friendly.

> withParens :: Parser a -> Parser a
> withParens p = do t <- p
>                   return t
>              <|>
>                do char '('
>                   t <- p
>                   char ')'
>                   return t

Example.

parse (withParens digit) "(1)23"

Ejercicio 4. Using trees instead of just computing.

> -- data Expr = Num Int | BinOp Op Expr Expr deriving Show
> -- data Op   = Add | Mul | Min | Div deriving Show
> -- 
> -- expr :: Parser Expr
> -- expr = do t <- term
> --           (do char '+'
> --               e <- expr
> --               return (BinOp Add t e)
> --            <|>
> --            do char '-'
> --               e <- expr
> --               return (BinOp Min t e)
> --            <|>
> --            return t)
> -- 
> -- term :: Parser Expr
> -- term = do f <- factor
> --           (do char '*'
> --               t <- term
> --               return (BinOp Mul f t)
> --             <|>
> --            do char '/'
> --               t <- term
> --               return (BinOp Div f t)
> --             <|> return f)
> -- 
> -- factor :: Parser Expr
> -- factor = do d <- digit
> --             return (Num (digitToInt d))
> --           <|>
> --          do char '('
> --             e <- expr
> --             char ')'
> --             return e

Ejercicio 5. Cool Lists.

> -- data Chint = C Char | I Int deriving Show
> -- 
> -- get_chint :: Parser Chint
> -- get_chint = do d <- digit
> --                return (I (digitToInt d))
> --              <|>
> --             do char '\''
> --                c <- item
> --                char '\''
> --                return (C c)
> -- 
> -- cool_list :: Parser [Chint]
> -- cool_list = do char '['
> --                c <- get_chint
> --                cs <- many (do char ','
> --                               get_chint)
> --                char ']'
> --                return (c:cs)

Ejercicio 6. Haskell Datatypes.

> -- data Basetype = DInt | DChar | DFloat deriving Show
> -- 
> -- token6 :: Parser Basetype
> -- token6 = do string "Int"
> --             return DInt
> --           <|>
> --          do string "Char"
> --             return DChar
> --           <|>
> --          do string "Float"
> --             return DFloat
> -- 
> -- hasktype6 :: Parser [Basetype]
> -- hasktype6 = do x <- token6
> --                xs <- many (do string "->"
> --                               token6)
> --                return (x:xs)

Ejercicio 7.

> data Hasktype = DInt | DChar | Fun Hasktype Hasktype deriving Show
> 
> hasktype7 :: Parser Hasktype
> hasktype7 = do a <- first_arg
>                (do string "->"
>                    b <- hasktype7
>                    return (Fun a b)
>                  <|>
>                 return a)
> 
> first_arg :: Parser Hasktype
> first_arg = do char '('
>                e <- hasktype7
>                char ')'
>                return e
>              <|>
>             do string "Int"
>                return DInt
>              <|>
>             do string "Char"
>                return DChar

Ejercicio 8.

expr  -> term expr'
expr' -> ('+' term | '-' term) expr' | e

> expr :: Parser Int
> expr = do t <- term
>           expr' t
> 
> expr' :: Int -> Parser Int
> expr' n = do char '+'
>              t <- term
>              e <- expr' (n + t)
>              return e
>            <|>
>           do char '-'
>              t <- term
>              e <- expr' (n - t)
>              return e
>            <|>
>           return n

term -> factor term'
term' -> ('*' factor | '/' factor) term' | e

> term :: Parser Int
> term = do f <- factor
>           term' f
> 
> term' :: Int -> Parser Int
> term' t = do char '*'
>              f <- factor
>              e <- term' (t * f)
>              return e
>            <|>
>           do char '/'
>              f <- factor
>              e <- term' (div t f)
>              return e
>            <|> return t

factor -> digit | '(' expr ')'

> factor :: Parser Int
> factor = do d <- digit
>             return (digitToInt d)
>           <|>
>          do char '('
>             e <- expr
>             char ')'
>             return e

Ejercicio 9. C types.

> data Pointer = PointerTo deriving Show
> data CType = CInt | CFloat | CChar deriving Show
> data CDeclaration = C CType [Pointer] String [Int] deriving Show

declaration -> type_specifier declarator ';'

> declaration :: Parser CDeclaration
> declaration = do t <- type_specifier
>                  (p, i, a) <- declarator
>                  char ';'
>                  return (C t p i a)
> 
> type_specifier :: Parser CType
> type_specifier = do string "int "
>                     return CInt
>                   <|>
>                  do string "float "
>                     return CFloat
>                   <|>
>                  do string "char "
>                     return CChar
> 
> declarator :: Parser ([Pointer], String, [Int])
> declarator = do char '*'
>                 (p, i, a) <- declarator
>                 return (PointerTo:p, i, a)
>               <|>
>              do (i, a) <- direct_declarator
>                 return ([], i, a)
> 
> direct_declarator :: Parser (String, [Int])
> direct_declarator = do char '('
>                        (s, as) <- direct_declarator
>                        char ')'
>                        as' <- brackets
>                        return (s, as ++ as')
>                      <|>
>                     dd
> 
> dd :: Parser (String, [Int])
> dd = do s <- identifier
>         a <- brackets
>         return (s, a)
> 
> brackets :: Parser [Int]
> brackets = do char '['
>               d <- constant_expression
>               char ']'
>               ds <- brackets
>               return (d:ds)
>             <|>
>            return []
> 
> constant_expression :: Parser Int
> constant_expression = nat
