p :: Parser String
p = do char '['
       d <- digit
       ds <- many (do char ','
                      digit)
       char ']'
       return (d:ds)

-- expr -> term ('+' expr | '-' expr | e)


expr :: Parser Int
expr = do t <- term
          (do char '+'
              e <- expr
              return (t + e)
            <|> return t)

term :: Parser Int
term = do f <- factor
          (do char '*'
              t <- term
              return (f * t)
            <|> return f)

factor :: Parser Int
factor = do d <- digit
            return (digitToInt d)
          <|>
         do char '('
            e <- expr
            char ')'
            return e
