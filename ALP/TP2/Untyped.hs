module Untyped where

import Data.List
import Data.Char
import Text.ParserCombinators.Parsec
import Text.Parsec.Token
import Text.Parsec.Language
import PrettyPrinter

import Common

------------------------
-- Ejercicio 1
------------------------

num :: Int -> LamTerm
num n = Abs "s" (Abs "z" (num' (LVar "z") n))

num' :: LamTerm -> Int -> LamTerm
num' t 0 = t
num' t n = num' (App (LVar "s") t) (n-1)

-----------------------
--- Sección 2 Parsers
-----------------------

totParser :: Parser a -> Parser a
totParser p = do whiteSpace untyped
                 t <- p
                 eof
                 return t

-- Analizador de Tokens
untyped :: TokenParser u
untyped = makeTokenParser
  (haskellStyle { identStart = letter <|> char '_',
                  reservedNames = ["def"] })

 
-- Parser para comandos
parseStmt :: Parser a -> Parser (Stmt a)
parseStmt p = do
          reserved untyped "def"
          x <- identifier untyped
          reservedOp untyped "="
          t <- p
          return (Def x t)
    <|> fmap Eval p

 
parseTermStmt :: Parser (Stmt Term)
parseTermStmt = fmap (fmap conversion)
                     (parseStmt parseLamTerm)

-- Sugar
pparse p s = parseTest p s

-- Helpers
a ||| b = try a <|> b

reduce :: [Int] -> Int
reduce xs = red xs (length xs)

red :: [Int] -> Int -> Int
red []     _ = 0
red (x:xs) n = x*10^(n-1) + red xs (n-1)

toInts :: [Char] -> [Int]
toInts []     = []
toInts (x:xs) = (digitToInt x : toInts xs)

ident :: Parser String
ident = do l <- letter
           ls <- many alphaNum
           return (l:ls)

nat :: Parser Int
nat = do d <- digit
         ds <- many digit
         return (reduce (toInts (d:ds)))

separate :: Parser a -> Parser sep -> Parser [a]
separate p sep = do x <- p
                    sep
                    xs <- separate0 p sep
                    return (x:xs)

separate0 :: Parser a -> Parser sep -> Parser [a]
separate0 p sep = do x <- p
                     sep
                     xs <- separate0 p sep
                     return (x:xs)
                   |||
                  return []

addAbs :: [String] -> LamTerm -> LamTerm
addAbs (x:xs) t = (Abs x (addAbs xs t))
addAbs []     t = t

toApps :: [LamTerm] -> LamTerm
toApps (x:[]) = x
toApps (x:xs) = (App x (toApps xs))

-- Parser para LamTerms 
parseLamTerm :: Parser LamTerm
parseLamTerm = do char '\\'
                  vars <- sepBy1 ident (char ' ')
                  char '.'
                  spaces
                  t <- parseLamTerm
                  return (addAbs vars t)
              |||
               do t1 <- nabs
                  t2 <- napp
                  return (App t1 t2)
              |||
               atom

atom :: Parser LamTerm
atom = do n <- nat
          return (num n)
        |||
       do x <- ident
          return (LVar x)
        |||
       pterm

napp :: Parser LamTerm
napp = atom
      |||
       do char '\\'
          spaces
          vars <- sepBy1 ident (char ' ')
          char '.'
          spaces
          t <- parseLamTerm
          return (addAbs vars t)

nabs :: Parser LamTerm
nabs = do ts <- separate atom (char ' ')
          return (toApps ts)

pterm :: Parser LamTerm
pterm = do char '('
           char '\\'
           spaces
           vars <- sepBy1 ident (char ' ')
           char '.'
           spaces
           t <- parseLamTerm
           char ')'
           return (addAbs vars t)
        |||
        do char '('
           t1 <- nabs
           t2 <- napp
           char ')'
           return (App t1 t2)

-- conversion a términos localmente sin nombres
conversion  :: LamTerm -> Term
conversion t = con [] t

con :: [String] -> LamTerm -> Term
con xs (Abs x t) = Lam (con (x:xs) t)
con xs (App p q) = (con xs p) :@: (con xs q)
con xs (LVar x) = case elemIndex x xs of
                    Just x  -> Bound x
                    Nothing -> Free (Global x)

-- para testear el parser interactivamente.
testParser :: Parser LamTerm
testParser = totParser parseLamTerm

-------------------------------
-- Sección 3
-------------------------------

get :: Name -> NameEnv Value -> Maybe Value
get n []          = Nothing
get n ((m, v):xs) = if m == n then Just v else get n xs

vapp :: Value -> Value -> Value
vapp (VLam f)     b = f b
vapp (VNeutral a) b = VNeutral (NApp a b)

eval :: [(Name, Value)] -> Term -> Value
eval e t = eval' t (e, [])

eval' :: Term -> (NameEnv Value, [Value]) -> Value
eval' (Bound ii)  d               = (snd d) !! ii
eval' (Free name) (names, bstack) =
  case get name names of
    Just a  -> a
    Nothing -> VNeutral (NFree name)
eval' (Lam term)  (names, bstack) = VLam (\b -> eval' term (names, b:bstack))
eval' (p :@: q)   d = vapp (eval' p d) (eval' q d)

-------------------------------
-- Sección 4
-------------------------------

quote :: Value -> Term
quote v = qq 0 v

qq :: Int -> Value -> Term
qq _ (VNeutral (NFree (Global x))) = Free (Global x)
qq n (VNeutral (NFree (Quote k))) = Bound (n - k - 1)
qq n (VNeutral (NApp p q)) =
  (qq n (VNeutral p)) :@: (qq n q)
qq n (VLam f) = Lam (qq (n + 1) (f (VNeutral (NFree (Quote n)))))

-- TODO: Remove this.
pepe = do t <- parseLamTerm
          return (quote (eval [] (conversion t)))
