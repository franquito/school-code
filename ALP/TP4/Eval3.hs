module Eval3 (eval) where

import AST
import Control.Applicative (Applicative(..))
import Control.Monad       (liftM, ap)  

-- Para calmar al GHC
instance Functor StateErrorTick where
    fmap = liftM
 
instance Applicative StateErrorTick where
    pure   = return
    (<*>)  = ap

-- Estados
type Env = [(Variable,Int)]

-- Estado nulo
initState :: Env
initState = []

newtype StateErrorTick a = StateErrorTick {
  runStateErrorTick :: Env -> Maybe ((a, Int), Env) }

-- Clase p/representar mónadas con estado de variables
class Monad m => MonadState m where
    -- Busca el valor de una variable
    lookfor :: Variable -> m Int
    -- Cambia el valor de una variable
    update :: Variable -> Int -> m ()

-- Clase para representar mónadas que lanzan errores
class Monad m => MonadError m where
    -- Lanza un error
    throw :: m a

class Monad m => MonadTick m where
  tick :: m ()

instance Monad StateErrorTick where
  return a = StateErrorTick (\s -> Just ((a, 0), s))
  m >>= f  = StateErrorTick (\s ->
    do ((a, c), s') <- runStateErrorTick m s
       ((a', c'), s'') <- runStateErrorTick (f a) s'
       return ((a', c + c'), s''))

instance MonadError StateErrorTick where
  throw = StateErrorTick (\s -> Nothing)

instance MonadState StateErrorTick where

  lookfor v = StateErrorTick (\s -> lookfor' v s s)
    where lookfor' v ((u, j):ss) cs | v == u = Just ((j, 0), cs)
                                    | v /= u = lookfor' v ss cs
          lookfor' v _ _                     = Nothing

  update v i = StateErrorTick (\s -> Just (((), 0), update' v i s))
    where update' v i [] = [(v, i)]
          update' v i ((u, _):ss) | v == u = (v, i):ss
          update' v i ((u, j):ss) | v /= u = (u, j):(update' v i ss)

instance MonadTick StateErrorTick where
  tick = StateErrorTick $ \s -> Just (((), 1), s)

-- Evalua un programa en el estado nulo
eval :: Comm -> (Env, Int)
eval p = case runStateErrorTick (evalComm p) initState of
  Nothing          -> ([], 0)
  Just ((a, c), e) -> (e, c)

-- Evalua un comando en un estado dado
evalComm :: (MonadState m, MonadError m, MonadTick m) => Comm -> m ()
evalComm Skip = return ()
evalComm (Let v ie)    = do n <- evalIntExp ie
                            update v n
evalComm (Seq c1 c2)   = do evalComm c1
                            evalComm c2
evalComm (Cond be l r) = do bool <- evalBoolExp be
                            if bool then evalComm l
                            else evalComm r
evalComm (While be c)  = do bool <- evalBoolExp be
                            if bool then evalComm $ Seq c (While be c)
                            else return ()

-- Evalua una expresion entera, sin efectos laterales
evalIntExp :: (MonadState m, MonadError m, MonadTick m) => IntExp -> m Int
-- evalIntExp = undefined
evalIntExp (Const n) = return n
evalIntExp (Var v)   = lookfor v
evalIntExp (UMinus a) = do e <- evalIntExp a
                           return (-e)
evalIntExp (Plus ie ie') = do a <- evalIntExp ie
                              b <- evalIntExp ie'
                              tick
                              return $ a + b
evalIntExp (Minus ie ie') = do a <- evalIntExp ie
                               b <- evalIntExp ie'
                               tick
                               return $ a - b
evalIntExp (Times ie ie') = do a <- evalIntExp ie
                               b <- evalIntExp ie'
                               tick
                               return $ a * b
evalIntExp (Div ie ie') = do a <- evalIntExp ie
                             b <- evalIntExp ie'
                             tick
                             if b == 0 then throw
                             else return $ a `div` b

-- Evalua una expresion entera, sin efectos laterales
evalBoolExp :: (MonadState m, MonadError m, MonadTick m) => BoolExp -> m Bool
evalBoolExp (Eq ie ie') = do a <- evalIntExp ie
                             b <- evalIntExp ie'
                             return $ a == b
evalBoolExp (Lt ie ie') = do a <- evalIntExp ie
                             b <- evalIntExp ie'
                             return $ a < b
evalBoolExp (Gt ie ie') = do a <- evalIntExp ie
                             b <- evalIntExp ie'
                             return $ a > b
evalBoolExp (And be be') = do a <- evalBoolExp be
                              b <- evalBoolExp be'
                              return $ a && b
evalBoolExp (Or  be be') = do a <- evalBoolExp be
                              b <- evalBoolExp be'
                              return $ a || b
evalBoolExp (Not be) = do a <- evalBoolExp be
                          return $ not a
