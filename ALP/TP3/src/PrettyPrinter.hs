module PrettyPrinter (
  printTerm, -- pretty printer para términos
  printType, -- pretty printer para tipos
) where

import Common
import Text.PrettyPrint.HughesPJ

-- Main Printer
printTerm :: Term -> Doc 
printTerm t = pp 0 validvars t
  where globals     = globals_of t
        not_in v vs = not $ elem v vs
        not_global  = \v -> v `not_in` globals
        validvars   = filter not_global vars

-- lista de posibles nombres para variables
vars :: [String]
vars = [ c : n | n <- printed_nats, c <- ['x','y','z'] ++ ['a'..'w'] ]
     -- printed_nats = ["", "1", "2", ..]
  where printed_nats = [""] ++ map show [1..]

-- Get all global names.
globals_of :: Term -> [String]
globals_of (Bound _)             = []
globals_of (Free (Global n))     = [n]
globals_of (Free _)              = []
globals_of (t :@: u)             =
  globals_of t ++ globals_of u
globals_of (Lam _ u)             =
  globals_of u
globals_of (Set (Global n) st t) =
  globals_of st ++ (remove n $ globals_of t)
        -- remove removes all y from list xs
  where remove y xs = filter (/=y) xs
globals_of (Set _ st t)          =
  error "absurd globals_of case"
globals_of (Ass term t)          =
  globals_of term
globals_of (Pairr a b)           =
  globals_of a ++ globals_of b
globals_of (Fstt term)           =
  globals_of term
globals_of (Sndd term)           =
  globals_of term
globals_of Zeroo                 =
  []
globals_of (Succ n)              =
  globals_of n
globals_of (Recc b op n)         =
  globals_of b ++ globals_of op ++ globals_of n

-- Real pretty printer implementation.
-- p ii vs t
--   ii initially is 0
--      vs is a list of valid non-global var names.
pp :: Int -> [String] -> Term -> Doc
pp ii vs (Bound k)         = text (vs !! (ii - k - 1))
pp _  vs (Free (Global s)) = text s
pp _  _  TUnit             = text "unit"
pp ii vs (i :@: c) = sep [parensIf (isLam i) (pp ii vs i),
                          nest 1 (parensIf (isLam c || isApp c) (pp ii vs c))]
pp ii vs (Lam t c) = text "\\" <>
                     text (vs !! ii) <>
                     text ":" <>
                     printType t <>
                     text ". " <> 
                     pp (ii+1) vs c
pp ii vs (Set n st t) = text "let " <>
                        pp ii vs (Free n) <>
                        text " = " <>
                        pp ii vs st <>
                        text " in " <>
                        pp ii vs t
pp ii vs (Ass term t) = pp ii vs term <>
                        text " as " <>
                        printType t
pp ii vs (Pairr a b)  = text "(" <>
                        pp ii vs a <>
                        text ", " <>
                        pp ii vs b <>
                        text ")"
pp ii vs (Fstt p)     = text "fst " <>
                        pp ii vs p
pp ii vs (Sndd p)     = text "snd " <>
                        pp ii vs p
pp ii vs Zeroo        = text "0"
pp ii vs (Succ n)     = text "suc " <>
                        pp ii vs n
pp ii vs (Recc b op n)= text "R " <>
                        pp ii vs b <>
                        text " " <>
                        pp ii vs op <>
                        text " " <>
                        pp ii vs n

-- Recordatorios:

-- text crea un documento (de altura 1) a partir de un
-- string.
-- text :: String -> Doc

-- nest indenta un documento n posiciones.
-- nest :: Int -> Doc -> Doc

-- parens encierra el documento entre paréntesis.
-- parens :: Doc -> Doc

-- Helpers for pp:

parensIf :: Bool -> Doc -> Doc
parensIf True  = parens
parensIf False = id

isLam (Lam _ _) = True
isLam  _        = False
   
isApp (_ :@: _) = True
isApp _         = False

-- pretty-printer de tipos
printType :: Type -> Doc
printType Base         = text "B"
printType Unit         = text "Unit"
printType Nat          = text "Nat"
printType (Fun t1 t2)  = sep [ parensIf (isFun t1) (printType t1),
                               text "->", 
                               printType t2]

-- Check for Type Fun.
isFun (Fun _ _)        = True
isFun _                = False
