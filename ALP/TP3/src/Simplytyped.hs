module Simplytyped (
  conversion, -- conversion a terminos 
              -- localmente sin nombre
  eval,       -- evaluador
  infer,      -- inferidor de tipos
  quote,      -- valores -> terminos
  ln,
  sub
) where

import Data.List
import Data.Maybe
import Prelude hiding ((>>=))
import Text.PrettyPrint.HughesPJ (render)
import PrettyPrinter
import Common

-- Conversion a terminos localmente sin nombres
conversion :: LamTerm -> Term
conversion = con []

con :: [String] -> LamTerm -> Term
con b (LVar n)    =
  maybe (Free (Global n)) Bound (n `elemIndex` b)
con b LUnit        =
  TUnit
con b (App t u)   =
  con b t :@: con b u
con b (Abs n t u) =
  Lam t (con (n:b) u)
con b (Let n v t) =
  Set (Global n) (con b v) (con b t)
con b (As term t) =
  Ass (con b term) t
con b (Pair fst snd) =
  Pairr (con b fst) (con b snd)
con b (Fst term)  =
  Fstt (con b term)
con b (Snd term)  =
  Sndd (con b term)
con b (Suc term)  =
  Succ (con b term)
con b (Rec t p q) =
  Recc (con b t) (con b p) (con b q)
con b Zero        =
  Zeroo

-----------------------
--- eval
-----------------------

sub :: Int -> Term -> Term -> Term
sub i t (Bound j) | i == j    = t
sub _ _ (Bound j) | otherwise = Bound j
sub _ _ (Free n)              = Free n
sub i t TUnit                 = TUnit
sub i t (u :@: v)             = sub i t u :@: sub i t v
sub i t (Lam t' u)            = Lam t' (sub (i+1) t u)
sub i t (Set n st term)       = Set n (sub i t st)
                                      (sub i t term)
sub i t (Ass term ty)         = Ass (sub i t term) ty
sub i t (Pairr a b)           = Pairr (sub i t a)
                                      (sub i t b)
sub i t (Fstt term)           = Fstt (sub i t term)
sub i t (Sndd term)           = Sndd (sub i t term)
sub i t (Succ term)           = Succ (sub i t term)
sub i t Zeroo                 = Zeroo
sub i t (Recc b op n)         = Recc (sub i t b)
                                     (sub i t op)
                                     (sub i t n)

ln :: NameEnv Value Type -> Term -> Term
ln _ (Bound n) = Bound n
ln e (Free n)  =
  case lookup n e of
    Just (VLam t t', _) -> Lam t t'
    Just _              -> error "VUnit Value in ln"
    Nothing             -> Free n
ln e TUnit         = TUnit
ln e (u :@: v)     = ln e u :@: ln e v
ln e (Lam t v)     = Lam t (ln e v)
ln e (Set n st t)  = Set n (ln e st) (ln e t)
ln e (Ass term t)  = Ass (ln e term) t
ln e (Pairr a b)   = Pairr (ln e a) (ln e b)
ln e (Fstt p)      = Fstt (ln e p)
ln e (Sndd p)      = Sndd (ln e p)
ln e (Succ n)      = Succ (ln e n)
ln e Zeroo         = Zeroo
ln e (Recc b op n) = Recc (ln e b) (ln e op) (ln e n)

-- evaluador de terminos
eval :: NameEnv Value Type -> Term -> Value
eval _ (Bound _)             =
  error "variable ligada inesperada en eval"
eval e (Free n)              =
  fst $ fromJust $ lookup n e
eval e TUnit                 =
  VUnit
eval e (Lam t u)             =
  VLam t (ln e u)
eval e (Lam _ u :@: Lam s v) =
  eval e (sub 0 (Lam s v) u)
eval e (Lam t u :@: v)       =
  case eval e v of
    VLam t' u' -> eval e (Lam t u :@: Lam t' u')
    VUnit      -> VUnit
    NumValue n -> eval e (sub 0 (quote (NumValue n)) u)
    _          -> error "7error, check type checker"
eval e (u :@: v)             =
  case eval e u of
    VLam t u' -> eval e (Lam t u' :@: v)
    _         -> error "6error, check type checker"
-- TODO: Check the added infer
eval e (Set n st t) =
  case eval e st of
    VLam t' u' -> let ntype = fromRight $ infer e (Lam t' u')
                  in eval ((n, (VLam t' u', ntype)):e) t
    VUnit      -> eval ((n, (VUnit, Unit)):e) t
    _          -> error "5error, check type checker"
eval e (Ass term t) =
  eval e term
eval e (Pairr fst snd) =
  VPair (quote first) (quote (eval e snd))
  where first = eval e fst
eval e (Fstt term) =
  case term of
    (Pairr a b) -> eval e a
    _           -> error "4error, check type checker"
eval e (Sndd term) =
  case term of
    (Pairr a b) -> eval e b
    _           -> error "3error, check type checker"
eval e Zeroo       =
  NumValue VZero
eval e (Succ term) =
  case eval e term of
    (NumValue v) -> NumValue (VSuc v)
    _            -> error "1error, check type checker"
eval e (Recc b op n) =
  let k = eval e n in
    case k of
      NumValue VZero    -> eval e b
      NumValue (VSuc k) ->
        eval e ((op :@: Recc b op (quote (NumValue k)))
                    :@: quote (NumValue k))
      _         -> error "2error, check type checker"

fromRight :: Either String b -> b
fromRight (Left a)  = error a
fromRight (Right b) = b

-----------------------
--- quoting
-----------------------

quote :: Value -> Term
quote (VLam t f)          = Lam t f
quote VUnit               = TUnit
quote (VPair a b)         = Pairr a b
quote (NumValue VZero)    = Zeroo
quote (NumValue (VSuc v)) = Succ (quote (NumValue v))

----------------------
--- type checker
-----------------------

-- ret is syntax sugar for Right
ret :: Type -> Either String Type
ret = Right

-- err is syntax sugar for Left
err :: String -> Either String Type
err = Left

(>>=) :: Either String Type -> (Type -> Either String Type) -> Either String Type
(>>=) v f = either Left f v
-- fcs. de error

matchError :: Type -> Type -> Either String Type
matchError t1 t2 = err $ "se esperaba " ++
                         render (printType t1) ++
                         ", pero " ++
                         render (printType t2) ++
                         " fue inferido."

asError :: Type -> Type -> Either String Type
asError t1 t2 = err $ "El tipo explicitado con AS es "
                         ++ render (printType t1) ++
                         ", pero "
                         ++ render (printType t2) ++
                         " fue inferido."

notfunError :: Type -> Either String Type
notfunError t1 = err $ render (printType t1) ++ " no puede ser aplicado."

notfoundError :: Name -> Either String Type
notfoundError n = err $ show n ++ " no esta definida."

-- type checker
infer :: NameEnv Value Type -> Term -> Either String Type
infer = infer' []

-- Nota: lookup :: Eq a => a -> [(a, b)] -> Maybe b
infer' :: Context -> NameEnv Value Type -> Term -> Either String Type
infer' c _ (Bound i) = ret (c !! i)
infer' _ e (Free n) = case lookup n e of
                        Nothing     -> notfoundError n
                        Just (_, t) -> ret t
infer' _ _ TUnit     = ret Unit
infer' c e (t :@: u) = infer' c e t >>= \tt ->
                       infer' c e u >>= \tu ->
                       case tt of
                         Fun t1 t2 -> if (tu == t1)
                                        then ret t2
                                        else matchError t1 tu
                         _         -> notfunError tt
infer' c e (Lam t u) = infer' (t:c) e u >>= \tu ->
                       ret $ Fun t tu
infer' c e (Set n st t) = infer' c e st >>= \tst ->
                          infer' c ((n, (VUnit, tst)):e) t >>= \tt ->
                          ret tt
infer' c e (Ass term t) = infer' c e term >>= \termt ->
                            if termt == t then ret t
                            else asError t termt
infer' c e (Sndd term)  = infer' c e term >>= \tt ->
                          case tt of
                            Tuple p q -> ret q
                            _         -> error "Not a tuple"
infer' c e (Fstt term)  = infer' c e term >>= \tt ->
                          case tt of
                            Tuple p q -> ret p
                            _         -> error "Not a tuple"
infer' c e (Pairr a b)  = infer' c e a >>= \ta ->
                          infer' c e b >>= \tb ->
                            ret (Tuple ta tb)
infer' c e Zeroo        = ret Nat
infer' c e (Succ m)     = infer' c e m >>= \tm ->
                          case tm of
                            Nat -> ret Nat
                            _   -> error "suc only allows Nat"
infer' c e (Recc b op n) =
  infer' c e n >>= \tn ->
  if tn == Nat then
    infer' c e b >>= \tb ->
    infer' c e op >>= \top ->
      if top == Fun tb (Fun Nat tb) then
        ret tb
      else error "Bad infer in Recc case"
  else error "3rd param of R isn't Nat"
