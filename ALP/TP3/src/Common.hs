module Common where

  -- Comandos interactivos o de archivos
  data Stmt i = Def String i -- Declarar un nuevo 
                             -- identificador x, let x = t
              | Eval i       -- Evaluar el término
    deriving (Show)

  instance Functor Stmt where
    fmap f (Def s i) = Def s (f i)
    fmap f (Eval i)  = Eval (f i)

  -- Tipos de los nombres
  data Name
     =  Global String
     |  Quote  Int
    deriving (Show, Eq)

  -- Entornos
  type NameEnv v t = [(Name, (v, t))]

  -- Tipo de los tipos
  data Type = Base
            | Unit
            | Nat
            | Fun Type Type
            | Tuple Type Type
    deriving (Show, Eq)
  
  -- Términos con nombres
  data LamTerm = LVar String
               | LUnit
               | Abs String Type LamTerm
               | App LamTerm LamTerm
               | Let String LamTerm LamTerm
               | As LamTerm Type
               | Pair LamTerm LamTerm
               | Fst LamTerm
               | Snd LamTerm
               | Suc LamTerm
               | Zero
               | Rec LamTerm LamTerm LamTerm
    deriving (Show, Eq)

  -- Términos localmente sin nombres
  data Term = Bound Int
            | Free Name
            | TUnit
            | Term :@: Term
            | Lam Type Term
            | Set Name Term Term
            | Ass Term Type
            | Pairr Term Term
            | Fstt Term
            | Sndd Term
            | Succ Term
            | Zeroo
            | Recc Term Term Term
    deriving (Show, Eq)

  -- Valores
  data Value = VLam Type Term
             | VUnit
             | NumValue NValue
             | VPair Term Term
    deriving (Show)

  data NValue = VSuc NValue
              | VZero
    deriving (Show)

  -- Contextos del tipado
  type Context = [Type]
