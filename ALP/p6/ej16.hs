import System.Environment
import Data.Char

main :: IO ()
main = do (from:(to:xs)) <- getArgs
          s <- readFile from
          writeFile to (up s)

up :: String -> String
up (t:ext) = toUpper t : up ext
up _       = []
