main :: IO ()
main = do putStrLn "Welcome!"
          l <- getLine
          putStrLn "Try to guess"
          guess (read l)

guess :: Int -> IO ()
guess n = do m <- getNum
             if n == m then
               do putStrLn "You win!"
                  return ()
             else if n < m then
               do putStrLn "Less"
                  guess n
             else
               do putStrLn "More"
                  guess n

getNum :: IO Int
getNum = do n <- getLine
            return $ read n
