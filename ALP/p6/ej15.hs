main :: IO ()
main = do game [1, 2, 3, 4, 5] True
          return ()

game :: [Int] -> Bool -> IO ()
game board player = 
  do printBoard 5 board
     printPlayer player
     putStrLn " choose"
     n <- getNum
     -- let newBoard = decBoard n board in
     if (sum $ decBoard n board) == 0 then
       do printPlayer player
          putStrLn " wins!"
          return ()
     else
       game (decBoard n board) (not player)

decBoard :: Int -> [Int] -> [Int]
decBoard _ []       = []
decBoard 1 (b:oard) = if b == 0 then b:oard
                      else (b-1):oard
decBoard i (b:oard) = b:(decBoard (i-1) oard)

getNum :: IO Int
getNum = do n <- getLine
            return $ read n

printBoard :: Int -> [Int] -> IO ()
printBoard 0 _      = return ()
printBoard i (x:xs) = do printCell (6 - i) x
                         printBoard (i - 1) xs

printCell :: Int -> Int -> IO ()
printCell i n = putStrLn $
                  show i ++ ": " ++ n `times` '*'

printPlayer :: Bool -> IO ()
printPlayer True  = putStr "Player 1"
printPlayer False = putStr "Player 2"

times :: Int -> Char -> String
times 0 c = []
times n c = c : times (n - 1) c
