module Eval1 (eval) where

import AST

-- Estados
type State = [(Variable,Int)]

variable_exists :: Variable -> State -> Bool
variable_exists x [] = False
variable_exists x ((variable, value):states) =
  x == variable || variable_exists x states

substitute :: Variable -> Int -> State -> State
substitute x n ((variable, value):states) =
  if x == variable then ((variable, n):states)
  else (variable, value):(substitute x n states)

-- Estado nulo
initState :: State
initState = []

-- Busca el valor de una variable en un estado
lookfor :: Variable -> State -> Int
lookfor x ((variable, value):states) =
  if variable == x then value else lookfor x states

-- Cambia el valor de una variable en un estado
update :: Variable -> Int -> State -> State
update x n state =
  if variable_exists x state then substitute x n state
  else (x, n):state

-- Evalua un programa en el estado nulo
eval :: Comm -> State
eval p = evalComm p initState

-- Evalua un comando en un estado dado
evalComm :: Comm -> State -> State
evalComm Skip        s = s
evalComm (Let x ie)  s = update x (evalIntExp ie s) s
evalComm (Seq c1 c2) s = evalComm c2 (evalComm c1 s)
evalComm (Cond bexp c1 c2) s = if evalBoolExp bexp s
  then evalComm c1 s else evalComm c2 s
evalComm (While bexp c) s = if evalBoolExp bexp s
  then evalComm (Seq c (While bexp c)) s else s

-- Evalua una expresion entera, sin efectos laterales
evalIntExp :: IntExp -> State -> Int
evalIntExp (Const n)      s = n
evalIntExp (Var x)        s = lookfor x s
evalIntExp (UMinus ie)    s = -(evalIntExp ie s)
evalIntExp (Plus ie ie')  s =
  (evalIntExp ie s) + (evalIntExp ie' s)
evalIntExp (Minus ie ie') s =
  (evalIntExp ie s) - (evalIntExp ie' s)
evalIntExp (Times ie ie') s =
  (evalIntExp ie s) * (evalIntExp ie' s)
evalIntExp (Div ie ie')   s =
  div (evalIntExp ie s) (evalIntExp ie' s)

-- Evalua una expresion entera, sin efectos laterales
evalBoolExp :: BoolExp -> State -> Bool
evalBoolExp BTrue        s = True
evalBoolExp BFalse       s = False
evalBoolExp (Eq ie ie')  s =
  (evalIntExp ie s) == (evalIntExp ie' s)
evalBoolExp (Lt ie ie')  s =
  (evalIntExp ie s) < (evalIntExp ie' s)
evalBoolExp (Gt ie ie')  s =
  (evalIntExp ie s) > (evalIntExp ie' s)
evalBoolExp (And ie ie') s =
  (evalBoolExp ie s) && (evalBoolExp ie' s)
evalBoolExp (Or ie ie')  s =
  (evalBoolExp ie s) || (evalBoolExp ie' s)
evalBoolExp (Not ie )    s =
  not (evalBoolExp ie s)
