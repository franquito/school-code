module Eval3 (eval) where

import AST

-- Estados
type State  = ([(Variable,Int)], Int)
data Safe a = Only a | Error String deriving Show

is_safe :: Safe a -> Bool
is_safe (Only a) = True
is_safe _        = False

not_safe :: Safe a -> Bool
not_safe x = not (is_safe x)

get_safe_int :: Safe Int -> Int
get_safe_int (Only n) = n
get_safe_int _        = 0

get_safe_bool :: Safe Bool -> Bool
get_safe_bool (Only b) = b
get_safe_bool _        = False

get_safe_state :: Safe State -> State
get_safe_state (Only s) = s
get_safe_state _        = ([], 0)

get_error :: Safe a -> String
get_error (Error s) = s
get_error _ = ""

increase :: State -> State
increase (s, m) = (s, m+1)

reset :: State -> State
reset (s, m) = (s, 0)

add_counters :: State -> State -> State
add_counters (s, m) (s', m') = (s, m + m')

variable_exists :: Variable -> State -> Bool
variable_exists x ([], _)                       = False
variable_exists x ((variable, value):states, n) =
  x == variable || variable_exists x (states, n)

substitute' :: Variable -> Int -> [(Variable,Int)] -> [(Variable,Int)]
substitute' x n ((variable, value):states) =
  if x == variable then ((variable, n):states)
  else (variable, value):(substitute' x n states)

substitute :: Variable -> Int -> State -> State
substitute x n (s, m) = (substitute' x n s, m)

-- Estado nulo
initState :: Safe State
initState = Only ([], 0)

-- Busca el valor de una variable en un estado
lookfor' :: Variable -> [(Variable,Int)] -> Int
lookfor' x ((variable, value):states) =
  if variable == x then value else lookfor' x states

lookfor :: Variable -> State -> Int
lookfor x (s, m) = lookfor' x s

-- Cambia el valor de una variable en un estado
update' :: Variable -> Int -> [(Variable,Int)] -> [(Variable,Int)]
update' x n state =
  if variable_exists x (state, 0)
  then substitute' x n state
  else (x, n):state

update :: Variable -> Int -> State -> State
update x n (s, m) = (update' x n s, m)

-- Evalua un programa en el estado nulo
eval :: Comm -> Safe State
eval p = evalComm p initState

-- Evalua un comando en un estado dado
evalComm :: Comm -> Safe State -> Safe State

evalComm Skip s = s

evalComm (Let x ie) state =
  if is_safe state then
    let s = get_safe_state state in
      let (sint, s') = evalIntExp ie s in
        if is_safe sint then
          Only (update x (get_safe_int sint) s')
        else Error (get_error sint)
  else state

evalComm (Seq c1 c2) s =
  if is_safe sstate then
    evalComm c2 sstate
  else
    sstate
  where sstate = evalComm c1 s

evalComm (Cond bexp c1 c2) state =
  if is_safe state then
    let (b, s) = evalBoolExp bexp (get_safe_state state) in
      if is_safe b then
        if get_safe_bool b then
          evalComm c1 (Only s)
        else evalComm c2 (Only s)
      else Error (get_error b)
  else state

evalComm (While bexp c) state =
  if is_safe state then
    let (b, s) = evalBoolExp bexp (get_safe_state state) in
      if is_safe b && get_safe_bool b then
        evalComm (Seq c (While bexp c)) (Only s)
      else state
  else state

-- Evalua una expresion entera, sin efectos laterales
evalIntExp :: IntExp -> State -> (Safe Int, State)

evalIntExp (Const n) s = (Only n, s)
evalIntExp (Var x)   s = (Only (lookfor x s), s)

evalIntExp (UMinus ie) state =
  if is_safe res then
    (Only (-(get_safe_int res)), increase s)
  else
    (res, s)
  where (res, s) = evalIntExp ie state

evalIntExp (Plus ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else
    (Only (get_safe_int r + get_safe_int r'),
     add_counters (increase state) (add_counters s s'))
  where (r,  s)  = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalIntExp (Minus ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else
    (Only (get_safe_int r - get_safe_int r'),
     add_counters (increase state) (add_counters s s'))
  where (r,  s)  = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalIntExp (Times ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else
    (Only (get_safe_int r * get_safe_int r'),
     add_counters (increase state) (add_counters s s'))
  where (r,  s)  = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalIntExp (Div ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else if get_safe_int r' == 0 then
    (Error "Division by zero", s')
  else
    (Only (div (get_safe_int r) (get_safe_int r')),
     add_counters (increase state) (add_counters s s'))
  where (r,  s)  = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

-- Evalua una expresion entera, sin efectos laterales
evalBoolExp :: BoolExp -> State -> (Safe Bool, State)

evalBoolExp BTrue  s = (Only True, s)
evalBoolExp BFalse s = (Only False, s)

evalBoolExp (Eq ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (Error (get_error r), state)
    else (Error (get_error r'), state)
  else
    (Only (get_safe_int r == get_safe_int r'),
     add_counters state (add_counters s s'))
  where (r , s ) = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalBoolExp (Lt ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (Error (get_error r), state)
    else (Error (get_error r'), state)
  else
    (Only (get_safe_int r < get_safe_int r'),
     add_counters state (add_counters s s'))
  where (r , s ) = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalBoolExp (Gt ie ie') state =
  if not_safe r || not_safe r' then
    if not_safe r then (Error (get_error r), state)
    else (Error (get_error r'), state)
  else
    (Only (get_safe_int r > get_safe_int r'),
     add_counters state (add_counters s s'))
  where (r , s ) = evalIntExp ie  (reset state)
        (r', s') = evalIntExp ie' (reset state)

evalBoolExp (Not be) state =
  if not_safe r then (r, s) else
    (Only (not (get_safe_bool r)), add_counters state s)
  where (r, s) = evalBoolExp be (reset state)

evalBoolExp (And be be') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else
    (Only (get_safe_bool r && get_safe_bool r'),
     add_counters state (add_counters s s'))
  where (r , s ) = evalBoolExp be  (reset state)
        (r', s') = evalBoolExp be' (reset state)

evalBoolExp (Or be be') state =
  if not_safe r || not_safe r' then
    if not_safe r then (r, s) else (r', s')
  else
    (Only (get_safe_bool r || get_safe_bool r'),
     add_counters state (add_counters s s'))
  where (r , s ) = evalBoolExp be  (reset state)
        (r', s') = evalBoolExp be' (reset state)
