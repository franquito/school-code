module Eval2 (eval) where

import AST

-- Estados
type State  = [(Variable,Int)]
data Safe a = Only a | Error String deriving Show

is_safe :: Safe a -> Bool
is_safe (Only a) = True
is_safe _        = False

not_safe :: Safe a -> Bool
not_safe x = not (is_safe x)

get_safe_int :: Safe Int -> Int
get_safe_int (Only n) = n
get_safe_int _        = 0

get_safe_bool :: Safe Bool -> Bool
get_safe_bool (Only b) = b
get_safe_bool _        = False

get_safe_state :: Safe State -> State
get_safe_state (Only s) = s
get_safe_state _        = []

get_error :: Safe a -> String
get_error (Error s) = s
get_error _ = ""

variable_exists :: Variable -> State -> Bool
variable_exists x [] = False
variable_exists x ((variable, value):states) =
  x == variable || variable_exists x states

substitute :: Variable -> Int -> State -> State
substitute x n ((variable, value):states) =
  if x == variable then ((variable, n):states)
  else (variable, value):(substitute x n states)

-- Estado nulo
initState :: Safe State
initState = Only []

-- Busca el valor de una variable en un estado
lookfor :: Variable -> State -> Int
lookfor x ((variable, value):states) =
  if variable == x then value else lookfor x states

-- Cambia el valor de una variable en un estado
update :: Variable -> Int -> State -> State
update x n state =
  if variable_exists x state then substitute x n state
  else (x, n):state

-- Evalua un programa en el estado nulo
eval :: Comm -> Safe State
eval p = evalComm p initState

-- Evalua un comando en un estado dado
evalComm :: Comm -> Safe State -> Safe State

evalComm Skip s = s

evalComm (Let x ie) s =
  if is_safe s then
    let state = get_safe_state s in
      let q = evalIntExp ie state in
        if is_safe q then
          Only (update x (get_safe_int q) state)
        else Error (get_error q)
  else s

evalComm (Seq c1 c2) s =
  if is_safe r then
    evalComm c2 r
  else
    r
  where r = evalComm c1 s

evalComm (Cond bexp c1 c2) s =
  if is_safe s then
    let q = evalBoolExp bexp (get_safe_state s) in
      if is_safe q then
        if get_safe_bool q then evalComm c1 s else evalComm c2 s
      else Error (get_error q)
  else s

evalComm (While bexp c) s =
  if is_safe s then
    let q = evalBoolExp bexp (get_safe_state s) in
      if is_safe q && get_safe_bool q then
        evalComm (Seq c (While bexp c)) s
      else s
  else s

-- Evalua una expresion entera, sin efectos laterales
evalIntExp :: IntExp -> State -> Safe Int

evalIntExp (Const n) s = Only n
evalIntExp (Var x)   s = Only (lookfor x s)

evalIntExp (UMinus ie) s =
  if is_safe res then
    Only (-(get_safe_int res))
  else
    res
  where res = evalIntExp ie s

evalIntExp (Plus ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
    -- if not_safe r then Error (get_error r) 
    -- else Error (get_error r')
  else
    Only (get_safe_int r + get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalIntExp (Minus ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
  else
    Only (get_safe_int r - get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalIntExp (Times ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
  else
    Only (get_safe_int r * get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalIntExp (Div ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
  else if get_safe_int r' == 0 then
    Error "Division by zero"
  else
    Only (div (get_safe_int r) (get_safe_int r'))
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

-- Evalua una expresion entera, sin efectos laterales
evalBoolExp :: BoolExp -> State -> Safe Bool

evalBoolExp BTrue  s = Only True
evalBoolExp BFalse s = Only False

evalBoolExp (Eq ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then Error (get_error r) 
    else Error (get_error r')
  else
    Only (get_safe_int r == get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalBoolExp (Lt ie ie')  s =
  if not_safe r || not_safe r' then
    if not_safe r then Error (get_error r) 
    else Error (get_error r')
  else
    Only (get_safe_int r < get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalBoolExp (Gt ie ie') s =
  if not_safe r || not_safe r' then
    if not_safe r then Error (get_error r) 
    else Error (get_error r')
  else
    Only (get_safe_int r > get_safe_int r')
  where r  = evalIntExp ie  s
        r' = evalIntExp ie' s

evalBoolExp (Not be) s =
  if not_safe r then r else Only (not (get_safe_bool r))
  where r  = evalBoolExp be s

evalBoolExp (And be be') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
  else
    Only (get_safe_bool r && get_safe_bool r')
  where r  = evalBoolExp be  s
        r' = evalBoolExp be' s

evalBoolExp (Or be be') s =
  if not_safe r || not_safe r' then
    if not_safe r then r else r'
  else
    Only (get_safe_bool r || get_safe_bool r')
  where r  = evalBoolExp be  s
        r' = evalBoolExp be' s
