import Control.Applicative
import Control.Monad (liftM, ap)

type Variable = String
type Env = Variable -> Int

data Exp2 = Cons  Int
          | Var   Variable
          | Plus  Exp2     Exp2
          | Div   Exp2     Exp2 deriving (Show)

-- maybe, cont, vars
data Reader a = Reader (Env -> Maybe (a, Int))

runReader :: Reader a -> Env -> Maybe (a, Int)
runReader (Reader r) = r

instance Functor Reader where
  fmap = liftM
instance Applicative Reader where
  pure  = return
  (<*>) = ap
instance Monad Reader where
  return a       = Reader (\e -> Just (a, 0))
  Reader r >>= f =
    Reader (\e -> do (a, c)  <- r e
                     (b, c') <- runReader (f a) e
                     return (b, c + c'))

ask :: Variable -> Reader Int
ask v = Reader (\e -> Just (e v, 0))

tick :: Reader ()
tick = Reader (\e -> Just ((), 1))

throw :: Reader Int
throw = Reader (\e -> Nothing)

eval5 :: Exp2 -> Reader Int
eval5 (Cons n) = return n
eval5 (Var v)  = do v' <- ask v
                    return v'
eval5 (Plus n m) = do n' <- eval5 n
                      m' <- eval5 m
                      return $ n' + m'
eval5 (Div n m)  = do n' <- eval5 n
                      m' <- eval5 m
                      tick
                      if m' == 0
                      then throw
                      else return $ n' `div` m'

equation = eval5 (Plus
  (Div
    (Cons 6)
    (Plus (Var "x")
          (Cons 2)))
  (Var "y"))

equation1 = eval5 (Div (Cons 1) (Cons 0))

env :: Env
env "y" = 100
env "x" = 1
env _   = 3
