import Control.Applicative
import Control.Monad (liftM, ap)

type Variable = String
type Env = Variable -> Int

data Exp2 = Cons  Int
          | Var   Variable
          | Plus  Exp2     Exp2
          | Div   Exp2     Exp2 deriving (Show)

data State a = State (Env -> a)

runState :: State a -> Env -> a
runState (State s) = s

instance Functor State where
  fmap = liftM
instance Applicative State where
  pure  = return
  (<*>) = ap
instance Monad State where
  return a      = State (\e -> a)
  State s >>= f =
    State (\e ->
      let State s' = f $ s e
      in s' e)

ask :: Variable -> State Int
ask v = State (\e -> e v)

eval4 :: Exp2 -> State Int
eval4 (Cons n) = return n
eval4 (Var v)  = do v' <- ask v
                    return v'
eval4 (Plus n m) = do n' <- eval4 n
                      m' <- eval4 m
                      return $ n' + m'
eval4 (Div n m)  = do n' <- eval4 n
                      m' <- eval4 m
                      return $ n' `div` m'

-- (6/(x+2)+y)

equation = eval4 (Plus
  (Div
    (Cons 6)
    (Plus (Var "x")
          (Cons 2)))
  (Var "y"))

env :: Env
env "y" = 100
env "x" = 1
env _   = 3
