import Control.Applicative -- Otherwise you can't do the Applicative instance.
import Control.Monad (liftM, ap)

-- data Env a = Env (Variable -> a)
-- 
-- data State a = State (Env a) (Exp a)
-- 
-- eval :: Exp a -> Env a -> a
-- eval (Const a) (Env e) = return a
-- eval (Var v)   e = 
-- 
-- evalM :: Exp a -> Env a -> Exp a
-- 
-- evalM :: State a -> State a
-- evalM (State (k
-- 
-- data Exp = Const Int
--          | Plus Exp
--          | Div  Exp deriving (Show)
-- 
-- eval :: Exp -> Int
-- eval (Const n)  = n
-- eval (Plus n m) = eval n + eval m
-- eval (Div  n m) = eval n `div` eval m

data Exp1 = Cons Int
          | Plus Exp1 Exp1
          | Div  Exp1 Exp1 deriving (Show)

throw :: Maybe Int
throw = Nothing

eval2 :: Exp1 -> Maybe Int
eval2 (Cons n)   = return n
eval2 (Plus n m) = do n' <- eval2 n
                      m' <- eval2 m
                      return $ n' + m'
eval2 (Div  n m) = do n' <- eval2 n
                      m' <- eval2 m
                      if m' == 0
                        then throw
                        else return $ n' `div` m'

data Counter a = Counter a Int deriving (Show)

tick :: Counter ()
tick = Counter () 1

instance Functor Counter where
  fmap = liftM
instance Applicative Counter where
  pure = return
  (<*>) = ap
instance Monad Counter where
  return a = Counter a 0
  (Counter a n) >>= f =
    let Counter b m = f a
        in Counter b (n+m)

eval3 :: Exp1 -> Counter Int
eval3 (Cons n)   = return n
eval3 (Plus n m) = do n' <- eval3 n
                      m' <- eval3 m
                      return $ n' + m'
eval3 (Div n m)  = do n' <- eval3 n
                      m' <- eval3 m
                      tick
                      return $ n' `div` m'

type Variable = String

data Exp2 a = Const a
            | Var   Variable
            | Plus  Exp2     Exp2
            | Div   Exp2     Exp2 deriving (Show)

data State = State (Variable -> a)

instance Functor State where
  fmap = liftM
instance Applicative State where
  pure  = return
  (<*>) = ap
instance Monad State where
  return a        = State (\v -> a)
  State e >>= f =
    State (\v ->
      let State e' = f $ e v
      in e')

-- ask :: Variable -> State a
-- ask v = State 

eval4 (Cons n) = return n
-- eval4 (Var v)  = do v' <- ask v
--                     return v'
eval4 (Plus n m) = do n' <- eval4 n
                      m' <- eval4 m
                      return $ n' + m'
eval4 (Div n m)  = do n' <- eval4 n
                      m' <- eval4 m
                      return $ n' `div` m'
