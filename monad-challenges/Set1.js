// Helpers for Ej1.3
function twice (a) {
  return a*2; }
function twicePlusOne (a) {
  return twice(a)+1; }
function byTen (a) {
  return a * 10; }

function mkPair (a) {
  return function (b) {
    return {a: a, b: b}; }; }
function fst (pair) {
  return pair.a }
function snd (pair) {
  return pair.b }

var M = 0x7FFFFFFF;

// type Seed = Integer

// rand :: Gen Integer
// rand :: Seed -> (Integer, Seed)
function rand (seed) {
  var new_seed = (seed * 16807) % M;
  return mkPair(new_seed)(new_seed); }

// fiveRands :: [Integer]
function fiveRands () {
  return rands (5, 1); }

// Integer -> Seed -> [Integer]
function rands (n, seed) {
  if (n == 0)
    return []
  else
    return (function (pair) {
      return [fst(pair)].concat(rands(n-1, snd(pair)));
    })(rand(seed)); }

// toLetter :: Integer -> Char
function toLetter (n) {
  return String.fromCharCode(97 + (n % 26)); }

// randLetter :: Seed -> (Char, Seed)
// randLetter :: Gen Char
function randLetter (seed) {
  return (function(pair) {
    return mkPair(toLetter(fst(pair)))(snd(pair));
  })(rand(seed)); }

// randString3 :: String
function randString3 () {
  return randLetters (3, 1); }

// Integer -> String
function randLetters (n, seed) {
  if (n == 0)
    return []
  else
    return (function (pair) {
      return [fst(pair)].concat(randLetters(n-1, snd(pair)));
    })(randLetter(seed)); }

// Ej1.3

// randEven :: Gen Integer -- the output of rand * 2
function randEven (seed) {
  return (function(pair) {
    return mkPair(fst(pair)*2)(snd(pair));
  })(rand(seed)); }
// randOdd :: Gen Integer -- the output of rand * 2 + 1
function randOdd (seed) {
  return (function(pair) {
    return mkPair(fst(pair)+1)(snd(pair));
  })(randEven(seed)); }
// randTen :: Gen Integer -- the output of rand * 10
function randTen (seed) {
  return (function(pair) {
    return mkPair(fst(pair)*5)(snd(pair));
  })(randEven(seed)); }

// generalA :: (a -> b) -> Gen a -> Gen b
function generalA (f, ga) {
  return function (seed) {
    return (function(pair) {
      return mkPair(f(fst(pair)))(snd(pair));
    })(ga(seed));
  };
}

// randEvenA :: Gen Integer -- the output of rand * 2
var randEvenA = generalA (twice, rand);
// randOddA :: Gen Integer -- the output of rand * 2 + 1
var randOddA = generalA (twicePlusOne, rand);
// randTenA :: Gen Integer -- the output of rand * 10
var randTenA = generalA (byTen, rand);

// Ej 1.4

// randPair :: Gen (Char, Integer)
function randPair (seed) {
  return (function(pair) {
    return (function(sndPair) {
      return mkPair(mkPair(fst(pair))(fst(sndPair)))(snd(sndPair));
    })(rand(snd(pair)));
  })(randLetter(seed)); }

// generalPair :: Gen a -> Gen b -> Gen (a, b)
function generalPair(ga, gb) {
  return function (seed) {
    return (function(pair) {
      return (function(sndPair) {
        return mkPair(mkPair(fst(pair))(fst(sndPair)))(snd(sndPair));
      })(gb(snd(pair)));
    })(ga(seed)); }; }

// generalB :: (a -> b -> c) -> Gen a -> Gen b -> Gen c
function generalB (op, ga, gb) {
  return function (seed) {
    return (function(pair) {
      return (function(sndPair) {
        return mkPair(op(fst(pair))(fst(sndPair)))(snd(sndPair));
      })(gb(snd(pair)));
    })(ga(seed)); }; }

// generalPair2 :: Gen a -> Gen b -> Gen (a, b)
var generalPair2 = generalB (mkPair, randLetter, rand);

// Ej 1.5

// repRandom :: [Gen a] -> Gen [a]

