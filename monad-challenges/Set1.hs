{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RebindableSyntax  #-}

module Set1 where

import MCPrelude

type Gen a = Seed -> (a, Seed)

fiveRands :: [Integer]
fiveRands = fiveRandsAux 5 (mkSeed 1)

fiveRandsAux :: Integer -> Seed -> [Integer]
fiveRandsAux 0 s = []
fiveRandsAux n s = r:(fiveRandsAux (n-1) s')
  where (r, s') = rand s
-- 
-- rand :: Seed -> (Int, Seed)
-- randString3 :: String
-- randString3 = randStringAux (mkSeed 1) 3
-- 
-- randStringAux :: Seed -> Integer -> String
-- randStringAux s 0 = []
-- randStringAux s n = l:(randStringAux s' (n-1))
--   where (l, s') = randLetter s

randLetter :: Gen Char
randLetter s = (toLetter i, s')
  where (i, s') = rand s

-- First try.
-- generalA :: (Integer -> Integer) -> Gen Integer
-- generalA f s = (f i', s')
--   where (i', s') = rand s
-- 
-- randEven :: Gen Integer
-- randEven = generalA (*2)
-- randOdd :: Gen Integer
-- randOdd s = generalA (*2+1)
-- randTen :: Gen Integer
-- randTen s = generalA (*10)

-- generalA :: Gen Integer -> (Integer -> Integer) -> Gen Integer
-- generalA gen f s = (f i', s')
--   where (i', s') = gen s
-- randEven :: Gen Integer
-- randEven = generalA rand (*2)
-- randOdd :: Gen Integer
-- randOdd = generalA randEven (+1)
-- randTen :: Gen Integer
-- randTen = generalA randEven (*5)

generalA :: Gen a -> (a -> b) -> Gen b
generalA gen f = (\s-> let (i', s') = gen s
                       in (f i', s'))
randEven :: Gen Integer
randEven = generalA rand (*2)
randOdd :: Gen Integer
randOdd = generalA randEven (+1)
randTen :: Gen Integer
randTen = generalA randEven (*5)

-- randEven :: Gen Integer
-- randEven s = (i*2, s')
--   where (i, s') = rand s
-- randOdd :: Gen Integer
-- randOdd s = (i+1, s')
--   where (i, s') = randEven s
-- randTen :: Gen Integer
-- randTen s = (i*5, s')
--   where (i, s') = randEven s

-- Generalizing Random Pairs
-- randPair :: Gen (Char, Integer)
-- randPair = \s -> let (l, s')  = randLetter s
--                      (i, s'') = rand s'
--                  in ((l, i), s'')

randPair :: Gen (Char, Integer)
randPair = generalPair randLetter rand

generalPair :: Gen a -> Gen b -> Gen (a, b)
generalPair f g = generalB f g (,)

generalB :: Gen a -> Gen b -> (a -> b -> c) -> Gen c
generalB f g glue = \s -> let (v1, s')  = f s
                              (v2, s'') = g s'
                          in (glue v1 v2, s'')

repRandom :: [Gen a] -> Gen [a]
repRandom [] = \s -> ([], s)
repRandom (f:fs) = \s -> let (v1, s') = f s
                             (vs, s'') = repRandom fs s'
                         in (v1:vs, s'')
randString3 :: Gen String
randString3 = repRandom (replicate 3 randLetter)

genTwo :: Gen a -> (a -> Gen b) -> Gen b
genTwo g f = \s -> let (v, s') = g s
                   in f v s'

mkGen :: a -> Gen a
mkGen v = \s -> (v, s)
