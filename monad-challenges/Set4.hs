{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RebindableSyntax  #-}

module Set4 where

import MCPrelude

-- Ej 4.1
-- genTwo :: m a -> (a -> m b) -> m b
-- link   :: m a -> (a -> m b) -> m b
-- 
-- generalB :: m a -> m b -> (a -> b -> c) -> m c
-- yLink    :: m a -> m b -> (a -> b -> c) -> m c

-- Ej 4.2
-- import Set1
-- 
-- generalB2 :: Gen a -> Gen b -> (a -> b -> c) -> Gen c
-- generalB2 ga gb op = genTwo ga
--               (\a -> genTwo gb
--               (\b -> mkGen $ op a b))
-- 
-- repRandom2 :: [Gen a] -> Gen [a]
-- repRandom2 []       = mkGen []
-- repRandom2 (ga:gas) = genTwo ga
--                (\a  -> genTwo (repRandom gas)
--                (\as -> mkGen $ a:as))

type Operator a = a -> a -> a
-- Ej 4.3
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     , height :: Float
                     , phoneNumber :: String
                     , flavor :: String
                     } deriving (Show)
data WhichWay a b = Left Int a
                  | Right Int b
                  | Top deriving (Show)

data Peperino a b c d e f = Peperino
p = Top
data Car = Car {company :: String, model :: String, year :: Int} deriving (Show)  
class Papo a where
  pap :: a
class Pepe a where
  pep :: a

class (Pepe a, Papo a) => Coco a where
  coc :: a

class Monad m where
  bind :: m a -> (a -> m b) -> m b
  return :: a -> (m a)

data Maybe a = Nothing
             | Just a deriving (Show)
instance Monad Maybe where
  bind Nothing  f = Nothing
  bind (Just a) f = f a
  return = Just

instance Monad [] where
  bind xs f = concat $ map f xs
  return = (:[])

-- newtype Gen a = Gen {
--   runGen :: Seed -> (a, Seed) }

instance Monad Gen where
  bind (Gen ga) f = Gen (\s -> let (v, s') = ga s
                                   Gen ga' = f v
                               in ga' s')
  return a = Gen (\s -> (a, s))

evalGen :: Gen a -> Seed -> a
evalGen (Gen g) s = fst $ g s

-- Ej 4.5

-- repRandom (sequence).
sequence :: Monad m => [m a] -> m [a]
sequence []     = return []
sequence (x:xs) = bind x
           (\a  -> bind (sequence xs)
           (\as -> return $ a:as))

-- generalB, allCombs (liftM2)
liftM2 :: Monad m => m a -> m b -> (a -> b -> c) -> m c
liftM2 ga gb op = bind ga
           (\a -> bind gb
           (\b -> return $ op a b))

-- chain (=<<)
(=<<) :: Monad m => (a -> m b) -> m a -> m b
f =<< ga = bind ga f

-- combine (join)
join :: Monad m => m (m a) -> m a
join gga = bind gga
   (\ga -> bind ga
   (\a  -> return a))

-- allCombs3 (liftM3)
liftM3 :: Monad m => (a -> b -> c -> d) -> m a -> m b -> m c -> m d
liftM3 f ma mb mc = bind ma
             (\a -> bind mb
             (\b -> bind mc
             (\c -> return $ f a b c)))

-- combStep (ap)
ap :: Monad m => m (a -> b) -> m a -> m b
ap mop ma = bind ma
    (\a  -> bind mop
    (\op -> return $ op a))

-- Ej 4.6

data Gen a = Gen (Seed -> (a, Seed))

runGen :: Gen a -> Seed -> a
runGen (Gen f) s = fst $ f s

-- fiveRands :: [Integer]

-- randLetter :: Gen Char
randG :: Gen Integer
randG = Gen rand

randEven :: Gen Integer
randEven = bind randG
    (\n -> return $ n * 2)

randOdd :: Gen Integer
randOdd = bind randG
   (\n -> return $ n * 2 + 1)

randTen :: Gen Integer
randTen = bind randG
   (\n -> return $ n * 10)

randTwice :: Gen Integer
randTwice = bind randG
     (\a -> bind randG
     (\b -> return $ a * b))

randLetter :: Gen Char
randLetter = bind randG (\v -> return $ toLetter v)

randString3 :: String
randString3 = runGen g (mkSeed 1)
  where g = sequence $ replicate 3 randLetter

randPair :: Gen (Char, Integer)
randPair = liftM2 randLetter randG (,)
