{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RebindableSyntax  #-}

module Set2 where

import MCPrelude

-- Ej2.1

data Maybe a = Nothing
             | Just a

instance Show a => Show (Maybe a) where
  show Nothing  = "Nothing"
  show (Just a) = "Just " ++ show a

-- Ej2.2

headMay :: [a] -> Maybe a
headMay []     = Nothing
headMay (x:xs) = Just x

tailMay :: [a] -> Maybe [a]
tailMay []     = Nothing
tailMay (x:xs) = Just xs

lookupMay :: Eq a => a -> [(a, b)] -> Maybe b
lookupMay k []                     = Nothing
lookupMay k ((k', v):xs) | k == k' = Just v
                         | k /= k' = lookupMay k xs

divMay :: (Eq a, Fractional a) => a -> a -> Maybe a
divMay p q | q == 0 = Nothing
           | q /= 0 = Just $ p/q

maximumMay :: Ord a => [a] -> Maybe a
maximumMay []     = Nothing
maximumMay (x:xs) = case maximumMay xs of
  Nothing -> Just x
  Just x' -> if x < x' then Just x' else Just x

minimumMay :: Ord a => [a] -> Maybe a
minimumMay []     = Nothing
minimumMay (x:xs) = case minimumMay xs of
  Nothing -> Just x
  Just x' -> if x < x' then Just x else Just x'

-- Observation: while you use tail recursion you'll
-- be fine, if not, you'll have to unwrap the
-- value messing up the code.

-- Ej2.3
-- type GreekData = [(String, [Integer])]
-- greekDataA, greekDataB :: GreekData

-- queryGreek :: GreekData -> String -> Maybe Double
-- queryGreek [] k = Nothing
-- queryGreek xs k = case lookupMay k xs of
--   Nothing -> Nothing
--   Just ns -> case tailMay ns of
--     Nothing  -> Nothing
--     Just ns' -> case maximumMay ns' of
--       Nothing -> Nothing
--       Just m  -> case headMay ns of
--         Nothing -> Nothing
--         Just h  -> divMay (fromIntegral m) (fromIntegral h)

-- Ej2.4

rewrap :: Maybe a -> (a -> Maybe b) -> Maybe b
rewrap Nothing  f = Nothing
rewrap (Just x) f = f x

queryGreek :: GreekData -> String -> Maybe Double
queryGreek xs k = rewrap (lookupMay k xs)
         (\ns  -> rewrap (tailMay ns)
         (\ns' -> rewrap (maximumMay ns')
         (\m   -> rewrap (headMay ns)
         (\h   -> divMay (fromIntegral m) (fromIntegral h)))))

chain :: (a -> Maybe b) -> Maybe a -> Maybe b
chain f Nothing  = Nothing
chain f (Just x) = f x

link :: Maybe a -> (a -> Maybe b) -> Maybe b
link Nothing  f = Nothing
link (Just x) f = f x

-- Ej2.5

salaries :: [(String, Integer)]
salaries = [ ("alice", 105000)
           , ("bob", 90000)
           , ("carol", 85000)
           ]

addSalaries :: [(String, Integer)] -> String -> String -> Maybe Integer
addSalaries ss a b = link (lookupMay a ss)
             (\a' -> link (lookupMay b ss)
             (\b' -> mkMaybe $ a' + b'))

yLink :: Maybe a -> Maybe b -> (a -> b -> c) -> Maybe c
yLink wa wb op = link wa
          (\a -> link wb
          (\b -> mkMaybe $ op a b))

addSalaries2 :: [(String, Integer)] -> String -> String -> Maybe Integer
addSalaries2 ss a b = yLink (lookupMay a ss) (lookupMay b ss) (+)

mkMaybe :: a -> Maybe a
mkMaybe = Just

-- Ej2.6

-- Note that:
-- product [] == 1

-- tailProd :: Num a => [a] -> Maybe a
-- tailProd xs = case tailMay xs of
--   Nothing      -> Nothing
--   Just []      -> Nothing
--   Just (p:xs') -> case tailProd (p:xs') of
--     Nothing -> Just $ p
--     Just p' -> Just $ p * p'

-- tailProd :: Num a => [a] -> Maybe a
-- tailProd xs = link (tailMay xs) (mkMaybe . product)
-- 
-- tailSum :: Num a => [a] -> Maybe a
-- tailSum xs = link (tailMay xs) (mkMaybe . sum)

transMaybe :: (a -> b) -> Maybe a -> Maybe b
transMaybe f ma = link ma (mkMaybe . f)

tailProd :: Num a => [a] -> Maybe a
tailProd xs = transMaybe product (tailMay xs)

tailSum :: Num a => [a] -> Maybe a
tailSum = transMaybe sum . tailMay

tailMax :: Ord a => [a] -> Maybe (Maybe a)
tailMax xs = transMaybe maximumMay (tailMay xs)

maximum :: Ord a => [a] -> a
maximum [x]    = x
maximum (x:xs) = max x (maximum xs)

combine :: Maybe (Maybe a) -> Maybe a
combine (Just x) = x
combine Nothing  = Nothing

-- tailSum :: Num a => [a] -> Maybe a
-- tailSum xs = case tailMay xs of
--   Nothing      -> Nothing
--   Just []      -> Nothing
--   Just (p:xs') -> case tailSum (p:xs') of
--     Nothing -> Just $ p
--     Just p' -> Just $ p + p'

-- Bad abstraction:
-- transMaybe :: (a -> a -> a) -> [a] -> Maybe a
-- transMaybe op xs = case tailMay xs of
--   Nothing      -> Nothing
--   Just []      -> Nothing
--   Just (p:xs') -> case transMaybe (p:xs') of
--     Nothing -> Just $ p
--     Just p' -> Just $ op p p'

-- transMaybe :: (a -> b) -> Maybe a -> Maybe b
-- transMaybe f Nothing  = Nothing
-- transMaybe f (Just a) = Just $ f a
-- 
-- tailProd :: Num a => [a] -> Maybe a
-- tailProd xs = case tailMay xs of
--   Nothing      -> Nothing
--   Just []      -> Nothing
--   Just [p]     -> Just p
--   Just (p:xs') -> transMaybe (p*) (tailProd (p:xs'))
-- 
-- tailSum :: Num a => [a] -> Maybe a
-- tailSum xs = case tailMay xs of
--   Nothing      -> Nothing
--   Just []      -> Nothing
--   Just [p]     -> Just p
--   Just (p:xs') -> transMaybe (p+) (tailSum (p:xs'))
-- 
-- tailMax :: Num a => [a] -> Maybe a
