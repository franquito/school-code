{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE RebindableSyntax  #-}

module Set3 where

import MCPrelude

-- Ej3.1

-- allPairs :: [a] -> [b] -> [(a, b)]
-- allPairs []     _      = []
-- allPairs _      []     = []
-- allPairs [x]    (y:ys) = (x, y):(allPairs [x] ys)
-- allPairs (x:xs) ys     = allPairs [x] ys ++ allPairs xs ys

-- Ej3.2

data Card = Card Int String

instance Show Card where
  show (Card a b) = show a ++ b

-- allCards :: [Int] -> [String] -> [Card]
-- allCards []     _      = []
-- allCards _      []     = []
-- allCards [n]    (s:ss) = (Card n s):(allCards [n] ss)
-- allCards (n:ns) ss     = allCards [n] ss ++ allCards ns ss

-- Ej 3.3

-- allCombs :: (a -> b -> c) -> [a] -> [b] -> [c]
-- allCombs cons []     _      = []
-- allCombs cons _      []     = []
-- allCombs cons [x]    ys     = map (x,) ys
-- allCombs cons (x:xs) ys     = allCombs cons [x] ys ++ allCombs cons xs ys
-- 
-- allPairs :: [a] -> [b] -> [(a, b)]
-- allPairs = allCombs (,)
-- 
-- allCards :: [Int] -> [String] -> [Card]
-- allCards = allCombs Card

-- Ej 3.4

-- allCombs3 :: (a -> b -> c -> d) -> [a] -> [b] -> [c] -> [d]
-- allCombs3 cons []     _      _      = []
-- allCombs3 cons _      []     _      = []
-- allCombs3 cons _      _      []     = []
-- allCombs3 cons [x]    [y]    (z:zs) = (cons x y z):(allCombs3 cons [x] [y] zs)
-- allCombs3 cons [x]    (y:ys) zs     = allCombs3 cons [x] [y] zs ++ allCombs3 cons [x] ys zs
-- allCombs3 cons (x:xs) ys     zs     = allCombs3 cons [x] ys zs ++ allCombs3 cons xs ys zs

combStep :: [a -> b] -> [a] -> [b]
combStep [f]    xs = map f xs
combStep (f:fs) xs = combStep [f] xs ++ combStep fs xs

allCombs :: (a -> b -> c) -> [a] -> [b] -> [c]
allCombs cons xs ys = combStep (combStep [cons] xs) ys

allCombs3 :: (a -> b -> c -> d) -> [a] -> [b] -> [c] -> [d]
allCombs3 cons xs ys zs =
  combStep (combStep (combStep [cons] xs) ys) zs
