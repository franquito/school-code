$ORIGIN acme.es.
$TTL    2d

@ IN SOA ns1 root (
  2015100000 ; version
  2d         ; refresh
  15m        ; retry
  2w         ; expirity
  2h         ; retry negative response.
)

        IN NS    ns1
        IN NS    ns2.cs
        IN MX 10 mx
        IN MX 20 mx.cs

ns1     IN A     200.13.147.60
ns2.cs  IN A     200.13.147.90

mx      IN A     200.13.147.59
mx.cs   IN A     200.13.147.113

PC_A    IN A     200.13.147.33
PC_F.cs IN A     200.13.147.65
