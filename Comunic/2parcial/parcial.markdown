# Segundo Parcial 2015.

## Ejercicio 3. Apartado b.
  Server web: www.kyatron.com en 2001:db8:fe::3 y para
dns.@. Resolución directa e inversa y un dns slave para
la zona directa dns2.@ (2001:db8:fe::6)
  Server correo info.@ (2001:db8:fe::20).
  El DNS Master dns.@ en resolución directa debe
permitir consultas de todos los hosts, notificar y
permitir transferencia al slave, pero no permitir
updates.

  Hacer `named.conf` y resolución directa e inversa para
dns.@.

## Ejercicio 4.
- Servidor web: www.@ (.250), mails: mx.@ (.236),
  alumnos.@ (.200)
- Subdominios cpn.@ y le.@
- Dominio principal y res directa dns.@ (.250) es master,
dns2.@ (.249) es slave y dns.cct-ros.org.ar (100.3.6.8)
externo.
- Subdominio le.@ tiene correo mx.le.@ (.8)
- Subdominio cpn.@ será delegado. Master dns.cpn.@ (.157)
y slave dns2.@

Realizar `named.conf` para el dominio principal y cpn.@

Hacer resolución directa para @.

**Nota**. @ = fcecon.unr.edu.ar y la base de la IP es
200.3.0.
