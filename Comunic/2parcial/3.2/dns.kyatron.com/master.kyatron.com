$ORIGIN kyatron.com
$TTL    2d

@  IN SOA dns root (
  2015100000 ; version
  2d         ; refresh
  15m        ; retry
  2w         ; expirity
  2h         ; retry negative response.
)

      IN NS    dns
      IN NS    dns2
   2w IN MX 10 info

dns   IN AAAA  2001:db8:fe::3
dns2  IN AAAA  2001:db8:fe::6
www   IN CNAME dns
ftp   IN AAAA  2001:db8:fe::2
info  IN AAAA  2001:db8:fe::20
