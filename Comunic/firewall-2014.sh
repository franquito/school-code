#!/bin/bash

_=iptables
EXTIP=200.1.2.29
LAN=10.0.0.0/8
PROXY=200.1.1.2
WEB=200.1.1.3

$_ -t filter -P FORWARD DROP

# Ejercicio 1.
$_ -t filter -A FORWARD \
  -m state --state INVALID \
  -j DROP
$_ -t filter -A FORWARD \
  -m state --state ESTABLISHED,RELATED \
  -j ACCEPT

$_ -t filter -A FORWARD \
  -i eth0 -s $LAN \
  -o eth2 \
  -m multiport --dports ! 80,443
  -j ACCEPT

$_ -t nat -A POSTROUTING \
  -s $LAN \
  -o eth2 \
  -j SNAT --to $EXTIP

# Ejercicio 2.
$_ -t filter -A FORWARD \
  -i eth0 -s $LAN \
  -o eth1 -d $DMZ \
  -p tcp --dport 22
  -j ACCEPT
for p in tcp udp; do
$_ -t filter -A FORWARD \
  -i eth0 -s $LAN \
  -o eth1 -d $DMZ \
  -p $p -m multiport --dports 53 \
  -j ACCEPT
done
$_ -t filter -A FORWARD \
  -i eth0 -s $LAN \
  -o eth1 -d $PROXY \
  --dport 3128 \
  -j ACCEPT
$_ -t filter -A FORWARD \
  -i eth0 -s $LAN \
  -o eth1 -d $WEB \
  --dport 80 \
  -j ACCEPT

# Ejercicio 4.
$_ -t filter -A FORWARD \
  -i eth2 \
  -o eth1 -p tcp -m multiport --dports 53,80
  -j ACCEPT
$_ -t filter -A FORWARD \
  -i eth2 \
  -o eth1 -p udp --dport 53
  -j ACCEPT

# Falta terminarlo...
$_ -t nat -A PREROUTING \
  -i eth2 \
