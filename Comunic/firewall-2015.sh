#!/bin/bash

_=iptables
WEBSRV=10.1.1.2
DBSERVER=10.1.1.2
MAILRELAY=10.1.1.3
LAN=10.23.0.0/16
DMZ=10.1.1.0/29
EXTIP=200.123.123.112

# Flush.
$_ -t filter -F
$_ -t nat    -F

$_ -t filter -P INPUT  DROP
$_ -t filter -P OUTPUT DROP

$_ -P FORWARD DROP

# Ejercicio 8.
for i in INPUT OUTPUT; do
  $_ -A $i -m state --state INVALID -j DROP
  $_ -A $i -m state --state RELATED,ESTABLISHED -j ACCEPT
done
$_ -t filter -A INPUT -i eth2 -p tcp \
  -s 10.23.23.5 --dport 22 -j ACCEPT

for s in $WEBSRV $MAILRELAY; do
  for p in tcp udp; do
    $_ -t filter -A OUTPUT -p $p \
      -d $s --dport 22 -j ACCEPT
  done
done

# Ejercicio 1.
$_ -t filter -A FORWARD -m state --state \
  INVALID -j DROP
$_ -t filter -A FORWARD -m state --state \
  ESTABLISHED,RELATED -j ACCEPT

$_ -t filter -A FORWARD -i eth2 -s $LAN -p tcp \
  -d $MAILSERVER -o eth1
  -m multiport \
  --dports imap,imaps,pop,pop3s,smtps -j ACCEPT
$_ -t filter -A FORWARD -i eth2 -s $LAN -p tcp \
  -d $DBSERVER -o eth1 --dport 443
for d in $WEBSRV $MAILRELAY; do
  for p in tcp udp; do
    $_ -t filter -A FORWARD -i eth2 -s $LAN \
      -p $p -o eth3 -d $d --dport 22 -j ACCEPT
  done
done

# Ejercicio 2.
for p in tcp udp; do
  $_ -t filter -A FORWARD -i eth1 -s $MAILSERVER \
    -o eth3 -m iprange \
    --dst-range $WEBSRV-$MAILRELAY -p $p \
    --dport 22 -j ACCEPT
done
$_ -t filter -A FORWARD \
  -i eth1 -s $MAILSERVER \
  -o eth3 -d $MAILRELAY \
  -p tcp --dport 465 \
  -j ACCEPT

# Ejercicio 3.
$_ -t filter -A FORWARD \
  -i eth3 -s $MAILRELAY \
  -o eth1 -d $MAILSERVER \
  -p tcp --dport 465 \
  -j ACCEPT

# Ejercicio 4.
$_ -t filter -A FORWARD \
  -i eth3 -s $WEBSRV \
  -o eth1 -d $DBSERVER \
  -p tcp --dport 3006 \
  -j ACCEPT

# Ejercicio 5.
$_ -t nat -A POSTROUTING \
  -s $LAN \
  -o eth0
  -j SNAT --to $EXTIP

# Ejercicio 6.
$_ -t nat -A PREROUTING \
  -i eth0 \
  -p tcp -m multiport --dports 53,80,443 \
  -j DNAT --to $WEBSRV
$_ -t nat -A PREROUTING \
  -i eth0 \
  -p udp -m multiport --dports 53 \
  -j DNAT --to $WEBSRV
$_ -t nat -A PREROUTING \
  -i eth0 \
  --dport 25 \
  --j DNAT --to $MAILRELAY

# Ejercicio 7.
for p in udp tcp; do
$_ -t nat -A POSTROUTING \
  -s $DMZ \
  -o eth0 \
  -p $p --dport 22 \
  -j SNAT --to $EXTIP
done
$_ -t nat -A POSTROUTING \
  -s $MAILRELAY \
  -o eth0 \
  -p tcp --dport 25 \
  -j SNAT --to $EXTIP
