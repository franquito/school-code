-module(sup).

-export([supervise/0]).

start_server() ->
  spawn(?MODULE, supervise, []).

supervise() ->
  % don't crash us but rather send us a message when
  % a linked process dies.
  process_flag(trap_exit, true),
  Pid = no_otp:start(),
  link(Pid),
  receive {'EXIT', Pid, Reason} ->
    case Reason of
      normal -> ok;
      _Other -> start_server()
    end
  end.
