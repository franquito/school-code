% Apartado a:
% Falla el pattern {C, C} = {5, 4} dado que intenta
% matchear distintos valores con una misma variable.
% Y Erlang sólo permite que una variable refiera a un
% unico valor.

-module(intro).
-export([init/0]).

match_test () -> 
  {A,B} = {5,4},
  % {C,C} = {5,4},
  {B,A} = {4,5},
  {D,D} = {5,5}.

string_test () -> [
  % true, dado que se puede usar single quotes para encerrar un atom.
  helloworld == 'helloworld', % true

  % false, dado que una lista es mayor que un atom.
  "helloworld" < 'helloworld',

  % false, dado que un atom es distinto a una lista.
  helloworld == "helloworld",

  % Un string es una lista de enteros representando ASCII,
  % Y se usa $c para repesentar el caracter c en ASCII, por lo tanto, true.
  [$h,$e,$l,$l,$o,$w,$o,$r,$l,$d] == "helloworld", % true

  % Las listas son mayores que las tuplas, por lo tanto, false.
  [104,101,108,108,111,119,111,114,108,100] < {104,101,108,108,111,119,111,114,108,100},

  % Las listas son mayores que los numeros, por lo tanto, true.
  [104,101,108,108,111,119,111,114,108,100] > 1,

  % Un string es una lista de enteros representando ASCII, por lo tanto, true.
  [104,101,108,108,111,119,111,114,108,100] == "helloworld"].

tuple_test (P1,P2) ->
  io:format("El nombre de P1 es ~p y el apellido de P2 es ~p~n",[nombre(P1),apellido(P2)]).

apellido ({persona, _, {apellido, Apellido}}) -> Apellido.
nombre ({persona, {nombre, Name}, _}) -> Name.

filtrar_por_apellido(Personas, Apellido) -> 
  [nombre(X) || X <- Personas, apellido(X) == Apellido].

init () -> 
  P1 = {persona,{nombre,"Juan"},{apellido, "Gomez"}},
  P2 = {persona,{nombre,"Carlos"},{apellido, "Garcia"}},
  P3 = {persona,{nombre,"Javier"},{apellido, "Garcia"}},
  P4 = {persona,{nombre,"Rolando"},{apellido, "Garcia"}},
  match_test(),
  tuple_test(P1,P2),
  string_test(),
  Garcias = filtrar_por_apellido([P4,P3,P2,P1],"Garcia").
