-module(fact).
%-export([fact/1, convert/2, maxlista/2]).
-compile(export_all).

fact(N) when N >= 0 ->
  fact(N, 1).

fact(0, R) -> R;
fact(N, R) -> fact(N-1, N*R).

convert({N, inch}, centimeter) ->
  {N*2.54, centimeter};
convert({N, centimeter}, inch) ->
  {N/2.54, inch}.

list_length([Head | Tail]) ->
  1 + list_length(Tail);
list_length([]) ->
  0.

% maxlista([H1, H2 | Tail]) when H1 > H2 ->
%   maxlista([H1
% maxlista([Head | Tail]) ->
  

% Writing output to terminal.
% io:format("Hola~n").

% module.
% export.
% pattern matching.
% atoms, tuples.
% lists.
%   [E1, E2, R] = [1, 2, 3, 4].
%   E1: 1, E2: 2, R: [3, 4]
% guardas
% [expr resultante | var1 <- gen1, ..., varn <- genn, condiciones]
perms([]) -> [[]];
perms(L) -> [[X|Y] || X <- L, Y <- perms(L--[X])].

% Para crear un nuevo proceso se usa spawn.
% spawn(Module, Exported_Function, List of Args).
say_something(What, 0) ->
  done;
say_something(What, Times) ->
  io:format("~p from: ~p~n", [What, self()]),
  say_something(What, Times - 1).

start() ->
  spawn(fact, say_something, [hello, 3]),
  spawn(fact, say_something, [bye, 3]).

% Los threads de Erlang no comparten memoria.
% Se pueden enviar mensajes usando pid! mensaje
% Donde pid es el PID del proceso con el cual nos
% queremos comunicar.
