-module(wait).
-export([wait/1, cronometro/3, setTriggers/3, waitUntil/2]).

wait (Time) ->
  timer:sleep(Time).

waitUntil (Periodo, Fun) ->
  wait(Periodo),
  Fun().

setTriggers (0, Periodo, Fun) ->
  ok;
setTriggers (N, Periodo, Fun) ->
  waitUntil(Periodo, Fun),
  setTriggers(N-1, Periodo, Fun).

cronometro (Fun, Hasta, Periodo) ->
  Times = Hasta div Periodo,
  spawn(wait, setTriggers, [Times, Periodo, Fun]).
