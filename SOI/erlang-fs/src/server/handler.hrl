handler(PPid, WPid, ListenSocket, UID) ->
  {ok, Socket} = gen_tcp:accept(ListenSocket),
  PPid ! {self(), ok}, % Unblock wait_connect
  io:format("[User ~p] Started a connection, "
    ++ "assigned worker ~p~n", [UID, WPid]),
  handler(UID, WPid, Socket).

handler(UID, WPid, Socket) ->
  receive
    {tcp, _, RawMsg} ->
      Msg = remove_newline(RawMsg),
      io:format("[User ~p] Sent: `~s`~n",
        [UID, Msg]),
      case get_cmd(Msg) of
        {cmd, {con}} ->
          gen_tcp:send(Socket, "ALREADY CON");
        {cmd, Cmd} ->
          io:format("[User ~p] Ask to worker ~p:"
            ++ " ~p~n", [UID, WPid, Cmd]),
          WPid ! {handler, self(), Cmd},
          receive
            {WPid, NewMsg} ->
              io:format("[User ~p] Response: "
                ++ "~p~n", [UID, NewMsg]),
              gen_tcp:send(Socket, NewMsg),
              handler(UID, WPid, Socket);
            _ ->
              throw(handler_wait_reponse)
          end;
        _ ->
          gen_tcp:send(Socket, "CMD UNKNOWN")
      end,
      handler(UID, WPid, Socket);
    {tcp_closed, Port} ->
      io:format("[User ~p] Exiting..~n", [UID]),
      exit(normal);
    _ ->
      throw(handler_receive)
  end.

% {con}
% {lsd}
% {bye}
% {rm, Filename}
% {cre, Filename}
% {opn, Filename}
% {clo, Fid}
% {rea, Fid, Size}
% {wrt, Fid, Size, Text}
get_cmd ("CON") ->
  {cmd, {con}};
get_cmd ("LSD") ->
  {cmd, {lsd}};
get_cmd ("BYE") ->
  {cmd, {bye}};
get_cmd ("RM " ++ Filename) ->
  io:format("~s~n", [Filename]),
  case re:run(Filename, "^([a-z]|[A-Z]|[0-9])+$") of
    {match, _} ->
      {cmd, {rm, Filename}};
    _ ->
      err
  end;
get_cmd ("CRE " ++ Filename) ->
  case re:run(Filename, "^([a-z]|[A-Z]|[0-9])+$") of
    {match, _} ->
      {cmd, {cre, Filename}};
    _ ->
      err
  end;
get_cmd ("OPN " ++ Filename) ->
  case re:run(Filename, "^([a-z]|[A-Z]|[0-9])+$") of
    {match, _} ->
      {cmd, {opn, Filename}};
    _ ->
      err
  end;
get_cmd("REA " ++ Data) ->
  case re:run(Data, "^FD +([1-9][0-9]*) +SIZE +([1-9][0-9]*)$") of
    {match, [_, Fid, Size]} ->
      {cmd, {rea, from_tuple(Data, Fid), from_tuple(Data, Size)}};
    _ ->
      err
  end;
get_cmd("WRT " ++ Data) ->
  case re:run(Data, "^FD +([1-9][0-9]*) +SIZE +([1-9][0-9]*) +") of
    {match, [{_, From}, Fid, Size]} ->
      RealSize = from_tuple(Data, Size),
      {cmd, {wrt, from_tuple(Data, Fid), RealSize, lists:sublist(Data, From+1, RealSize)}};
    _ ->
      err
  end;
get_cmd(_) ->
  err.

dispatch_from_handler(WPid, Cmd) ->
  throw(dispatch_from_handler).

remove_newline([]) ->
  [];
remove_newline([$\n|[]]) ->
  [];
remove_newline([X|[]]) ->
  [X];
remove_newline([X|XS]) ->
  [X| remove_newline(XS)].

from_tuple(String, {From, Length}) ->
  list_to_integer(
    lists:sublist(String, From+1, Length)).
