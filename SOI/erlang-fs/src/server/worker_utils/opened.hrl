% open ({opn, Filename}, Handler, Info, W)
open ({opn, Filename, Handler}, InfoList, W) ->
  open ({opn, Filename}, Handler, InfoList, W).
open ({opn, Filename}, Handler,
      InfoList, W) ->
  Info1 = dict:from_list (InfoList),
  Info2 = dict:append (handler, Handler, Info1),
  Info3 = dict:append (rm_marked, false,
                         Info2),
  Obj = {opened, Filename},
  NW = track (Obj, Handler, W),
  track_info (Obj, NW, Info3).

opened (Obj, W) ->
  tracked (Obj, W).

aux_opened (Fid, Handler,
            #w{tracked = Tracks}) ->
  filter (
    fun (#track{info = Info}) ->
      (dict:is_key (fid, Info)) and
        (dict:fetch (fid, Info) == [Fid]) and
        (dict:is_key (handler, Info)) and
        (dict:fetch (handler, Info) == [Handler])
    end, Tracks).

i_opened ({filename, Filename}, H, W) ->
  Obj = {opened, Filename},
  case tracked (Obj, W) of
  true ->
    track_handler (Obj, W) == H;
  false ->
    false
  end;
i_opened (Fid, H, W) ->
  length (aux_opened (Fid, H, W)) > 0.

opened_filename (Fid, H, W) ->
  #track{obj = Obj} =
    nth (1, aux_opened (Fid, H, W)),
  {opened, Filename} = Obj,
  Filename.
