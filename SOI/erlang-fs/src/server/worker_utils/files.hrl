% Define:
% file_exists(Cmd, W)
% file_fid(Filename, W)
% opened(Cmd, W)
% open(Cmd, W)
-record(file, {filename    = "",
               fid         = 0 }).

add_file(Filename, W) ->
  add_file_aux(Filename, W#w.files).
add_file_aux(Filename, Files) ->
  file:write_file ("storage/"++Filename, ""),
  fider_srv ! self(),
  FreshFid = receive FreshFid -> FreshFid end,
  Files ++ [#file{filename=Filename,
                  fid=FreshFid}].

get_files(W) ->
  W#w.files.

file_exists({cre, Filename}, W) ->
  file_exists_aux({filename, Filename},
                  W#w.files);
file_exists({opn, Filename}, W) ->
  file_exists_aux({filename, Filename},
                  W#w.files);
file_exists({wrt, Fid, _, _}, W) ->
  file_exists_aux({fid, Fid},
                  W#w.files);
file_exists({rea, Fid, _}, W) ->
  file_exists_aux({fid, Fid},
                  W#w.files);
file_exists({rea, Fid, _, _}, W) ->
  file_exists_aux({fid, Fid},
                  W#w.files).

file_write (Cmd = {wrt, Fid, Size, Text}, W) ->
  assert (file_exists (Cmd, W)),
  Filename = filename (Fid, W),
  RealText = take (Text, Size),
  file:write_file ("storage/"++Filename,
    RealText, [append]).

file_read ({rea, Fid, Size, _}, W) ->
  file_read ({rea, Fid, Size}, W);
file_read (Cmd = {rea, Fid, Size}, W) ->
  assert (file_exists (Cmd, W)),
  Filename = filename (Fid, W),
  {ok, Binary} =
    file:read_file ("storage/"++Filename),
  take (binary_to_list (Binary), Size).

file_exists_aux({filename, Filename},
                Files) ->
  any( fun(File) ->
         File#file.filename == Filename
       end,
       Files);
file_exists_aux({fid, Fid}, Files) ->
  any( fun(File) ->
         File#file.fid == Fid
       end,
       Files).

filename (Fid, W) ->
  Files = get_files (W),
  File = nth(1,
      filter( fun(File) ->
                File#file.fid == Fid
              end, Files)),
  File#file.filename.

file_fid(Filename, W) ->
  Files = get_files(W),
  File = nth(1,
      filter( fun(File) ->
                File#file.filename == Filename
              end, Files)),
  File#file.fid.

list_files(W) ->
  list_files_aux(W#w.files).
list_files_aux(Files) ->
  foldl(
    fun(File, List) ->
      List ++ " " ++ File#file.filename
    end, "", Files).
