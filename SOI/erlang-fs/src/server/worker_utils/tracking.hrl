-record(track, {obj     = "",
                handler = "",
                counter = 0,
                info    = []}).

track (O, H, W = #w{tracked = Tracks}) ->
  W#w{tracked =
      [#track{obj=O, handler=H} | Tracks]
  }.

untrack (Obj, W = #w{tracked = Tracks}) ->
  W#w{tracked =
    filter (fun (#track{obj = Object}) ->
              Obj /= Object
            end, Tracks)
  }.

switch_tracks (OldTrack = #track{obj = Obj},
               NewTrack,
               W = #w{tracked = Tracks}) ->
  W#w{tracked =
  map (fun (Track = #track{obj = Obj}) ->
             NewTrack;
           (Track) -> Track
       end, Tracks)
  }.

tracked (Obj, #w{tracked = Tracks}) ->
  any (fun (Track) ->
         Track#track.obj == Obj
       end, Tracks).

get_track (Obj, #w{tracked = Tracks}) ->
  nth (1,
      filter (fun (#track{obj = Object}) ->
                Obj == Object
              end, Tracks)).

% Getters:
track_handler (Obj, W) ->
  #track{handler = H} =
    get_track (Obj, W),
  H.
track_counter (Obj, W) ->
  #track{counter = C} =
    get_track (Obj, W),
  C.
track_info (Obj, W) ->
  #track{info = Info} =
    get_track (Obj, W),
  Info.

% Setters
track_handler (Obj, W, NewHandler) ->
  OldTrack = get_track (Obj, W),
  NewTrack = OldTrack#track{handler =
                            NewHandler},
  switch_tracks (OldTrack, NewTrack, W).
track_counter (Obj, W, NewCounter) ->
  OldTrack = get_track (Obj, W),
  NewTrack = OldTrack#track{counter =
                            NewCounter},
  switch_tracks (OldTrack, NewTrack, W).
track_info (Obj, W, NewInfo) ->
  OldTrack = get_track (Obj, W),
  NewTrack = OldTrack#track{info =
                            NewInfo},
  switch_tracks (OldTrack, NewTrack, W).

% Other
inc_tracking (Obj, W) ->
  Counter = track_counter (Obj, W),
  track_counter (Obj, W, Counter + 1).

fully_tracked (Obj, W) ->
  track_counter (Obj, W) == 3.
