wait_connect(WPids, ListenSocket, UserCounter) ->
  Index = random:uniform(length(WPids)),
  HPid = spawn_link (?MODULE, handler,
           [self(), nth(Index, WPids),
            ListenSocket, UserCounter]),
  receive
    {HPid, ok} ->
      wait_connect (WPids, ListenSocket,
                    UserCounter + 1);
    _ -> throw(wait_connect)
  end.
