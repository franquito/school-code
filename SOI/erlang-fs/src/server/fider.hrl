fider(CurrFid) ->
  receive
    Worker ->
      Worker ! CurrFid,
      fider(CurrFid + 1)
  end.
