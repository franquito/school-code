% Worker state.
-record(w, { workers = [],
             files   = [],
             tracked = [] }).

mon() ->
  
  % Initialize workers.
  WPids = lists:map(
    fun(_) ->
      spawn_link (?MODULE, init_worker, [self()])
    end,
    lists:seq(1, 5)),
  
  % Send to each worker its init info.
  lists:map(
    fun(WPid) ->
      OtherPids = lists:delete(WPid, WPids),
      WPid ! {self(),
              [#w{workers=OtherPids}]}
    end,
    WPids),

  % Initialize the FID service.
  Fider = spawn_link (?MODULE, fider, [1]),
  register(fider_srv, Fider),
  
  % Listen for a new connection.
  {ok, ListenSocket} = gen_tcp:listen(
    1234, [{active,    true},
           {reuseaddr, true}]),
  wait_connect (WPids, ListenSocket, 1).
