init_worker(Caller) ->
  io:format("[Worker ~p] Initialized.~n",
    [self()]),
  
  receive
    {Caller, WorkerArgs} ->
      apply (?MODULE, worker, WorkerArgs);
    _ ->
      throw (bad_worker_arg)
  end.
