from_rworker(W, Worker, Resp) ->
  io:format("[Worker ~p] Received from rWorker "
    ++ "~p~n", [self(), Resp]),
  
  case Resp of
  
  {{cre, Filename}, Err} ->
    Cmd = {cre, Filename},
    track_handler (Cmd, W) !
      {self(), "FILE EXISTS"},
    worker (untrack (Cmd, W));
  {cre, Filename} ->
    Cmd = {cre, Filename},
    case tracked(Cmd, W) of
      true ->
        case fully_tracked(Cmd, W) of
          true -> track_handler (Cmd, W) !
                    {self(), "OK"},
                  NW = untrack (Cmd, W),
                  worker(NW#w{
                    files=add_file(Filename, NW) });
          false -> worker (
                     inc_tracking (Cmd, W))
        end;
      false ->
        worker(W)
    end;
  
  {lsd, Handler, FilesList} ->
    Obj = {lsd, Handler},
    case tracked(Obj, W) of
      true ->
      NewList = track_info (Obj, W) ++ FilesList,
      NW = track_info (Obj, W, NewList),
      case fully_tracked (Obj, W) of
        true -> 
          FinalList = list_files(W) ++ NewList
                      ++ " ",
          NNW = track_info (Obj, NW, FinalList),
          Handler !
            {self(), track_info (Obj, NNW)},
          worker (untrack (Obj, NNW));
        false -> worker ( inc_tracking (Obj, NW))
      end;
      
      false -> worker(W)
    end;
  
  {opn, Status, Obj} ->
    case tracked (Obj, W) of
    true ->
      case Status of
      already_opened ->
        Handler = track_handler (Obj, W),
        respond (Handler, "ALREADY OPENED"),
        worker (untrack (Obj, W));
      not_found ->
        case fully_tracked (Obj, W) of
        true ->
          Handler = track_handler (Obj, W),
          respond (Handler, "FILE NOT EXISTS"),
          worker (untrack (Obj, W));
        false ->
          worker (inc_tracking (Obj, W))
        end;
      Fid ->
        Handler = track_handler (Obj, W),
        respond (Handler, "OK FID " ++
          integer_to_list (Fid)),
        NW = untrack (Obj, W),
        worker (open (Obj,
          [{fid, [Fid]}, {worker, Worker}], NW))
      end;
    false ->
      worker (W)
    end;
  
  {rea, Text, Handler} ->
    respond (Handler, Text),
    worker (W);
  
  _ -> throw(from_rworker)
  end.
