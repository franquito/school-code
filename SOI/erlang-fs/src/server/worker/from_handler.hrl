from_handler (W, Handler, Cmd) ->
  io:format("[Worker ~p] Received ~p~n",
    [self(), Cmd]),
  
  case Cmd of
  {cre, Filename} ->
    case file_exists (Cmd, W) or
             tracked (Cmd, W) of
    true ->
      respond (Handler, "FILE EXISTS"),
      worker (W);
    false ->
      broadcast (Cmd, W),
      worker (track (Cmd, Handler, W))
    end;
  
  {opn, Filename} ->
    case opened ({opened, Filename}, W) of
    true ->
      respond (Handler, "ALREADY OPENED"),
      worker (W);
    false ->
      case file_exists (Cmd, W) of
      true ->
        Fid = file_fid (Filename, W),
        respond (Handler,
          "OK FID " ++ integer_to_list (Fid)),
        worker (
          open (Cmd, Handler,
                [{fid, [Fid]}], W));
      false ->
        Obj = {opn, Filename, Handler},
        broadcast (Obj, W),
        worker (track (Obj, Handler, W))
      end
    end;
  
  {lsd} ->
    Msg = {lsd, Handler},
    broadcast (Msg, W),
    worker (track (Msg, Handler, W));

  {wrt, Fid, Size, Text} ->
    case i_opened (Fid, Handler, W) of
    true ->
      Filename =
        opened_filename (Fid, Handler, W),
      case file_exists (Cmd, W) of
      true ->
        file_write (Cmd, W);
      false ->
        Obj  = {opened, Filename},
        Info = track_info (Obj, W),
        WPid = dict:fetch (worker, Info),
        WPid ! {worker, self(), Cmd}
      end,
      respond (Handler, "OK");
    false ->
      respond (Handler, "FILE NOT OPENED/OWNED")
    end,
    worker (W);
  
  {rea, Fid, Size} ->
    case i_opened (Fid, Handler, W) of
    true ->
      Filename =
        opened_filename (Fid, Handler, W),
      case file_exists (Cmd, W) of
      true ->
        respond (Handler, file_read (Cmd, W));
      false ->
        Obj  = {opened, Filename},
        Info = track_info (Obj, W),
        WPid = dict:fetch (worker, Info),
        WPid ! {worker, self(),
                  {rea, Fid, Size, Handler}}
      end;
    false ->
      respond (Handler, "FILE NOT OPENED/OWNED")
    end,
    worker (W);
  
  {rm, Filename} ->
    % If file exists
    %   If It's opened
    %     marked as to be removed
    %   else
    %     remove
    %   say ok.
    worker (W);
  
  _ ->
    throw(bad_worker_arg1)
  end.
