from_worker (W, WPid, Msg) ->
  io:format("[Worker ~p] Received from worker "
    ++ "~p~n", [self(), Msg]),
  
  case Msg of
  
  {cre, Filename} ->
    BiggerId = self() > WPid,
    I_win    = tracked (Msg, W) and not BiggerId,
    case file_exists (Msg, W) or I_win of
      true ->
        WPid ! {rworker, self(), {Msg, error}};
      false ->
        WPid ! {rworker, self(), Msg}
    end,
    case tracked (Msg, W) and BiggerId of
      true -> worker (untrack (Msg, W));
      false -> worker (W)
    end;
  
  {lsd, Handler} ->
    Res = {lsd, Handler, list_files (W)},
    wanswer (Res, WPid),
    worker (W);
  
  {opn, Filename, Handler} ->
    case opened ({opened, Filename}, W) of
    true ->
      Res = {opn, already_opened, Msg},
      NW  = W;
    false ->
      case file_exists ({opn, Filename}, W) of
      true ->
        Fid = file_fid (Filename, W),
        Res = {opn, Fid, Msg},
        NW  = open (Msg, [{fid, [Fid]}], W);
      false ->
        Res = {opn, not_found, Msg},
        NW = W
      end
    end,
    wanswer (Res, WPid),
    worker (NW);
  
  {wrt, Fid, Size, Text} ->
    file_write (Msg, W),
    worker (W);
  
  {rea, Fid, Size, Handler} ->
    Res = {rea, file_read (Msg, W), Handler},
    wanswer (Res, WPid),
    worker (W);
  
  _ -> throw(worker_from_handler)
  
  end.
