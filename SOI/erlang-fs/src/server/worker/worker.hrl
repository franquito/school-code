worker(W) ->
  receive
    {handler, Handler, Cmd} ->
      from_handler(W, Handler, Cmd);
    {worker, Worker, Cmd} ->
      from_worker(W, Worker, Cmd);
    {rworker, Worker, Resp} ->
      from_rworker(W, Worker, Resp);
    _ ->
      throw(bad_worker_arg1)
  end.
