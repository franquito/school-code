% El servidor de Erlang consume mucha menos memoria.
% Esto se debe a que Erlang crea procesos de tamaño
% mucho más chico que C con PThreads.
% No pudimos probar con más de ~1000 conexiones
% (Independientemente del servidor que usemos) dado
% que daba error:
% ECHOCLNT: Error creating socket: Too many open files

-module(ej3_eco).
-compile(export_all).

server() ->
    {ok, ListenSocket} = gen_tcp:listen(8000, [{active, false}]),
    wait_connect(ListenSocket, 1).

wait_connect(ListenSocket, N) ->
    {ok, Socket} = gen_tcp:accept(ListenSocket),
    spawn(?MODULE, wait_connect, [ListenSocket, N+1]),
    get_request(Socket, N).

get_request(Socket, N) ->
 inet:setopts(Socket, [{active, once}]),
    receive
    {tcp, Socket, Data} ->
        io:format("Got packet: ~p~n", [Data]),
        gen_tcp:send(Socket, Data);
    {tcp_closed, Socket} ->
        io:format("Socket ~p closed~n", [Socket]);
    {tcp_error, Socket, Reason} ->
        io:format("Error on socket ~p reason: ~p~n", [Socket, Reason])
    end.

% c(ej3_eco).
% ej3_eco:server().
