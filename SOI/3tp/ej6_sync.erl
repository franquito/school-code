% Para la implementación de locks uso:
% Un proceso aparte que ejecuta la función handler.
% handler/1 se encarga de administrar los locks, y
% su único parámetro sirve para saber que Pid puede
% destruir el lock.
% handler es inicializada por createLock y destruida
% por destroyLock/2
% lock y unlock se comunican con handler y
% modificamos waiter/2, se agregó un parámetro para
% enviar el Pid de quien creó el lock.

% Para la implementación de semáforos usamos
% un proceso análogo a handler llamado manejador
% que tiene en cuenta la cantidad de locks y los
% correspondientes Pids.

-module(ej6_sync).
-export([testLock/0, testSem/0]).

%internal
-export([f/2, waiter/3]).
-export([waiter_sem/2, sem/2]).

-export([handler/1]).
-export([manejador/2]).

-import(lists, [any/2, delete/2]).

handler(CreatorPid) ->
  receive
    {Pid, lock} ->
      Pid ! {locked},
      receive
        {Pid, unlock} ->
          Pid ! {unlocked}
      end;
    {CreatorPid, exit} ->
      io:format("Exiting from handler/1~n"),
      exit(normal)
  end,
  handler(CreatorPid).

lock (L) ->
  L ! {self(), lock},
  receive
    {locked} ->
      ok
  end.

unlock (L) ->
  L ! {self(), unlock},
  receive
    {unlocked} ->
      ok
  end.

createLock () ->
  spawn(?MODULE, handler, [self()]).

destroyLock (L, Initiator) ->
  L ! {Initiator, exit}.

manejador(N, Pids) ->
  receive
    {Pid, p} when N > 0 ->
      Pid ! {lock},
      manejador(N-1, [Pid|Pids]);
    {Pid, s} ->
      Pid ! {unlock},
      Maybe = lists:any(fun(Elem) ->
        Elem =:= Pid
      end, Pids),
      if Maybe ->
          manejador(N+1, lists:delete(Pid, Pids));
         true ->
          manejador(N, Pids)
      end;
    {exit} ->
      io:format("Destruyendo el semáforo~n"),
      exit(normal)
  end.

createSem(N) ->
  spawn(?MODULE, manejador, [N, []]).

destroySem(S) ->
  S ! {exit}.

semP(S) -> 
  S ! {self(), p},
  receive
    {lock} ->
      ok
  end.

semV(S) ->
  S ! {self(), s},
  receive
    {unlock} ->
      io:format("Unlock de ~p~n", [self()]),
      ok
  end.

f(L,W) ->
  lock(L),
  % regioncritica(),
  io:format("uno ~p~n", [self()]),
  io:format("dos ~p~n", [self()]),
  io:format("tre ~p~n", [self()]),
  io:format("cua ~p~n", [self()]),
  unlock(L),
  W ! finished.

waiter(L, 0, Initiator) ->
  destroyLock(L, Initiator);
waiter(L, N, Initiator) ->
  receive
    finished ->
      waiter(L, N-1, Initiator)
  end.

waiter_sem(S, 0) ->
  destroySem(S);
waiter_sem(S, N) ->
  receive finished ->
    waiter_sem(S, N-1)
  end.

testLock() ->
  L = createLock(),
  W = spawn(?MODULE, waiter, [L,3, self()]),
  spawn(?MODULE, f, [L, W]),
  spawn(?MODULE, f, [L, W]),
  spawn(?MODULE, f, [L, W]),
  ok.
 
sem(S,W) -> 
  semP(S),
  %regioncritica(), bueno, casi....
  io:format("uno ~p~n", [self()]),
  io:format("dos ~p~n", [self()]),
  semV(S),
  W ! finished.

testSem () ->
  io:format("I'm ~p~n", [self()]),
  S = createSem(2), % a lo sumo dos usando io al mismo tiempo
  W = spawn(?MODULE, waiter_sem, [S, 5]),
  spawn(?MODULE, sem, [S, W]),
  spawn(?MODULE, sem, [S, W]),
  spawn(?MODULE, sem, [S, W]),
  spawn(?MODULE, sem, [S, W]),
  spawn(?MODULE, sem, [S, W]).
