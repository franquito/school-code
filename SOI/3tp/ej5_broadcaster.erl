-module(ej5_broadcaster).
-export([loop/1, main/0]).
-import(lists, [map/2, delete/2]).

loop(Suscribers) ->
  receive
    {suscribe, PID} ->
      io:format("Registering a new suscriber~n"),
      loop([PID|Suscribers]);
    {say, PID, Msg} ->
      io:format("Broadcasting a message from ~p~n", [PID]),
      map(fun(Suscriber) ->
        Suscriber ! Msg
        end, lists:delete(PID, Suscribers)),
      loop(Suscribers);
    {unsuscribe, PID} ->
      io:format("Ususcribing ~p~n", [PID]),
      loop(lists:delete(PID, Suscribers))
  end.

main() ->
  PID = spawn(?MODULE, loop, [[]]),
  register(?MODULE, PID).

% c(ej5_broadcaster).
% ej5_broadcaster:main().

% Server = whereis(ej5_broadcaster).
% Server ! {suscribe, self()}.
% Server ! {say, self(), "Cachafaz"}.
% Server ! {unsuscribe, self()}.
