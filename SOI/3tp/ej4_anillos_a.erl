% Ejercicio 4. Apartado a.
% Lanzar procesos en anillos.

-module(ej4_anillos_a).
-compile(export_all).
-import(lists, [map/2, zipwith/3]).

% rotatel toma una lista y devuelve la misma lista
% con el primer elemento al final.
rotatel ([]) ->
  [];
rotatel ([H|T]) ->
  lists:append(T, [H]).

nodo () ->
  receive
    {setNext, PIDNext} ->
      io:format("Setting NextPID of ~p~n", [self()]),
      nodo(PIDNext)
  end.

nodo (PIDNext) ->
  receive
    {msg, 0} ->
      % Mandar exit, 
      PIDNext ! {exit};
    {msg, N} ->
      % Mandar msg al siguiente.
      io:format("I got ~p~n", [N]),
      PIDNext ! {msg, N-1};
    {exit} ->
      io:format("exit ~n", []),
      PIDNext ! {exit},
      % Espero un último exit.
      receive
        {exit} ->
          exit(normal)
      end
  end,
  nodo(PIDNext).

% Launcher comunica a cada nodo/0 sobre el PID al cual comunicarse.
launcher([PID, NextPID]) ->
  PID ! {setNext, NextPID},
  [PID, NextPID].

anillos (N, M) ->
  % Lanzo los N procesos.
  io:format("Lanzando N procesos nodo/0~n"),
  PIDs = map(fun(M) ->
    spawn(?MODULE, nodo, [])
    end, lists:seq(1,N)),
  NextPIDs = rotatel(PIDs),

  %% Note about map:
  %% https://llaisdy.wordpress.com/2010/05/11/erlang-using-listsmap2-with-a-named-function/

  % Comunico a los N procesos sobre el PID al cual debe comunicarse.
  map(fun launcher/1, lists:zipwith(fun(X, Y) -> [X, Y] end,
    PIDs, NextPIDs)),

  % Envío el mensaje inicializador al primer nodo.
  io:format("Llamo al nodo inicializador~n"),
  lists:nth(1, PIDs) ! {msg, M}.
