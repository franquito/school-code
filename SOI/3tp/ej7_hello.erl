-module(ej7_hello).

% API:
-export([start/0]).

% Exporto hello dado que le aplico spawn.
-export([supervise/0, hello/0]).

hello() ->
  {A1,A2,A3} = now(),
  random:seed(A1,A2,A3),
  helloloop().

helloloop() ->
  receive
    after 1000 -> ok
  end,
  R = random:uniform(10),
  io:format("Hello ~p w/number ~p~n",
    [case R of 10 -> 1/uno; _ -> self() end, R]),
  helloloop().

init() -> spawn(?MODULE, hello, []).

start() ->
  spawn(?MODULE, supervise, []).

% El supervisor se encarga de llamar a init en el caso
% de que el PID que devuelve el llamado a init termine
% por una razon distinta a la terminación normal.
supervise() ->
  process_flag(trap_exit, true),
  Pid = init(),
  link(Pid),
  receive {'EXIT', Pid, Reason} ->
    case Reason of
      normal -> ok;
      _Other -> start()
    end
  end.

% c(ej7_hello).
% ej7_hello:supervise().
