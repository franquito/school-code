#include <stdio.h>
#include <sys/socket.h>       
#include <sys/types.h>       
#include <arpa/inet.h>      
#include <unistd.h>        
#include <string.h>
#include <stdlib.h>
#include <pthread.h>     

#define BUFF_SIZE 1024

pthread_mutex_t s = PTHREAD_MUTEX_INITIALIZER;

int id=0;

void *handle_client(void *arg)
{
    char buffer[BUFF_SIZE],buffer2[BUFF_SIZE];
    int res;
    int conn_s = *(int *)arg;
    pthread_mutex_lock(&s);
    int id_client = id++;
    pthread_mutex_unlock(&s);
    fprintf(stderr,"New client %d connected\n",id_client);

    while (1) {
        res = read(conn_s,buffer,BUFF_SIZE);
        if (res <= 0)
        {
          close (conn_s);
          break;
        }
        buffer [res] = '\0';
        sprintf (buffer2,"Response to client %d: %s",id_client,buffer);
        write (conn_s,buffer2,strlen(buffer2));
    }
}

int main()
{
    int list_s, conn_s=-1, *arg, res;
    struct sockaddr_in servaddr;
    pthread_t client;
    char buffer [BUFF_SIZE], buffer2 [BUFF_SIZE];

    if ( (list_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
        fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
        return -1;
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(8000);

    if ( bind(list_s, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
        fprintf(stderr, "ECHOSERV: Error calling bind()\n");
        return -1; 
    }

    if ( listen(list_s, 10) < 0 ) {
        fprintf(stderr, "ECHOSERV: Error calling listen()\n");
        return -1;                          
    }
    while (1) {
        if ( (conn_s = accept(list_s, NULL, NULL) ) < 0 ) {
            fprintf(stderr, "ECHOSERV: Error calling accept()\n");
            return -1;
        }
        arg = malloc(sizeof(int));
        *arg = conn_s;
        pthread_create(&client, NULL, handle_client, (void*)arg);
    }
    return 0;
}
