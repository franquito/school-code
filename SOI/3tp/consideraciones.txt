Hola!

Les dejamos algunas consideraciones:
+ En el ejercicio 3 Podrían cambiar los limites con 'ulimit' (Ver practica 2)
+ En el ejercicio 4 Es una mala practica utilizar '{exit}' en favor de 'exit', el capitulo 4 de libro Erlang Programming (Cesarini - Thompson) explica bien esto. El ejercicio esta muy bien.
+ En el ejercicio 6 En la implementación de los semáforos influye al comportamiento de manejador, tener la lista _Pids_?. El ejercicio está muy bien.
+ En el ejercicio 8 Se pedía modificar el comportamiento de un proceso hello mientras se estaba ejecutando, en esta implementación se espera que el proceso muera para 'cambiarlo'.

Nota 9

Guillermo y José
