#!/usr/bin/env python

from threading import Thread

ticket     = [0, 0, 0]
visitantes = 0
eligiendo  = [False, False, False]

def favorecido(i, j):
  """TODO: Docstring for favorecido.

  :i: TODO
  :j: TODO
  :returns: TODO

  """
  global ticket
  if (ticket[i] == 0 or ticket[j] < ticket[i]):
    return True
  elif (ticket[j] > ticket[i]):
    return False
  else:
    return i >= j

def molinete(m_id, c_visitantes, c_molinetes):
  global ticket, visitantes, eligiendo
  """TODO: Docstring for molinete.

  :m_id: ID del molinete
  :c_visitantes: cantidad de visitantes.
  :c_molinetes: cantidad de molinetes.
  :returns: TODO

  """
  for c in range(c_visitantes):
    eligiendo[m_id] = True
    ticket[m_id]    = max(ticket) + 1
    eligiendo[m_id] = False
    for n in range(c_molinetes):
      while(eligiendo[n]):
        pass
      while(not favorecido(n, m_id)):
        pass
    visitantes  += 1
    ticket[m_id] = 0

def main():
  """TODO: Docstring for main.

  :arg1: TODO
  :returns: TODO

  """
  threads = []
  n = len(ticket)
  for i in range(n):
    thread.append(Thread(target = molinete, args = (i, 1000, n)))
  for t in threads:
    t.start()
  for t in threads:
    t.join()
  print(visitantes)
