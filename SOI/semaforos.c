Semáforos de Dijkstra
Un semáforo de Djkstra es algo que tiene un entero (con 
un valor inicial) y dos operaciones,
P (proberen): toma un semaforo y decrementa su entero.
si este se hace <= 0 el proceso que hizo P queda
bloqueado. Esta operacion es atomica.
V (verheugen): Toma un semaforo e incrementa su entero.
Si el resultado es > 0 , despiert uno de los procesos
dormidos. Esta operacion es atomica.

Otros nombres:
P: {lock, drecrement and block if not positive
V: signal, increment and wake up if positive unlock.
