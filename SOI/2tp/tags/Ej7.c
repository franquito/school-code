#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef enum {
  true = 1, false = 0
} bool;

#define N_VISITANTES 30
#define N_ASIENTOS   10
bool sentados[N_VISITANTES];
int ctos_sentados = 0;

pthread_mutex_t s         = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t visitor_in = PTHREAD_COND_INITIALIZER;

void *barbero(void *arg) {
  int visitor_id;
  int i;

  for (;;) {
    pthread_mutex_lock(&s);
    while (!ctos_sentados) {
      printf("Barbero duerme\n");
      pthread_cond_wait(&visitor_in, &s);
    }

    // Grab visitor.
    i = 0;
    while (!sentados[i]) {
      i++;
    }
    visitor_id = i;
    ctos_sentados--;
    sentados[visitor_id] = false;
    pthread_mutex_unlock(&s);

    printf("Barbero atiende a %d\n", visitor_id);
    sleep(random()%3);
    sentados[visitor_id] = false;
  }
}

void *visitante(void *arg) {
  int id = *((int *)arg);

  pthread_mutex_lock(&s);
  if (ctos_sentados < N_ASIENTOS) {
    sentados[id] = true;
    ctos_sentados++;
    printf("Visitante %d se sienta\n", id),
    pthread_cond_signal(&visitor_in);
  } else {
    printf("Visitante %d no encuentra lugar y se va\n",
          id);
    pthread_mutex_unlock(&s);
    return ;
  }
  pthread_mutex_unlock(&s);

  while (sentados[id]) {;}
  printf("Visitante %d atendido se retira\n", id);
}

int main(int argc, char *argv[]) {
  int i, ids[N_VISITANTES];
  pthread_t barbero_thread;
  pthread_t visitantes[N_VISITANTES];

  for (i = 0; i < N_ASIENTOS; ++i) {
    sentados[i] = false;
  }

  pthread_create(&barbero_thread, NULL, barbero,
    (void *)&i);
  for (i = 0; i < N_VISITANTES; ++i) {
    ids[i] = i;
    pthread_create(
      &visitantes[i], NULL, visitante, (void *)&ids[i]);
  }

  pthread_join(barbero_thread, NULL);

  return 0;
}
