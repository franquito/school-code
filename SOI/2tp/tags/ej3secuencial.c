#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define N 50000000

int numbers[N];

int min(int *arr, int len) {
  int i, minimum;

  minimum = *arr;
  i = 1;
  for (; i < len; ++i) {
    if (*(arr+i) < minimum)
      minimum = *(arr+i);
  }

  return minimum;
}

int main(int argc, char *argv[]) {
  srand(time(NULL));
  clock_t begin, end;
  int i = 0;

  for (; i < N; i++) {
    numbers[i] = random();
  }

  begin = clock();
  printf("%d\n", min(numbers, N));
  end   = clock();
  printf("Tiempo: %fms\n", (double)(end - begin)/CLOCKS_PER_SEC);
  
  return 0;
}
