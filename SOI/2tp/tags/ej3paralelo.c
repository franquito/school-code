#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

#define N 50000000
#define T 3

int numbers[N];

int min(int *arr, int len) {
  int i, minimum;

  minimum = *arr;
  i = 1;
  for (; i < len; ++i) {
    if (*(arr+i) < minimum)
      minimum = *(arr+i);
  }

  return minimum;
}

void pmin (int *arr, int length, int id, int *result) {

  if (id == T-1)
    length += N%T;

  *result = min(arr, length);
}

int main(int argc, char *argv[]) {
  srand(time(NULL));
  double begin, end;
  int i;
  int collected_mins[T];

  omp_set_num_threads(T);
  for (i = 0; i < N; i++)
    numbers[i] = random();

  begin = omp_get_wtime();

  int j;
  #pragma omp parallel for
  for (j = 0; j < T; j++)
    pmin(numbers+N/T, N/T,
         omp_get_thread_num(), collected_mins+j);

  printf("El minimo es: %d\n", min(collected_mins, T));

  end = omp_get_wtime() - begin;

  printf("Tiempo: %fms\n", end);
  
  return 0;
}
