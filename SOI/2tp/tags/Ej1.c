/*
  c) Si supongo que tengo dos molinetes.
  - P0 toma un ticket. Y guarda en cache que P1 tiene 
  ticket = 0. Pasa por la zona critica y termina 
  poniendo su ticket en 0.
  - P1 toma un ticket y entra en la zona
  crítica.
  - P0 toma un ticket y como tiene en cache que P1
  tiene ticket = 0, entra a zona crítica.
*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N_VISITANTES 100000

typedef enum {
  true = 1, false = 0
} bool;

int  numeros[]   = {0, 0};
bool eligiendo[] = {false, false};

int visitantes = 0;

bool hasTicket(int pid) {
  return pid == 0;
}

int max(int a, int b) {
  if (a > b)
    return a;
  return b;
}

void molinete(int id) {
  for(;;) {
    int i;

    /* Get Ticket */
    eligiendo[0] = true;
    numeros[0]   = 1 + max(eligiendo[1], eligiendo[0]);
    eligiendo[0] = false;

    __asm__("mfence");
    for (i = 0; i < 2; i++) {
      while (eligiendo[id]) {;}
      while (hasTicket(i) && numeros[i] < numeros[id]
        && i < id) {;}
    }
    /* Critical Zone */
    if (visitantes < 2*N_VISITANTES)
      visitantes++;
    else {
      numeros[id] = 0;
      break;
    }
    /* End Critical Zone */
    numeros[id] = 0;
  }
}

int main () {
  omp_set_num_threads(2);
  #pragma omp parallel
    molinete(omp_get_thread_num());

  printf("Hoy hubo %d visitantes!\n", visitantes);
  return 0;
}

