#include <stdio.h>
#include <pthread.h>

#define N 2
#define ARRLEN 2014

/*
  int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                     void *(*start_routine) (void *), void *arg);
*/

pthread_mutex_t s = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t lleno = PTHREAD_MUTEX_INITIALIZER;

int arr[ARRLEN];

void *escritor(void *arg) {
  int i, num;

  num = *((int *)arg);
  for(;;) {
    sleep(random() % 3);
    for (i = 0; i < ARRLEN; i++) {
      arr[i] = num;
    }
    return NULL;
  }
}

void *lector(void *arg) {
  int v, i, err;
  int num = *((int *)arg);

  for (;;) {
    sleep(random()%3);
    err = 0;
    v = arr[0];
    for (i=1; i<ARRLEN; i++) {
      if (arr[i]!=v) {
        err=1;
        break;
      }
    }
    if (err) printf("Lector %d, error de lectura\n", num);
    else printf("Lector %d, dato %d\n", num, v);
  }
  return NULL;
}
int main() {
  int i;
  pthread_t lectores[N], escritores[N];
  int arg[N];

  for (i=0; i<ARRLEN; i++)
    arr[i] = -1;

  for (i=0; i<N; i++) {
    arg[i] = i;
    pthread_create(&lectores[i], NULL, lector, (void *)&arg[i]);
    pthread_create(&escritores[i], NULL, escritor, (void *)&arg[i]);
  }
  pthread_join(lectores[0], NULL); /* Espera para siempre */

  return 0;
}
