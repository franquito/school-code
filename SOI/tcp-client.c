/* Cliente TCP */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/* sys/socket.h defines the sockaddr structure, and
the function socket, which creates an endpoint and
returns a descriptor.
int socket(int domain, int type, int protocol) */
#include <sys/socket.h>
/* defines the sockaddr_in structure */
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 5000

typedef struct sockaddr *sad;

void error(char *s) {
  exit((perror(s), -1));
}

int main (int argc, char **argv) {
  int                sock;
  struct sockaddr_in sin;
  char               linea[1024];
  int                cto;

  if ((sock=socket(PF_INET, SOCK_STREAM, 0)) < 0)
    error("socket");

  sin.sin_family = AF_INET;
  sin.sin_port   = htons(PORT);
  inet_aton("127.0.0.1", &sin.sin_addr);

  if (connect(sock, (sad)&sin, sizeof sin) < 0)
    error("connect");

  linea[cto] = 0;
  printf("%s\n", linea);

  close(sock);
  return 0;
}
