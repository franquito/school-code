#include <stdbool.h>
#include "errors.h"
#include <pthread.h>
#include "queue.h"
#include "error_strings.h"

/* This includes:
  int socket(int domain, int type, int protocol);
    The domain argument tells what protocol family you
    want to use. Use PF_INET for UDP, TCP & other
    Internet Protocols (IPv4).
    The type argument must be SOCK_STREAM for TCP.
  struct sockaddr {
    unsigned char sa_len;   total length
    sa_family_t sa_family;  address family
    char    sa_data[14];  actually longer; address value
  };
  int listen(int s, int backlog);
  int accept
    (int s, struct sockaddr *addr, socklen_t *addrlen);
*/
#include <sys/socket.h>
/* This includes:
  sockaddr_in can be used wherever sockaddr is expected.
  struct sockaddr_in {
    uint8_t        sin_len;
    sa_family_t    sin_family; // Same as sockaddr.sa_family_t
    in_port_t      sin_port;   // port
    struct in_addr sin_addr;
    char           sin_zero[8];
  };
    where in_addr is
    struct in_addr {
      in_addr_t s_addr; // ip.
    };
  #define INADDR_ANY (u_int32_t)0x00000000
*/
#include <netinet/in.h>
/* This includes:
  htons: On MSB systems does nothing. On LSB systems
  converts the unsigned short int to MSB.
*/
#include <arpa/inet.h>

#define PORT       12345
#define BUFF_SIZE  8092
#define QUEUE_SIZE 1024

int user_ids = 0;
int curr_fid = 0;

typedef struct sockaddr *sad;

typedef struct my_user_data
{
  pthread_t thread;
  int     socket;
  struct  sockaddr_in address;
  char    linea[BUFF_SIZE];
  int     cto;
  int     id;
  queue * queue;
} user_data;

#include "util.c"

typedef struct my_queue_item
{
  inquiry *  inq;
  command    cmd;
  char       text_response[BUFF_SIZE];
  queue *    from;

  bool       is_worker;
  bool       is_response;
  int        request_id;
  int        file_exists;
} queue_item;

void print_answer (queue_item *qi)
{
  printf("Answer: \n\t- is_worker: %d \n\t- is_response %d\n", qi->is_worker, qi->is_response);

  return;
}
#define NFILES 100

typedef struct file_t
{
  int  fid;
  char filename[64];
  char content[BUFF_SIZE];
  int  opener_id;
} file;

typedef struct library_t
{
  file files[NFILES];
  int  size;
} library;

file * nth_file (library * lib, int i)
{
  return &lib->files[i];
}

library * create_library ()
{
  int       i   = 0;
  library * lib = malloc (sizeof (library));
  file    * f;

  for (; i < NFILES; ++i) {
    f = nth_file (lib, i);
    f->opener_id = -1;
  }
  lib->size = 0;

  return lib;
}

void add_file (library * lib, char * filename,
               char * content)
{
  file f;
  f.fid = curr_fid;
  strcpy (f.filename, filename);
  strcpy (f.content, content);
  f.opener_id = -1;

  *(nth_file (lib, lib->size)) = f;
  lib->size++;
  curr_fid++;
}

void list_files (library * lib, char save[])
{
  int i = 0;
  strcpy (save, "");
  for (; i < lib->size; ++i) {
    strcat (save, (nth_file (lib, i))->filename);
    if (i != lib->size-1)
      strcat (save, " ");
  }
}

file * get_file (library * lib, char * filename)
{
  int    i = 0;
  file * f;

  for (; i < lib->size; i++) {
    f = nth_file (lib, i);

    if (sequal (f->filename, filename))
      return f;
  }
  return NULL;
}

file * get_file_by_fid (library * lib, int fid)
{
  int    i = 0;
  file * f;

  for (; i < lib->size; i++) {
    f = nth_file (lib, i);

    if (f->fid == fid)
      return f;
  }
  return NULL;
}

file * last_file (library * lib)
{
  return nth_file (lib, lib->size-1);
}

typedef struct workerarg_t
{
  int id;
  queue * queues[5];
} workerarg;

#define TH_REQ 100

typedef struct request_t
{
  int          counter;
  bool         completed;
  queue *      queue;
  queue_item   packets[4];
} request;

typedef struct thread_requests_t
{
  request requests[TH_REQ];
  int     size;
} requests;

requests * init_requests ()
{
  int i, j;
  requests * tr =
    malloc (sizeof (requests));
  for (i = 0; i < TH_REQ; ++i)
  {
    tr->requests[i].counter = 0;
    tr->requests[i].queue   = NULL;
    tr->requests[i].queue   = false;
    for (j = 0; j < 4; j++)
      strcpy (tr->requests[i].packets[j].text_response,"");
  }
  tr->size = 0;

  return tr;
}

void save_request (requests * tr, queue * new)
{
  tr->requests[tr->size].queue = new;
  tr->size++;
}

void increase (request * r)
{
  r->counter++;
}

bool completed (request * r)
{
  return r->counter > 4;
}

/* This is just to copy queue_item struct */
void enqueue_in (queue * q, queue_item qi)
{
  queue_enqueue (q, &qi);

  return ;
}

void broadcast (queue_item qi, queue * qs[], int id)
{
  int i = 0;
  for (; i < 1; i++)
    if (i != id)
      enqueue_in (qs[i], qi);
}

void * worker (void * arg)
{
  workerarg *  warg = (workerarg *)arg;

  int          worker_id    = warg->id;
  queue *      worker_queue = warg->queues[warg->id];

  queue_item   message;
  queue_item   answer;

  inquiry *    inq;
  library *    lib;
  file *       f;

  requests *   ths_requests;
  request *    th_request;

  /* Auxiliar Variables */
  int          i;
  bool         flag = false;

  printf("[W %d] Initilized w/queue: %p\n",
    worker_id, worker_queue);

  lib          = create_library ();
  ths_requests = init_requests ();

  for (;;) {

  message = *(queue_item *)queue_dequeue (worker_queue);
  printf ("[W %d] Grabs %p\n", worker_id,
    (void *)&message);
  inq     = message.inq;
  answer  = message;

  switch (inq->cmd) {

  case CRE:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      increase (th_request);

      if (th_request->completed)
        break;

      if (message.file_exists) {
        th_request->completed = true;
        strcpy (answer.text_response, s_errors[1]);
        enqueue_in (th_request->queue, answer);
        printf ("[W %d] %s File: %s\n", worker_id,
          s_errors[1], inq->file);
      } else if (th_request->counter == 4) {
        add_file (lib, inq->file, "");
        printf ("About to add file: %s\n", inq->file);
        strcpy (answer.text_response, "OK");
        enqueue_in (th_request->queue, answer);
        printf("[W %d] Added file <%s>\n",
          worker_id, (last_file (lib))->filename);
      }
    } else if (message.is_worker) {

      if (get_file (lib, inq->file))
        answer.file_exists = true;

      answer.is_response = true;
      enqueue_in (message.from, answer);
    } else {
      if (get_file (lib, inq->file)) {
        printf ("[W %d] %s File: %s\n", worker_id,
          s_errors[1], inq->file);
        strcpy (answer.text_response, s_errors[1]);
        enqueue_in (message.from, answer);
        break; }

      save_request (ths_requests, message.from);
      answer.is_worker  = true;
      answer.request_id = ths_requests->size - 1;
      answer.from       = worker_queue;

      // @TODO: Why this function doesn't work?
      // broadcast (answer, warg->queues, worker_id);
      printf ("[W %d] CRE Request, File: %s\n",
        worker_id, inq->file);
      for (i = 0; i < 5; i++)
        if (i != worker_id)
          enqueue_in (warg->queues[i], answer);
    }
    break;
  case LSD:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      th_request->packets[th_request->counter] =
        message;
      increase (th_request);

      if (th_request->counter == 4) {
        list_files (lib, answer.text_response);
        for (i = 0; i < 4; i++) {
          strcat (answer.text_response, " ");
          strcat (answer.text_response,
                  th_request->packets[i].text_response);
        }
        enqueue_in (th_request->queue, answer);
        printf ("[W %d] LSD: %s\n",
          worker_id, answer.text_response);
      }
    } else if (message.is_worker) {
      answer.is_response = true;

      list_files (lib, answer.text_response);
      enqueue_in (message.from, answer);
    } else {
      save_request (ths_requests, message.from);

      answer.is_worker  = true;
      answer.request_id = ths_requests->size - 1;
      answer.from       = worker_queue;
      for (i = 0; i < 5; i++)
        if (i != worker_id)
          enqueue_in (warg->queues[i], answer);
    }
    break;
  case OPN:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      increase (th_request);

      if (th_request->completed)
        break;

      if (message.file_exists) {
        th_request->completed = true;
        enqueue_in (th_request->queue, answer);
      } else if (th_request->counter == 4) {
        strcpy (answer.text_response, s_errors[3]);
        enqueue_in (th_request->queue, answer); }
    } else if (message.is_worker) {
      answer.is_response = true;
      if (f = get_file (lib, inq->file)) {
        /* If file is here */
        answer.file_exists = true;
        if (f->opener_id > -1)
          strcpy (answer.text_response, s_errors[2]);
        else {
          sprintf (answer.text_response, "OK FID %d",
            f->fid);
          f->opener_id = inq->user_id;
        }
      }
      enqueue_in (message.from, answer);
    } else {
      if (f = get_file (lib, inq->file)) {
        /* If file is here */
        if (f->opener_id > -1) {
          /* Answer w/error if the file already open */
          printf ("[W %d] %s File: %s\n", worker_id,
            s_errors[2], inq->file);
          strcpy (answer.text_response, s_errors[2]);
          enqueue_in (message.from, answer);
          break; }

        /* Return OK FID fid */
        f->opener_id = inq->user_id;
        sprintf (answer.text_response, "OK FID %d",
          f->fid);
        enqueue_in (message.from, answer);
      } else {
        /* Broadcast to other workers */
        save_request (ths_requests, message.from);

        answer.is_worker  = true;
        answer.request_id = ths_requests->size - 1;
        answer.from       = worker_queue;
        for (i = 0; i < 5; i++)
          if (i != worker_id)
            enqueue_in (warg->queues[i], answer);
      }
    }
    break;
  case CLO:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      increase (th_request);

      if (th_request->completed)
        break;

      if (message.file_exists) {
        th_request->completed = true;
        enqueue_in (th_request->queue, answer);
      } else if (th_request->counter == 4) {
        strcpy (answer.text_response, s_errors[3]);
        enqueue_in (th_request->queue, answer); }
    } else if (message.is_worker) {
      answer.is_response = true;
      if (f = get_file_by_fid (lib, inq->fid)) {
        /* If file is here */
        answer.file_exists = true;
        if (f->opener_id == inq->user_id) {
          strcpy (answer.text_response, "OK");
          f->opener_id = -1;
        } else
          strcpy (answer.text_response, s_errors[4]);
      }
      enqueue_in (message.from, answer);
    } else {
      /* If file is here */
      if (f = get_file_by_fid (lib, inq->fid)) {
        if (f->opener_id == inq->user_id) {
          strcpy (answer.text_response, "OK");
          f->opener_id = -1;
        } else
          strcpy (answer.text_response, s_errors[4]);
        enqueue_in (message.from, answer);
      } else {
        /* Broadcast to other workers */
        save_request (ths_requests, message.from);
        answer.is_worker  = true;
        answer.request_id = ths_requests->size - 1;
        answer.from       = worker_queue;
        for (i = 0; i < 5; i++)
          if (i != worker_id)
            enqueue_in (warg->queues[i], answer);
      }
    }
    break;
  case WRT:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      increase (th_request);

      if (th_request->completed)
        break;

      if (message.file_exists) {
        th_request->completed = true;
        enqueue_in (th_request->queue, answer);
      } else if (th_request->counter == 4) {
        strcpy (answer.text_response, s_errors[3]);
        enqueue_in (th_request->queue, answer); }
    } else if (message.is_worker) {
      answer.is_response = true;
      if (f = get_file_by_fid (lib, inq->fid)) {
        /* File's here */
        answer.file_exists = true;
        if (f->opener_id == inq->user_id) {
          /* I opened it. */
          inq->text[inq->size] = '\0'; // Cut string.
          strcat (f->content, inq->text);
          printf ("New Content: %s\n", f->content);
          strcpy (answer.text_response, "OK");
        } else
          strcpy (answer.text_response, s_errors[4]);
      }
      enqueue_in (message.from, answer);
    } else {
      if (f = get_file_by_fid (lib, inq->fid)) {
        /* File is here */
        if (f->opener_id == inq->user_id) {
          /* I opened it. */
          inq->text[inq->size] = '\0'; // Cut string.
          strcat (f->content, inq->text);
          strcpy (answer.text_response, "OK");
        } else
          strcpy (answer.text_response, s_errors[4]);
        enqueue_in (message.from, answer);
      } else {
        /* Broadcast to other workers */
        save_request (ths_requests, message.from);
        answer.is_worker  = true;
        answer.request_id = ths_requests->size - 1;
        answer.from       = worker_queue;
        for (i = 0; i < 5; i++)
          if (i != worker_id)
            enqueue_in (warg->queues[i], answer);
      }
    }
    break;
  case REA:
    if (message.is_response) {
      th_request =
        &ths_requests->requests[message.request_id];
      increase (th_request);

      if (th_request->completed)
        break;

      if (message.file_exists) {
        th_request->completed = true;
        enqueue_in (th_request->queue, answer);
      } else if (th_request->counter == 4) {
        strcpy (answer.text_response, s_errors[3]);
        enqueue_in (th_request->queue, answer); }
    } else if (message.is_worker) {
      answer.is_response = true;
      if (f = get_file_by_fid (lib, inq->fid)) {
        /* File's here */
        answer.file_exists = true;
        if (f->opener_id == inq->user_id) {
          /* I opened it. */
          strcpy (answer.text_response, f->content);
          answer.text_response[inq->size] = '\0';
        } else
          strcpy (answer.text_response, s_errors[4]);
      }
      enqueue_in (message.from, answer);
    } else {
      if (f = get_file_by_fid (lib, inq->fid)) {
        /* File is here */
        if (f->opener_id == inq->user_id) {
          /* I opened it. */
          strcpy (answer.text_response, f->content);
          answer.text_response[inq->size] = '\0';
        } else
          strcpy (answer.text_response, s_errors[4]);
        enqueue_in (message.from, answer);
      } else {
        /* Broadcast to other workers */
        save_request (ths_requests, message.from);
        answer.is_worker  = true;
        answer.request_id = ths_requests->size - 1;
        answer.from       = worker_queue;
        for (i = 0; i < 5; i++)
          if (i != worker_id)
            enqueue_in (warg->queues[i], answer);
      }
    }
    break;
  default:
    // queue_enqueue (message->from, &s_errors[0]);
    break;
  }
  }

  return NULL;
}

void * thread (void * arg)
{
  user_data *  ud;
  void *       buffer[QUEUE_SIZE];
  queue *      q;
  queue_item * qi;
  inquiry *    curr_inquiry;
  char *       response;

  ud = (user_data *)arg;

  /* Initialization of Queue & Queue Item */
  q  = create_queue (buffer);
  qi = malloc (sizeof (queue_item));

  curr_inquiry = malloc (sizeof (inquiry));

  for (;;) {
    ud->cto = read (ud->socket, ud->linea,
                    sizeof ud->linea);
    if (ud->cto == -1)
      errno_abort ("Read error");

    ud->linea[ud->cto] = 0;
    printf ("de %s:%d llega [%s]\n",
      inet_ntoa (ud->address.sin_addr),
      ntohs (ud->address.sin_port),
      ud->linea);

    curr_inquiry = fill_inquiry (curr_inquiry, ud);
    if (curr_inquiry->cmd == BYE)
    {
      write (ud->socket, "OK", BUFF_SIZE);
      close (ud->socket);
      break;
    }
    else
    {
      qi->file_exists   = false;
      qi->inq           = curr_inquiry;
      qi->from          = q;
      qi->is_worker     = false;
      qi->is_response   = false;
      enqueue_in (ud->queue, *qi);

      qi = queue_dequeue (qi->from);
      write (ud->socket, qi->text_response, BUFF_SIZE);
    }
  }

  printf("[Thread %d] Finishing\n", ud->thread);
  free (ud);

  return NULL;
}

// Dispatcher:
int main (int argc, char *argv[])
{
  // Create 5 queues & spawn 5 workers.
  int i = 0;
  int j = 0;
  void * buffer[5][QUEUE_SIZE];
  queue * queues[5];
  for (i = 0; i < 5; i++)
    queues[i] = create_queue(buffer[i]);

  workerarg wa[5];
  pthread_t t_workers[5];
  for (i = 0; i < 5; i++) {
    wa[i].id = i;
    for (j = 0; j < 5; ++j)
      wa[i].queues[j] = queues[j];
    pthread_create (&t_workers[i], NULL,
                    worker, &wa[i]); }

  // Start socket listening...
  int    main_socket;
  struct sockaddr_in address;
  srand (time (NULL));

  main_socket = socket (
    /* int domain */ PF_INET,
    /* int type */ SOCK_STREAM,
    /* int protocol */ 0);
  if (main_socket == -1)
    errno_abort ("Socket Error");

  // To avoid bind error: address already in use.
  if (setsockopt (main_socket, SOL_SOCKET, SO_REUSEADDR,
                  &(int){ 1 }, sizeof (int)) == -1)
    errno_abort ("setsockopt(SO_REUSEADDR) failed");

  // Filling sockaddr_in
  address.sin_family      = AF_INET;
  address.sin_port        = htons(PORT);
  address.sin_addr.s_addr = INADDR_ANY;

  // tell the socket which ip:port we want to listen
  if (bind (main_socket, (sad)&address, sizeof address)
      == -1)
    errno_abort ("Binding error");

  // do more stuff related to connection
  if (listen (main_socket, 100) == -1)
    errno_abort ("Listening error");

  // loop
  while (1) {
    user_data *ud = malloc (sizeof (user_data));
    socklen_t l   = sizeof ud->address;

    // Wait for a client.
    ud->socket = accept (
      main_socket, (sad)&(ud->address), &l);
    if (ud->socket == -1)
      errno_abort ("Accept error");
    ud->id = user_ids;
    ud->queue = queues[rand() % 5];

    printf ("New connection from %s:%d with ID %d\n",
      inet_ntoa(ud->address.sin_addr),
      ntohs(ud->address.sin_port),
      ud->id
      );

    // Say Hi
    char welcome[BUFF_SIZE] = "OK ID X";
    welcome[6] = user_ids + '0';
    if (write (ud->socket, welcome, BUFF_SIZE) < 0)
      errno_abort ("Write error");

    // Move the new user to a thread.
    printf("[Thread %d] Initializing\n", thread);
    pthread_create (&ud->thread, NULL, thread, ud);
    pthread_detach (ud->thread);
    user_ids++;
  }

  close(main_socket);
  return 0;
}
