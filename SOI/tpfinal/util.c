typedef enum {
  CON,
  LSD,
  DEL, /* file */
  CRE, /* file */
  OPN, /* file */
  WRT, /* FD fid SIZE size text */
  REA, /* FD fid SIZE size */
  CLO, /* FD fid */
  BYE } command;

typedef struct m_inquiry
{
  command cmd;
  char *  file;
  int     fid;
  int     size;
  char *  text;
  int     user_id;
} inquiry;

command to_command (char *cmd)
{
  if (strncmp ("CON", cmd, 3) == 0)
    return CON;
  if (strncmp ("LSD", cmd, 3) == 0)
    return LSD;
  if (strncmp ("DEL", cmd, 3) == 0)
    return DEL;
  if (strncmp ("CRE", cmd, 3) == 0)
    return CRE;
  if (strncmp ("OPN", cmd, 3) == 0)
    return OPN;
  if (strncmp ("WRT", cmd, 3) == 0)
    return WRT;
  if (strncmp ("REA", cmd, 3) == 0)
    return REA;
  if (strncmp ("CLO", cmd, 3) == 0)
    return CLO;

  return BYE;
}

inquiry * fill_inquiry (inquiry * inq,
                        user_data * user)
{
  char * line = user->linea;
  char * tok;

  tok = strtok(line, " ");
  inq->cmd = to_command (tok);
  inq->user_id = user->id;

  switch (inq->cmd)
  {
    case DEL: case CRE: case OPN:
      inq->file = strtok (NULL, " ");
      break;
    case WRT: case REA: case CLO:
      strtok (NULL, " "); // Drop 'FD' string.
      inq->fid = atoi (strtok (NULL, " "));
      break;
  }

  switch (inq->cmd)
  {
    case REA:
      strtok (NULL, " "); // Drop 'SIZE' string.
      inq->size = atoi (strtok (NULL, " "));
      break;
    case WRT:
      strtok (NULL, " "); // Drop 'SIZE' string.
      inq->size = atoi (strtok (NULL, " "));
      inq->text = strtok (NULL, "\0");
      break;
  }

  return inq;
}

bool sequal (const char * str1, const char * str2)
{
  return strcmp (str1, str2) == 0;
}

