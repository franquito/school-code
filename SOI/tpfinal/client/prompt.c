#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libdfs.h"
#include "../util.c"

// @TODO: This is awful. Just need to remove the last
// carrier return from the read line.
// Aux function.
void remchar (char * str, char c) {
  char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != c) dst++;
    }
    *dst = '\0';
}

int conn;

int main(int argc, char *argv[])
{
  char line[BUFF_SIZE];
  char token;
  command cmd;

  while (1) {
    fgets (line, BUFF_SIZE, stdin);
    remchar (line, '\n');
    cmd = to_command (line);

    char * tokens;
    char * cmd_token;
    char * arg0;
    char * args[5];

    // Aliases.
    char * file, fid, size, text;

    // cmd_token = strtok (line, " ");
    // args[0]   = strtok (NULL, " ");

    switch (cmd)
    {
      case CON:
        if (dfs_connect ("127.0.0.1") == -1) {
          printf("Bad Connection\n");
          abort();
        }
        read_reply(line);
        printf("%s\n", line);
        break;
      case BYE:
        rrp (line);
        exit(0);
        break;
      default:
        rrp (line);
        break;
    }
  }
  return 0;
}
