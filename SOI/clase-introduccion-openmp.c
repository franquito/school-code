/* Introducción a OpenMP
- Una API que permite escribir programas multi-hilo
- Está formada por directivas, funciones de biblioteca y variables de entorno.
- Basado en el modelo Fork-Join
- Para compilar uso GCC, con la flag -fopenmp
*/

// Ejemplo:
#include <stdio.h>
#include <omp.h>


// int main () {
//   omp_set_num_threads(10);
//   #pragma omp parallel
//   printf("Hola mundo %d\n", omp_get_thread_num());
//   return 0;
// }

/* 
El ámbito de cada directiva OpenMP es el del bloque que 
hay justo a continuación.
Con la directiva #pragma omp parallel se puede ejecutar
una instruccion
(o bloque de instrucciones) en paralelo, un número dado de veces.
  omp_set_num_threads() permite determinar ese numero.
  omp_get_num_threads() devuelve la cantidad de hilos que están corriendo en un momento dado.
  omp_get_thread_num() devuelve el numero de hilo que la ejecuta.

#pragma omp <nombre> [clausulas opcionales]
A parallel se le puede agregar private(lista de
variables) que permite usar variables definidas
anteriormente como locales de cada hilo. (Sin importarle
el valor que tenìa antes).
*/

// int main () {
//   int i = 999;
//   omp_set_num_threads(10);
//   printf("omp_get_num_threads: %d\n",
//     omp_get_num_threads());
//   #pragma omp parallel private(i)
//   {
//     printf("%d\n", i);
//     i = omp_get_thread_num();
//     printf("Hola mundo %d de %d \n", i, omp_get_num_threads());
//   }
//   printf("%d\n", i);
//   return 0; 
// }

// int main() {
// 	omp_set_num_threads(10);
// 	#pragma omp parallel
// 	{
// 		int i;
// 		#pragma omp for
// 		for (i=0; i<10; i++)
// 			printf("iteracion %d, hilo %d\n", i, omp_get_thread_num());
// 	}
// 	return 0;
// }

/*
Con la directiva #pragma omp for permite ejecutar las iteraciones de un for en 
paralelo. Divide el trabajo entre los hilos que se estàn ejecutando (Hay que
hacerlo dentro de un parallel).
Se puede resumir con #pragma omp parallel for
Clausula: schedule (static)
		   (dynamic)
*/

// int main() {
// 	int i;
// 	omp_set_num_threads(10);
// 	#pragma omp parallel
// 	{
// 		#pragma omp sections
// 		{
// 			#pragma omp section
// 			{
// 				printf("Seccion 1, ejecutada por %d\n", omp_get_thread_num());
// 			}
// 			#pragma omp section
// 			{
// 				printf("Seccion 2, ejecutada por %d\n", omp_get_thread_num());
// 			}
// 		}
// 	}
// 	return 0;
// }

/*
La clausula reduction(op:var) hace una copia local de var ante de la ejecucion en
paralelo, y despues de terminada esta, reduce estas
variables con el poperador "op".
Copai el resultado en la variable original.
*/
int main() {
	int i;
	double a[100], res;
	for (i=0; i<100; i++)
		a[i] = i*3.14;
	#pragma omp parallel for reduction (+:res)
	for (i=0;i<100;i++)
		res+=a[i];
	printf("Resultado: %f\n", res);
	return 0;
}

/* 
*/

int main () {
	int i;
}
