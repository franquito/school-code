#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/* This includes:
  int socket(int domain, int type, int protocol);
    The domain argument tells what protocol family you
    want to use. Use PF_INET for UDP, TCP & other
    Internet Protocols (IPv4).
    The type argument must be SOCK_STREAM for TCP.
  struct sockaddr {
    unsigned char sa_len;   total length
    sa_family_t sa_family;  address family
    char    sa_data[14];  actually longer; address value
  };
  int listen(int s, int backlog);
  int accept
    (int s, struct sockaddr *addr, socklen_t *addrlen);
*/
#include <sys/socket.h>
/* This includes:
  sockaddr_in can be used wherever sockaddr is expected.
  struct sockaddr_in {
    uint8_t        sin_len;
    sa_family_t    sin_family; // Same as sockaddr.sa_family_t
    in_port_t      sin_port;   // port
    struct in_addr sin_addr;
    char           sin_zero[8];
  };
    where in_addr is
    struct in_addr {
      in_addr_t s_addr; // ip.
    };
  #define INADDR_ANY (u_int32_t)0x00000000
*/
#include <netinet/in.h>
/* This includes:
  htons: On MSB systems does nothing. On LSB systems
  converts the unsigned short int to MSB.
*/
#include <arpa/inet.h>

#define PORT 5000

typedef struct sockaddr *sad;

/*
  About the comma operator:
  https://en.wikipedia.org/wiki/Comma_operator
  perror(<string>) prints in std error output <string>
  followed by a description of the last error 
  encountered on a system call.
  A system call usually returns -1 if there's an error,
  and sets errno. errno is used in conjunction with an
  array of errors descriptions by perror for the
  printing.
  Note: the return of perror doesn't matter since we
  use the comma operator to discard it.
*/
void error(char *s) {
  exit((perror(s), -1));
}

int main(int argc, char *argv[]) {
  int       sock, sock1;
  struct    sockaddr_in sin, sin1;
  char      linea[1024];
  int       cto;
  socklen_t l;

  if ((sock=socket(PF_INET, SOCK_STREAM, 0)) < 0)
    error("Socket error");

  sin.sin_family      = AF_INET;
  sin.sin_port        = htons(PORT);
  sin.sin_addr.s_addr = INADDR_ANY;

  /* tell socket which addr/port we want to listen */
  if (bind(sock, (sad)&sin, sizeof sin) < 0)
    error("Binding error");

  /* do more stuff related to connection */
  if (listen(sock, 5) < 0)
    error("Listening error");

  /* the server accepts connections with accept */
  l = sizeof sin1;
  if ((sock1 = accept(sock, (sad)&sin1, &l)) < 0)
    error("Accept error");

  if ((cto = read(sock1, linea, sizeof linea)) < 0)
    error("Read error");

  linea[cto] = 0;
  printf("de (%d, %s) llega [%s]\n",
    ntohs(sin1.sin_port),
    inet_ntoa(sin1.sin_addr),
    linea);

  linea[0]++;

  if (write(sock1, linea, cto) < 0)
    error("Write error");

  close(sock);
  close(sock1);
  return 0;
}
