/* Cliente UDP */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 5000

typedef struct sockaddr *sad;

void error(char *s) {
  exit((perror(s), -1));
}

int main(int argc, char **argv) {
  int    sock;
  struct sockaddr_in sin;
  char   linea[1024];
  int    cto;

  if ((sock=socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    error("socket");

  sin.sin_family = AF_INET;
  sin.sin_port = htons(PORT);
  inet_aton("127.0.0.1", &sin.sin_addr);
  if (sendto(sock, argv[1], strlen(argv[1]),0,
    (sad)&sin, sizeof sin) < 0)
    error("sendto");
  // if ((cto=recvfrom(sock, linea, 1024, 0, NULL, NULL)) < 0)
  //   error("recufrom");
  // linea[cto] = 0;
  // printf("[%s]\n", linea);
  // close(sock);
  return 0;
}
