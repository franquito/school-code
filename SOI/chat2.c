/*chat*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>

typedef struct sockaddr *sad;

void error(char *s){
	exit((perror(s),-1));
}

#define MAX(x,y) ((x)>(y)? (x):(y))

int main(int argc, char **argv){
	int sock;
	struct sockaddr_in sin;
	char linea[1024];
	int cto, mfdm1;
	fd_set in, in_orig;
	if(argc!=4){
		fprintf(stderr, "Uso: %s [host remoto][port remoto][port local]\n",argv[0]);
		exit(-1);
	}
	if((sock=socket(PF_INET,SOCK_DGRAM,0))<0) 
		error("socket");
	/*nuestra direccion de recepcion*/
	sin.sin_family = AF_INET;
	sin.sin_port = htons(atoi(argv[3]));
	sin.sin_addr.s_addr = INADDR_ANY;
	if(bind(sock,(sad)&sin,sizeof sin)<0) 
		error("bind");
	
	/*direccion remota*/
	sin.sin_family = AF_INET;
	sin.sin_port = htons(atoi(argv[2]));
	inet_aton(argv[1],&sin.sin_addr);
	
	/*los fd_set*/
	FD_ZERO(&in_orig);
	FD_SET(0,&in_orig);
	FD_SET(sock,&in_orig);
	mfdm1 = MAX(0,sock) +1;
	for(;;){
		memcpy(&in,&in_orig,sizeof in);
		if(select(mfdm1,&in,NULL,NULL,NULL)<0)
			error("select");
		if(FD_ISSET(0,&in)){
			fgets(linea,1024,stdin);
			if(sendto(sock,linea,strlen(linea),0,(sad)&sin,sizeof sin)<0)
				error("sendto");
		}
		if(FD_ISSET(sock,&in)){
			if((cto=recvfrom(sock,linea,1024,0,NULL,NULL))<0)
				error("recvfrom");
			linea[cto]=0;
			printf("%s\n",linea);
		}
	}
	close(sock);
	return 0;
}
