Nota: para evitar este tipo de errores:
-module(q).
f(args) -> ...
  spawn(p, f, [args]), % cut and paste

Se recomienda usar, si f está en éste módulo,
  spawn(?MODULE, f, [ags])


Records:
Son como los records de Haskell, solo qu más prácticos.
Se debe declarar con (ej.)
-record(nombre,{miembros, con posibles valores iniciales})

Si no tienen valores iniciales, reciben undefined. Ej:
-record(persona, {nombre, edad, ciudad="Rosario"}).
Se crea un record con
#nombre {posibles valores de sus miembros}
Ej:
B = #persona {nombre = "Pirulo"} edad = undefined, ciudad = "Rosario"
C = #persona {nombre = "NN", ciudad = "SF"}
Sus miembros se acceden con
B#persona.nombre
Los records son inmutables, pero puede hacerse copias,
alterando miembros en el momento del copiado.
f(B#persona {edad = 55})
Por supuesto, se puede hacer pattern matching con records.
Funciones Anónimas:
fun(args) -> expr(s) 
admiten PM, como en ML.
fun(0) -> ok; (_) > error enn
¿Las funciones anonimas no pueden ser recursivas?
=> fn 0 => 1 | n => n*fact(n-1)
Necesitamos algo que meta a esta función dentro de ella.
Una solución fue encontrada por A. Church en 1936.
Hay que encontrar una función Y que cumpla
esta ecuación.
  Yf = f(Yf)
Punto fijo de una función:
  x_0 = f(x_0)
En Haskell
y f = f(y f)
fact = \fact -> \n -> if n == 0 then 1 else n*fact(n-1)


(Y fact) 3 = fact(Y fact) 3 =
(fn 0 => 1 | fn n => n*(Y fact)(n - 1)) 3 =
3*(Y fact)(3
La versión eager del Y es
  fun Y f = fn a => f(Y f) a

En Erlang:
y(F) -> fun(A) -> (F(y(F)))(A) end
y se puede usar para hacer recursivas funciones
anónimas. Hace unos años,  usando un parser transform,
pudimos hacerlo más bonito.

En la última versión agregaron la posibilidad de
funciones anonimas recursivas.
  fun fact(0) -> 1; (N) -> N*fact(N-1) end

Preprocesador. No es tan potente como el de C.
Ejs:
-define(MACRO(args), expr).
Se usa con ?MACRO(args)
-ifdefined(MACRO)
  ...
-else
  ...
-endif
-undef(MACRO).
Se pueden inclir archivos de cabecera (.hrl)
-include(cosa). % Análogo a #include "cosa.h"
-include_lib(cosa). % Análogo a #include <cosa.h>
Una funcion que reciba una lista de pids y un mensaje,
y lo envíe a c/u de ellos.
Una versión.
  broadcast(Pids, Msg) ->
    [P! Msg || P <- Pids].
Otra versión
  broadcast(Pids, Msg) ->
    lists:foreach(fun(P) -> P! Msg end, Pids).
Una función para vaciar el Inbox.
empty() ->
  receive _ -> empty();
  after 0 -> ok
  end.
Un par productor/consumidor con buffer acotado.
Como no tenemos memoria compartida, modelaremos el buffer
con un proceso.

buffer(Max, Items) ->
  receive
    {get, PidC} when length(Items)>0 ->
      [H|T] = Items,
      PidC H,
      buffer(Max, T);
    {put, PidP, Item} when length(items)<Max ->
      PidP! ok,
      buffer(Max, Items++[Item])
  end.

Esta versión necesita que el productor, inmediatamente 
despues de un put debe esperar un ok.

Mas sintaxis: Erlang tiene case:
  case expr of
  pm1 (when cond1) -> expr1;
  .
  .
  pmn (when condn) -> exprn
  end
Erlang tiene if, aunque se usa raramente.
  if
    cond1 -> expr1;
    cond2 -> expr2;
    ...
    true -> exprtrue
  end
Excepciones. Erlang permite capturar excepciones.
Inicialmente lo hacía así:
  catch(expr)
Si se producía un error, catch devolvía una tupla
infame con info sobre el error.
Luego, con C++ y Java se optó por agregar otra manera.
  try
    expr
  catch
    pm1 -> expr1
    ...
    pmn -> exprn
  finally
    expr se ejecuta siempre
  end
Los pm son de la forma:
  tipo de excep: Nombre de la except
  Los tipos son 3, exit, error y otro.

Tratemos de hacer un map/reduce

1er caso: reduce puede hacerse en cualquier orden.
mapreduce(Mod, Func, LItems, NProcs) ->
  L = lists:split(LItems, NProcs),
  Pids = [spawn(?MODULE, fun(Arq) -> self()!apply(Mod, Func, Arg) end || Arg <- L],
  loop(Pids
loop(Pids) -> loop(Pids, Inicial).
loop([], R) -> R;
loop([_|T], R) ->
  receive X -> loop(T, R+X).

2ndo caso: importa el orden de reducción
Cambios.
  loop([Pid | T, R) ->
    receive {Pid, X} -> 
Hasta ahora hemos usado Pids para comunicar procesos. Un
proceso puede registrarse con un nombre  con
  register(nombre, pid)
y puede preguntarse qué pid tiene un proceso registrado
con where(nombre)

Campos de bits.
<<Items>>
Los Items se pueden adornar con longitud en bits y
atributos.
  <<"ABC">> equivale a A=<<$A, $B: 8, $C: 8>>
Se pueden extraer con pm
  <<X,Y,Z>> = A
Se pueden construir pro compresión
C = <<X:8 || X <- "ABC">>
y desempaquetar por compresión
  [X || X <= C]
