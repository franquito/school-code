/* Server UDP */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 5000

typedef struct sockaddr *sad;

void error(char *s) {
  exit((perror(s), -1));
}

int main() {
  int sock;
  struct sockaddr_in sin;
  char linea[1024];
  int cto;
  socklen_t l;
  if ((sock=socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    error("socket");
  sin.sin_family = AF_INET;
  sin.sin_port = htons(PORT);
  sin.sin_addr.s_addr = INADDR_ANY;
  if (bind(sock, (sad)&sin, sizeof sin) < 0)
    error("bind");
  l = sizeof sin;
  if ((cto=recvfrom(sock, linea, sizeof linea, 0, (sad)&sin, &l)) < 0)
    error("recufrom");
  linea[cto] = 0;
  printf("de (%d, %s) llega %s \n",
    ntohs(sin.sin_port), inet_ntoa(sin.sin_addr), linea);
  linea[0]++;
  if (sendto(sock, linea, cto, 0, (sad)&sin, sizeof sin)  < 0)
    error("sendto");
  close(sock);
  return 0;
}
