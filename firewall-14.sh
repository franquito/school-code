#!/bin/bash

LAN=10.0.0.0/8
PROXY=200.1.1.2
WEBSERVER=200.1.1.3
DNS=200.1.1.3
PUBLICIP=200.1.2.29

_=iptables

# Flush.
$_ -t filter -F
$_ -t nat    -F

# Policy for FORWARD chain.
$_ -t filter -P FORWARD DROP

# Filter states.
$_ -t FORWARD -m state --state INVALID -j DROP
$_ -t FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
# Only state NEW here.

# Ejercicio 1.
$_ -t filter -A FORWARD -i eth0 -o eth2 -s $LAN \
  -m multiport --dports ! 80,443 -j ACCEPT
$_ -t nat -A POSTROUTING -o eth2 -s $LAN \
  -m multiport --dports ! 80,443 -j SNAT --to $PUBLICIP

# Ejercicio 2.
for PROTOCOL in tcp udp; do
$_ -t filter -A FORWARD -i eth0 -o eth1 -s $LAN \
  -m iprange --dest-range $PROXY-$WEBSERVER \
  -p $PROTOCOL --dport 53 -j ACCEPT
done
$_ -t filter -A FORWARD -i eth0 -o eth1 -s $LAN \
  -m iprange --dest-range $PROXY-$WEBSERVER \
  -p tcp --dport 22 -j ACCEPT
$_ -t filter -A FORWARD -i eth0 -o eth1 -s $LAN \
  -d $PROXY -p tcp --dport 3128 -j ACCEPT
$_ -t filter -A FORWARD -i eth0 -o eth1 -s $LAN \
  -d $WEBSERVER -p tcp --dport 80 -j ACCEPT

# Ejercicio 4.
$_ -t nat -A PREROUTING -i eth2 -d $PUBLICIP -p udp \
  --dport 53 -j SNAT --to $WEBSERVER
$_ -t filter -A FORWARD -i eth2 -o eth1 -d $WEBSERVER \
  -p udp --dport 53 -j ACCEPT
$_ -t nat -A PREROUTING -i eth2 -d $PUBLICIP -p tcp 
  -m multiport --dports 53,80,443 -j SNAT --to $WEBSERVER
$_ -t filter -A FORWARD -i eth2 -o eth1 -d $WEBSERVER \
  -m multiport -p tcp --dports 53,80,443 -j ACCEPT

# Avoid external access to the firewall.
$_ -A INPUT -P DROP
$_ -A OUTPUT -P DROP
