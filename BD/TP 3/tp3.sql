--5) a.

SELECT Persona.nombre FROM Persona, Propietario 
WHERE Propietario.codigo = Persona.codigo;


--5) b.

SELECT codigo FROM Inmueble 
WHERE precio >=  600000 AND precio <= 700000;

--5) c.

SELECT Persona.nombre FROM PrefiereZona, Persona 
WHERE Persona.codigo = PrefiereZona.codigo_cliente 
    AND PrefiereZona.nombre_zona = "Norte" 
    AND PrefiereZona.nombre_poblacion = "Santa Fe" 
    AND
    PrefiereZona.nombre_zona NOT IN 
        (SELECT PrefiereZona.nombre_zona FROM PrefiereZona 
            WHERE PrefiereZona.nombre_zona <> "Norte");
    
--5) d.

SELECT DISTINCT Persona.nombre, Persona.apellido FROM Persona, Vendedor, Cliente, PrefiereZona
WHERE Persona.codigo = Vendedor.codigo
    AND Vendedor.codigo = Cliente.vendedor
    AND Cliente.codigo = PrefiereZona.codigo_cliente
    AND PrefiereZona.nombre_zona = "Centro"
    AND PrefiereZona.nombre_poblacion = "Rosario";
    
--5) e.
SELECT DISTINCT Persona.nombre, Persona.apellido FROM Persona, Vendedor
WHERE Persona.codigo = Vendedor.codigo
    AND Vendedor.codigo IN (SELECT DISTINCT Cliente.vendedor FROM Persona, Vendedor, Cliente
                                WHERE Persona.codigo = Vendedor.codigo
                                    AND Persona.codigo = Cliente.codigo);

--5) f.
                                    
SELECT DISTINCT Persona.nombre, Persona.apellido FROM Persona, PrefiereZona as P1, PrefiereZona as P2, PrefiereZona as P3, PrefiereZona as P4
WHERE P1.codigo_cliente = Persona.codigo AND P1.codigo_cliente = P2.codigo_cliente AND P2.codigo_cliente = P3.codigo_cliente AND 
P3.codigo_cliente = P4.codigo_cliente
    AND P1.nombre_poblacion = "Rosario" AND P2.nombre_poblacion = "Rosario" AND P3.nombre_poblacion = "Rosario" AND P4.nombre_poblacion = "Rosario"
    AND P1.nombre_zona = "Norte" AND P2.nombre_zona = "Centro" AND P3.nombre_zona = "Sur" AND P4.nombre_zona = "Oeste";

--5) g.

SELECT Persona.nombre, Inmueble.codigo, Inmueble.zona, Inmueble.precio FROM Persona, Inmueble, Cliente, Limita, PrefiereZona, Visitas
    
    
    
SELECT DISTINCT Persona.nombre, Persona.Apellido, I2.codigo, I2.nombre_poblacion, I2.nombre_zona, I2.precio FROM Cliente, Inmueble, PrefiereZona, Persona, Limita, Inmueble AS I2
WHERE Inmueble.nombre_poblacion = PrefiereZona.nombre_poblacion
    AND Inmueble.nombre_zona = PrefiereZona.nombre_zona
    AND Cliente.codigo = PrefiereZona.codigo_cliente
    AND NOT EXISTS (SELECT * FROM Cliente AS C1, Visitas, PrefiereZona AS P1, Inmueble AS I1
                WHERE C1.codigo = Visitas.codigo_cliente
                    AND Visitas.codigo_inmueble = I1.codigo
                    AND P1.nombre_poblacion = I1.nombre_poblacion
                    AND P1.nombre_zona = I1.nombre_zona
                    AND Cliente.codigo = C1.codigo
                    AND Inmueble.codigo = I1.codigo)
    AND Persona.codigo = Cliente.codigo
    AND Limita.nombre_poblacion = PrefiereZona.nombre_poblacion
    AND Limita.nombre_zona = PrefiereZona.nombre_zona
    AND I2.nombre_poblacion = Limita.nombre_poblacion_2
    AND I2.nombre_zona = Limita.nombre_zona_2
ORDER BY Persona.nombre;

