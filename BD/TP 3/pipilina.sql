DROP DATABASE lcc_pachamama;
CREATE DATABASE lcc_pachamama;
USE lcc_pachamama;

CREATE TABLE autor (
  id           INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nacionalidad VARCHAR(100) NOT NULL,
  nombre       VARCHAR(100) NOT NULL,
  apellido     VARCHAR(100) NOT NULL,
  residencia   VARCHAR(100) NOT NULL
);

CREATE TABLE libro (
  isbn      INT NOT NULL PRIMARY KEY,
  titulo    VARCHAR(100) NOT NULL,
  editorial VARCHAR(100) NOT NULL,
  precio    INT NOT NULL
);

CREATE TABLE escribe (
  id         INT NOT NULL,
  isbn       INT NOT NULL,
  primaveras INT NOT NULL,

  FOREIGN KEY (id)   REFERENCES autor (id),
  FOREIGN KEY (isbn) REFERENCES libro (isbn) ON DELETE CASCADE
);

ALTER TABLE autor ADD INDEX (apellido);
ALTER TABLE libro ADD INDEX (titulo);

-- Inserciones.

INSERT INTO autor
  (nacionalidad, nombre, apellido, residencia)
VALUES
  ("Imperio Austrohúngaro", "Franz", "Kafka", "Austria");

INSERT INTO autor
  (nacionalidad, nombre, apellido, residencia)
VALUES
  ("USA", "Ernest", "Hemingway", "UK");

INSERT INTO libro
  (isbn, titulo, editorial, precio)
VALUES
  (123123, "La Metamorfosis", "Salamandra", 10);

INSERT INTO libro
  (isbn, titulo, editorial, precio)
VALUES
  (321321, "El Viejo y el Mar", "Santillana", 11);

INSERT INTO escribe
  (id, isbn, primaveras)
VALUES
  (1, 123123, 1915);

INSERT INTO escribe
  (id, isbn, primaveras)
VALUES
  (2, 321321, 1952);

-- Ejercicio 4.a

UPDATE autor SET residencia = "Buenos Aires"
WHERE autor.apellido = "Castillo"
  AND autor.nombre = "Abelardo";

-- Ejercicio 4.b

UPDATE libro SET precio = libro.precio*1.1
WHERE libro.editorial = "UNR";

-- Ejercicio 4.c

UPDATE libro SET precio = libro.precio*1.2
WHERE libro.isbn IN (
  SELECT escribe.isbn FROM autor, escribe
  WHERE autor.id = escribe.id
  AND autor.nacionalidad <> "Argentina"
) AND libro.precio <= 200;

UPDATE libro SET precio = libro.precio*1.1
WHERE libro.isbn IN (
  SELECT escribe.isbn FROM autor, escribe
  WHERE autor.id = escribe.id
  AND autor.nacionalidad <> "Argentina"
) AND libro.precio > 200;

DELETE FROM escribe WHERE escribe.primaveras = 1998;
