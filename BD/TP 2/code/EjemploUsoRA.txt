alumno@administrador-desktop:~/Descargas/TBD.tp2/code$ java -jar ra.jar inmobiliaria.properties 

RA: an interactive relational algebra interpreter
Version 2.2b by Jun Yang (junyang@cs.duke.edu)
http://www.cs.duke.edu/~junyang/ra/
Type "\help;" for help

ra> \help;
Terminate your commands or expressions by ";"

Commands:
\help: print this message
\quit: exit ra
\list: list all relations in the database
\sqlexec_{STATEMENT}: execute SQL in the database

Relational algebra expressions:
R: relation named by R
\select_{COND} EXP: selection over an expression
\project_{ATTR_LIST} EXP: projection of an expression
EXP_1 \join EXP_2: natural join between two expressions
EXP_1 \join_{COND} EXP_2: theta-join between two expressions
EXP_1 \cross EXP_2: cross-product between two expressions
EXP_1 \union EXP_2: union between two expressions
EXP_1 \diff EXP_2: difference between two expressions
EXP_1 \intersect EXP_2: intersection between two expressions
\rename_{NEW_ATTR_NAME_LIST} EXP: rename all attributes of an expression

----------------------------------------------

ra> \list;
-----
POBLACION
ZONA
INMUEBLE
PERSONA
VENDEDOR
CLIENTE
PROPIETARIO
POSEEINMUEBLE
PREFIEREZONA
VISITAS
-----
Total of 10 table(s) found.

----------------------------------------

ra> \project_{nombre_poblacion} POBLACION;
Output schema: (nombre_poblacion text)
-----
Casilda
Rosario
San Lorenzo
Santa Fe
-----
Total number of rows: 4

-----------------------------------------

