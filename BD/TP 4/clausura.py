A = "A"
B = "B"
C = "C"
D = "D"
E = "E"
F = "F"
G = "G"
H = "H"
I = "I"
J = "J"
def cierre(alfa, F):
    resultado = alfa
    cambios = True
    while cambios:
        cambios = False
        for (b, c) in F:
            if b.issubset(resultado) and not c.issubset(resultado):
                resultado = resultado.union(c)
                cambios = True
    return resultado          
                
R1 = set([A, B, C, D, E, F, G, H, I, J])
F1 = [(set([A,B]), set([C])),(set([B, D]), set([E, F])), (set([A, D]), set([G, H])), (set([A]), set([I ])), (set([H]), set([J]))]
A1 = set([B, D])

print cierre(A1, F1)
