module Lab01 where

import Data.List
import Data.Char


-- q = (\x -> (\y -> if x <= y then x else y))
-- q = (\x -> (\y -> if x <= y then x else y))::(Ord a) => a -> a -> a
-- q = (\x -> (\y -> if x <= y then x else y))::Int -> Int -> Int

-- l = \x -> (\y -> (\z -> (if (x <= y && x <= z) then x else if (y <= x && y <= z) then y else z)))

modulus::[Float] -> Float
modulus = sqrt . (sum . (map (**2)))

uniuni xs = [x | x <- xs, length (filter (\y -> y == x) xs) == 1]
divdiv n = [x | x <- [1..n], mod n x == 0, x > 0]

andThen = (\x -> (\y -> if x == True then y else False))
second  = \x -> \x -> x
twice   = \f -> \x -> f ( f x )

zzip3::[a] -> [b] -> [c] -> [(a,b,c)]
zzip3 [] [] [] = []
zzip3 (x:xs) (y:ys) (z:zs) = (x,y,z):(zip3 xs ys zs)


-- type NumBin = [Bool]
-- bin_sum::NumBin -> NumBin -> NumBin
-- 
-- -- I have to add this patterns so Haskell doesn't weep:
-- bin_sum [] [] = []
-- bin_sum xs [] = xs
-- bin_sum [] xs = xs
-- 
-- bin_sum [True]  [True]  = [False, True]
-- bin_sum [False] [True]  = [True]
-- bin_sum [True]  [False] = [True]
-- bin_sum [False] [False] = [False]
-- bin_sum (True:xs) (False:ys) = True:(bin_sum xs ys)
-- bin_sum (False:xs) (True:ys) = True:(bin_sum xs ys)
-- bin_sum (False:xs) (False:ys) = False:(bin_sum xs ys)
-- bin_sum (True:xs) (True:ys) = True:(bin_sum (bin_sum [True] xs) ys)
-- bin_sum _ _ = []
-- 
-- bprod::NumBin -> NumBin -> NumBin
-- bprod [] [] = []
-- bprod [False] [_] = [False]
-- bprod [_] [False] = [False]
-- bprod [True] xs = xs
-- bprod xs [True] = xs
-- bprod (False:xs) ys = False:(bprod xs ys)
-- bprod (True:xs)  ys = bin_sum ys (False:(bprod xs ys))

{-
1) Corregir los siguientes programas de modo que sean aceptados por GHCi.
-}

-- a)
-- not b = case b of True -> False
--                   False -> True

-- b)
kk (x:y:xs) = x : (kk (y:xs))
kk [x] = []
kk [] = error "lala"

hola::[Char]
hola = "Hello"

(***)::Int -> Int -> Int
infixr 8 ***
a *** b = a+b+b

-- c)
size []    = 0
size (_:l) = 1 + size l

-- d)
list123 = 1 : 2 : 3 : []

-- e)
[]     ++! ys = ys
(x:xs) ++! ys = x : (xs ++! ys)

-- f)
addToTail x xs = map (+x) (tail xs)

-- g)
listmin xs = head (sort xs)

-- h) (*)
smap f [] = []
smap f [x] = [f x]
smap f (x:xs) = f x : smap f xs
-- smap f (x:xs) = f x : smap (smap f) xs

{-
2. Definir las siguientes funciones y determinar su tipo:

a) five, que dado cualquier valor, devuelve 5
-}
five n = 5
{-
b) apply, que toma una función y un valor, y devuelve el resultado de
aplicar la función al valor dado
-}
apply f x = f x
{-
c) ident, la función identidad
-}
ident x = x
{-
d) first, que toma un par ordenado, y devuelve su primera componente
-}
first (x,y) = x
{-
e) derive, que aproxima la derivada de una función dada en un punto dado
-}
derive f x_0 = (f x_0 - f (x_0 + 0.001))/(x_0 - (x_0 + 0.001))
{-
f) sign, la función signo
-}
sign x | x == 0 = 0
       | x > 0  = 1
       | x < 0  = -1
{-
g) vabs, la función valor absoluto (usando sign y sin usarla)
-}
vabs x | x < 0     = sign x * x
       | otherwise = x
{-
h) pot, que toma un entero y un número, y devuelve el resultado de
elevar el segundo a la potencia dada por el primero
-}
pot n x = x^n
{-
i) xor, el operador de disyunción exclusiva
-}
xor::Bool -> Bool -> Bool
xor True True   = False
xor False False = False
xor _ _ = True
{-
j) max3, que toma tres números enteros y devuelve el máximo entre llos
-}
max3 a b c = head (sort [a, b, c])
{-
k) swap, que toma un par y devuelve el par con sus componentes invertidas
-}
swap (x, y) = (y, x)
{- 
3) Definir una función que determine si un año es bisiesto o no, de
acuerdo a la siguiente definición:

año bisiesto 1. m. El que tiene un día más que el año común, añadido al mes de febrero. Se repite
cada cuatro años, a excepción del último de cada siglo cuyo número de centenas no sea múltiplo
de cuatro. (Diccionario de la Real Academia Espaola, 22ª ed.)

¿Cuál es el tipo de la función definida?
-}
bisiesto n | ((mod n 4) == 0) && ((mod n 400) /= 0) = True
           | True = False
{-
4)

Defina un operador infijo *$ que implemente la multiplicación de un
vector por un escalar. Representaremos a los vectores mediante listas
de Haskell. Así, dada una lista ns y un número n, el valor ns *$ n
debe ser igual a la lista ns con todos sus elementos multiplicados por
n. Por ejemplo,

[ 2, 3 ] *$ 5 == [ 10 , 15 ].

El operador *$ debe definirse de manera que la siguiente
expresión sea válida:

-}

(*$)::(Num a) => [a] -> a -> [a]
[] *$ _ = []
(x:xs) *$ y = (x*y):(xs *$ y)
-- v = [1, 2, 3] *$ 2 *$ 4


{-
5) Definir las siguientes funciones usando listas por comprensión:

a) 'divisors', que dado un entero positivo 'x' devuelve la
lista de los divisores de 'x' (o la lista vacía si el entero no es positivo)
-}
divisors n = [x | x <- [1..n], mod n x == 0]
{-
b) 'matches', que dados un entero 'x' y una lista de enteros descarta
de la lista los elementos distintos a 'x'
-}
matches n ns = [x | x <- ns, x == n]
{-
c) 'cuadrupla', que dado un entero 'n', devuelve todas las cuadruplas
'(a,b,c,d)' que satisfacen a^2 + b^2 = c^2 + d^2,
donde 0 <= a, b, c, d <= 'n'
-}
cuadrupla n = [(a, b, c, d) | a <- [0..n], b <- [0..n], c <- [0..n], d <- [0..n], a^2+b^2 == c^2+d^2]
{-
(d) 'unique', que dada una lista 'xs' de enteros, devuelve la lista
'xs' sin elementos repetidos
-}
unique :: [Int] -> [Int]
unique xs = [x | (x,i) <- (zip xs [0..]), not (elem x (take i xs))]
{- 
6) El producto escalar de dos listas de enteros de igual longitud
es la suma de los productos de los elementos sucesivos (misma
posición) de ambas listas.  Definir una función 'scalarProduct' que
devuelva el producto escalar de dos listas.

Sugerencia: Usar las funciones 'zip' y 'sum'. -}
scalarProduct :: [Int] -> [Int] -> Int
scalarProduct xs ys = sum (map (\x -> fst x + snd x) (zip xs ys))

{-
7) Definir mediante recursión explícita
las siguientes funciones y escribir su tipo más general:

a) 'suma', que suma todos los elementos de una lista de números
-}
suma xs | xs == [] = 0
        | True = (head xs) + suma (tail xs)
{-
b) 'alguno', que devuelve True si algún elemento de una
lista de valores booleanos es True, y False en caso
contrario
-}
alguno b | b == [] = False
         | True    = (head b) || alguno (tail b)
{-
c) 'todos', que devuelve True si todos los elementos de
una lista de valores booleanos son True, y False en caso
contrario
-}
todos b | b == [] = True
        | True    = (head b) && todos (tail b)
{-
d) 'codes', que dada una lista de caracteres, devuelve la
lista de sus ordinales
-}
codes c | c == [] = []
        | True    = (ord (head c)):(codes (tail c))
{-
e) 'restos', que calcula la lista de los restos de la
división de los elementos de una lista de números dada por otro
número dado
-}
restos xs n | xs == [] = []
            | True     = (mod (head xs) n):(restos (tail xs) n)
{-
f) 'cuadrados', que dada una lista de números, devuelva la
lista de sus cuadrados
-}
cuadrados xs | xs == [] = []
             | True     = ((head xs)*(head xs)):(cuadrados (tail xs))
{-
g) 'longitudes', que dada una lista de listas, devuelve la
lista de sus longitudes
-}
longitudes xss | xss == [] = []
               | True      = ((head xss)*(head xss)):(cuadrados (tail xss))
{-
h) 'orden', que dada una lista de pares de números, devuelve
la lista de aquellos pares en los que la primera componente es
menor que el triple de la segunda
-}
orden xs | xs == []  = []
         | True      = if (fst (head xs)) < (snd (head xs))
                       then (head xs):(orden (tail xs))
                       else orden (tail xs)
{-
i) 'pares', que dada una lista de enteros, devuelve la lista
de los elementos pares
-}
pares xs | xs == []  = []
         | True      = if mod (head xs) 2 == 0 then (head xs):(pares (tail xs)) else pares (tail xs)
{-
j) 'letras', que dada una lista de caracteres, devuelve la
lista de aquellos que son letras (minúsculas o mayúsculas)
-}

{-
k) 'masDe', que dada una lista de listas 'xss' y un
número 'n', devuelve la lista de aquellas listas de 'xss'
con longitud mayor que 'n' -}
