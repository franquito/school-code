module ListSeq where

(|||) :: a -> b -> (a, b)
a ||| b = (a,b)

data Tree v a = Empty| Leaf v a | Branch v (Tree v a) (Tree v a) deriving (Show)

-- Condicion 1: lengthT (Branch v l r) >= 1 + max(lengthT l, lengthT r)
-- Condicion 2: Subarbol derecho == lengthT/2 + 1 (si es impar) , izquierdo = lengthT/2
tree1 :: Tree Int Int
tree1 = Branch 6 (Branch 3 (Leaf 1 1) (Branch 2 (Leaf 1 2) (Leaf 1 3))) (Branch 3 (Leaf 1 4) (Branch 2 (Leaf 1 5) (Leaf 1 6)))

tree2 :: Tree Int Int
tree2 = Branch 3 (Leaf 1 1) (Branch 2 (Leaf 1 2) (Leaf 1 3))

tree3 :: Tree Int Int
tree3 = Branch 5 (Branch 2 (Leaf 1 1) (Leaf 1 2)) (Branch 3 (Leaf 1 3) (Branch 2 (Leaf 1 4) (Leaf 1 5)))

tree4 :: Tree Int Int
tree4 = Branch 4 (Branch 2 (Leaf 1 6) (Leaf 1 7)) (Branch 2 (Leaf 1 8) (Leaf 1 9)) 

emptyT :: Tree v a
emptyT = Empty

singletonT :: Num v => v -> a -> Tree v a
singletonT v a = Leaf v a

lengthT :: Num v => Tree v a -> v
lengthT Empty          = 0
lengthT (Leaf v a)     = v
lengthT (Branch v l r) = v

isPair :: Int -> Bool
isPair a = if (mod a 2) == 0 then True else False

nthT :: Tree Int a -> Int -> a
nthT (Leaf v a) _ = a
nthT (Branch 2 l r) n | n == 1 = nthT l n 
                      | n == 2 = nthT r n
nthT (Branch v l r) n = if n <= (div v 2) then nthT l n else nthT r (n - lengthT l)

--tabulateT :: (Int -> a) -> Int -> Tree Int a
--tabulateT f 0 = Leaf 1 (f 0)
--tabulateT f 1 = Leaf 1 (f 1)
--tabulateT f n = let (l, r) = tabulateT f (div (n-1) 2) ||| tabulateT f (n - (lengthT l))
--              in Branch n l r 

mapT :: (a -> b) -> Tree v a -> Tree v b
mapT f Empty = Empty
mapT f (Leaf v a)     = Leaf v (f a)
mapT f (Branch v l r) = let (l1, r1) = mapT f l ||| mapT f r
                        in Branch v l1 r1

--filterT :: (a -> Bool) -> Tree Int a -> Tree Int a
--filterT f Empty      = Empty
--filterT f (Leaf v a) = if (f a) then Leaf v a else Empty
--filterT f (Branch v l r) = let (l1, r1) = filterT f l ||| filterT f r
--                         in Branch (lengthT l1 + lengthT r1) l1 r1

lastT :: Tree Int a -> a
lastT (Leaf 1 a) = a
lastT (Branch v l r) = lastT r

firstT :: Tree Int a -> a
firstT (Leaf 1 a) = a
firstT (Branch v l r) = firstT l

hooman :: Tree Int a -> Tree Int a
hooman (Branch 2 l r) = Leaf 1 (lastT l)
hooman (Branch v l (Leaf 1 a)) = l
hooman (Branch v l r) = Branch (lengthT l + lengthT r1) l r1
                        where r1 = hooman r

--Christopher
walken :: Tree Int a -> Tree Int a
walken (Leaf 1 a) = Leaf 1 a
walken (Branch 2 l r) = Leaf 1 (firstT r)
walken (Branch v (Leaf 1 a) r) = r
walken (Branch v l r) = Branch (lengthT l1 + lengthT r) l1 r
                        where l1 = walken l

--Revisar balance (Emprolijar)  

balance :: Tree Int a -> Int -> Tree Int a
balance t 0 = t
balance (Branch v l r) n | ((lengthT l + 1 == lengthT r) || (lengthT l == lengthT r)) = Branch v l r 
                         | lengthT l > lengthT r = let (l1, r1) = hooman l ||| appendT r (Leaf 1 (lastT l))
                                                   in balance (Branch v l1 r1) (n-1)
                         | lengthT l < lengthT r = let (l1, r1) = appendT (Leaf 1 (firstT r)) l ||| (walken r)
                                                   in balance (Branch v l1 r1) (n-1)

appendT :: Tree Int a -> Tree Int a -> Tree Int a
appendT s Empty = s
appendT s l@(Leaf 1 a) = if (lengthT s > 2) then balance (Branch (lengthT s + 1) l s) (lengthT s - 1)
                         else Branch (lengthT s + 1) l s
appendT s t | (lt + 1 == ls) || (lt == ls)  = (Branch (ls + lt) t s)
            | (lt s > lt)                   = balance (Branch (ls + lt) t s) (abs (ls - lt))
            | otherwise                     = balance (Branch (ls + lt) t s) (abs (ls - lt - 1))
               where  lt = lengthT t 
                      ls = lengthT s
       
--appendT s t = if ((lengthT t + 1 == lengthT s) || (lengthT t == lengthT s)) then (Branch (lengthT s + lengthT t) t s)
--            else if (lengthT s > lengthT t) then balance (Branch (lengthT s + lengthT t) t s) (abs (lengthT s - lengthT t))
--                 else balance (Branch (lengthT s + lengthT t) t s) (abs (lengthT s - lengthT t - 1))
