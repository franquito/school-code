module ListSeq2 where

import Data.List as Ls
import Par


data TreeView a t = EMPTY | ELT a | NODE t t deriving (Show)
data ListView a t = NIL | CONS a t deriving (Show)

class Seq s where
   emptyS     :: s a
   singletonS :: a -> s a
   lengthS    :: s a -> Int 
   nthS       :: s a -> Int -> a 
   tabulateS  :: (Int -> a) -> Int -> s a
   mapS       :: (a -> b) -> s a -> s b 
   filterS    :: (a -> Bool) -> s a -> s a
   appendS    :: s a -> s a -> s a
   takeS      :: s a -> Int -> s a
   dropS      :: s a -> Int -> s a
   showtS     :: s a -> TreeView a (s a)
   showlS     :: s a -> ListView a (s a)
   joinS      :: s (s a) -> s a
   reduceS    :: (a -> a -> a) -> a -> s a -> a
   scanS      :: (a -> a -> a) -> a -> s a -> (s a, a)
   fromList   :: [a] -> s a

--(|||) :: a -> b -> (a, b)
--a ||| b = (a,b)


singletonL :: a -> [a]
singletonL a = [a]

lengthL :: [a] -> Int 
lengthL xs = Ls.length xs

nthL        :: [a] -> Int -> a 
nthL (x:xs) 0 = x
nthL (x:xs) n = nthL xs (n-1)

tabulateL     :: (Int -> a) -> Int -> [a]
tabulateL f n = [f a | a <- [0..n-1]]

mapL      :: (a -> b) -> [a] -> [b]
mapL f []        = []
mapL f [x]       = [f x]
mapL f (x:y:xs)  = let (x1, y1) = f x ||| f y
                   in x1:(y1:(mapL f xs))

tupL :: (a -> Bool) -> a -> (Bool, a)
tupL f a = (f a, a)

filterL :: (a -> Bool) -> [a] -> [a]
filterL f []       = []
filterL f [a]      = if f a then [a] else []
filterL f (x:y:xs) = let (x1, y1) = tupL f x ||| tupL f y in
                     if fst(x1) && fst(y1)      then x:(y:(filterL f xs)) else 
                     if fst(x1) && not(fst(y1)) then x:(filterL f xs)     else
                     if not(fst(x1)) && fst(y1) then y:(filterL f xs)     
                     else filterL f xs

appendL :: [a] -> [a] -> [a]
appendL = (++)

takeL :: [a] -> Int -> [a]
takeL xs n = take n xs

dropL :: [a] -> Int -> [a]
dropL xs n = drop n xs

showtL :: [a] -> TreeView a ([a])
showtL []     = EMPTY
showtL [a]    = ELT a
showtL (x:xs) = NODE (takeL (x:xs) l) (dropL (x:xs) l)
               where l = div (lengthL xs) 2

showlL :: [a] -> ListView a ([a])
showlL []     = NIL
showlL [a]    = CONS a []
showlL (x:xs) = CONS x xs

joinL :: [[a]] -> [a]
joinL [[]]     = []
joinL [xs]     = xs
joinL (xs:xss) = xs ++ (joinL xss)

paresL :: (a -> a -> a) -> a -> [a] -> [a]
paresL f e []  = []
paresL f e [x] = [x]
paresL f e (x:(y:xs)) = (f x y):(paresL f e xs)

reduceL :: (a -> a -> a) -> a -> [a] -> a
reduceL f e []  = e
reduceL f e [x] = x
reduceL f e xs  = reduceL f e (paresL f e xs)

contraer :: (a -> a -> a) -> a -> [a] -> [a]
contraer f e []       = []
contraer f e [x]      = [x]
contraer f e (x:y:xs) = let (x1, xs1) = f x y||| contraer f e xs
                        in (x1 : xs1)

a1 = contraer (+) 0 [1,2,3,4,5]
a3 = [1,2,3,4,5]
a4 = [3,7]
a2 = [2,3,4,5,6,7]


contraerL :: (a -> a -> a) -> [a] -> [a]
contraerL f []       = []
contraerL f [x]      = [x]
contraerL f [x,y]    = [f x y]
contraerL f (x:y:xs) = let(x', xs') = (f x y) ||| (contraerL f xs) in (x':xs')

expandirL :: (a -> a -> a) -> [a] -> [a] -> Int -> [a]
expandirL f [] _ i          = []
expandirL f (x:xs) [] i     = []
expandirL f (x:xs) (y:ys) i | even i = x : (expandirL f (x:xs) ys (i+1))
                            | otherwise  = let(k, ks) = (f x y) ||| (expandirL f xs ys (i+1)) in (k:ks)

scanL :: (a -> a -> a) -> a -> [a] -> ([a], a)
scanL f e []    = ([e], e)
scanL f e [x]   = ([e], f e x)
scanL f e [x,y] = ([e, f e x], f x y)
scanL f e xs    = (xs1, snd w)
                  where    w   = scanL f e (contraerL f xs)
                           xs1 = expandirL f (fst w) (e:xs) 0

fromListL :: [a] -> [a]
fromListL xs = xs

instance Seq [] where
   emptyS     = []
   singletonS = singletonL
   lengthS    = lengthL
   nthS       = nthL
   tabulateS  = tabulateL
   mapS       = mapL
   filterS    = filterL
   appendS    = appendL
   takeS      = takeL
   dropS      = dropL
   showtS     = showtL
   showlS     = showlL
   joinS      = joinL
   reduceS    = reduceL
   scanS      = scanL
   fromList   = fromListL

