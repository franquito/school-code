module ArrSeq where

import qualified Arr as A
import qualified Data.Vector as V

data TreeView a t = EMPTY | ELT a | NODE t t deriving (Show)
data ListView a t = NIL | CONS a t deriving (Show)

(|||) :: a -> b -> (a, b)
a ||| b = (a,b)

class Seq s where
   emptyS     :: s a
   singletonS :: a -> s a
   lengthS    :: s a -> Int 
   nthS       :: s a -> Int -> a 
   tabulateS  :: (Int -> a) -> Int -> s a
   mapS       :: (a -> b) -> s a -> s b 
   filterS    :: (a -> Bool) -> s a -> s a
   appendS    :: s a -> s a -> s a
   takeS      :: s a -> Int -> s a
   dropS      :: s a -> Int -> s a
   showtS     :: s a -> TreeView a (s a)
   showlS     :: s a -> ListView a (s a)
   joinS      :: s (s a) -> s a
   reduceS    :: (a -> a -> a) -> a -> s a -> a
   scanS      :: (a -> a -> a) -> a -> s a -> (s a, a)
   fromList   :: [a] -> s a

emptyA :: A.Arr a
emptyA = A.empty

singletonA :: a -> A.Arr a
singletonA x = A.fromList [x]

lengthA :: A.Arr a -> Int
lengthA = A.length

nthA :: A.Arr a -> Int -> a
nthA = (A.!)

tabulateA :: (Int -> a) -> Int -> A.Arr a
tabulateA = A.tabulate

mapA :: (a -> b) -> A.Arr a -> A.Arr b
mapA f v = tabulateA (\x -> f (nthA v x)) (lengthA v)

filterA :: (a -> Bool) -> A.Arr a -> A.Arr a
filterA f v = joinA (tabulateA (\x -> if f (nthA v x) then (singletonA (nthA v x)) else emptyA) (lengthA v))

--filterA :: (a -> Bool) -> A.Arr a -> A.Arr a
--filterA f v | (lengthA v == 1) = if (f (nthA v 0)) then v else (emptyA)
--            | otherwise = let (l1, r1) = filterA f (takeA v n) ||| filterA f (dropA v n)
--                          in appendA l1 r1
--                          where n = div (lengthA v) 2

a1 = tabulateA (\x -> x + 1) 5
a2 = tabulateA (\x -> x + 6) 5

appendA :: A.Arr a -> A.Arr a -> A.Arr a
appendA v1 v2 = tabulateA (\x -> if x < n then (nthA v1 x) else (nthA v2 (x-n))) (n + m)
                where n = lengthA v1
                      m = lengthA v2

takeA :: A.Arr a -> Int -> A.Arr a
takeA v n = A.subArray 0 n v

dropA :: A.Arr a -> Int -> A.Arr a
dropA v n = A.subArray n (lengthA v - n) v

joinA :: A.Arr (A.Arr a) -> A.Arr a
joinA = A.flatten

reduceA :: (a -> a -> a) -> a -> A.Arr a -> a
reduceA f e v | (lengthA v == 1) = f e (nthA v 0)
              | otherwise = let (l1, r1) = reduceA f e (takeA v n) ||| reduceA f e (dropA v n)
                            in f l1 r1
                            where n = div (lengthA v) 2

showlA :: A.Arr a -> ListView a (A.Arr a)
showlA s   | n == 0 = NIL
           | n > 0  = CONS (nthA s 0) (dropA s 1)
                where n = lengthA s

showtA :: A.Arr a -> TreeView a (A.Arr a)
showtA  s   | n == 0  = EMPTY 
            | n == 1  = ELT (nthA s 0)
            | n > 1   = NODE l r
                where n = lengthA s
                      l = takeA s (div n 2)
                      r = dropA s (div n 2)


contraer :: (a -> a -> a) -> a -> A.Arr a -> A.Arr a
contraer f e v = tabulateA g m
                where n = lengthA v
                      m = div (lengthA v + 1) 2
                      g x | x == 0    = e
                          | x > 0 && x < m = f (nthA v (2*x-2)) (nthA v (2*x-1))
                          | x == m    = f e (nthA v (2*x-1))

contraer' :: (a -> a -> a) -> a -> A.Arr a -> A.Arr a
contraer' f e v = tabulateA g n
                 where n = lengthA v
                       g x | x == 0    = e
                           | otherwise = f e (reduceA f e (A.subArray 1 x v))


contraer'' op s = tabulate g m
                where n   = lengthS' s
                      m   = div (n + 1) 2
                      g i | i==m = if (odd n) then (s!(n-1)) else elem
                          | otherwise = elem
                            where elem = op (s!(2*i)) (s!(2*i+1))

expandir :: (a -> a -> a) -> a -> A.Arr a -> A.Arr a -> A.Arr a
expandir f e vc vo = tabulateA g n
                   where n = lengthA vo
                         g x | even x    = nthA vc (div x 2)
                             | otherwise = f (nthA vc (div x 2)) (nthA vo (x-1))

a4 = fromListA [3,2,8,4,7,9,11,0,1]
a5 = fromListA [3,2,8,4,7,9,11,0,1,1]
a6 = fromListA [3,2,1]
a7 = fromListA [4,3,2,1]
a8 = fromListA [5,4,3,2,1]
a9 = fromListA [6,5,4,3,2,1]
a10 = fromListA [9,1,2,4,5,1,4,7,1,0,3,1,1]

scanA :: (a -> a -> a) -> a -> A.Arr a -> (A.Arr a, a)
scanA f e v | n == 0    = (emptyA, e)
            | n == 1    = (singletonA e, f e (nthA v 0))
            | otherwise = ve ||| f (nthA v (n-1)) (nthA ve (n-1))
            where n = lengthA v
                  s = contraer f e v
                  ve = expandir f e (contraer' f e s) v

scanA' :: (a -> a -> a) -> a -> A.Arr a -> (A.Arr a, a)
scanA' f e v | n == 0    = (emptyA, e)
             | n == 1    = (singletonA e, f e (nthA v 0))
             | otherwise = ve ||| snd s
             where n = lengthA v
                   s = scanA' f e (contraer'' f e v)
                   ve = expandir f e (fst s) v

fromListA :: [a] -> A.Arr a
fromListA = A.fromList