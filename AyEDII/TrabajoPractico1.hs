module TP1 where

data BTree32 k a = Nil | Node 
                            (BTree32 k a)   --subarbol izquierdo
                            Int             --tamaÃ±o del arbol
                            (k, a)          --elemento del nodo
                            (BTree32 k a)   --subarbol derecho 
                            deriving (Show) 

size :: BTree32 k a -> Int
size Nil            = 0
size (Node _ n _ _) = n

btlookup :: Ord k => k -> BTree32 k a -> Maybe a
btlookup _ Nil                     = Nothing
btlookup k (Node a1 _ (k2, a2) a3) = if k == k2 then (Just a2) else
                                     if k <= k2 then (btlookup k a1) else (btlookup k a3)

singleL :: BTree32 k a -> BTree32 k a
singleL Nil                       = Nil
singleL l@(Node Nil n (k, a) Nil) = l
singleL l@(Node a n (k, b) Nil)   = l
singleL (Node Nil n (k,b) (Node l m (kb, ab) r))	  = Node (Node Nil m (k,b) l) n (kb, ab) r
singleL (Node tl@(Node tl1 n1 (k1, a1) tr1) n (k, a) (Node tl2 n2 (k2, a2) tr2)) = Node (Node tl (n1 + (size tl2) + 1) (k, a) tl2) n (k2, a2) (tr2)
                                                                                    
doubleL :: BTree32 k a -> BTree32 k a
doubleL Nil                       = Nil
doubleL l@(Node Nil n (k, a) Nil) = l
doubleL l@(Node a n (k,b) Nil)    = l
doubleL l@(Node Nil n (k, b) (Node (Node lc nm (kc, ac) rc) m (kb, ab) r)) = Node (Node Nil 2 (k, b) lc) n (kc, ac) (Node rc ((size rc) + m) (kb, ab) r)
doubleL (Node tl@(Node tl1 n1 (k1, a1) tr1) --subarbol izq
        n -- tamaño arbol
        (k, a) --nodo-elemento
        (Node (Node tl3 n3 (k3, a3) tr3) n2 (k2, a2) tr2)) = --subarbol izq
        Node (Node tl (n1 + (size tl3) + 1) (k, a) tl3) n (k3, a3) (Node tr3 ((size tr2) + (size tr3) + 1) (k2, a2) tr2)

singleR :: BTree32 k a -> BTree32 k a
singleR Nil                       = Nil
singleR r@(Node Nil n (k, a) Nil) = r
singleR r@(Node Nil n (k, b) a)   = r
singleR (Node (Node l m (kb, ab) r) n (ka, ba) Nil) = Node l n (kb, ab) (Node r ((size r) + 1) (ka, ba) Nil)
singleR (Node tb@(Node tlb nb (kb, ab) trb) n (ka, aa) tc@(Node tlc nc (kc, ac) trc)) = Node tlb n (kb, ab) (Node trb ((size trb) + (size tc) + 1) (ka, aa) tc)

doubleR :: BTree32 k a -> BTree32 k a
doubleR Nil                       = Nil
doubleR r@(Node Nil n (k, a) Nil) = r
doubleR r@(Node Nil n (k,b) a)    = r
doubleR (Node (Node r m (kb, ab) (Node lc nm (kc, ac) rc)) n (k, b) Nil) = Node (Node r ((size lc) + (size r) + 1) (kb, ab) lc) n (kc, ac) (Node rc 2 (k, b) Nil)
doubleR (Node (Node tl2 n2 (k2, a2) (Node tl3 n3 (k3, a3) tr3)) --subarbol izq
        n -- tamaño arbol
        (k, a) --nodo-elemento
        tl@(Node tl1 n1 (k1, a1) tr1)) =
        Node (Node tl2 ((size tl2) + (size tl3) + 1) (k2, a2) tl3) n (k3, a3) (Node tr3 ((size tr3) + (size tr1) + 1) (k, a) tr1)

size_izq :: Ord k => BTree32 k a -> Int                                 
size_izq Nil            = 0
size_izq (Node l n _ _) = size l

size_der :: Ord k => BTree32 k a -> Int                                 
size_der Nil            = 0
size_der (Node _ n _ r) = size r

balance :: Ord k => BTree32 k a -> (k, a) -> BTree32 k a -> BTree32 k a
balance Nil x@(k, a) Nil = Node Nil 1 x Nil
balance Nil x@(kx, ax) r@(Node Nil 1 v@(kv, av) Nil) = if kx < kv then (Node Nil 2 x r) else (Node Nil 2 v (Node Nil 1 x Nil))
balance l@(Node Nil 1 v@(kv, av) Nil) x@(kx, ax) Nil = if kx >= kv then (Node l 2 x Nil) else (Node (Node Nil 1 x Nil) 2 v Nil)
balance l x@(k, a) r | balprop                                       = Node l (n + m + 1) x r
	                 | (m > 3*n) && ((size_izq r) < 2*(size_der r))  = singleL (Node l (n + m + 1) x r)
	                 | (m > 3*n) && ((size_izq r) >= 2*(size_der r)) = doubleL (Node l (n + m + 1) x r)
	                 | (n > 3*m) && ((size_izq l) < 2*(size_der l))  = doubleR (Node l (n + m + 1) x r)
	                 | (n > 3*m) && ((size_izq l) >= 2*(size_der l)) = singleR (Node l (n + m + 1) x r)
	            	     where 
	                      n = size l
	                      m = size r
	                      balprop = (((n <= 3*m) && (m <= 3*n)) || ((n + m) <= 1)) --propiedad de balanceo

actBT :: BTree32 k a -> BTree32 k a
actBT Nil                                           = Nil
actBT (Node Nil _ (k,a) Nil)                        = (Node Nil 1 (k,a) Nil)
actBT (Node l@(Node Nil 1 (k1,a1) Nil) _ (k,a) Nil) = (Node l 2 (k,a) Nil)
actBT (Node Nil _ (k,a) r@(Node Nil 1 (k1,a1) Nil)) = (Node Nil 2 (k,a) r)
actBT (Node l n (k,a) r)                            = Node (actBT l) ((size (actBT l)) + (size (actBT r)) + 1) (k,a) (actBT r)

btree3 = Node (Node (Node Nil 1 (8,8) Nil) 2 (9,9) Nil) 3 (10,10) (Node Nil 1 (11,11) Nil)
btree4 = Node (Node (Node Nil 1 (1,1) Nil) 5 (6,6) (Node (Node Nil 1 (7,7) Nil) 3 (8,8) (Node Nil 1 (9,9) Nil))) 7 (10,10) (Node Nil 1 (15,15) Nil)


insert :: Ord k => (k, a) -> BTree32 k a -> BTree32 k a
insert x Nil = Node Nil 1 x Nil
insert x@(kx, ax) (Node l n v@(k, a) r) | kx < k  = actBT(balance (actBT(insert x l)) v r)
										| kx >= k = actBT(balance l v (actBT(insert x r)))

delRoot :: Ord k => BTree32 k a -> BTree32 k a
delRoot Nil                = Nil
delRoot (Node Nil _ _ Nil) = Nil
delRoot (Node l _ _ Nil)   = l
delRoot (Node Nil _ _ r)   = r
delRoot (Node l n x r@(Node _ _ (kr, ar) _)) = balance l (kr, ar) (delRoot r)

delete :: Ord k => k -> BTree32 k a -> BTree32 k a
delete k Nil = Nil
delete dk tree@(Node l w (k, a) r) | dk > k  = (Node l w (k,a) (delete dk r))
                                   | dk < k  = (Node (delete dk l) w (k,a) r)
                                   | dk == k = delRoot tree
                                   
class Diccionario t where
    vacia    :: Ord k => t k v
    insertar :: Ord k => (k, v) -> t k v -> t k v
    eliminar :: Ord k => k -> t k v -> t k v
    buscar   :: Ord k => k -> t k v -> Maybe v

instance Diccionario BTree32 where
    vacia    = Nil
    insertar = insert
    eliminar = delete
    buscar   = btlookup

{-							
let a = Nil
let a1 = insert (10,10) a
let a2 = insert (11,11) a1
let a3 = insert (15,15) a2
let a4 = insert (6,6) a3
let a5 = insert (8,8) a4
let a6 = insert (5,5) a5
let a7 = insert (12,12) a6
let a8 = insert (14,14) a7
let a9 = insert (20,20) a8
let a10 = insert (3,3) a9
let a11 = insert (7,7) a10
let a12 = insert (13,13) a11

btree1 = Node (Node (Node Nil 1 (0,0) Nil) 2 (1,2) Nil) 7 (2,1) (Node (Node (Node Nil 1 (2,2) Nil) 2 (3,3) Nil) 5 (4,4) (Node (Node Nil 1 (5,5) Nil) 2 (6,6) Nil))
btree2 = Node (Node Nil 1 (2,1) Nil) 3 (3,3) (Node Nil 1 (4,4) Nil)

Node (Node (Node Nil 1 (3,3) Nil) 4 (5,5) (Node Nil 2 (6,6) (Node Nil 1 (7,7) Nil))) 12 (8,8) (Node (Node (Node Nil 1 (10,10) Nil) 4 (11,11) (Node Nil 2 (12,12) (Node Nil 1 (13,13) Nil))) 7 (14,14) (Node Nil 2 (15,15) (Node Nil 1 (20,20) Nil)))
-}
