import qualified Arr as A
import Seq
import Par

instance Seq A.Arr where
  emptyS = A.empty

  singletonS a = A.fromList [a]

  lengthS = A.length

  nthS s i = s A.! i

  tabulateS = A.tabulate

  mapS f s = A.tabulate (\i -> f (nthS s i))
                              (A.length s)

  filterS p s = A.flatten (mapS f s)
    where f i = if p i then singletonS i else emptyS

  appendS s1 s2  = tabulateS f (len1 + len2)
    where len1 = lengthS s1
          len2 = lengthS s2
          f  i = if i < len1 then nthS s1 i else nthS s2 (i-len1)

  takeS s k = if k > n then s else A.subArray 0 k s
    where n = lengthS s

  dropS s k = if k > n then emptyS
              else A.subArray k (n - k) s
    where n = lengthS s

  showtS s | len == 0 = EMPTY
           | len == 1 = ELT (nthS s 0)
           | len  > 1 = NODE (takeS s l) (dropS s r)
    where len = lengthS s
          l   = div len 2
          r   = l + mod len 2

  showlS s | n == 0 = NIL
           | n > 0  = CONS (nthS s 0) (dropS s 1)
    where n = lengthS s

  joinS = A.flatten

  -- contraer está definido después.
  reduceS op e s | len == 0 = e
                 | len == 1 = op e (nthS s 0)
                 |   True   = reduceS op e y
    where len = lengthS s
          y   = contraer op s

  fromList = A.fromList

  -- scanL está definido después.
  scanS = scanL

--
-- Definiciones extra --
--

-- scanL usa contraer y expandir, éstas dos funciones
-- usan tabulate, dandole alto paralelismo.
scanL :: (a -> a -> a) -> a -> A.Arr a -> (A.Arr a, a)
scanL op e xs | n == 0 = (emptyS, e)
              | n == 1 = (singletonS e, op e (nthS xs 0))
              |  True  = let shrinked = scanS op e (contraer op xs) 
                             ys = expandir op e xs (fst shrinked)
                         in ys ||| op (nthS ys (n-1)) (nthS xs (n-1))
              where n  = lengthS xs

contraer :: (a -> a -> a) -> A.Arr a -> A.Arr a
contraer op xs = tabulateS f m
  where n   = lengthS xs
        m   = div n 2 + mod n 2
        f i = if i == m - 1 && odd n then
                nthS xs i
              else
                op (nthS xs (i*2)) (nthS xs (i*2+1))

expandir :: (a -> a -> a) -> a -> A.Arr a -> A.Arr a -> A.Arr a
expandir op e xs ys = tabulateS f n
  where n   = lengthS xs
        f i = if even i then nthS ys (div i 2)
              else op (nthS ys (div i 2)) (nthS xs (i-1))
