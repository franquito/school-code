import Seq
import Par

instance Seq [] where
  emptyS = []

  singletonS a  = [a]

  -- TODO: Can't
  lengthS = length

  nthS (x:xs) 0 = x
  nthS (x:xs) n = nthS xs (n - 1)

  tabulateS f n = tab f 0
    where tab f m | (m - 1) == n = []
                  |     True     = (f m):(tab f (m + 1))

  mapS f []     = []
  mapS f (x:xs) = (f x):(mapS f xs)

  filterS p []     = []
  filterS p (x:xs) = if p x then x:(filterS p xs)
                     else filterS p xs

  appendS [] ys     = ys
  appendS (x:xs) ys = x:(appendS xs ys)

  takeS []     _ = []
  takeS xs     0 = []
  takeS (x:xs) n = x:(takeS xs (n-1))

  dropS []     n = []
  dropS (x:xs) n | n == 0 = xs
                 |  True  = dropS xs (n-1)

  showtS []       = EMPTY
  showtS [x]      = ELT x
  showtS l@(x:xs) = NODE l1 l2
    where len      = lengthS l
          half     = div len 2
          (l1, l2) = (takeS l half ||| dropS l half)

  showlS []     = NIL
  showlS (x:xs) = CONS x xs

  reduceS op e []  = e
  reduceS op e [x] = op e x
  reduceS op e xs  = op e (redL op xs)

  joinS xss = reduceS appendS [] xss

  scanS op e []  = (emptyS, e)
  scanS op e [x] = (singletonS e, op e x)
  scanS op e xs  = (ys, op (nthS ys (n-1)) (nthS xs (n-1)))
    where n  = lengthS xs
          ys = expandirL op xs (fst (scanS op e (contraerL op xs))) n 0

  fromList a = a

--
-- Definiciones extra --
--

redL op s | len == 1 = nthS s 0
          |   True   = let two = fromIntegral 2
                           x   = fromIntegral (len - 1)
                           n   = 2 ^ floor (logBase two x)
                           (s1, s2) = (takeS s n ||| dropS s n)
                       in op (redL op s1) (redL op s2)
  where len = lengthS s

contraerL op (x:y:xs) = let (n, ns) = op x y ||| contraerL op xs
                        in n:ns
contraerL op xs       = xs

expandirL f xs ys n i | n == i = []
                      | even i = (nthS ys 0):(expandirL f xs ys n (i+1))
                      | odd i  = let (n, ns) = f (nthS ys 0) (nthS xs 0) ||| expandirL f (dropS xs 2) (dropS ys 1) n (i+1) 
                                 in n:ns
