-- 1)
type Color = (Float, Float, Float)
mezclar::Color -> Color -> Color
mezclar (r1, g1, b1) (r2, g2, b2) = ((r1+r2)/2, (g1+g2)/2, (b1+b2)/2)

-- 2)
type Linea = ([Char], Int)

vacia = ([], 0)

moverIzq::Linea -> Linea
moverIzq (xs, 0) = (xs, 0)
moverIzq (xs, n) = (xs, n-1)

moverDer::Linea -> Linea
moverDer (xs, n) | (length xs) == n = (xs, n)
                 | True             = (xs, n+1)

moverIni::Linea -> Linea
moverIni (xs, n) = (xs, 0)

moverFin::Linea -> Linea
moverFin (xs, n) = (xs, length xs)

insertar::Char -> Linea -> Linea
insertar c (xs, n) | n == 0 = (c:xs, n+1)
                   | n > 0  = ((head xs):(fst (insertar c (tail xs, n-1))), n+1)

borrar::Linea -> Linea
borrar (xs, 0) = (xs, 0)
borrar (xs, 1) = (tail xs, 0)
borrar (xs, n) = ((head xs):(fst (borrar (tail xs, n-1))), n-1)

-- 3)
data CList a = EmptyCL | CUnit a | Consnoc a (CList a) a deriving Show

isEmptyCL EmptyCL = True
isEmptyCL _       = False

isCUnit (CUnit a) = True
isCUnit _         = False

tailCL (CUnit a) = EmptyCL
tailCL (Consnoc _ a _) = a

headCL (CUnit a) = (CUnit a)
headCL (Consnoc a _ b) = (CUnit a)

reverseCL EmptyCL          = EmptyCL
reverseCL (CUnit a)        = (CUnit a)
reverseCL (Consnoc a cl b) = (Consnoc b (reverseCL cl) a)

-- concatCL::(CList (CList a)) -> (CList a)
-- concatCL (CList EmptyCL)   = CList EmptyCL
-- concatCL (CList (CUnit a)) = CList (CUnit a)
-- concatCL (CList (Consnoc a cl b)) = 

-- 4)
data Aexp = Num Int | Prod Aexp Aexp | Div Aexp Aexp

eval::Aexp -> Int
eval (Num a) = a
eval (Prod a b) = (eval a)*(eval b)
eval (Div a b)  = div (eval a) (eval b)

seval::Aexp -> Maybe Int
seval (Num a) = (Just a)
seval (Div a (Num b)) = if (b == 0 || (seval a) == Nothing) then Nothing else (Just (div (eval a) b))
seval (Div a b) = if ((seval b) == Nothing || (seval a) == Nothing) then Nothing else (Just (div (eval a) (eval b)))
seval (Prod a b) | (seval b) == Nothing = Nothing
                 | (seval a) == Nothing = Nothing
                 | True                 = (Just ((eval a)*(eval b)))

-- 5)

-- 6)
data GenTree a = EmptyG | NodeG a [GenTree a] deriving Show
data BinTree a = EmptyB | NodeB (BinTree a) a (BinTree a) deriving Show

t = NodeG "A" [ NodeG "B" [ NodeG "G" [], NodeG "H" [] ], NodeG "C" [] ]

g2bt :: GenTree a -> BinTree a
g2bt t = g2bt' t []

g2bt' :: GenTree a -> [GenTree a] -> BinTree a
g2bt' EmptyG           []     = EmptyB
g2bt' (NodeG a [])     []     = NodeB EmptyB a EmptyB
g2bt' (NodeG a [])     (h:hs) = NodeB EmptyB a (g2bt' h hs)
g2bt' (NodeG a (x:xs)) []     = NodeB (g2bt' x xs) a EmptyB
g2bt' (NodeG a (x:xs)) (h:hs) = NodeB (g2bt' x xs) a (g2bt' h hs)

-- 7)
data Bin a = Hoja | Nodo (Bin a) a (Bin a)

maxi::(Ord a) => Bin a -> a
maxi (Nodo Hoja a Hoja) = a
maxi (Nodo tree a Hoja) = max a (maxi tree)
maxi (Nodo Hoja a tree) = max a (maxi tree)
maxi (Nodo tree1 a tree2) = max (max a (maxi tree1)) (maxi tree2)

mini::(Ord a) => Bin a -> a
mini (Nodo Hoja a Hoja) = a
mini (Nodo tree a Hoja) = min a (mini tree)
mini (Nodo Hoja a tree) = min a (mini tree)
mini (Nodo tree1 a tree2) = min (min a (mini tree1)) (mini tree2)

check::(Ord a) => Bin a -> Bool
check Hoja               = True
check (Nodo Hoja a Hoja) = True
check (Nodo tree a Hoja) = (maxi tree <= a) && (check tree)
check (Nodo Hoja a tree) = (mini tree > a) && (check tree)
check (Nodo tree1 a tree2) = (mini tree2 > a) && (check tree2) && (check tree1) && (maxi tree1 <= a)

-- 8) Nota: No sé si es exactamente lo que pide el ej.
member::(Ord a) => Bin a -> a -> Bool
member Hoja x = False
member (Nodo t1 a t2) x | x <= a = (member t1 x) || x == a
                        | True   = member t2 x

-- 9)

