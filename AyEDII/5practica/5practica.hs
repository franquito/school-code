data BTree a = Empty | Node Int (BTree a) a (BTree a) deriving Show

weight :: BTree a -> Int
weight Empty          = 0
weight (Node w _ _ _) = w

value :: BTree a -> a
value (Node _ _ v _) = v

nth :: BTree a -> Int -> a
nth (Node w l a r) n | n <= weight l     = nth l n
                     | n == w - weight r = a
                     | n > (weight l)+1  = nth r (n - weight l -1)

cons :: a -> BTree a -> BTree a
cons x Empty = (Node 1 Empty x Empty)
cons x (Node w l a r) = (Node (w+1) (cons x l) a r)

tabulate :: (Int -> a) -> Int -> BTree a
tabulate f n = tab f 1
  where tab f k | k < n  = cons (f k) (tab f (k+1))
                | k == n = (Node 1 Empty (f k) Empty)

-- Forma paralela:
-- tabulate f n = tab f 0 n
--   tab f from to | n == 1 = Node n Empty (f to) Empty
--                 |  True  = let l = 2 ** ilg n - 1,
--                                r = n-l-1
--                            in Node n (tab f from (from+l-1)) (tab f (to-r+1) to)
--     where n = to - from + 1

inc n = n + 1

tmap :: (a -> b) -> BTree a -> BTree b
tmap f Empty = Empty
tmap f (Node w l a r) = (Node w (tmap f l) (f a) (tmap f r))

ttake :: Int -> BTree a -> BTree a
ttake 0 t = Empty
ttake n t = cons (nth t 1) (ttake (n-1) (dropfirst t))

dropfirst :: BTree a -> BTree a
dropfirst (Node w Empty a r) = r
dropfirst (Node w l a r) = (Node (w-1) (dropfirst l) a r)

tdrop :: Int -> BTree a -> BTree a
tdrop 0 t = t
tdrop n t = tdrop (n-1) (dropfirst t)

tree = (Node 3 (Node 1 Empty 3 Empty) 5 (Node 1 Empty 9 Empty))

-- Ejercicio 2.
data Tree a = E | Leaf a | Join (Tree a) (Tree a)
  deriving Show

mapreduce f g e = mr
    where mr E          = e
          mr (Leaf a)   = f a
          mr (Join l r) = let (cl, cr) = (mr l, mr r)
                          in g cl cr

to4tuple n = let v = (max n 0) in (v, v, v, n)
reductor (lm, lp, ls, lt) (rm, rp, rs, rt) = 
    (maximum [ls+rp, lm, rm],
     max lp (lt + rp),
     max rs (ls + rt),
     rt + lt)

mcss = mapreduce to4tuple reductor (0,0,0,0)

-- Testing Tree:
t1 = (Join (Join (Leaf (-1)) (Leaf 3)) (Join (Leaf 0) (Leaf 1)))

-- Ejercicio 3.
sufijos t = aux t E
  where aux E s          = E
        aux (Leaf x)   s = Leaf s
        aux (Join l r) E = 
          let (l', r') = (aux l r, aux r E)
          in (Join l' r')
        aux (Join l r) s = 
          let (l', r') = (aux l (Join r s), aux r s)
          in (Join l' r')

-- conSufijos :: Tree Int -> Tree (Int, Tree
