-- data Tree a = E | Leaf a | Join (Tree a) (Tree a) deriving Show
-- 
-- sufijos :: Tree Int -> Tree (Tree Int)
-- sufijos t = sufijos' t E
-- 
-- t = Join (Join (Leaf 10) (Leaf 15)) (Leaf 20) :: Tree Int
-- --sufijos t = Join (Join (Leaf (Join (Leaf 15) (Leaf 20))) (Leaf (Leaf 20))) (Leaf E)
-- 
-- sufijos' (Leaf a)   t = (Leaf t)
-- sufijos' (Join a b) E = (Join (sufijos' a b) (sufijos' b E))
-- sufijos' (Join a b) t = (Join (sufijos' a (Join b t)) (sufijos' b t))
-- 
-- conSufijos :: Tree Int -> Tree (Int, Tree Int)
-- conSufijos t = conSufijos' t E
-- 
-- conSufijos' (Leaf a)   t = (Leaf (a, t))
-- conSufijos' (Join a b) E = (Join (conSufijos' a b) (conSufijos' b E))
-- conSufijos' (Join a b) t = (Join (conSufijos' a (Join b t)) (conSufijos' b t))
-- 
-- maxT :: Tree Int -> Int
-- maxT E          = 0
-- maxT (Leaf a)   = a
-- maxT (Join a b) = max (maxT a) (maxT b)
-- 
-- maxAll :: Tree (Tree Int) -> Int
-- maxAll tt = mapreduce maxT max 0 tt
-- 
-- 
-- mapreduce f op e (Leaf a)   = op e (f a)
-- mapreduce f op _ (Join a b) = op (mapreduce' f op a) (mapreduce' f op b)
-- 
-- mapreduce' f op (Leaf a)   = f a
-- mapreduce' f op (Join a b) = op (mapreduce' f op a) (mapreduce' f op b)


-- Ejercicio 4.
-- data T a = E | N (T a) a (T a) deriving Show
-- 
-- t1 = N (N E 1 E) 2 (N E 3 E)
-- t2 = N (N E 6 E) 5 (N E 4 E)
-- 
-- altura :: T a -> Int
-- altura E = 0
-- altura (N l x r) = 1 + max (altura l) (altura r)
-- 
-- combinar :: T a -> T a -> T a
-- combinar E  t  = t
-- combinar t  E  = t
-- combinar t1 t2 = N (delete t1) (delete' t1) t2
-- 
-- delete' (N E x E) = x
-- delete' (N l _ _) = delete' l
-- 
-- delete (N E x E) = E
-- delete (N l x r) = (N (delete l) x r)
-- 
-- filterT p E = E
-- filterT p (N l x r) | p x  = N (filterT p l) x (filterT p r)
--                     | True = combinar (filterT p l) (filterT p r)
-- 
-- quicksortT E = E
-- quicksortT (N l x r) = N (quicksortT (filterT (< x) tt)) x (quicksortT (filterT (>x) tt))
--   where tt = combinar l r

-- Ejercicio 5.
data BTree a = Empty | Node Int (BTree a) a (BTree a) deriving Show
t1 = Node 4 (Node 3 (Node 1 Empty 3 Empty) 2 (Node 1 Empty 4 Empty)) 1 Empty :: BTree Int

weight Empty          = 0
weight (Node n l x r) = n

splitAtT :: BTree a -> Int -> (BTree a, BTree a)
splitAtT (Node n l x r) i 
  | wl + 1 == i = (Node i l x Empty, r)
  | wl + 1 >  i = let t' = splitAtT l i
                  in (fst t', Node (n-i) (snd t') x r)
  |    True     = let t' = splitAtT r (i - wl - 1)
                  in (Node i l x (fst t'), snd t')
  where wl = weight l

-- TODO: Define combinar.

rebalance :: BTree a -> BTree a
rebalance (Node n l x r) | abs (wl-wr) < 2 = Node n (rebalance l) x (rebalance r)
                         |   wl > wr       = let p = splitAtT l wl'
                                             in Node n (rebalance (fst p)) x (rebalance (Node (1+wr+weight 
  where wl = weight l
        wr = weight r
        wl' = div (n - 1) 2
        wr' = wl' + mod (n - 1) 2
