module Mercaderia where

import Data.List

(|||) :: a -> b -> (a,b)
a ||| b = (a,b)

contraer :: (a -> a -> a) -> a -> [a] -> [a]
contraer f e []       = []
contraer f e [x]      = [x]
contraer f e (x:y:xs) = let (x1, xs1) = f x y||| contraer f e xs
                        in (x1 : xs1)

reduceL :: (a -> a -> a) -> a -> [a] -> a
reduceL f e []  = e
reduceL f e [x] = f e x
reduceL f e (x:y:xs)  = reduceL f e (contraer f e (x:y:xs))

sortGT (a1, b1) (a2, b2)
  | a1 > a2 = GT
  | a1 < a2 = LT
  | a1 == a2 = compare b1 b2

ej = [(-1, ["Combancha"]), (2, ["caca"]), (2, ["porron"])]

collect :: (Ord t, Ord a) => [(t, [a])] -> [(t, [a])]
collect xs = reduceL sarasa [] (map (\x -> [x]) (sortBy sortGT xs))

key   (k, x) = k
value (k, x) = x


sarasa :: Eq t => [(t, [a])] -> [(t, [a])] -> [(t, [a])]
sarasa [] ys = ys
sarasa xs ys | fst (last xs) == fst (head ys) = reverse(tail(reverse xs)) ++ [(key (last xs), (value (last xs) ++ value (head ys)))] ++ tail ys
           | True = xs ++ ys