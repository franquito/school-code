module Ej3 where

data Paren = Open | Close deriving (Show, Eq)

(|||) :: a -> b -> (a,b)
a ||| b = (a,b)

tabulateL     :: (Int -> a) -> Int -> [a]
tabulateL f n = [f a | a <- [0..n-1]]

contraer :: (a -> a -> a) -> a -> [a] -> [a]
contraer f e []       = []
contraer f e [x]      = [x]
contraer f e (x:y:xs) = let (x1, xs1) = f x y||| contraer f e xs
                        in (x1 : xs1)

reduceL :: (a -> a -> a) -> a -> [a] -> a
reduceL f e []  = e
reduceL f e [x] = f e x
reduceL f e (x:y:xs)  = reduceL f e (contraer f e (x:y:xs))

expandirL :: (a -> a -> a) -> [a] -> [a] -> Int -> [a]
expandirL f [] _ i          = []
expandirL f (x:xs) [] i     = []
expandirL f (x:xs) (y:ys) i | even i = x : (expandirL f (x:xs) ys (i+1))
                            | otherwise  = let(k, ks) = (f x y) ||| (expandirL f xs ys (i+1)) in (k:ks)

scanL :: (a -> a -> a) -> a -> [a] -> ([a], a)
scanL f e []    = ([e], e)
scanL f e [x]   = ([e], f e x)
scanL f e [x,y] = ([e, f e x], f x y)
scanL f e xs    = (xs1, snd w)
                  where    w   = scanL f e (contraer f e xs)
                           xs1 = expandirL f (fst w) (e:xs) 0
a = [Open, Close, Close, Open, Open, Close]
a1 = [Open, Open, Close, Open, Open, Open, Close, Close, Open, Close, Close, Close]
a2 = [Open, Close]

matchParen :: [Paren] -> Bool
matchParen xs = (reduceL (&&) True (map (\x -> (x >= 0)) (fst s))) && (snd s == 0) 
    where
        s = scanL (+) 0 (map (\x -> if (x == Open) then 1 else (-1)) xs)

-- Apartado a.
matchParenn ps = let (l, r) = matchParenn' ps
                 in l == 0 && r == 0

matchParenn' ps | n == 0 = (0, 0)
                | n == 1 && head ps == Open  = (0, 1)
                | n == 1 && head ps == Close = (1, 0)
                | True = let (l, r) = split ps
                             (l', r') = (matchParenn' l, matchParenn' r)
                         in merge l' r'
  where n = length ps

split xs = (take h xs, drop h xs)
  where h = div (length xs) 2

merge (l, l') (r, r') = if l' < r then (l+r-l', r')
                        else (l, l'-r+r')
