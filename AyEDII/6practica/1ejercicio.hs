module Ej1 where

(|||) :: a -> b -> (a,b)
a ||| b = (a,b)

tabulateL     :: (Int -> a) -> Int -> [a]
tabulateL f n = [f a | a <- [0..n-1]]

contraer :: (a -> a -> a) -> a -> [a] -> [a]
contraer f e []       = []
contraer f e [x]      = [x]
contraer f e (x:y:xs) = let (x1, xs1) = f x y||| contraer f e xs
                        in (x1 : xs1)

expandirL :: (a -> a -> a) -> [a] -> [a] -> Int -> [a]
expandirL f [] _ i          = []
expandirL f (x:xs) [] i     = []
expandirL f (x:xs) (y:ys) i | even i = x : (expandirL f (x:xs) ys (i+1))
                            | otherwise  = let(k, ks) = (f x y) ||| (expandirL f xs ys (i+1)) in (k:ks)

scanL :: (a -> a -> a) -> a -> [a] -> ([a], a)
scanL f e []    = ([e], e)
scanL f e [x]   = ([e], f e x)
scanL f e [x,y] = ([e, f e x], f x y)
scanL f e xs    = (xs1, snd w)
                  where    w   = scanL f e (contraer f e xs)
                           xs1 = expandirL f (fst w) (e:xs) 0

type Tup = (Int, Int, Int, Int)

mulmat :: Tup -> Tup -> Tup
mulmat (a1, b1, c1, d1) (a2, b2, c2, d2) = (a1*a2 + b1*c2, a1*b2 + b1*d2, c1*a2 + d1*c2, c1*b2 + d1*d2)

fibo :: Int -> Int
fibo (-1) = 1
fibo 0 = 0
fibo 1 = 1
fibo (n) = fibo(n-1) + fibo(n-2)

fibSeq :: Int -> Tup
fibSeq n = snd(scanL (mulmat) (1,0,0,1) (tabulateL (\x -> (1,1,1,0)) n))