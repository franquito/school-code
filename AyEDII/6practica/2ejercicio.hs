module Ej2 where

(|||) :: a -> b -> (a,b)
a ||| b = (a,b)

reduceL :: (a -> a -> a) -> a -> [a] -> a
reduceL f e []  = e
reduceL f e [x] = f e x
reduceL f e (x:y:xs)  = reduceL f e (contraer f e (x:y:xs))

contraer :: (a -> a -> a) -> a -> [a] -> [a]
contraer f e []       = []
contraer f e [x]      = [x]
contraer f e (x:y:xs) = let (x1, xs1) = f x y ||| contraer f e xs
                        in (x1 : xs1)

a1 :: [Int]
a1 = [2,3,4,7,5,2,3,2,6,4,3,5,2,1]

mapreduce :: (a -> b) -> (b -> b -> b) -> b -> [a] -> b
mapreduce f op e xs = reduceL op e (map f xs)

takeL :: [a] -> Int -> [a]
takeL xs n = take n xs

dropL :: [a] -> Int -> [a]
dropL xs n = drop n xs

aguaHist :: [Int] -> Int
aguaHist xs = mapreduce (\(x,y,z) -> max 0 ((min x y) - z)) (+) 0 (zip3 (scanl max 0 xs) (scanr max 0 xs) xs)