module Papuchi where

import Data.List as Ls

(|||) :: a -> b -> (a,b)
a ||| b = (a,b)

lengthL :: [a] -> Int
lengthL xs = Ls.length xs

takeL :: [a] -> Int -> [a]
takeL xs n = take n xs

dropL :: [a] -> Int -> [a]
dropL xs n = drop n xs

divide :: [a] -> ([a], [a])
divide xs = (take n xs, drop n xs)
            where n = div (lengthL xs) 2

map' :: [Int] -> [(Int, Int)]
map' xs = dropL (zip (0:xs) xs) 1

sccml :: [Int] -> Int
sccml xs = last (scanl (+) 0 (map (\(x,y) -> if (y == x + 1) then (1::Int) else (0::Int)) (map' xs)))

