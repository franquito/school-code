-- No funciona.
closestPair ps = cP ps

cP ps | n == 2 = dist (ps ! 0) (ps ! 1)
      | n == 3 = min (dist (ps ! 0) (ps ! 1))
                     (min (dist (ps ! 0) (ps ! 2))
                          (dist (ps ! 1) (ps ! 2)))
      |  True  = let (ps1, ps2) = split ps
                     (d1, d2)   = (cP ps1, cP ps2)
                 in min (combine ps1 ps2)
                        (min d1 d2)
  where n = length ps


sad ps = dist (ps ! 0) (ps ! 1)


xs ! n = head (drop n xs)

nth xs n = head (drop n xs)

split xs = (take h xs, drop h xs)
  where h = div (length xs) 2

--dist :: (Double, Double) -> (Double, Double) -> Double
dist (p, p') (q, q') = sqrt ((p-q)**2) ((p'-q')**2)

combine [p]    ps = check p ps
combine (q:qs) ps = min (check q ps) (combine qs ps)

check e [p]    = dist e p
check e (p:ps) = min (dist e p) (check e ps)
