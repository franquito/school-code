-- mergesort con listas no es paralelizable.

--   msort :: [Int] -> [Int]
--   msort []  = []
--   msort [x] = [x]
--   msort xs  = let (ys, zs) = split xs
--               in merge (msort ys) (msort zs)
-- 
--   split :: [Int] -> ([Int], [Int])
--   split (x:y:xs) = let (xs', ys') = split xs
--                    in (x:xs', y:ys')
--   split xs       = (xs, [])
-- 
--   merge :: [Int] -> [Int] -> [Int]
--   merge [] xs = xs
--   merge xs [] = xs
--   merge (x:xs) (y:ys) | x < y = x:(merge xs (y:ys))
--                       |  True = y:(merge (x:xs) ys)

-- arboles para paralelizarr
data BT a = Empty | Node (BT a) a (BT a) deriving Show

x ||| y = (x, y)

t1 = Node (Node Empty 1 Empty) 5 (Node Empty 3 Empty)
t2 = Node (Node Empty 2 Empty) 4 (Node Empty 7 Empty)

-- Wmsort (n) = 2Wmsort(n/2) + Wmerge(n)
-- Usando el mareteo troesma:
-- Wmsort (n) ∈ O(nln(n))
msort :: Ord a => BT a -> BT a
msort Empty        = Empty
msort (Node l x r) = let (l', r') = msort l ||| msort r
                     in merge (merge l' r')
                              (Node Empty x Empty)

-- Wmerge (n, m) = 2Wmerge(n/2, m) + WsplitAt(n)
-- Wmerge (n, m) = 2Wmerge(n/2, m) + ln(n)
-- Usando un toque de análisis con árboles:
-- Wmerge (n, m) ∈ O(n)
merge Empty t = t
merge t Empty = t
merge t1@(Node l x r) t2@(Node l' x' r')
  | x < x' = Node (merge l  rl) x  (merge r  rr)
  |  True  = Node (merge l' ll) x' (merge lr r')
  where (rl, rr) = splitAtT t2 x
        (ll, lr) = splitAtT t1 x'

-- WsplitAtT(n) ∈ O(ln(n))
splitAtT Empty _ = (Empty, Empty)
splitAtT (Node l x r) n | n < x = let (s1, s2) = splitAtT l n
                                  in (s1, Node s2 x r)
                        |  True = let (s1, s2) = splitAtT r n
                                  in (Node l x s1, s2)
