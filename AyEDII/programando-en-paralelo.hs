split :: ([Int], [Int])
split []       = ([], [])
split [x]      = ([x], [])
split (x:y:ys) = let (l, r) = split ys
                    in (x:l, y:r)
merge :: [Int] -> [Int]
merge [x] [] = [x]
merge [] [y] = [y]
merge a@(x:xs) b@(y:ys) | x < y = x:(merge xs b)
                        | True  = y:(merge a ys)
msort :: [Int] -> [Int]
msort []  = []
msort [x] = [x]
msort xs  = let (ls, rs) = split xs
                (l, r)   = (msort ls, msort rs)
            in merge l r

data BT a = Empty | Node (BT a) a (BT a)

listar :: BT a -> [a]
listar Empty        = []
listar (Node l a r) = listar l ++ [a] ++ listar r

-- tmsort :: BT a -> BT a
-- tmsort Empty = Empty
-- tmsort (Node l a r) = let (nl, nr) = tmsort l || tmsort r
--                       in tmerge (tmerge nl nr)
--                                 (Node Empty a Empty)
-- tmerge :: BT a -> BT a
-- tmerge Empty Empty = Empty
-- tmerge (Node ll a lr) (Node rl b rr) | a < b = let (newlr, newrl) = splitAt rl a
--                                                in (Node (tmerge ll newlr) a (tmerge lr (Node r1 b rr)))

data BTree a = Empty | Node Int (BTree a) a (BTree a)


-- Programando con árboles.
data T a = Empty | Leaf a | Node (T a) (T a)

mapT :: (a -> b) -> T a -> T b
mapT f Empty      = Empty
mapT f (Leaf a)   = Leaf (f a)
mapT f (Node l r) = (Node (mapT f l) (mapT f r)

sumT :: T Int -> Int
sumT Empty      = 0
sumT (Leaf a)   = a
sumT (Node l r) = sumT l + sumT r

flattenT :: T String -> String
flattenT Empty      = []
flattenT (Leaf s)   = s
flattenT (Node l r) = flattenT l ++ flattenT r

reduceT :: (a -> a -> a) -> a -> T a -> a
reduceT f e Empty      = e
reduceT f e (Leaf a)   = a
reduceT f e (Node l r) = f (reduceT f e l) (reduceT f e r)

mapreduce :: (a -> b) -> (b -> b -> b) -> b -> T a -> b
mapreduce f op e = mr
  where mr Empty      = e
        mr (Leaf a)   = f a
        mr (Node l r) = op (mr l) (mr r)
