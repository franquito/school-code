#!/bin/bash

WEBSERVER=10.1.1.2
MAILRELAY=10.1.1.3
DBSERVER=10.10.1.2
MAILSERVER=10.10.1.3
ADMIN=10.23.23.5

PUBLICIP=200.123.131.112

DMZ=10.1.1.0/29
LAN=10.23.0.0/16
SERVERS=10.10.0.0/22

_=iptables

# Ejercicio 8.

$_ -t filter -P INPUT DROP
$_ -t filter -P INPUT -m state --state INVALID -j DROP
$_ -t filter -P INPUT -m state --state RELATED,ESTABLISHED -j DROP
$_ -t filter -A INPUT -i eth2 -s $ADMIN -p tcp --sport 22 -j ACCEPT

$_ -t filter -P OUTPUT DROP
$_ -t filter -P OUTPUT -m state --state INVALID -j DROP
$_ -t filter -P OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
for PROTOCOL in tcp udp; do
$_ -t filter -A OUTPUT -o eth3 -m iprange -p $PROTOCOL \
  --dest-range $WEBSERVER-$MAILRELAY --dport 53 -j ACCEPT
done

$_ -t filter -P FORWARD DROP
$_ -t filter -A FORWARD -m state --state INVALID -j DROP
$_ -t filter -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT

# Ejercicio 2.
for PROTOCOL in tcp udp; do
$_ -t filter -A FORWARD -i eth1 -m iprange \
  -s $MAILSERVER --dest-range $WEBSERVER-$MAILRELAY \
  -p $PROTOCOL --dport 53 -j ACCEPT
done
$_ -t filter -A FORWARD -i eth1 -s $MAILSERVER \
  -p tcp -d $MAILRELAY --dport 465 -j ACCEPT

# Ejercicio 3.
$_ -t filter -A FORWARD -i eth3 -s $MAILRELAY \
  -p tcp -d $MAILSERVER --dport 465 -j ACCEPT

# Ejercicio 4.
$_ -t filter -A FORWARD -i eth3 -s $WEBSERVER \
  -p tcp -d $DBSERVER --dport 3306 -j ACCEPT

# Ejercicio 1.
$_ -t filter -A FORWARD -i eth2 -s $LAN \
  -d $MAILSERVER -p tcp -m multiport \
  --dports imap,imaps,pop,pop3s,smtp,smpts -j ACCEPT
$_ -t filter -A FORWARD -i eth2 -s $LAN \
  -d $MAILSERVER -p tcp --dport 443 -j ACCEPT
for PROTOCOL in tcp udp; do
$_ -t filter -A FORWARD -i eth2 -s $LAN -m iprange
  --dest-range $WEBSERVER-$MAILRELAY -p $PROTOCOL \
  --dport 53 -j ACCEPT
done

# Ejercicio 5.
$_ -t filter -A FORWARD -i eth2 -s $LAN -j ACCEPT

$_ -t nat -A POSTROUTING -s $LAN -o eth0 \
  -j SNAT --to $PUBLICIP

# Ejercicio 7.
$_ -t filter -A FORWARD -i eth3 -s $DMZ -o eth0 \
  -j ACCEPT
$_ -t nat -A POSTROUTING -s $DMZ -o eth0 \
  -j SNAT --to $PUBLICIP

$_ -t filter -A FORWARD -i eth0 -o eth3 -m multiport \
  -d $WEBSERVER -p tcp --dports 80,443,53 -j ACCEPT
$_ -t filter -A FORWARD -i eth0 -o eth3 \
  -d $WEBSERVER -p udp --dport 53 -j ACCEPT
$_ -t filter -A FORWARD -i eth0 -o eth3 -d $MAILRELAY
  -p tcp --dport 25 -j ACCEPT

$_ -t nat -A PREROUTING -i eth0 -d $PUBLICIP -p udp \
  --dport 53 -j DNAT --to $WEBSERVER
$_ -t filter -A FORWARD -i eth0 -o eth3 -d $WEBSERVER \
  -p udp --dport 53 -j ACCEPT
$_ -t nat -A PREROUTING -i eth0 -d $PUBLICUP \
  -m multiport -p tcp --dports 53,80,443 \
  -j DNAT --to $WEBSERVER
$_ -t filter -A FORWARD -i eth0 -o eth3 -d $WEBSERVER \
  -m multiport -p tcp --dports 53,80,443 -j ACCEPT
$_ -t nat -A PREROUTING -i eth0 -d $PUBLICIP -p tcp \
  --dport 25 -j DNAT --to $MAILRELAY
$_ -t filter -A FORWARD -i eth0 -o eth3 -d $MAILRELAY \
  -p tcp --dport 25 -j ACCEPT
