# Info about setjmp and longjmp:
# http://web.eecs.utk.edu/~huangj/cs360/360/notes/Setjmp/lecture.html
.text
.global setjmp2
  setjmp2:
  movq %rbx, (%rdi)
  movq %rbp, 8(%rdi)
  movq %rsp, 16(%rdi)
  movq %r10, 24(%rdi)
  movq %r13, 32(%rdi)
  movq %r14, 40(%rdi)
  movq %r15, 48(%rdi)
  movq (%rsp), %rax
  movq %rax, 56(%rdi)
  xorq %rax, %rax
  retq

# Si se llama a longjmp con el segundo argumento en 0,
# entonces devuelve 1.
.global longjmp2
  longjmp2:
  movq (%rdi), %rbx
  movq 8(%rdi), %rbp
  movq 16(%rdi), %rsp
  movq 24(%rdi), %r10
  movq 32(%rdi), %r13
  movq 40(%rdi), %r14
  movq 48(%rdi), %r15
  movq 56(%rdi), %rcx
  movq %rcx, (%rsp)
  cmpq $0, %rdi
  jne skip
    movq $1, %rax
  skip:
    movq %rdi, %rax
  retq
