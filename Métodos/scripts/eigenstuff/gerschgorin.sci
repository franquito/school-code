function [offset, radius] = gerschgorin(A)
  n = sqroot(length(A));
  for i = 1:n
    r(i) = 0;
    for j = 1:n
      if j ~= i then
        r(i) = r(i) + abs(A(i,j));
      end
    end
    offset(i) = A(i,i);
    radius(i) = r(i);
  end
  // Graphing...
  maxx = max(offset+radius);
  minx = min(offset-radius);
  maxy = max(radius);
  miny = -maxy;
  plot2d(0,0,rect=[minx-1,miny-1,maxx+1,maxy+1])
  xgrid(1)
  for i = 1:n
    if (radius(i) == 0) then
      plot(offset(i), 0, '.b');
    else
      a = linspace(0, 2*%pi, 100);
      x = offset(i) + radius(i)*cos(a);
      y = 0 + radius(i)*sin(a);
      plot(x, y);
    end
    s = spec(A);
    plot(s(i), 0, '.r');
  end
endfunction
