// f ∊ C in [a, b] and f(a)f(b)<0
// Cota del error:
// |α - c_n| <= (b_n - a_n)/2 = (b_1 - a_1)/2^n
// Convergencia asegurada.

function y = bisect(fun, a, b, e)
  b_1 = b;
  a_1 = a;
  c = (b + a)/2;
  n = 1;
  while (((b_1-a_1)/2^n)>e)
    if (fun(a)*fun(c)<=0)
      b = c;
    else
      a = c;
    end
    c = (b + a)/2;
    n=n+1;
  end
  y = c;
endfunction
