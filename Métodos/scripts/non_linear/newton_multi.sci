clear;
clc;

function xi=newton(f,df,x0,its)
  xi = x0;
  for i=1:its
    xi = xi - inv(df(xi))*f(xi);
  end
endfunction

function r = f(x)
  r(1) = 1+x(1)^2-x(2)^2+%e^x(1)*cos(x(2));
  r(2) = 2*x(1)*x(2)+%e^x(1)*sin(x(2));
endfunction

function r = jf(x)
  r(1)(1) = 2*x(1)+%e^(x(1))*cos(x(2));
  r(1)(2) = -2*x(2)-%e^(x(1))*sin(x(2));
  r(2)(1) = 2*x(2)+%e^(x(1))*sin(x(2));
  r(2)(2) = 2*x(1)+%e^(x(1))*cos(x(2));
  // r = numderivative(f, x, 0.0001)
endfunction

f([-1, 4])
[-1; 4] - jf([-1; 4])*f([-1; 4])
newton(f, jf, [-1; 4], 5)
