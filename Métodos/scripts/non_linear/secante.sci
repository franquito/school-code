// f ∊ C² in [a, b]
// No hay garantía de convergencia.
// Orden de convergencia: (1+5^(1/2))/2

function y = secante(fun, a, b, n)
  for i = 0:n
    if (a == b) then break end;
    y = a - ((b - a)/(fun(b) - fun(a)))*fun(a);
    a = b;
    b = y;
  end
endfunction
