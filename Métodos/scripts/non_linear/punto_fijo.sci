// (1) a <= x <= b → a <= g(x) <= b
// Entonces existe raíz.
// Si (1) y además:
// λ = sup|g(x)|<1 in [a,b] then
// - Unique root.
// - Converge a α
// - |α-x_n| < (λ^n/(1-λ))*|x_0-x_1|
// - lim of x_n+1-α/x_n-α = g'(α) when n -> Infinity.

// Using λ as sup|g(x)| in [a, b]
function y = punto_fijo(fun, x, lambda, e)
  old = x;
  x = fun(x);
  n = 1;
  while (abs(x-old)*lambda^n/(1-lambda) > e)
    old = x;
    x = fun(x);
    n = n + 1;
  end
  disp(n);
  y = x;
endfunction

// Usando x_n+1-x_n como cota.
function y = punto_fijo(fun, x, e)
  old = x;
  x = fun(x);
  n = 1;
  while (abs(x-old) > e)
    old = x;
    x = fun(x);
    n = n + 1;
  end
  disp(n);
  y = x;
endfunction

function y = exp(x)
  y = %e.^(x)./3;
endfunction

punto_fijo(exp, 0, %e/3, 0.1)
