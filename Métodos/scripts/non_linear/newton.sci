// Convergencia cuadrática.
// Si f∊C² en un entorno de la raíz, => existe x_0 / converge.
function y = newton(fun, dfun, x, e)
  old = x;
  x = x - fun(x)/dfun(x);
  while (abs(x - old) > e)
    old = x;
    x = x - fun(x)/dfun(x);
  end
  y = x;
endfunction
