// |(x-x_0)...(x-x_n)|<= 1/2^(n-1) para x∊[-1, 1]
// Donde x_i son las raíces de chebyshev(n)
function P = raw_chebyshev(n)
  if (n == 0)
    P = 1;
  elseif (n == 1)
    P = poly([0 1], 'x', 'c');
  else
    twox = poly([0 2], 'x', 'c');
    P = twox*raw_chebyshev(n-1)-raw_chebyshev(n-2);
  end
endfunction
function P = chebyshev(n)
  P = raw_chebyshev(n);
  P = P/2^(length(coeff(P))-2);
endfunction

function P = transform(C, a, b)
  Q = poly([-1 2/(b-a)], 'x', 'c');
  P = horner(C, Q); // Transforms Q.
endfunction
