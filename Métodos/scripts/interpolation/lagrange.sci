// Error:
// f(x) - P_n(x) = (x-x_0)...(x-x_n)/(n+1)!*f^(n+1)(c_x)
// Error lineal (Dos puntos):
// max (x-x_0)(x-x_1) = (x_1-x_0)^2/4

// Takes a vector of points and the index of the proper one.
function L_k = generate_ele(x, k)
  x_k = x(k);
  x(k) = []; // Remove x_k from x
  L_k = poly(x, 'x', 'r')/prod(x_k - x);
endfunction

// Returns P_n(x) = L_0(x)*y_0 + ... + L_n(x)*y_n
function P = interpol_lagrange(x,y)
  n = length(x);
  P = 0;
  for i = 1:n
    P = P + generate_ele(x,i)*y(i);
  end
endfunction

// Interpolación lineal y cuadrática de Lagrange.
interpol_lagrange([.2 .4], [1.2214 1.4918]);
interpol_lagrange([0 .2 .4 .6], [1 1.2214 1.4918 1.8221]);
