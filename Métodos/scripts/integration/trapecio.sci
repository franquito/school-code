// Error: -h^3/12*f^2(c_x)
function y = trapecio(fun, a, b)
  y = ((b-a)/2)*(fun(a)+fun(b));
endfunction

function y = ln(x)
  y = log(x);
endfunction

trapecio(ln, 1, 2); //0.3465736
// 0.38629
