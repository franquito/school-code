// Error: -h^5/90*f^4(c_x) con c_x∊[x_0, x_2]
function y = simpson(fun, a, c)
  b = (a+c)/2;
  y = ((b-a)/3)*(fun(a)+4*fun(b)+fun(c));
endfunction
