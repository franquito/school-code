// Gauss sin pivoteo.
function y = gauss(A, b)
  n = sqrt(length(A));
  // Forma escalonada de A.
  for j = 1:n-1
    for i = j+1:n
      aux = A(i,j)
      for k = j:n
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end
      b(i) = b(i) - (aux/A(j,j))*b(j);
    end
  end
  // Remonte..
  for i = n:-1:1
    y(i) = b(i);
    for j = i+1:n
      y(i) = y(i) - A(i,j)*y(j);
    end
    y(i) = y(i)/A(i,i);
  end
endfunction

A_1 = [1 1 0 3; 2 1 -1 1; 3 -1 -1 2; -1 2 3 -1];
b_1 = [4; 1; -3; 4];
gauss(A_1, b_1)
