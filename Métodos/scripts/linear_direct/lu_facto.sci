// Doolittle factorization.
function [L, U] = lu_cool(A)
  n = sqrt(length(A));
  L = eye(n,n);
  for j = 1:n-1
    for i = j+1:n
      aux = A(i,j);
      m = A(i,j)/A(j,j);
      for k = j:n
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end L(i,j) = m;
    end
  end
  U = A;
endfunction

A_1 = [1.012 -2.132 3.104; -2.132 4.096 -7.013; 3.104 -7.013 0.014];
[R N] = lu_cool(A_1)
