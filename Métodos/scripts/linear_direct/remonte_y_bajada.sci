// Siendo A triangular inferior, resulvo Ax=b
function x = pa_abajo(A,b)
  n = sqrt(length(A))
  for i = 1:n
    x(i) = b(i);
    for j = 1:i-1
      x(i) = x(i) - x(j)*A(i,j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

function x = pa_arriba(A,b)
  n = sqrt(length(A));
  for i = n:-1:1
    x(i) = b(i);
    for j = i+1:n
      x(i) = x(i) - A(i,j)*x(j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

function [L, U] = lu_cool(A)
  n = sqrt(length(A));
  L = eye(n,n);
  for j = 1:n-1
    for i = j+1:n
      aux = A(i,j);
      m = A(i,j)/A(j,j);
      for k = j:n
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end L(i,j) = m;
    end
  end
  U = A;
endfunction

A_1 = [1 2 3 4; 1 4 9 16; 1 8 27 64; 1 16 81 256];
b_1 = [2 10 44 190];

[L U] = lu_cool(A_1);
pa_arriba(U, pa_abajo(L, b_1))
// https://www.wolframalpha.com/input/?i=row+reduce+%7B%7B1%2C2%2C3%2C4%2C2%7D%2C%7B1%2C4%2C9%2C16%2C10%7D%2C%7B1%2C8%2C27%2C64%2C44%7D%2C%7B1%2C+16%2C+81%2C256%2C190%7D%7D


