// Converge sii p(I - (L+D)^(-1)*A) < 1
function y = gauss_seidel(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
endfunction

A = [0 2 4; 1 -1 1; 1 -1 2];
b_1 = [0; 0.375; 0];
gauss_seidel(A, b_1, [0; 0; 0], .01)
