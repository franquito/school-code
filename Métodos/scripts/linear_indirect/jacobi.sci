// Iteration: x^k+1 = (I - N^-1*A)x^k + N^-1*b
// ‖x-~x‖/‖x‖ <= ‖A‖‖A^(-1)‖‖b-~b‖/‖b‖
// 1 <= k(A) = ‖A‖‖A^-1‖
// ‖e^(k+1)‖ = ‖I-N^(-1)*A‖^(k+1)*‖e^0‖

// A = L + D + U
// jacobi converge sii p(I-D^(-1)*A) < 1
function y = jacobi(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        if j ~= i then
          y(i) = y(i) - A(i,j)*x(j);
        end
      end
      y(i) = y(i)/A(i,i);
    end
    // Checking the error.
    if (norm(x-y,'inf') <= e) then
      break;
    end
    x = y;
  end
endfunction

A = [0 2 4; 1 -1 1; 1 -1 2];
b_1 = [0; 0.375; 0];
jacobi(A,b_1,[0; 0; 0], .01);
