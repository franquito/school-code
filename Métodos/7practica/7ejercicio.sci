function y = gauss_pivoteador(A, b)
  n = sqrt(length(A));
  for j = 1:n-1
    if (A(j,j) == 0) then
      // Getting the index
      for i = j+1:n
        if A(j,i) ~= 0 then
          index = i;
          break;
        end
      end
      // Swapping rows.
      aux_row = A(j,:);
      A(j,:) = A(index,:);
      A(index,:) = aux_row;
      disp(A);
    end
    for i = j+1:n
      aux = A(i,j)
      for k = j:n
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end
      b(i) = b(i) - (aux/A(j,j))*b(j);
    end
  end
  // Remonte..
  for i = n:-1:1
    y(i) = b(i);
    for j = i+1:n
      y(i) = y(i) - A(i,j)*y(j);
    end
    y(i) = y(i)/A(i,i);
  end
endfunction

// Being x = [x_0 ... x_n], y = [f(x_0) ... f(x_n)]
// Having in mind that m represents |{δ_0, ..., δ_(m-1)}| = m
// where δ_k = x^k.
// Returns a matrix A, with size mxm and a vector b 
// a_(i,j) = sum from k=0 to n of x_k^(i+j). If i=j=0 => a_(0,0) = n+1.
// b_(i) = sum from k=0 to n of f(x_k)*x_k^i
// NOTE: I'm assuming that the functions for the aproximation are the 
// polynomials 1, x^1, x^2, etc.
function [A, b] = coefs(x, y, m)
  m = m+1;
  n = length(x);
  for k = 1:m
    // Building A.
    for l = 1:m
      A(k,l) = 0;
      for i = 1:n
        A(k,l) = A(k,l) + x(i)^(k+l-2);
      end
    end
    A(1,1) = n+1;
    // Building b.
    b(k) = 0;
    for i = 1:n
      b(k) = b(k) + y(i)*x(i)^(k-1);
    end
  end
endfunction

x = [4 4.2 4.5 4.7 5.1 5.5 5.9 6.3 6.8 7.1];
y = [102.56 113.18 130.11 142.05 167.53 195.14 224.87 256.73 299.5 326.72];

// Linear aproximation.
[A, b] = coefs(x, y, 1)
linear_coefs = gauss_pivoteador(A,b)
linear_poly = poly(linear_coefs, 'x', 'c');

// Cuadratic aproximation.
[A, b] = coefs(x, y, 2)
cuadratic_coefs = gauss_pivoteador(A,b)
cuadratic_poly = poly(cuadratic_coefs, 'x', 'c');

// Cubic aproximation.
[A, b] = coefs(x, y, 3)
cubic_coefs = gauss_pivoteador(A,b)
cubic_poly = poly(cubic_coefs, 'x', 'c');
// http://fooplot.com/#W3sidHlwZSI6MywiZXEiOltbIjQiLCIgMTAyLjU2Il0sWyI0LjIiLCIgMTEzLjE4Il0sWyI0LjUiLCIgMTMwLjExIl0sWyI0LjciLCIgMTQyLjA1Il0sWyI1LjEiLCIgMTY3LjUzIl0sWyI1LjUiLCIgMTk1LjE0Il0sWyI1LjkiLCIgMjI0Ljg3Il0sWyI2LjMiLCIgMjU2LjczIl0sWyI2LjgiLCIgMjk5LjUiXSxbIjcuMSIsIiAzMjYuNzIiXV0sImNvbG9yIjoiI0ZGMDAwMCJ9LHsidHlwZSI6MCwiZXEiOiItNTAuNjQ4NjY1KzQ2LjQ5NzY5NSp4IiwiY29sb3IiOiIjMDBGRjUxIn0seyJ0eXBlIjowLCJlcSI6IjAuMDEwMzA5Mi0wLjY5MDAyNTkqeCs2LjU3NzY5NTQqeF4yIiwiY29sb3IiOiIjRkY4NDAwIn0seyJ0eXBlIjowLCJlcSI6IjAuMDAwNjM4MC0wLjQ2MjQ5NTkqeCs2LjQ5NTU1OTEqeF4yKzAuMDA3MjI1OSp4XjMiLCJjb2xvciI6IiMwMDAwMDAifSx7InR5cGUiOjEwMDAsIndpbmRvdyI6WyItNy4zNDkxNjAzOTEzNTY2ODUiLCIxNC43MTY1MjIyMjMwNjgyNjkiLCItNzEuMzc1MTc0MDU5OTczIiwiMzcwLjY0OTcxNDg1MzY3NjQ3Il19XQ--

// NOTE: I'm assuming that the functions for the aproximation are the 
// polynomials 1, x^1, x^2, etc.
function r = error_minimos_cuadrados(x, y, coefs)
  n = length(x);
  m = length(coefs);
  r = 0;
  for k = 1:n
    // First, calculate a_0 + a_1*x_k + ... + a_m*x_k^m
    g_k = 0;
    for i = 1:m
      g_k = g_k + coefs(i)*x(k)^(i-1);
    end
    r = r + (y(k)-g_k)^2
  end
  r = sqroot(r);
endfunction

// Errores..!
error_minimos_cuadrados(x, y, linear_coefs)
error_minimos_cuadrados(x, y, cuadratic_coefs)
error_minimos_cuadrados(x, y, cubic_coefs)

// Graphing..

// Ploting the dots.
plot2d(x, y, style=0)

// Wrap polynomials in new functions ¬¬
function y = P1(x)
  y = horner(linear_poly, x);
endfunction
function y = P2(x)
  y = horner(cuadratic_poly, x);
endfunction
function y = P3(x)
  y = horner(cubic_poly, x);
endfunction

x_interval = linspace(4, 8, 51);
plot(x_interval, P1)
plot(x_interval, P2, 'r')
plot(x_interval, P3, 'g')
