// Takes a vector of points and the index of the proper one.
function L_k = generate_ele(x, k)
  x_k = x(k);
  x(k) = []; // Remove x_k from x
  L_k = poly(x, 'x', 'r')/prod(x_k - x);
endfunction

// Returns P_n(x) = L_0(x)*y_0 + ... + L_n(x)*y_n
function P = interpol_lagrange(x,y)
  n = length(x);
  P = 0;
  for i = 1:n
    P = P + generate_ele(x,i)*y(i);
  end
endfunction

// Interpolación lineal y cuadrática de Lagrange.
interpol_lagrange([.2 .4], [1.2214 1.4918]);
interpol_lagrange([0 .2 .4 .6], [1 1.2214 1.4918 1.8221]);

// Returns the result of f[x_0,...,x_n] having in mind the f(x_i) values.
function r = diferencias(x, y)
  n = length(x);
  if n == 1 then
    r = y(1);
  else
    x_head = x; x_head(n) = [];
    x_tail = x; x_tail(1) = [];
    y_head = y; y_head(n) = [];
    y_tail = y; y_tail(1) = [];
    r = (diferencias(x_tail, y_tail) - diferencias(x_head, y_head))/(x(n) - x(1));
  end
endfunction

// Returns:
//   y_0 + f[x_0,x_1](x-x_0) + .. + f[x_0,..,x_n](x-x_0)..(x-x_n-1)
// using recursion.
function P = diferencias_newton(x, y)
  n = length(x);
  if n == 1 then
    P = y(1);
  else
    x_head = x; x_head(n) = [];
    y_head = y; y_head(n) = [];
    P = diferencias_newton(x_head, y_head) + poly(x_head, 'x', 'r')*diferencias(x,y);
  end
endfunction
diferencias_newton([.2 .4], [1.2214 1.4918])
diferencias_newton([0 .2 .4 .6], [1 1.2214 1.4918 1.8221])
