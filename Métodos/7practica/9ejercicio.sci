// Takes a vector of points and the index of the proper one.
function L_k = generate_ele(x, k)
  x_k = x(k);
  x(k) = []; // Remove x_k from x
  L_k = poly(x, 'x', 'r')/prod(x_k - x);
endfunction

// Returns P_n(x) = L_0(x)*y_0 + ... + L_n(x)*y_n
function P = interpol_lagrange(x,y)
  n = length(x);
  P = 0;
  for i = 1:n
    P = P + generate_ele(x,i)*y(i);
  end
endfunction

function P = raw_chebyshev(n)
  if (n == 0)
    P = 1;
  elseif (n == 1)
    P = poly([0 1], 'x', 'c');
  else
    twox = poly([0 2], 'x', 'c');
    P = twox*raw_chebyshev(n-1)-raw_chebyshev(n-2);
  end
endfunction

function y = e(x)
  y = %e.^x
endfunction

x = roots(raw_chebyshev(4))/2^3;
y = 0;
for i = 1:4
  y(i) = e(x(i));
end

P = interpol_lagrange(x, y);
function y = e(x)
  y = e(x) - horner(P,x);
endfunction

