// Takes a vector of points and the index of the proper one.
function L_k = generate_ele(x, k)
  x_k = x(k);
  x(k) = []; // Remove x_k from x
  L_k = poly(x, 'x', 'r')/prod(x_k - x);
endfunction

// Returns P_n(x) = L_0(x)*y_0 + ... + L_n(x)*y_n
function P = interpol_lagrange(x,y)
  n = length(x);
  P = 0;
  for i = 1:n
    P = P + generate_ele(x,i)*y(i);
  end
endfunction

function y = f(x)
  y = 1./(1+x.^2);
endfunction



for i=[2 4 6 10 14]
  x_i = linspace(-5, 5, i);
  for j=1:length(x_i)
    y_i(i) = f(x_i(j));
  end
  P_i = interpol_lagrange(x_i, y_i);
  disp(P_i);
  function y = e_i(x)
    y = f(x) - horner(P_i,x);
  endfunction
  c = 'b';
  if i == 4 then c = 'g'; end
  if i == 6 then c = 'r'; end
  if i == 10 then c = 'cyan'; end
  plot(linspace(-5, 5, 300), e_i, c);
end

// Fooplot:
// http://fooplot.com/#W3sidHlwZSI6MCwiZXEiOiIxLygxK3heMiktKDAuMDE5MjMwOCswLjAwMzg0NjJ4KSIsImNvbG9yIjoiI0ZGMDAwMCJ9LHsidHlwZSI6MCwiZXEiOiIxLygxK3heMiktKDAuMDE5MjMwOC0wLjAxMzQ2MTV4KzAuMDAwNjkyM3heMykiLCJjb2xvciI6IiMzQ0ZGMDAifSx7InR5cGUiOjAsImVxIjoiMS8oMSt4XjIpLSgwLjAxOTIzMDgrMC4wMjM4NzgyeC0wLjAwNDgwNzd4XjMrMC4wMDAxNjAzeF41KSIsImNvbG9yIjoiIzAwMDAwMCJ9LHsidHlwZSI6MCwiZXEiOiIxLygxK3heMiktKDAuMDE3ODk5NyswLjA0NTA5ODZ4KzAuMDA0OTMzMHheMi0wLjAzNjAxNzl4XjMtMC4wMDIwNjQ2eF40KzAuMDA2Nzg1MXheNSswLjAwMDE3NzR4XjYtMC4wMDA0NDIzeF43LTAuMDAwMDA0MXheOCswLjAwMDAwOTB4XjkpIiwiY29sb3IiOiIjMDAxNUZGIn0seyJ0eXBlIjowLCJlcSI6IjEvKDEreF4yKS0oLTAuMDA0NTE1NiswLjAwNjM1NjB4KzAuMDMyNTg4MnheMi0wLjA0NzMyNTN4XjMtMC4wMTQzNTAweF40KzAuMDMwNDQ2MHheNSswLjAwMjc4MTB4XjYtMC4wMDY3Mzk2eF43LTAuMDAwMjU4N3heOCswLjAwMDY0ODF4XjkrMC4wMDAwMTExeF4xMC0wLjAwMDAyNzd4XjExLTAuMDAwMDAwMnheMTIrMC4wMDAwMDA0eF4xMykiLCJjb2xvciI6IiNGQzAwQzEifSx7InR5cGUiOjEwMDAsIndpbmRvdyI6WyItMTMuOTM5OTU5NzcyOTk5OTc1IiwiMTcuNzk4MzIxNDc2OTk5OTciLCItNS4zNTMyMjUxODI5OTk5ODE1IiwiMTQuMTc4MDI0ODE2OTk5OTgiXX1d
