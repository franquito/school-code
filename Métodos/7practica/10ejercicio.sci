// Takes a vector of points and the index of the proper one.
function L_k = generate_ele(x, k)
  x_k = x(k);
  x(k) = []; // Remove x_k from x
  L_k = poly(x, 'x', 'r')/prod(x_k - x);
endfunction

// Returns P_n(x) = L_0(x)*y_0 + ... + L_n(x)*y_n
function P = interpol_lagrange(x,y)
  n = length(x);
  P = 0;
  for i = 1:n
    P = P + generate_ele(x,i)*y(i);
  end
endfunction

function P = raw_chebyshev(n)
  if (n == 0)
    P = 1;
  elseif (n == 1)
    P = poly([0 1], 'x', 'c');
  else
    twox = poly([0 2], 'x', 'c');
    P = twox*raw_chebyshev(n-1)-raw_chebyshev(n-2);
  end
endfunction

T_4 = raw_chebyshev(4)/2^3;
// Makes the chevyshev 3 in the interval [0, pi/2]
function y = trans(x)
  t = (x-(%pi/2+0)/2)*2/(%pi/2+0);
  y = horner(T_3, t);
endfunction

// Por las dudas, como siempre bate scilab para graficar:
function y = seno(x)
  y = sin(x);
endfunction

T_4r = horner(T_4, poly([-(%pi+0)/(%pi-0) 2/(%pi/2-0)], 'x', 'c'));
raices = roots(T_4r);
y = 0;
for i = 1:4
  y(i) = sin(raices(i));
end

P_3 = interpol_lagrange(raices, y);
function y = wrapped_P_3(x)
  y = horner(P_3, x);
endfunction
plot(linspace(-1, 3, 400), wrapped_P_3);
