// Devuelve la triangular inferior.
function l = cholevsky(A)
  n = sqrt(length(A));
  l = zeros(n,n);
  for i = 1:n
    for j = 1:i
      if i == j then
        l(i,i) = A(i,i);
        for k = 1:i-1
          l(i,i) = l(i,i) - (l(i,k))^2;
        end
        l(i,i) = sqrt(l(i,i));
      else
        l(i,j) = A(i,j);
        for k = 1:j-1
          l(i,j) = l(i,j) - l(i,k)*l(j,k);
        end
        l(i,j) = l(i,j)/l(j,j);
      end
    end
  end
endfunction

// Siendo A triangular inferior, resulvo Ax=b
function x = pa_abajo(A,b)
  n = sqrt(length(A))
  for i = 1:n
    x(i) = b(i);
    for j = 1:i-1
      x(i) = x(i) - x(j)*A(i,j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

function x = pa_arriba(A,b)
  n = sqrt(length(A));
  for i = n:-1:1
    x(i) = b(i);
    for j = i+1:n
      x(i) = x(i) - A(i,j)*x(j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

A = [25 15 -5; 15 18 0; -5 0 11];
b = [30; 27; 24];
R = cholesky(A);
pa_arriba(R', pa_abajo(R,b))

// https://www.wolframalpha.com/input/?i=row+reduce+%7B%7B5%2C0%2C0%2C30%7D%2C%7B3%2C3%2C0%2C27%7D%2C%7B-1%2C1%2C3%2C24%7D%7D
// https://www.wolframalpha.com/input/?i=row+reduce+%7B%7B5%2C3%2C-1%2C6%7D%2C%7B0%2C3%2C1%2C3%7D%2C%7B0%2C0%2C3%2C9%7D%7D
