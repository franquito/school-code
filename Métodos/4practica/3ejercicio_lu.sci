// Doolittle factorization.
function [L, U] = lu_cool(A)
  n = sqrt(length(A));
  L = eye(n,n);
  for j = 1:n-1
    for i = j+1:n
      aux = A(i,j);
      m = A(i,j)/A(j,j);
      for k = j:n
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end L(i,j) = m;
    end
  end
  U = A;
endfunction

A_1 = [1.012 -2.132 3.104; -2.132 4.096 -7.013; 3.104 -7.013 0.014];
[R N] = lu_cool(A_1)

A_2 = [2.1756 4.0231 -2.1732 5.967;
       -4.0231 6.0000 0 1.1973;
       -1.0000 5.2107 1.1111 0;
       6.0235 7.0000 0 4.1562];

[L_1 U_1] = lu_cool(A_2);
