function l = cholevsky(A)
  n = sqrt(length(A));
  l = zeros(n,n);
  for i = 1:n
    for j = 1:i
      if i == j then
        l(i,i) = A(i,i);
        for k = 1:i-1
          l(i,i) = l(i,i) - (l(i,k))^2;
        end
        l(i,i) = sqrt(l(i,i));
      else
        l(i,j) = A(i,j);
        for k = 1:j-1
          l(i,j) = l(i,j) - l(i,k)*l(j,k);
        end
        l(i,j) = l(i,j)/l(j,j);
      end
    end
  end
endfunction

A = [16 -12 8 -16; -12 18 6 9; 8 -6 5 -10; -16 9 -10 46];
B = [4 12 -16; 12 37 -43; -16 -43 98];
