// Funca para cuando no tiene elementos nulos en la diagonal.
function y = p_banda(A, b, p)
  n = sqrt(length(A));
  // Forma escalonada de A.
  for j = 1:n-1
    for i = j+1:n
      aux = A(i,j)
      m = min(n, j+p);
      disp(m);
      for k = j:m
        A(i,k) = A(i,k) - (aux/A(j,j))*A(j,k);
      end
      b(i) = b(i) - (aux/A(j,j))*b(j);
    end
  end
  // Remonte optimizado.
  for i = n:-1:1
    y(i) = b(i);
    m = min(n, i+1+p);
    for j = i+1:m
      y(i) = y(i) - A(i,j)*y(j);
    end
    y(i) = y(i)/A(i,i);
  end
endfunction

A_2 = [1 3 0 0; 4 5 3 0; 0 2 1 1; 0 0 3 4];
p_banda(A_2, [0 0 1 1], 2);
