function y = simpson_comp(fun, a, b, n)
  // n debe ser par.
  h = (b-a)/n;
  for i = 1:n+1
    x(i) = a + (i-1)*h;
  end
  y = fun(a);
  for i = 2:n
    if (modulo(i, 2) == 0)
      y = y + 4*fun(x(i));
    else
      y = y + 2*fun(x(i));
    end
  end
  y = y + fun(b);
  y = y*h/3;
endfunction

function y = inverse(x)
  y = 1./x;
endfunction

simpson_comp(inverse, 1, 2, 4)

// Simpson Compuesto:
// h/3*[f(x_0)+4*f(x_1)+2*f(x_2)+...+4*f(x_n-1)+f(x_n)]
// Error:
// -(h^4*(b-a))*f^4(c_x)/180
