function y = jacobi(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        if j ~= i then
          y(i) = y(i) - A(i,j)*x(j);
        end
      end
      y(i) = y(i)/A(i,i);
    end
    // Checking the error.
    if (norm(x-y,'inf') <= e) then
      break;
    end
    x = y;
  end
endfunction

function y = gauss_seidel(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
endfunction

A = [10 1 2 3 4; 1 9 -1 2 -3; 2 -1 7 3 -5; 3 2 3 12 -1; 4 -3 -5 -1 15];
b = [12; -27; 14; -17; 12];

jacobi(A, b, [0;0;0;0;0], .000000001)
gauss_seidel(A, b, [0;0;0;0;0], .000000001)
// https://www.wolframalpha.com/input/?i=row+echelon+%7B%7B10%2C+1%2C+2%2C+3%2C+4%2C+12%7D%2C%7B1%2C+9%2C+-1%2C+2%2C+-3%2C+-27%7D%2C%7B2%2C+-1%2C+7%2C+3%2C+-5%2C+14%7D%2C%7B3%2C+2%2C+3%2C+12%2C+-1%2C+-17%7D%2C%7B4%2C+-3%2C+-5%2C+-1%2C+15%2C+12%7D%7D
