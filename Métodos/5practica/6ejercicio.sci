function y = gauss_seidel_diag(A, b, x, p, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Reducing computation for p-diagonal matrix.
        if abs(i-j) < p then
          // Dividing cases for reducing computation.
          if j < i then
            y(i) = y(i) - A(i,j)*y(j);
          else
            if j ~= i then
              y(i) = y(i) - A(i,j)*x(j);
            end
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
endfunction

A = [2 -1 0 0 0; -1 2 -1 0 0; 0 -1 2 -1 0; 0 0 -1 2 -1; 0 0 0 -1 2];
