function y = jacobi(A, b, x, e)
  n = sqrt(length(A));
  c = 0;
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        if j ~= i then
          y(i) = y(i) - A(i,j)*x(j);
        end
      end
      y(i) = y(i)/A(i,i);
    end
    c = c+1;
    // Checking the error.
    if (norm(x-y,'inf') <= e) then
      break;
    end
    x = y;
  end
  disp('Se iteró: ')
  disp(c)
endfunction

function y = gauss_seidel(A, b, x, e)
  n = sqrt(length(A));
  c = 0;
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    c = c+1;
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
  disp('Se iteró: ')
  disp(c);
endfunction

function y = sor(A, b, x, w, e)
  n = sqrt(length(A));
  c = 0;
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)*w/A(i,i)
      y(i) = y(i) + x(i)*(1-w)
    end
    c = c+1;
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
  disp('Se iteró: ')
  disp(c)
endfunction

A = [4 3 0; 3 4 -1; 0 -1 4];
b = [24 30 -24];

jacobi(A, b, [0;0;0], .001)
