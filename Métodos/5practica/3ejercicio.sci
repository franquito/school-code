function y = jacobi(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        if j ~= i then
          y(i) = y(i) - A(i,j)*x(j);
        end
      end
      y(i) = y(i)/A(i,i);
    end
    // Checking the error.
    if (norm(x-y,'inf') <= e) then
      break;
    end
    x = y;
  end
endfunction

function y = gauss_seidel(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    // Checking tolerance.
    if (norm(x-y, 'inf') < e) then
      break;
    end
    x = y
  end
endfunction

A = [0 2 4; 1 -1 1; 1 -1 2];
b_1 = [0; 0.375; 0];
B = [1 -1 0; -1 2 -1; 0 -1 1.1];
b_2 = [0; 1; 0];

jacobi(B,b_2,[0; 0; 0], .01);
gauss_seidel(B,b_2,[0; 0; 0], .01)
// https://www.wolframalpha.com/input/?i=row+echelon+%7B%7B1%2C+-1%2C+0%2C+0%7D%2C%7B-1%2C+2%2C+-1%2C+1%7D%2C%7B0%2C+-1%2C+1.1%2C+0%7D%7D
