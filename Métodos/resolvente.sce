function y = resolvente(a, b, c)
  discriminant = b*b - 4*a*c;
  if b < 0 then
    y(1) = (-b + sqroot(discriminant)) / (2*a);
    y(2) = (2*c)/(-b + sqroot(discriminant));
  else
    y(1) = (2*c)/(-b - sqroot(discriminant));
    y(2) = (-b - sqroot(discriminant)) / (2*a);
  end,
endfunction
