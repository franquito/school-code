clear;
clc;

function xi=newton(f,df,x0,its)
  xi = x0;
  for i=1:its
    xi = xi - inv(df(xi))*f(xi);
  end
endfunction

function r = f(x)
  r(1) = x(1)^2+x(1)*x(2)^3-9;
  r(2) = 3*x(1)^2*x(2) - 4 - x(2)^3;
endfunction

function r = jf(x)
  r(1)(1) = 2*x(1)+x(2)^3;
  r(1)(2) = x(1)*3*x(2)^2;
  r(2)(1) = 6*x(1)*x(2);
  r(2)(2) = 3*x(1)^2-3*x(2)^2;
  // r = numderivative(f, x, 0.0001)
endfunction

newton(f, jf, [1.2; 2.5], 5)
newton(f, jf, [-2; 2.5], 5)
newton(f, jf, [-1.2; -2.5], 5)
newton(f, jf, [2; -2.5], 5)
