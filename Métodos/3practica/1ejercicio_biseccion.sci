// |α - c_n| <= (b_n - a_n)/2 = (b_1 - a_1)/2^n

function y = bisect(fun, a, b, e)
  b_1 = b;
  a_1 = a;
  c = (b + a)/2;
  n = 1;
  while (((b_1-a_1)/2^n)>e)
    if (fun(a)*fun(c)<=0)
      b = c;
    else
      a = c;
    end
    c = (b + a)/2;
    n=n+1;
  end
  y = c;
endfunction

function y = fun1(x)
  y = sin(x)-x.^2/2;
endfunction

function y = fun2(x)
  y = %e.^(-x) - x.^4;
endfunction

function y = fun3(x)
  y = log(x)-x+1;
endfunction

bisect(fun1, 1, 3, 10^(-2))
bisect(fun1, -1, 1, 10^(-2))
bisect(fun2, -2, 0, 10^(-2))
bisect(fun2, 0, 2, 10^(-2))
bisect(fun3, 0, 1.5, 10^(-2))
