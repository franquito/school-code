function y = secante(fun, a, b, n)
  for i = 0:n
    if (a == b) then break end;
    y = a - ((b - a)/(fun(b) - fun(a)))*fun(a);
    a = b;
    b = y;
  end
endfunction

function y = ejercicio3(x)
  y = x^2/4-sin(x)
endfunction

secante(ejercicio3, 1.5, 1.6, 20);

function y = lala(x)
  y = x.^3-3.*x+2;
endfunction
