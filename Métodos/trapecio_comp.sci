function y = trapecio_comp(fun, a, b, n)
  h = (b-a)/n;
  for i = 1:n+1
    x(i) = a+(i-1)*h;
  end
  // Note that:
  // a = x(1), b = x(n+1)
  acum = fun(a)/2;
  for i = 2:n
    acum = acum + fun(x(i));
  end
  acum = acum + fun(b)/2;
  y = h*acum;
endfunction

function y = weird_exp(x)
  y = %e.^(-x.^2);
endfunction

trapecio_comp(weird_exp, 0, 1, 2); //0.7313703
// Real Value: 0.746824
