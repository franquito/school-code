function y = dfa(f, x, h)
  y = (f(x+h) - f(x))./h;
endfunction

function y = sq(x)
  y = x*x
endfunction

function y=Df(f,v,n)
  deff("s=D0f(x)","s=f(x)");
  for i=1:(n-1)
    deff("s=D"+string(i)+"f(x)","s=derivative(D"+string(i-1)+"f,x)");
  end
  deff("s=DNf(x)","s=derivative(D"+string(n-1)+"f,x)");
  y = DNf(v);
endfunction
