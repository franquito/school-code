
// Algoritmo de Gauss modificado con pivoteo parcial.
// Obtiene también la descomposición PLU ;) ;) (con L(i,i)=1)

function [U,c,L,P]=Gauss(A,b)
  n = length(b);
  L=zeros(n,n);
  permut(n) = 0;
  for i=1:n permut(i) = i; end
  
  
  for fila=1:n
    // Pivoteo
    if abs(A(fila,fila))<1e-10 then
    maxp = fila;
    for i=fila+1:n
      if (abs(A(i,fila)) > abs(A(maxp,fila))) maxp = i; end
    end
    if (maxp <> fila)
      for j=fila:n
        vaux = A(maxp,j);
        A(maxp,j) = A(fila,j);
        A(fila,j) = vaux;
      end
      for j=1:fila
        vaux = L(maxp,j);
        L(maxp,j) = L(fila,j);
        L(fila,j) = vaux;
      end      
      vaux = b(maxp);
      b(maxp) = b(fila);
      b(fila) = vaux;
      vaux = permut(maxp);
      permut(maxp) = permut(fila);
      permut(fila) = vaux;
    end
    end
    
    // Eliminacion
    for i=fila+1:n
      m = A(i,fila) / A(fila,fila);
      L(i,fila) = m;
      for j=fila:n
        A(i,j) = A(i,j) - A(fila,j)*m;
      end
      b(i) = b(i) - b(fila)*m
    end  
  end
  
  P = zeros(n,n);
  for i=1:n 
    P(permut(i),i)=1;
  end
  for i=1:n
      L(i,i)=1;
  end
  
  
  U=A;
  c=b;
  return;
endfunction

// Resuelve el sistema U*x = c, siendo U triangular superior con diagonal no nula (inversible)
function s=RegresivaSuperior(U,c)
  n = length(c);
  for i=n:-1:1
    s(i) = c(i);
    for j=n:-1:i+1
      s(i) = s(i) - U(i,j)*s(j);
    end
    s(i) = s(i) / U(i,i);
  end
endfunction

// Resuelve el sistema L*x = c, siendo L triangular inferior con diagonal no nula (inversible)
function s=RegresivaInferior(L,c)
  n = length(c);
  for i=1:n
    s(i) = c(i);
    for j=1:i-1
      s(i) = s(i) - L(i,j)*s(j);
    end
    s(i) = s(i) / L(i,i);
  end
endfunction

