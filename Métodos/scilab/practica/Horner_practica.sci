function r = b(a,n,i,x)  //b(a,n,i,x) devuelve el b_i correspondiente al polinomio cuyos coeficientes son a(1),...,a(n).
  if i==n then
    r = a(n);
  else
    r = a(i) + x*b(a,n,i+1,x);
  end
endfunction

function y = hornerPoly(a,x0) 
  n1 = length(a);   //El grado del polinio mas uno.
  y = b(a,n1,1,x0); //b0 = b(a,n1,1,x) porque a_0 = a(1) ... a_i = a(i+1)
endfunction         

function y = hornerPolyDP(a,x0) 
  y(1) = hornerPoly(a,x0); //p(x0).
  n1 = length(a);
  for i=1:n1-1
  co(i) = b(a,n1,i+1,x0)
  end
  y(2) = hornerPoly(co,x0);
endfunction
