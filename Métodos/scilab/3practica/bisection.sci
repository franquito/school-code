clear;
clc;

function y = bisection(fun, a, b, tolerance)
  n = 1;
  _a = a; _b = b;
  while %t
    c = (b+a)/2;
    if fun(a)*fun(c) <= 0 then
      b = c;
    else // fun(b)*fun(c) <= 0
      a = c;
    end

    if (_b-_a)/2^n <= tolerance then
      break;
    end
    n = n + 1;
  end
  y = c;
endfunction

function y = apartado_a(x)
  y = cos(x) - x^2/2;
endfunction

bisection(apartado_a, -%pi, 0, .01)
bisection(apartado_a, 0, %pi, .01)

function y = apartado_b(x)
  y = %e^(-x)-x^4;
endfunction

bisection(apartado_b, -%pi, 0, .01)
bisection(apartado_b, 0, %pi, .01)

function y = apartado_c(x)
  y = log(x)+1-x;
endfunction

bisection(apartado_c, .001, .5, .01)
bisection(apartado_c, .5, 1, .01)
