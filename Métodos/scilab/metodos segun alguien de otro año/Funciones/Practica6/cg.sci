//Función que encuentra los centros y radios de los círculos de Gerschgorin (en caso de círculos superpuestos, cambia el espesor de ellos en el gráfico)
//Parámetros:
//      A - matriz cuadrada a coeficientes reales
//      p - Color en que se quiere graficar los círculos (2: azul; 5: rojo)

function [c,r] = cg(A,p)
  [m,n] = size(A)

  if (m<>n) then
        error('La matriz debe ser cuadrada')
  end

  for i=1:m
    c(i) = A(i,i)   //c es un vector que contiene los centros
    suma = 0
    for j=1:n
      suma = suma + abs(A(i,j))
    end
    r(i) = suma - abs(A(i,i))   //r es un vector que contiene los radios
  end

  t=ones(m,1)
  for i=1:m
    for j=1:m
      if i<>j & c(i)==c(j) & r(i)==r(j) then
        t(i)=t(i)+1  //t es un vector que contiene el espesor de los círculos para graficar
      end
    end
  end

  maxx = max(c+r)
  minx = min(c-r)
  maxy = max(r)
  miny = -max(r)

  plot2d(0,0,rect=[minx-1,miny-1,maxx+1,maxy+1])
  xgrid(1)

  for i=1:n
    if (r(i) <> 0) then
      x(i) = c(i)-r(i)
      y(i) = r(i)
      w(i) = 2*r(i)
      h(i) = 2*r(i)
    end
  end


  for i=1:n
    if (r(i) == 0) then
     plot(c(i),0,'.b')
    else
      xarc(x(i),y(i),w(i),h(i),0,360*64)
      arc=get("hdl")
      arc.thickness=t(i)
      arc.foreground=p;
    end
  end
    
endfunction
