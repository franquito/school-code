//Función que encuentra los centros y radios de los círculos de Gerschgorin de la matriz (en azul) y de su transpuesta (en rojo)
//      A - matriz cuadrada a coeficientes reales

function [c1,r1,c2,r2]=cg2(A)
    [c1,r1]=cg(A,2);
    [c2,r2]=cg(A',5);
endfunction