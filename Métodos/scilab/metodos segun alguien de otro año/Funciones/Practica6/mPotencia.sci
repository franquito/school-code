//Método de la Potencia
//    Suponiendo que hay un único autovalor de módulo máximo, la función devuelve
//    una aproximación de dicho autovalor y de su autovector asociado
//Parámetros:
//            A  -  matriz cuadrada simétrica con un único autovalor de módulo máximo
//           z0  -  vector de aproximación inicial (¡¡No debe ser nulo!!)
//         maxit -  número máximo de iteraciones a realizar
//          tol  -  tolerancia máxima aceptada

function [aval,avec]=mPotencia(A,z0,maxit,tol)
    [m,n]=size(A)
    if (m <> n) then
        error('La matriz debe ser cuadrada')
    end
    if (A <> A') then
        error('La matriz debe ser simétrica')
    end
    [mz0,nz0]=size(z0)
    if ([mz0,nz0] <> [m,1]) then
        error('El vector de aproximacion inicial no tiene las dimensiones correctas')
    end
    if (z0 == zeros([m,1])) then
        error('El vector inicial no puede ser nulo')
    end
    
    for i=1:maxit
        w=A*z0;
        z1=w/norm(w,'inf');
        k=1
        while ((z0(k) == 0) & (k < n))
            k=k+1
        end
        if (z0(k) == 0) then
          error('El proceso se detuvo en'+string(i)+'iteraciones porque el vector es nulo. Error de division por 0.')
        end
        
        lambda=w(k)/z0(k);
        
        if (norm(z0-z1,'inf') <= tol) then
            break;
        end
        
        z0=z1
    end
    aval=lambda
    avec=z1
    
    disp('Se realizaron '+string(i)+' iteraciones.')
    
endfunction