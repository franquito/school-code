function ans=PALUsol(A,b)
    [nr nc] = size(A);
    
    if (nr <> nc) then
        error("La matriz no es cuadrada.");
    end
    
    [P,L,U]=PALU(A)
    
    b=P*b
    
    g = sustAdelante(L, b);
    ans = sustAtras(U, g);
endfunction
