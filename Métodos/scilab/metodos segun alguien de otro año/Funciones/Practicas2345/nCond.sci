//Función que calcula el número de condición de una matriz
//K2 se calcula usando la norma l2
//K1 se calcula usando la norma l1
//Kinf se calcula usando la norma infinito
//Parámetros:
//      A - matriz
function [K2,K1,Kinf]=nCond(A)
    K2=norm(A)*norm(inv(A))
    K1=norm(A,1)*norm(inv(A),1)
    Kinf=norm(A,'inf')*norm(inv(A),'inf')
endfunction
