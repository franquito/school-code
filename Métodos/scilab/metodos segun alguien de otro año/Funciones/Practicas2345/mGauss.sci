// Método de Gauss:
// Parámetros: 
//    A - Matriz de coeficientes
//    b - Vector de términos independientes

function ans = mGauss(A,b)
    [M, v] = eGauss(A, b);
    ans = sustAtras(M, v);
endfunction

//-----------------------------------------------------------------------------
