// Eliminación de Gauss con pivote parcial:
// Parámetros: 
//    A - Matriz a escalonar
//    b - Vector de términos independientes

function [M,v] = eGaussCP(A,b) 
    [nr nc] = size(A);
    [bm bn] = size(b);
    
    if (nr <> nc) then
        error("La matriz no es cuadrada.");
    end
    if (bm <> nr | bn <> 1) then
        error("El vector no tiene la dimensión correcta.")
    end
    
    A = [A b];
    
    for (k = 1 :nr-1),
        [q, c] = max(abs(A(k : nr, k)));
        c = c + k - 1;
        t = A(k, :);
        A(k, :) = A(c, :);
        A(c, :) = t;
        for (i = k+1 : nr),
            m = A(i, k) / A(k, k);
            for (j = k : nc+1),
                A(i, j) = A(i, j) - m * A(k, j);
            end
        end
    end
    
    M = A(:, 1 : nc);
    v = A(:, nc+1);
    
endfunction

//-----------------------------------------------------------------------------
