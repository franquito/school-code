// Mínimos Cuadrados
// Parámetros:
//    P - matriz con vectores fila representando [x, y]

function minCuadrados(P)
    A(:, 2) = P(:, 1);
    A(:, 1) = 1;

    b = P(:,2);

    x = resolQR(A,b);
    ord = x(1);
    pend = x(2);

    disp("Aproximación lineal:  "+string(pend)+"x + "+string(ord));
    
    deff("y = f(x)","y = "+string(pend)+"*x + "+string(ord));
    
    minV = min(P(:,1)) -1;
    maxV = max(P(:,1)) +1;
    
    x = [minV:0.01:maxV];
plot2d(P(:,1),P(:,2), -4);
plot2d(x, f(x), 2);
       
endfunction

//-----------------------------------------------------------------------------