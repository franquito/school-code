// Resolución de sistemas con factorización LU:
// Parámetros: 
//    A - Matriz de coeficientes
//    b - Vector de términos independientes

function ans = LUsol(A, b)
    [L, U] = LU(A);
    g = sustAdelante(L, b);
    ans = sustAtras(U, g);

endfunction

//-----------------------------------------------------------------------------
