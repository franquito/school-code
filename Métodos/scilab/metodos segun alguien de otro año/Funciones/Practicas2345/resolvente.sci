// Hayar las dos raíces de una ecuación cuadrática aplicando la resolvente:
// Parámetros: 
//    a,b,c - parámetros de la función cuadrática

function [x,y] = resolvente(a,b,c)
    x=(-b+sqrt(b^2-4*a*c))/(2*a)
    y=(-b-sqrt(b^2-4*a*c))/(2*a)
    
endfunction

//-----------------------------------------------------------------------------
