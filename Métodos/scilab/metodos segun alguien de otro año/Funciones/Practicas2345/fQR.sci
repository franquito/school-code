//Factorizacion QR
//Parámetros:
//      A - matriz a factorizar
//Nota: Q es ortonormal y es base de span(A) (Q*Q'=I)
//Nota: R es triangular superior y también se puede calcular haciendo Q'*A

function [Q,R]=fQR(A)
    [m,n]=size(A)
    if rank(A) <> n then
        error('Las columnas de A deben ser LI')
    else
        if A*A'==eye(m) then    //Caso en el que A ya es ortonormal
            Q=A
            R=eye(n)
        else
            Q=zeros(m,n)
            nu=zeros(n)
            nu(1)=norm(A(:,1))
            Q(:,1)=(A(:,1)/nu(1))
            for k=2:n
                suma=0
                for i=1:k-1
                    suma=suma+((A(:,k)'*Q(:,i))*(Q(:,i)))
                end
                nu(k)=norm((A(:,k))-suma)
                Q(:,k)=((A(:,k)-suma)/nu(k))
            end
            R=zeros(n,n)
            for i=1:n
                R(i,i)=nu(i)
            end
            for j=1:n-1
                for k=j+1:n
                    R(j,k)=A(:,k)'*Q(:,j)
                end
            end
        end
    end
endfunction
    
//Resolución de sistemas mediante factorización QR
//Si A es no singular, Q'=inv(Q)
//Ax=b <-> QRx=b <-> Rx=Q'b
//Si rank(A)=n (sol. única) tambien resuelve minimos cuadrados donde
//E(i)=abs(f(ti)-bi)=abs(a'+b'*ti-bi)
//E=Ax-b, A=[1t1;1t2;...;1tm],b=[b1;...;bn],x=[a',b']
//El conjunto solucion de mínimos cuadrados es A'*Ax=A'*b

function sol=resolQR(A,b)
    [m,n]=size(A)
    [bm,bn]=size(b)
    //if det(A) == 0 then
   //     error('La matriz debe ser no singular')
   // else
        if [bm,bn] <> [m,1] then
            error('El vector b tiene dimensiones incorrectas')
        else
            [Q,R]=fQR(A)
            nb=Q'*b
            x=zeros(n)
            x(n) = nb(n) / R(n, n);
            for (k = n-1 : -1 : 1)
                x(k) = (nb(k) - R(k,k+1 : n) * x(k+1 : n)) / R(k,k);
            end
        end
   // end
    sol=x
endfunction
