// Deteriminante utilizando U:
// Parámetros: 
//    A - Matriz original

function ans = LUdet(A)
    [L, U] = LU(A);
    [nr, nc] = size(U);
    
    ans = 1;
    for (i = 1 : nr),
        ans = ans * U(i, i);
    end
    
endfunction

//-----------------------------------------------------------------------------
