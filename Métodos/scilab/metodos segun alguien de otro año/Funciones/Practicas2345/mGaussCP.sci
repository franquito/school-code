// Método de Gauss con Pivoteo:
// Parámetros: 
//    A - Matriz de coeficientes
//    b - Vector de términos independientes

function ans = mGaussCP(A, b)
    [M, v] = eGaussCP(A, b);
    ans = sustAtras(M, v);
endfunction

//-----------------------------------------------------------------------------
