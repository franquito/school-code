//Algoritmo de Thomas
//Variante del Método de Gauss para matrices tridiagonales
//Parámetros:
//  a - vector correspondiente a la diagonal inferior de la matriz de coeficientes
//  d - vector correspondiente a la diagonal principal de la matriz de coeficientes
//  c - vector correspondiente a la diagonal superior de la matriz de coeficientes
//  b - vector de términos independientes
//NOTA: Éste método es sólo aplicable a matrices estríctamente diagonal dominantes (ver función auxiliar)
//

function y=mThomas(a,d,c,b)

//Comprobaciones
    [dm,dn]=size(d)
    [am,an]=size(a)
    [cm,cn]=size(c)
    [bm,bn]=size(b)
    if (dn <> 1 | an <> 1 | cn <> 1 | bn <> 1) then
        error('Las entradas deben ser vectores columna')
    end
    if (dm <> bm) then
        error('Los vectores d y b deben tener igual longitud')
    end
    if(am <> dm-1 | cm <> dm-1) then
        error('a y c deben tener un elemento menos que d')
    end
    for i=1:n
        w=abs(a(i))+abs(c(i))
        if (d(i) <= w) then
            error('La matriz debe ser diagonal estríctamente dominante')
        end
    end
//Escalonamiento
    for k=1:n-1
        if (d(k) == 0) then
            error('Sistema sin solución')
        else
            d(k+1)=d(k+1)-a(k+1)*c(k)/d(k)
            b(k+1)=b(k+1)-a(k+1)*b(k)/d(k)
        end
    end
//Sustitución Regresiva
    if (d(n) == 0) then
        error('El sistema no tiene solución')
    else
        x(n)=b(n)/d(n)
        for k=n-1:-1:1
            x(k)=(b(k)-c(k)*x(k+1))/d(k)
        end
    end
    y=x
endfunction
