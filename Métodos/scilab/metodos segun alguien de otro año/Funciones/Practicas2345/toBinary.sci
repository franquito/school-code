//Conversor de decimal a número en punto flotante IEEE754 Precisión Simple por truncamiento
//Parámetros:
//  n - Número decimal

function ans=toBinary(n)
if n==0 then
   ans=zeros(1,32)
 else
  
  //Signo
  if n < 0 then
        s=1;
    else
        s=0
  end
 
  n=abs(n);
  
  //Exponente
  ex=0;
  while 2^ex<n,
        ex=ex+1;
  end
  ex=ex+126;

  e=zeros(1,8);
  tmp=ex;
  for i = 8:-1:1,
        if tmp/2 <> int(tmp/2) then
          resto=1;
          cociente=(tmp-1)/2;
        else
          resto=0;
          cociente=tmp/2;
        end
        e(i)=resto;
        tmp=cociente;
  end
  
  //Mantisa
  man=n/(2^(ex-127))-1;
  m=zeros(1,23);
  tmp=man;
  for i = 1:23,
        m(i)=int(2*tmp);
        tmp=2*tmp-int(2*tmp);
  end
  
  disp('La conversion a binario en punto flotante ieee754 32 bits es: ')
  ans=zeros(1,32)
  tmp=[s e m];
  for i=1:32,
        ans(i)=tmp(i)
  end
  
end
endfunction

