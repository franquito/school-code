//Factorización PA=LU
//P matriz de permutación
//L matriz triangular inferior con unos en la diagonal
//U matriz triangular superior
//Parámetros:
//      A - matriz a factorizar

function [P,L,U] = PALU(A) 
    [nr nc] = size(A);
    
    if (nr <> nc) then
        error("La matriz no es cuadrada.");
    end
    
    P=eye(A)
    L=eye(A)
    
    for (k = 1 :nr-1),
        [q, c] = max(abs(A(k : nr, k)));
        c = c + k - 1;
        t = A(k, :);        //intercambio de filas en A
        A(k, :) = A(c, :);
        A(c, :) = t;
        p = P(k, :);        //intercambio de filas en P
        P(k, :) = P(c, :);
        P(c, :) = p;
        for (i = k+1 : nr),
            m = A(i, k) / A(k, k);
            L(i,k) = m;
            for (j = k : nc),
                A(i, j) = A(i, j) - m * A(k, j);
            end
        end
    end
    
    U = A
    
endfunction
