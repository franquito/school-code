//Función que calcula las matrices de los métodos Jacobi y Gauss Seidel
//Parámetro:
//      A  -  matriz de coeficientes del sistema Ax=b

function [MJ,MGS]=matMetodos(A)
    [m,n]=size(A)
    if (m <> n) then
        error('La matriz A debe ser cuadrada')
    else
        NJ=eye(A).*A
        NGS=tril(A)
        if (det(NJ) == 0 | det(NGS) == 0) then
            error('No se pueden invertir la matrices N. Intente cambiar el orden de las ecuaciones.')
        else
            MJ=eye(A)-inv(NJ)*A
            MGS=eye(A)-inv(NGS)*A
        end 
    end
endfunction
//------------------------------------------------------------------------------


//Función que verifica si una matriz es diagonal dominante
//Parámetro:
//      A  -  matriz cuadrada

function dd=diagonalDominante(A)
    [m,n]=size(A)
    if m <> n then
        error('La matriz A debe ser cuadrada')
    end
    for i=1:n
        s = sum(abs(A(i,:)))-abs(A(i,i))
        if (abs(A(i,i)) > s) then
            d(i)=%T
        else
            d(i)=%F
        end
    end
    if (and(d) == %T) then
        dd=%T
    else
        dd=%F
    end
endfunction
//-----------------------------------------------------------------------------


//Función que verifica la convergencia de los métodos iterativos para la
//resolución de sistemas de ecuaciones lineales
//Parámetro:
//      A  -  matriz cuadrada de coeficientes del sistema Ax=b

function [J,GS]=convergenciaIt(A)
    //Determinación de convergencia de métodos J y GS
    //usando el criterio de la diagonal dominante
    if (diagonalDominante(A) == %T) then
        disp('1) La matriz es diagonal dominante')
        j(1)=%T
        g(1)=%T
    else
        disp('1) La matriz NO es diagonal dominante')
        j(1)=%F
        g(1)=%F
    end
    //Determinación de la convergencia del método de Jacobi usando
    //el criterio de la norma de la matriz del sistema
    [MJ,MGS]=matMetodos(A)
    if (norm(MJ) < 1 | norm(MJ,1) < 1 | norm(MJ,'inf') < 1) then
        disp('2J) La norma de la matriz del método de Jacobi es menor a 1')
        j(2)=%T
    else
        disp('2J) La norma de la matriz del método de Jacobi es mayor o igual a 1')
        j(2)=%F
    end
    //Determinación de la convergencia del método de Gauss Seidel
    //usando el criterio de la norma de la matriz del sistema
    if (norm(MGS) < 1 | norm(MGS,1) < 1 | norm(MGS,'inf') < 1) then
        disp('2GS) La norma de la matriz del método de GS es menor a 1')
        g(2)=%T
    else
        disp('2GS) La norma de la matriz del método de GS es mayor o igual a 1')
        g(2)=%F
    end
    //Determinación de la convergencia del método de Jacobi usando
    //el criterio del radio espectral de la matriz del sistema
    if (max(abs(spec(MJ))) < 1) then
        disp('3J) El radio espectral de la matriz del método de Jacobi es < 1')
        j(3)=%T
    else
        disp('3J) El radio espectral de la matriz del método de Jacobi es > 1')
        j(3)=%F
    end
    //Determinación de la convergencia del método de Gauss Seidel usando
    //el criterio del radio espectral de la matriz del sistema
    if (max(abs(spec(MGS))) < 1) then
        disp('3GS) El radio espectral de la matriz del método de Jacobi es < 1')
        g(3)=%T
    else
        disp('3GS) El radio espectral de la matriz del método de Jacobi es > 1')
        g(3)=%F
    end
    J = or(j)
    GS = or(g)
    if (J == %T) then
        disp('El metodo de Jacobi converge a la solucion del sistema Ax=b para cualquier est. inicial')
    else
        disp('La convergencia del metodo de Jacobi no esta garantizada')
    end
    if (GS == %T) then
        disp('El metodo de GS converge a la solucion del sistema Ax=b para cualquier est. inicial')
    else
        disp('La convergencia del metodo de GS no esta garantizada')
    end
endfunction
