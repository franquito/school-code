// Factorización LU:
// Parámetros: 
//    A - Matriz a factorizar

function [L,U] = LU(A)
    L = eye(A);
    [nr nc] = size(A);
    n = 0;

    if (nr<>nc) then
        error("La matriz no es cuadrada.");
    end

    for (k = 1:nr),
        for (i = k+1:nr),
            m = A(i,k)/A(k,k);
            L(i,k) = m;
            for j = k:nc,
                A(i,j) = A(i,j) - m*A(k,j);
                n = n + 1;
            end
        end
    end
    U=A;     

endfunction

//-----------------------------------------------------------------------------