//Resolución práctica 4 ejercicio 8
//La función calcula un vector no nulo z tal que Uz=0, donde
//U es una matriz cuadrada de nxn triangular superior con U(i,i)<>0 para i=1..n-1

function z=resol48(U)
    [nr nc]=size(U);
    
    if nr<>nc then
        error("La matriz no es cuadrada.");
    end

    for k=1:nr-1,
        if U(k+1:nr,k)<>zeros(nr-k,1) then
            error("La matriz no es triangular");
        end
    end
    
    b=zeros(nc,1)
    z=zeros(nr,1)
    if (U(nr,nr)<>0) then
        error('Para que z sea no nulo U(nr,nr)debe ser == 0')
    else
        z(nr)=rand()
        while (z(nr) == 0),
            z(nr)=rand()
        end
        
        for k=nr-1:-1:1,
          s=0;
            for i=k:nr,
                s=s+U(k,i)*z(i);
            end
          z(k)=(b(k)-s)/U(k,k);
        end
    end
endfunction
