//Error de la interpolación polinómica
//Parámetros:
//        x  -  vector columna con los valores conocidos de x
//        f  -  función a aproximar
//        val  -  punto en el que queremos una aproximación del valor de la función
//        n  -  grado máximo del polinomio interpolante
// Fórmula del error:
//        e(x) = f(x) - Pn(x) = ((x-x0)...(x-xn)/(n+1)!)*f^(n+1)(cx)
//                              con cx entre a y b.



function ans = eIntPol(x, f, val, n)
    e = 1;
    for i=1:n
        e = e*(val - x(i));
    end
    
    e = abs(e/factorial(length(x)+1));
    vec = [min(x):0.001:max(x)];
    
    for i=1:length(vec)
        vec(i) = derivar(f, vec(i), n+1);
    end
    
    z = max(abs(vec));
    ans = e*z;
    
endfunction