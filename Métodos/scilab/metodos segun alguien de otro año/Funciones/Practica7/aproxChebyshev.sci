//Aproximación mediante Polinomios de Chebyshev

//Esta función utiliza las raices del polinomio de Chebyshev de grado g para
//encontrar los nodos {x0...xg} que minimizan max(abs(f(x)-Pg(x))) en el
//intervalo [x0,xg] donde f(x)-Pg(x) = (((x-x0)*...*(x-xg))/(n+1)!)*(deriv(n+1) de f)(cx)

//Este método reduce el error causado por el Fenómeno de Runge para nodos
//uniformemente espaciados.


//Pasos:

//(1º)
//Gráfica del polinomio de Chebyshev de grado g para visualizar raices

//Intervalo [-1,1]
//Parámetros:
//        g  -  grado del polinomio de aproximación

function Chebyshev(g)
    
    deff('y=T(x)','y=cos('+string(g)+'*(acos(x)))')
    
    x=[-1:0.0001:1]
    
    minx=-1.05
    miny=min(T(x))-0.5
    maxx=1.05
    maxy=max(T(x))+0.5
    
    plot2d(0,0,rect=[minx,miny,maxx,maxy])
    
    xgrid(1)
    
    plot2d(x,zeros(x))
    
    plot2d(x,T(x),2)
    
endfunction

//(1º ')
//Si se desea aplicar en un intervalo [a,b] diferente de [-1,1] los nodos serán

//t=((b+a)+x(b-a))/2 donde x es la raíz en [-1,1]

//Luego f(x)=g(t), x perteneciente a [-1,1]

//(2º)
//Búsqueda de raices por métodos de bisección, Newton-Raphson, secante,
//falsa posición, punto fijo, etc.

//(3º)
//Búsqueda del vector y que corresponde a los valores de la función dada en los nodos

//(4º)
//Hallar el polinomio de interpolación con los datos dados mediante los métodos
//de Lagrange o Newton