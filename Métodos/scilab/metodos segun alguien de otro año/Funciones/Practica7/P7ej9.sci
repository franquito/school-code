// Ejercicio 9 - Práctica 7

function P7ej9(m)
    
    in = [-5:(10/m):5];    // intervalo uniformemente espaciado
    deff("y = f(x)", "y = 1/(1+x^2)");
    
    y = feval(in, f);
    
    p = poly(minimosCuadradosCoeff(in, y, m), "x", "coeff");
    
    g = [-5:0.05:5];
    
    plot2d(g, feval(g, f)-horner(p, g));
endfunction