//Aproximación Polinomial de Mínimos Cuadrados

//Obs.: Esta función es para el caso particular de aproximación por polinomios
//      Para aproximar utilizando otras funciones hay que modificar las columnas

//Parámetros:
//        x  -   vector columna
//        y  -   vector columna de igual dimensión que x
//        g  -   grado del polinomio de aproximación
//        flag - bandera para realizar o no el gráfico

function  sol=minCuadPoli(x, y, g, flag)
    
   [mx,nx]=size(x)
   [my,ny]=size(y)
    
    if g = mx then
        error('El grado especificado es incorrecto')
    end
    
    p=g+1
    
    if ((nx <> 1) | (ny <> 1)) then
        error('Los vectores x e y deben ser columna')
    end
    
    if (mx <> my) then
        error('Las dimensiones de x e y deben ser iguales')
    end
    
    A=zeros(mx,p)
    
    for r=1:p
        A(:,r) = x^(r-1)
    end
    
    B=y
    
    s=A\B
    
    sol=poly(s,'x','coeff')
   
   if flag then 
        //Gráfico de los puntos a aproximar y del polinomio de aproximación
        minx=min(x)
        miny=min(y)
        maxx=max(x)
        maxy=max(y)
        
        for i=1:mx
            plot(x(i),y(i),'.')
        end
        xe = [minx:0.001:maxx];
        plot2d(xe,horner(sol,xe))  
    end
endfunction