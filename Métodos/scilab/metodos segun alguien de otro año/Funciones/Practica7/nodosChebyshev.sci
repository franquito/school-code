//Función que calcula las raices del polinomio de Chebyshev de grado g

//Parámetros:
//        g  -  grado del polinomio de Chebyshev

function x=nodosChebyshev(g)
    
    if (g <= 0) then
        error('El grado del polinomio debe ser mayor o igual a 1')
    end
    
    for i=1:g
        x(i)=cos((%pi/2)*((2*i-1)/g))
    end
    
endfunction