//Error medio de aproximacion polinomial por mínimos cuadrados

//Parámetros:
//        x  -  vector columna
//        y  -  vector columna de igual dimensión que x
//        m  -  grado del polinomio de aproximación

function ans = eMinCuad(x, y, m)
    p = mincuadPoli(x, y, m, %F);
    z = horner(p, x);
    e = y - z;
    e = e^2;
    ans = sqrt((1/m)*sum(e));
endfunction