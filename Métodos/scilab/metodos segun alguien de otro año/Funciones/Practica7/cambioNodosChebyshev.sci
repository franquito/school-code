// Cambio lineal de intervalo para nodos de Chebyshev.
// Calcula los nodos del polinomio de Chebyshev para intervalos
// distintos de [-1:1]
// Parámetros:
//     a,b - extremos del polinomio.
//     g - grado del polinomio.

function ans = cambioNodosChebyshev(a, b, g)
    if (g <= 0) then
        error('El grado del polinomio debe ser mayor o igual a 1');
    end
    
    deff("y = f(x)","y = ((b+a)+x*(b-a))/2");
    
    x = nodosChebyshev(g);
    
    ans = feval(x, f);
    
endfunction