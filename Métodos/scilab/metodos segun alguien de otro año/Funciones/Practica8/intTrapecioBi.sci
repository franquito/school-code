//Regla del Trapecio para funciones de dos variables (devuelve string dependiente de x)

//Función que aplica la regla del Trapecio con n subintervalos de igual longitud
//Aproxima la función mediante polinomios de interpolación (Lagrange) de grado n.

//Obs.: Para el método simple se toma 1 subintervalo

//Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        f  -  función a integrar. Debe ser string.
//        v  -  variable de integración.
//        n  -  número de subintervalos usados al aplicar la regla.

function sol=intTrapecioBi(a,b,f,v,n)
  
    if (v=="y") then
      f=strsubst(f,"y","z")
      f=strsubst(f,"x","y")
      f=strsubst(f,"z","x")
    end
    
    h=(b-a)/n    //Longitud de los intervalos
    
    //Definición de los n+1 puntos
    x0=a
    
    for i = 1:n
        x(i)=a+i*h
    end
    
    //Aplicación de la regla en los n+1 puntos
    T="("+string(1/2)+"*"+strsubst(f,"x",string(x0))
    
    for i=1:n-1
        T = T +"+"+ strsubst(f,"x",string(x(i)))
    end
        
    T = T +"+"+ string(1/2)+"*"+strsubst(f,"x",string(x(n)))+")"
        
    T = T +"*"+ string(h)
        
    T=strsubst(T,"y","x")
        
    sol = T
endfunction