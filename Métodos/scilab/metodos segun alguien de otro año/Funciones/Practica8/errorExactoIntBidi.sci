// Cota exacta del error para integración bidimensional
// Parámetros:
//        a  -  punto inicial intervalo de integración sobre x.
//        b  -  punto final intervalo de integración sobre x.
//        c  -  punto inicial intervalo de integración sobre y.
//        d  -  punto final intervalo de integración sobre y.
//        cotax  -  cota de la derivada cuarta respecto a x de la función a integrar.
//        cotax  -  cota de la derivada cuarta respecto a y de la función a integrar.
//        n  -  número de subintervalos usados al aplicar la regla.

function ans = errorExactoIntBidi(a, b, c, d, cotax, cotay, n)
    hx = (b-a)/n;
    hy = (d-c)/n;
    ans = (hx^4*(b-a)*hy^4*(d-c)/180^2)*abs(cotax)*abs(cotay);
endfunction