// Número mínimo de intervalos equiespaciados para que el error de integración
// mediante el método del Trapecio sea menor a la cota dada
// 
//Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        maxder  -  cota de la derivada segunda de la función a integrar.
//        cota  -  cota del error de integración.

function ans = pasoTrapecio(a,b,maxder,cota)
    
    d = (12*cota)/(((b-a)^3)*maxder)
    
    pasomin=1/sqrt(d)
    
    pT=ceil(pasomin)
    
    if (pT == pasomin) then    //n es mayor estricto que pasomin
        pT=pT+1
    end
    
    disp('El paso minimo para obtener un error menor que '+string(cota)+' es '+string(pT)+'.')

    ans=pT

endfunction