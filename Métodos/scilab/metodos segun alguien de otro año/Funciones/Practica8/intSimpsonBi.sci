//Regla de Simpson para funciones de dos variables (devuelve string dependiente de x)

//Función que aplica la regla de Simpson con n subintervalos de igual longitud
//Aproxima la función mediante polinomios de interpolación de grado n.

//Obs.: Para el método simple se toman 2 subintervalos

//Parámetros:
//        a  -  punto inicial del intervalo de integración
//        b  -  punto final del intervalo de integración
//        f  -  función a integrar. Debe ser string.
//        v  -  variable de integración.
//        n  -  números de subintervalos usados al aplicar la regla.
//              Debe ser entero no negativo y par.

function sol=intSimpsonBi(a,b,f,v,n)
    
    if (modulo(n,2) <> 0) then
        error('El numero de intervalos debe ser par.')
    end
    
    if (v=="y") then
      f=strsubst(f,"y","z")
      f=strsubst(f,"x","y")
      f=strsubst(f,"z","x")
    end
    
    h=(b-a)/n        //Longitud de los intervalos
    
    //Definición de los n+1 puntos
    x0=a
    
    for i = 1:n
        x(i)=a+i*h
    end
    
    s="("
    //Aplicación de la regla en los n+1 puntos
    s=s + strsubst(f,"x",string(x0))
    
    for i = 1:n-1
      
        if (modulo(i,2) <> 0) then
            s = s +"+"+ string(4) +"*"+ strsubst(f,"x",string(x(i)))
        else
            s = s +"+"+ string(2) +"*"+ strsubst(f,"x",string(x(i)))
        end
    end
    
    s = s +"+"+ strsubst(f,"x",string(x(n))) +")"
        
    s = s +"*"+ string(h/3)
    
    s=strsubst(s,"y","x")
    
    sol = s

endfunction