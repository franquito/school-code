// Número mínimo de intervalos equiespaciados para que el error de integración
// mediante el método de Simpson sea menor a la cota dada
// 
//Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        maxder  -  cota de la derivada cuarta de la función a integrar.
//        cota  -  cota del error de integración.

function ans = pasoSimpson(a,b,maxder,cota)
    
    d = (180*cota)/(((b-a)^5)*maxder)
    
    pasomin=1/d^(1/4)
    
    pS=ceil(pasomin)
    
    if (pS == pasomin) then    //n es mayor estricto que pasomin
        pS=pS+1
    end
    
    if (modulo(pS,2) <> 0) then //El número de intervalos para el método de Simpson es par
        pS=pS+1
    end
    
    disp('El paso minimo para obtener un error menor que '+string(cota)+' es '+string(pS)+'.')

    ans=pS

endfunction