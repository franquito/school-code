// Estimación del error del método de Simpson.
// Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        f  -  función a integrar. Debe ser string.
//        n  -  número de subintervalos usados al aplicar la regla.

// Obs: para el método simple usar n=2
// Obs: depende de derivar.sci!!!

function ans = errorEstimadoSimpson(a, b, f, n)
    h = (b-a)/n;
    ans = (-h^4/180)*(derivar(f, b, 3)-derivar(f, a, 3));
endfunction