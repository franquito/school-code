// Error exacto del método compuesto de Simpson.
// Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        cota  -  cota de la derivada cuarta de la función a integrar.
//        n  -  número de subintervalos usados al aplicar la regla.

// Obs: para el método simple usar n=2

function ans = errorExactoSimpson(a, b, cota, n)
    h = (b-a)/n;
    ans = (-h^4*(b-a)/180)*abs(cota);
endfunction