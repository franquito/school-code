//Regla del Trapecio

//Función que aplica la regla del Trapecio con n subintervalos de igual longitud
//Aproxima la función mediante polinomios de interpolación (Lagrange) de grado n.

//Obs.: Para el método simple se toma 1 subintervalo

//Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        f  -  función a integrar. Debe ser string.
//        n  -  número de subintervalos usados al aplicar la regla.

function sol=intTrapecio(a,b,f,n)
    
    deff('y=f(x)','y='+f)
    
    h=(b-a)/n    //Longitud de los intervalos
    
    //Definición de los n+1 puntos
    x0=a
    
    for i = 1:n
        x(i)=a+i*h
    end
    
    //Aplicación de la regla en los n+1 puntos
    T=(1/2)*f(x0)
    
    for i=1:n-1
        T = T + f(x(i))
    end
        
    T = T + (1/2)*f(x(n))
        
    T = h * T
        
    sol = T
endfunction