// Estimación del error del método del trapecio.
// Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        f  -  función a integrar. Debe ser string.
//        n  -  número de subintervalos usados al aplicar la regla.

// Obs: para el método simple usar n=1

function ans = errorEstimadoTrapecio(a, b, f, n)
    deff('y=f(x)','y='+f)
    h = (b-a)/n;
    ans = (-h^2/12)*(derivative(f, b) - derivative(f, a));
endfunction