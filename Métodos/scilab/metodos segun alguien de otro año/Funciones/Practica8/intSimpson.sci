//Regla de Simpson

//Función que aplica la regla de Simpson con n subintervalos de igual longitud
//Aproxima la función mediante polinomios de interpolación de grado n.

//Obs.: Para el método simple se toman 2 subintervalos

//Parámetros:
//        a  -  punto inicial del intervalo de integración
//        b  -  punto final del intervalo de integración
//        f  -  función a integrar. Debe ser string.
//        n  -  números de subintervalos usados al aplicar la regla.
//              Debe ser entero no negativo y par.

function sol=intSimpson(a,b,f,n)
    
    if (modulo(n,2) <> 0) then
        error('El numero de intervalos debe ser par.')
    end
    
    deff('y=f(x)','y='+f)
    
    h=(b-a)/n        //Longitud de los intervalos
    
    //Definición de los n+1 puntos
    x0=a
    
    for i = 1:n
        x(i)=a+i*h
    end
    
    //Aplicación de la regla en los n+1 puntos
    s=f(x0)
    
    for i = 1:n-1
        
        if (modulo(i,2) <> 0) then
            s = s + (4 * f(x(i)))
        else
            s = s + (2 * f(x(i)))
        end
    end
    
    s = s + f(x(n))
    
    s = s * (h/3)
    
    sol = s
    
endfunction