// Calcula la máxima derivada de grado n en un intervalo [a,b]:
// Parámetros:
//    a - punto inicial del intervalo
//    b - punto final del intervalo
//    f - función (String)
//    n - orden de derivación
// Nota: la función deriva respecto a la variable x.

function ans=cotaDerivada(a,b,f,n)
    m=0;
    for i=a:0.01:b,
        if abs(derivar(f,i,n))>m then
            m=abs(derivar(f,i,n));
        end
    end
    
    ans=m;
endfunction