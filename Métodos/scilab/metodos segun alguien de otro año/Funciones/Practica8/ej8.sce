function I=regla_trapecio(f,a,b,n)
    h=(b-a)/n
    I=(f(a)+f(b))/2
    for i=1:n-1
        I=I+f(a+i*h)
    end
    I=I*h
endfunction

function I=Simpson(f,a,b,n)
    h=(b-a)/n
    I=(f(a)+f(b))
    for i=1:n-1
        if (modulo(i,2)==0) then
            I=I+2*f(a+i*h)
        else
            I=I+4*f(a+i*h)
        end
    end
    I=I*h/3
endfunction
//2.ii COMANDOS APROPIADOS DE SCILAB??


function I=aux_t(f,p,c,d,n)
    h=(d-c)/n
    I=(f([p c])+ f([p d]))/2
    for i=1:n-1
         I=I+f([p c+i*h]) 
     end 
    I=I*h
endfunction


function I=trapecio_ext(f,a,b,c,d,n1,n2) //INTEGRO PRIMERO RESPECTO A Y
    h1=(b-a)/n1
    
    for i=0:n1
          G(i+1)=aux_t(f,a+i*h1,c,d,n2) 
    end
    I=(G(0+1)+G(n1+1))/2
    for i=2:n1
        I=I+G(i)
    end
    I=I*h1

endfunction




function I=aux_S(f,p,c,d,n)
    h=(c-d)/n
    I=(f([p c])+f([p d]))
    for i=1:n-1
        if (modulo(i,2)==0) then
            I=I+2*f([p c+i*h])
        else
            I=I+4*f([p c+i*h])
        end
    end
    I=I*h/3
endfunction


function I=simpson_ext(f,a,b,c,d,n1,n2) //INTEGRO PRIMERO RESPECTO A Y
    h1=(b-a)/n1
    
    for i=0:n1
          G(i+1)=aux_S(f,a+i*h1,c,d,n2) 
    end
    I=(G(1)+G(n1+1))
    for i=2:n1
        if (modulo(i,2)==0) then
            I=I+4*G(i)
        else
            I=I+2*G(i)
        end
    end
    I=I*h1/3

endfunction
//la integral doble da ~1.6095. Simpson con 2 intervalos da  -0.0761259 pero con 4 da 1.6126607. Trapecio con 2 intervalos da: 1.4424001

function I=aux_t(f,p,c,d,n)
    h=(d-c)/n
    I=(f([p c])+ f([p d]))/2
    for i=1:n-1
        printf("%d \t",I)
         I=I+f([p c+i*h]) 
     end 
    I=I*h
endfunction


function I=trapecio_bid(f,a,b,y1,y2,n1,n2) //INTEGRO PRIMERO RESPECTO A Y
    h1=(b-a)/n1
    
    for i=0:n1
           c=y1(a+i*h1)
           d=y2(a+i*h1)
          G(i+1)=aux_t(f,a+i*h1,d,c,n2)
          printf("%d \n",G(i+1)) 
    end
    I=(G(0+1)+G(n1+1))/2
    for i=2:n1
        I=I+G(i)
    end
    I=I*h1
    
endfunction 
//trapecio_bid(c,0,2,y1,y2,20,20)=3.1045183  donde y1(x)=sqrt(2*x - x*x), y2(x)=(-1)*sqrt(2*x - x*x) y c(x)=1
