// Error exacto del método compuesto del Trapecio.
// Parámetros:
//        a  -  punto inicial intervalo de integración.
//        b  -  punto final intervalo de integración.
//        cota  -  cota de la derivada segunda de la función a integrar.
//        n  -  número de subintervalos usados al aplicar la regla.

// Obs: para el método simple usar n=2

function ans = errorExactoTrapecio(a, b, cota, n)
    h = (b-a)/n;
    ans = (-h^2*(b-a)/12)*abs(cota);
endfunction