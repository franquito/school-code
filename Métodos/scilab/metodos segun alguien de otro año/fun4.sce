function r = raices2(p)
c = coeff(p,0);
b = coeff(p,1);
a = coeff(p,2);
if b > 0 then 
  x1 = (2*c)/(-b+sqrt((b^2)-(4*a*c)));
  x2 = (-b + sqrt((b^2)-(4*a*c)))/(2*a);
else 
  x2 = (2*c)/(-b-sqrt((b^2)-(4*a*c)));
  x1 = (-b - sqrt((b^2)-(4*a*c)))/(2*a);  
end
r = [x1 x2]
endfunction