
A = [3 2 1; 0 4 5; 0 0 7]
b = [1 2 3]

function x = remonte(A, b)
    n = sqroot(length(A))
    x(n) = b(n) / A(n,n)
    
    for f = n-1:-1:1
        acum = 0
        for r = f+1:n
            acum = acum + A(f, r)*x(r)
        end 
        x(f) = (b(f) - acum)/A(f,f)
    end
endfunction

function [s1, s2] = gauss(A,b)
  a = size(A)
  n = a(1)
  for i = 1:(n-1)
    for j = (i+1):a(1)
      mjk= A(j,i)/A(i,i)
      A(j,i) = 0
      A(j, i+1:n) = A(j, i+1:n) - mjk*A(i, i+1:n)
      b(j) = b(j) - mjk *b(i)
    end
  end
  s1 = A
  s2 = b
endfunction
