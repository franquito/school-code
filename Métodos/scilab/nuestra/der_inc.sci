function y = der_inc(f,v,n,h)
    if(n == 0)
        y = f(v)
    end
    if(n == 1)
        y = ((f(v+h)-f(v))/h)
    end
    if(n > 1)
        y = der_inc(f,v, n-1,h) 
    end
endfunction
