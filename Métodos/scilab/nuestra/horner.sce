function c = b(coef_pol,grad_mas_uno,indice,x0)
  if(indice == grad_mas_uno) then
    c = coef_pol(grad_mas_uno);
  else
    c = coef_pol(indice) + x0*b(coef_pol, grad_mas_uno, indice + 1, x0)
  end

endfunction

function c = hornerPro(poli,x0)
  a = coeff(poli);
  n = length(a)
  c = b(a,n,1,x0)
endfunction 
  
  