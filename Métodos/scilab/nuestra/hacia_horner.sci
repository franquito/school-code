function k = semi_horner(pol_coefs, grado, indice, x0)
    if (indice == grado)
        k = pol_coefs(grado)
    else
        k = pol_coefs(indice) + (x0*(semi_horner(pol_coefs, grado, indice +1, x0)))
    end
endfunction

function h = buen_horner(pol, x0)
    a = coeff(pol)
    gr = length(a)
    h = semi_horner(a,gr,1,x0)
endfunction

function d = horner_gen(pol,x0)
    d(1) = buen_horner(pol,x0)
    a = coeff(pol)
    sz = length(a)
    for i=1:sz-1
        co(i) = semi_horner(a,sz,i+1,x0)
    end
    col = poly(co,'x','coeff')
    d(2) = buen_horner(col,x0)
endfunction
