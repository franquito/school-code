clear
function y = deripaso(f,v,n)
    h=f(v)*(10^-3)
    if(h==0) then
        h=%eps
    end
    if n==1 then
        y= (f(v+h) - f(v-h))/(2*h)
    else
       y=( deripaso(f,v+h,n-1) - deripaso(f,v-h,n-1) )/(2*h)
    end
endfunction
function y= tapaso(f,n,v)
    fact=1
    pTotal = f(v)
    for i= 1:1:n-1
        fact=fact*i
        c=deripaso(f,v,i)/fact
        rAux(i+1)=v
        pAux=poly(rAux,'x','r')
        pTotal= pTotal + c*pAux
    end
    y=pTotal
endfunction
