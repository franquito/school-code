
function r = met_rob(p)
	a = coeff(p,2)
    c = coeff(p,0)
	b = coeff(p,1)
	if b < 0 then
        r(1) = ((2*c) / ((-b) + sqrt((b*b) - (4*a*c))));
        r(2) = (((-b) + sqrt((b*b) - (4*a*c))) / (2*a));
	else
        r(2) = ((2*c) / ((-b) - sqrt((b*b) - (4*a*c))));
        r(1) = (((-b) - sqrt((b*b) - (4*a*c))) / (2*a));
	end
	e1 = 1e-8;
	error1 = abs(r(1) - e1)/e1;
	error2 = abs(r(2) - e1)/e1;
	printf("Esperado :%e\n", e1);
	printf("r1: %e  (error=%e)\n", r(1), error1);
	printf("r2: %e (error=%e)\n", r(2), error2);
endfunction




















