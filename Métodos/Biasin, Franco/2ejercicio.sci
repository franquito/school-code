x = [0 .25 .5 .75]
y = [2 1 0 1]
function y = one(x)
	y = 1;
endfunction
plot(linspace(0, 1, 123), one, 'b');

for i = 1:4
	plot2d(x(i), y(i), style=0);
end
