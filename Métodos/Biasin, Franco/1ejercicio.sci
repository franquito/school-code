// Returns the result of f[x_0,...,x_n] having in mind the f(x_i) values.
function r = diferencias(x, y)
  n = length(x);
  if n == 1 then
    r = y(1);
  else
    x_head = x; x_head(n) = [];
    x_tail = x; x_tail(1) = [];
    y_head = y; y_head(n) = [];
    y_tail = y; y_tail(1) = [];
    r = (diferencias(x_tail, y_tail) - diferencias(x_head, y_head))/(x(n) - x(1));
  end
endfunction

function r = diferencias_new(x, y)
	n = length(x);
	if n == 1 then
		r = y(1);
	else
		new_x = x; new_x(n) = [];
		new_y = y; new_y(n) = [];	
		r = diferencias_new(new_x, new_y) + poly(new_x, 'x', 'r')*diferencias(x,y);
	end
endfunction

// Apartado a.
x = [-1 0 1 2 3];
y = [3 0 -1 1 2];

P = diferencias_new(x, y)
// P  =
//                           2            3            4  
//  - 2.5833333x + 1.2083333x + 0.5833333x - 0.2083333x   

// Apartado b.
P5 = P + poly([-1 0 1 2 3], 'x', 'r')*diferencias([-1 0 1 2 3 4], [3 0 -1 1 2 3])
// P5  =
//                       2   3        4            5  
//  - 3.0833333x + 1.625x + x - 0.625x + 0.0833333x   


