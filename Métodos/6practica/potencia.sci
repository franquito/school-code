// A simétrica. z debe tener cierta prop.
function [vector, lambda] = potencia(A, z, iter)
  // old_z = z;
  // w = A*z;
  // z = old_z;
  // z = w/norm(w, 'inf');

  for i = 1:iter
    w = A*z;
    old_z = z;
    z = w/norm(w, 'inf');
  end
  k = 1;
  while (old_z(k)==0)
    k = k+1;
  end
  vector = z;
  lambda = w(k)/old_z(k);
endfunction

A_1 = [6 4 4 1; 4 6 1 4; 4 1 6 4; 1 4 4 6];
A_2 = [12 1 3 4; 1 -3 1 5; 3 1 6 -2; 4 5 -2 -1];

A = [.1 .1 0 .1 0 0;
     .1 9 0 0 0 0;
     0 0 9 .1 0 0;
     .1 0 .1 9 .1 0;
     0 0 0 .1 9 .1;
     0 0 0 0 .1 .2];
B = [9 .1 0 .1 0 0;
     .1 .1 0 0 0 0;
     0 0 .1 .1 0 0;
     .1 0 .1 .2 .1 0;
     0 0 0 .1 .2 .1;
     0 0 0 0 .1 .2];
