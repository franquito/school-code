function l = cholevsky(A)
  n = sqrt(length(A));
  l = zeros(n,n);
  for i = 1:n
    for j = 1:i
      if i == j then
        l(i,i) = A(i,i);
        for k = 1:i-1
          l(i,i) = l(i,i) - (l(i,k))^2;
        end
        l(i,i) = sqrt(l(i,i));
      else
        l(i,j) = A(i,j);
        for k = 1:j-1
          l(i,j) = l(i,j) - l(i,k)*l(j,k);
        end
        l(i,j) = l(i,j)/l(j,j);
      end
    end
  end
endfunction

function x = bajamos(A,b)
  n = sqrt(length(A))
  for i = 1:n
    x(i) = b(i);
    for j = 1:i-1
      x(i) = x(i) - x(j)*A(i,j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

function x = remonte(A,b)
  n = sqrt(length(A));
  for i = n:-1:1
    x(i) = b(i);
    for j = i+1:n
      x(i) = x(i) - A(i,j)*x(j);
    end
    x(i) = x(i)/A(i,i);
  end
endfunction

function X = inversa(A)
  n = sqrt(length(A));
  R = cholevsky(A)'; // R triangular superior.
  X = zeros(n,n);
  I = eye(n,n);
  for i = 1:n
    X(:,i) = remonte(R, bajamos(R', I(:,i)));
  end
endfunction
