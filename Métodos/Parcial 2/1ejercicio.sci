clear;
clc;

function y = secante(fun, a, b)
  while %t
    if (a == b) then break end;
    y = a - ((b - a)/(fun(b) - fun(a)))*fun(a);
    a = b;
    b = y;
  end
endfunction

function y = ejercicio1(x)
  y = x^3-3*x+2;
endfunction

secante(ejercicio1, -2.6, -2.4);
//ans  =
// 
//  - 2.
secante(ejercicio1, 2.6, 2.4);
//ans  =
// 
//  1.
