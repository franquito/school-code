function y = gauss_seidel(A, b, x, e)
  n = sqrt(length(A));
  while %t
    for i = 1:n
      y(i) = b(i);
      for j = 1:n
        // Dividing cases for reducing computation.
        if j < i then
          y(i) = y(i) - A(i,j)*y(j);
        else
          if j ~= i then
            y(i) = y(i) - A(i,j)*x(j);
          end
        end
      end
      y(i) = y(i)/A(i,i)
    end
    // Checking tolerance.
    if (norm(x-y) < e) then
      break;
    end
    x = y
  end
endfunction

A = [4 -2 0 0; -2 4 -1 0; 0 -1 3.8 -2; 0 0 -2 3.8];
b = [0.4 0.9 1 3];

gauss_seidel(A, b, [0; 0; 0; 0], 1e-6)

// ans  =
// 
//    0.4867003  
//    0.7734009  
//    1.2202033  
//    1.4316859  
