function [l, z] = potencia(matA, z, e)
  c = 0;
  l = 0; 
  while %t
    w = matA*z;
    z = w/norm(w,'inf');
    n = norm(w, 'inf');
    if (norm(l-n, 'inf') < e) then break; end
	c = c + 1;
    l = n;
  end
  disp(c);
endfunction

A = [.1 .1 0 .1 0 0; .1 9 0 0 0 0; 0 0 9 .1 0 0; .1 0 .1 9 .1 0; 0 0 0 .1 9 .1; 0 0 0 0 .1 .2];
B = [9 .1 0 .1 0 0; .1 .1 0 0 0 0; 0 0 .1 .1 0 0; .1 0 .1 .2 .1 0; 0 0 0 .1 .2 .1; 0 0 0 0 .1 .2];
z = [1; 1; 1; 1; 1; 1];

potencia(A,z,1e-8)
//   382.
// ans  =
// 
//    9.1422599  
potencia(B,z,1e-8)
//   8.
// ans  =
//  
//    9.0022597
