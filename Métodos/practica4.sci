// Ejercicio 1
// i)
//A1 = [1 1 0 3 ; 2 1 -1 1 ; 3 -1 -1 2 ; -1 2 3 -1];
//b1 = [4 ; 1 ; -3 ; 4];
// [ -1 ; 2 ; 0 ; 1 ]

// ii)
//A2 = [1 -1 2 -1 ; 2 -2 3 -3 ; 1 1 1 0 ; 1 -1 4 3];
//b2 = [-8 ; -20 ; -2 ; 4];
// [ -7 ; 3 ; 2 ; 2 ]

// iii)
//A3 = [1 1 0 4 ; 2 1 -1 1 ; 4 -1 -2 2 ; 3 -1 -1 2];
//b3 = [2 ; 1 ; 0 ; -3];
// [-4 ; 2/3 ; -7 ; 4/3]

// Para resolver hacemos:
//[U,c,L,P] = Gauss(A,b);
//x = Regresiva(U,c);
//abs(A*x - b) < 1e-10 (comprobación)

// Ejercicio 3
// Con el Gauss Pro sale trivial:
// Aa = [1.012 -2.132 3.104 ; -2.132 4.096 -7.013 ; 3.104 -7.013 0.014]
// [U,c,L,P] = Gauss(A,A(:,1))
// (el (b) es análogo)

// Ejericio 4
//A = [1 2 3 4 ; 1 4 9 16 ; 1 8 27 64 ; 1 16 81 256];
//b = [2 ; 10 ; 44 ; 190];
// [L,c,U,P] = Gauss(A,b);
// x = RegresivaSuperior(U,RegresivaInferior(L,b));

// Ejercicio 5
// Aa = [16 -12 8 -16 ; -12 18 6 9 ; 8 -6 5 -10 ; -16 9 -10 46]

