function y = hor(p, x)
  n = length(p)
  y = p(n);
  for i = (n-1:-1:1)
    y = x*y+p(i)
  end;

  return y;
endfunction

function y = hor_reloaded(p, x)
  n = length(p)
  y(1) = p(n);
  y(2) = p(n);
  for i = (n-1:-1:1)
    y(1) = x*y(1)+p(i);
    if i > 1 then
      y(2) = y(1)*x;
    end;
  end;

  return y;
endfunction
