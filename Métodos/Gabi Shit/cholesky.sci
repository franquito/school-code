//Recordar que cholesky sirve para matrices semidefinidas positivas
//chol(), de scilab solo sirve para las dp y te da L'(traspuesta) encima
function L = cholesky(A)
    n = size(A,1);
    L = zeros(n,n);
    for(i=1:n)
        for(j=i+1:n)
            p = 0;
            for(k=1:i-1)
                p = p + (L(i,k))^2;
            end
            L(i,i) = sqrt(A(i,i) - p);
            o = 0;
            for(k=1:i-1)
                o = o + (L(j,k)*L(i,k));
            end
            L(j,i) = (A(j,i)- o)/L(i,i);
        end
    end
    p = 0;
    for(k=1:i-1)
        p = p + (L(i,k))^2;
    end
    L(i,i) = sqrt(A(i,i) - p);
endfunction

// Ax = b ->  A=LL'  || 1)   Ly = b   || 2)   L'x = y
