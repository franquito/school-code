function x = Gauss_Seidel(A, b, X0, delta, max1)
  n = length(b);
  for k = 1:max1
    for i = 1:n
      x(i) = (b(i) - ((A(i, [1:i-1])*x) +(A([i+1:n])*X0))*X0([1:i-1, i+1:n]))/A(i,i);
    end
    err = norm(x-X0, 'inf');
    X0 = x;
    if err < delta then break end
    end
  endfunction
  
  function x = fj(A)
    b = eye(A)-(inv(eye(A).*A))*A
    x = norm(b, 'fro');
    
    endfunction 
