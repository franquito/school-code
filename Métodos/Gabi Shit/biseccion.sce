function [c, fc] = bisec(f, a, b, delta)        //f= funcion
  fa= f(a);                                     //a y b tales que f(a).f(b) < 0
  fb= f(b);                                     //delta = aproximacion/precision (10e-2 masomenos buena)
  c=(a+b)/2;
  fc=f(c)
  while(b-c > delta)
    if(fc==0) then
      return
    end
    if (fb*fc<0) then
      a=c
      fa=f(a)
    else if(fa*fc<0) then
        b=c
        fb=f(b)
      end
    end
  c=(a+b)/2;
  fc=f(c)
  end
endfunction
