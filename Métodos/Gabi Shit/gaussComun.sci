function [matA, vecB] = gaussComun(A,b)
    n = size(A,1);
//    n = a(1);
    for(i=1:n-1)
        for(j=i+1:n)
            mjk = A(j,i)/A(i,i);
            A(j,i) = 0;
            A(j,i+1:n) = A(j,i+1:n) - mjk*A(i,i+1:n);
            b(j) = b(j) - mjk*b(i);
        end
    end
    matA = A;
    vecB = b;
endfunction
