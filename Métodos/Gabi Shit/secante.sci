function [p1, err, k, y] = secante( f, p0, p1, delta, max1) 
  y = f(p1);                                                    
  for( k = 1:max1)
    p2 = p1 -y* ((p1-p0)/(y-f(p0)));
    err = abs(p2-p1);
    p0 = p1;
    p1 = p2;
    y = f(p1);
    if((err<delta) & (abs(y)<delta)) then
      break;
    end
  end
endfunction
//p1 = raiz
//err = error
//k = numero iteraciones
//y = f(y)
//
//
//f(p0)*f(p1)<0
//delta = error
//max1 = numero maximo iteraciones
