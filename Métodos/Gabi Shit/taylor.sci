// Polinomio de Taylor
// Parámetros:
//    f - función a aproximar (String)
//    a - valor alrededor del que aproximar.
//    x - valor donde obtener la aproximación.
//    n - orden del polinomio de Taylor.

//function ans = taylor(f, a, x, n)
//    deff("y = g(x)", "y = "+f);
//    t = g(a);
//    
//    for(k = 1 : n),
//        t = t + (derivar (f, a, k) / factorial(k)) * (x - a)^k;
//    end
//    
//   ans = t;
//    
//endfunction

function y=Df(f,v,n)
  deff("s=D0f(x)","s=f(x)");
  for i=1:(n-1)
    deff("s=D"+string(i)+"f(x)","s=derivative(D"+string(i-1)+"f,x)");
  end
  deff("s=DNf(x)","s=derivative(D"+string(n-1)+"f,x)");
  y = DNf(v);
endfunction


function p = fac(n)
    if(n==0) then
        p = 1
    end
    if(n==1) then
        p = 1
    end
    if (n>1) then
        p = n*(fac(n-1))
    end
endfunction

function x = taylor(f,a,x,n)
    t = f(a)
    for (k=1:n)
       t = t + (Df(f,a,k) / fac(k)) * (x-a)^k;
    end
    x = t
endfunction
