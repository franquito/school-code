function c = remonteArriba(A,b)
  n = size(A,1);
  x(n) = b(n)/A(n,n);
  for(i = n-1:-1:1)
      p = 0;
      for(j = i+1:n)
          p = p + (A(i,j)*x(j));
      end
      x(i) = (b(i) - p)/A(i,i);
  end
  c = x
endfunction 

function c = remonteAbajo(A,b)
  n = size(A,1);
  x(1) = b(1)/A(1,1);
  for(i = 2:n)
      p = 0;
      for(j = 1:i-1)
          p = p + (A(i,j)*x(j));
      end
      x(i) = (b(i) - p)/A(i,i);
  end
  c = x;
endfunction
