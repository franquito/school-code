function x1 = matTriang(A,b)
    n = size(A,1)
    x = []
    for(i=n:-1:1)
        x(i) = (b(i) - A(i,i+1:n)*x(i+1:n)) / A(i,i);
    end
    x1 = x;
endfunction
