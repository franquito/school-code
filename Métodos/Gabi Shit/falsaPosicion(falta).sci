function y = secante( f, p0, p1, delta, maxiter)
  y=f(p1)
  for( k=1:maxiter:1)
    p2= p1 -y* ((p1-p0)/(y-f(p0)))
    err= abs(p2-p1)
    p0=p1
    p1=p2
    y=f(p1)
    if((err<delta) & (abs(y)<delta)) then
      break
    end
  end
endfunction


function y = falsaPosicion(f, a, b, delta, iter)
    a1 = a;
    b1 = b;
    c1 = secante(f,a1,b1,delta,iter)
    for(i=0:iter)
        if(f(a1) * f(c1) < 0) then
            a2 = a1;
            b2 = c1;
        end
        if(f(b1) * f(c1) < 0) then
            a2 = c1;
            b2 = b1;
        end
        if(f(c1) == 0) then
            y = c1;
            break
        end
        c1 = secante(f,a2,b2,delta,iter)
    end
    y = c1;
endfunction    
