function x = Jacobi(A, b, x0, delta, max1)
  n = length(b);
  for k = 1:max1
    for i = 1:n
      x(i) = (b(i) - A(i, [1:i-1, i+1:n])*x0([1:i-1, i+1:n]))/A(i,i);
    end
    err = norm(x-x0, 'inf');
    x0 = x;
    if err < delta then break end
    end
  endfunction
  
function [x, nIter] = jacobi(A, b, x0, delta, maxIter) //esta anda
    n = size(A,1);
    err = 1;
    x = x0;
    for (k = 1:maxIter)
        for (i = 1:n)
            p = 0;
                for (j = 1:n)
                    if(j <> i) then 
                        p = p + A(i,j)*x0(j);
                    end
                end
            x(i) = (b(i) - p)/A(i,i);
            err = norm(x - x0, 'inf');
            x0 = x;
        end
        if(err < delta) then
            break;
        end
    end
    nIter = k;
endfunction
//A es la matriz
//b es el vector
//x0 es la aproximacion inicial a nuestro x final
//delta es el error que queremos
//maxIter es la cantidad maxima de iteraciones que queremos
//x0 = [1,1] se puede
