function [maxlambda, vlambda] = potencia(matA, z, maxIter)
  for (i = 1:maxIter)
    j = z;
    w = (matA * z);
    z = w/(norm(w,'inf'));
  end
  
  vlambda = j;
  maxlambda = norm(w,'inf');
  
endfunction
