function x = gaussDiag(A,b)
    n = length(b);
    x = []
    for(i=1:n)
        x(i) = b(i)./A(i,i);
    end
endfunction

