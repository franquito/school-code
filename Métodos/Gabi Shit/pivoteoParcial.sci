function [matA, vecB] = pivoteoParcial(A,b)
    n = size(A,1)
    for(i=1:n-1)
        c = A(i, :);
        if(A(i,i) == 0) then
            for(k=i+1:n)
                if(c(i) < A(k,i)) then
                    t = k;
                end
            end
            temp = A(i,:);
            A(i, :) = A(t,:);
            A(t,:) = temp;
        end
        for(j=i+1:n)
            mjk = A(j,i)/A(i,i);
            A(j,i) = 0;
            A(j,i+1:n) = A(j,i+1:n) - mjk*A(i,i+1:n);
            b(j) = b(j) - mjk*b(i);
        end
    end
    matA = A;
    vecB = b;
endfunction
