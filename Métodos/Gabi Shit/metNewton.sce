function y = newton(func, a, delta, iter)
    error1 = 1;
    x0 = a;
    for (i=0:iter)
        f0 = func(x0);
        f1 = derivative(func, x0, 1);
        x1 = x0 - (f0/f1)
        //error1 = abs((x1-x0)/x1);
        x0 = x1;
        if(abs(func(x0)) < delta ) then
            break;
        end
    end
    y = x0;
endfunction
