function [L,U] = doolittle(A)
    n = size(A,1);
    U = [];
    L = [];
    for j=1:n
      for i=1:n
    //Estamos por encima de la diagonal, hallamos elemento de U
        if i<=j then
          U(i,j) = A(i,j);
          for k=1:i-1
            U(i,j) = U(i,j) - L(i,k)*U(k,j);
          end
        end
    //Estamos por debajo de la diagonal, hallamos elemento de L
        if j<=i then
          L(i,j) = A(i,j);
          for k=1:j-1
            L(i,j) = L(i,j) - L(i,k)*U(k,j);
          end
            if(i==3 & j==3) then
                U(3,3) = A(3,3);
                end
            L(i,j) = L(i,j)/U(j,j);
        end
      end
    end
endfunction
