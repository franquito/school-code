function P = lagran(x,y)
  n = length(x);
  x0 = poly(0, "x");
  P = 0;
  for k=1:n;
    L = 1;
    for (i = [1:k-1, k+1:n])
      L = L*(x0 - x(i))/(x(k) - x(i));
    end
    P = P + L*y(k);
  end
endfunction
  