function x = sor(A, b, x0,w, delta,max1)
    n = length(b)

    for k=1:max1
      for i=1:n
        x(i) = (w/A(i,i))*(b(i) - A(i, [1:i-1])*x0([1:i-1]) - A(i,[i+1:n])*x0([i+1:n])) + (1-w)*x0(i);
      end
      err = norm(x-x0, 'inf')
      x0 = x;
      if err<delta then break
      end
    end
endfunction
