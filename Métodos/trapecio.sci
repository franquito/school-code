function y = trapecio(fun, a, b)
  y = ((b-a)/2)*(fun(a)+fun(b));
endfunction
// -h^3/12*f(cx)

function y = simpson(fun, a, c)
  b = (a+c)/2;
  y = ((b-a)/3)*(fun(a)+4*fun(b)+fun(c));
endfunction

function y = ln(x)
  y = log(x);
endfunction

function y = cubic_root(x)
  y = x.^(1/3);
endfunction

function y = square_sin(x)
  y = sin(x).^2;
endfunction

trapecio(ln, 1, 2); //0.3465736
simpson(ln, 1, 2); //0.3858346
// https://www.wolframalpha.com/input/?i=integrate+ln(x)+from+1+to+2
// 0.38629
trapecio(cubic_root, 0, .1); //0.0232079
simpson(cubic_root, 0, .1); //0.0322962
// https://www.wolframalpha.com/input/?i=integrate+x%5E(1%2F3)+from+0+to+.1
// 0.0348119
trapecio(square_sin, 0, %pi/3); //0.3926991
simpson(square_sin, 0, %pi/3); //0.3054326
// https://www.wolframalpha.com/input/?i=integrate+sin(x)%5E2+from+0+to+Pi%2F3
// 0.30709


