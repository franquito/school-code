function y = trapecio_comp(fun, a, b, n)
  h = (b-a)/n;
  for i = 1:n+1
    x(i) = a+(i-1)*h;
  end
  // Note that:
  // a = x(1), b = x(n+1)
  acum = fun(a)/2;
  for i = 2:n
    acum = acum + fun(x(i));
  end
  acum = acum + fun(b)/2;
  y = h*acum;
endfunction

function y = simpson_comp(fun, a, b, n)
  // n debe ser par.
  h = (b-a)/n;
  for i = 1:n+1
    x(i) = a + (i-1)*h;
  end
  y = fun(a);
  for i = 2:n
    if (modulo(i, 2) == 0)
      y = y + 4*fun(x(i));
    else
      y = y + 2*fun(x(i));
    end
  end
  y = y + fun(b);
  y = y*h/3;
endfunction

function y = left_inverse(x)
  y = (x+1).^(-1);
endfunction

trapecio_comp(left_inverse, 0, 1.5, 10)
// 0.9178617
simpson_comp(left_inverse, 0, 1.5, 10)
// 0.9163064

// Real value: 0.916291
