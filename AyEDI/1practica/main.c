#include <stdio.h>
#include <stdlib.h>
#include "SList.h"
#include "DList.h"
#include "CList.h"
#include "AList.h"

static void print_int (int data, void *extra_data)
{
	printf("%d ", data);
}

int main(int argc, char *argv[])
{
	SList *list = NULL;
	list = slist_prepend(list, 1);
	list = slist_prepend(list, 2);
	list = slist_prepend(list, 3);
	list = slist_append(list, 4);
	slist_foreach(list, &print_int, NULL);
	puts("");

	printf("Tiene siguiente elemento? %d\n", slist_has_next(list));
	printf("Tiene %d elementos\n", slist_length(list));


	SList *pi = NULL;
	pi = slist_prepend(pi, 314);

	slist_foreach(slist_concat(list, pi), print_int, NULL);
	printf("\n");

	SList *ins = slist_insert(list, 20, 3);
	slist_foreach(ins, print_int, NULL);
	printf("\n");
	printf("%d\n", slist_length(ins));

	slist_foreach(slist_remove(ins, 3), print_int, NULL);
	printf("\n");
	
	printf("Contiene el 314? %d\n", slist_contains(ins, 314));

	printf("Posición del 314? %d\n", slist_index(ins, 314));

	SList *pares = NULL;
	pares = slist_prepend(pares, 2);
	pares = slist_prepend(pares, 4);
	pares = slist_prepend(pares, 6);
	slist_foreach(slist_intersect(list, pares), print_int, NULL);
	printf("\n");

	DList *coso = malloc(sizeof(DList));
	DList *coso1 = malloc(sizeof(DList));
	coso->prev = coso1;
	coso->next = NULL;
	coso->data = 123;
	coso1->prev = NULL;
	coso1->next = coso;
	coso1->data = 321;
	dlist_foreach(coso, print_int, DLIST_TRAVERSAL_ORDER_REWARD, NULL);
	printf("\n");

	CList *circulito = malloc(sizeof(CList));
	circulito->next = circulito;
	circulito->data = 666;
	clist_foreach(circulito, print_int, NULL);
	printf("\n");

	printf("Ejercicio 8:\n");
	AList *myAList = alist_append(NULL, 3);
	myAList = alist_append(myAList, 4);
	myAList = alist_append(myAList, 4);
	myAList = alist_append(myAList, 4);
	myAList = alist_append(myAList, 314);
	printf("%d\n", *((myAList->array)+4));
	printf("%d\n", myAList->size);
	return 0;
}
