#include <stdio.h>
#include <stdlib.h>
#include "SList.h"

SList *slist_append(SList *list, int data) {
      SList *newNode = malloc(sizeof(SList));
      SList *node;
      slist_data(newNode) = data;
      slist_next(newNode) = NULL;
      if (list == NULL) {
         return newNode;
      }
      node = list;
      while (slist_next(node) != NULL) {
            node = slist_next(node);
      }
      /* ahora 'node' apunta al ultimo nodo en la lista */
      slist_next(node) = newNode;
      return list;
}

SList *slist_prepend(SList *list, int data) {
      SList *newNode = malloc(sizeof(SList));
      slist_next(newNode) = list;
      slist_data(newNode) = data;
      return newNode;
}

void  slist_destroy(SList *list) {
      SList *nodeToDelete;

      while (list != NULL) {
            nodeToDelete = list;
            list = slist_next(list);
            free(nodeToDelete);
      }
}

void  slist_foreach(SList *list, VisitorFuncInt visit, void *extra_data) {
      SList *node = list;

      while (node != NULL) {
            visit(slist_data(node), extra_data);
            node = slist_next(node);
      }
}

int slist_has_next(SList *list) {
  SList *n = list;
  return (slist_next(n) == NULL) ? 0 : 1;
}

int slist_length(SList *list) {
  SList *node = list;
  int len = 0;
  while( node != NULL) {
    len++;
    node = slist_next(node);
  }
  return len;
}

SList *slist_concat(SList *list1, SList *list2) {
  SList *node = list2;
  while (node != NULL) {
    list1 = slist_append(list1, slist_data(node));
    node = slist_next(node);
  }
  return list1;
}

SList *slist_insert(SList *list1, int data, int pos) {
	SList *newlist = NULL;
	SList *node = list1;
	while (node != NULL) {
		if (pos == 0)
			newlist = slist_append(newlist, data);
		newlist = slist_append(newlist, slist_data(node));
		pos--;
		node = slist_next(node);
	}
	return newlist;
}

SList *slist_remove(SList *list1, int n) {
	SList *first = list1;
	SList *temp = slist_next(list1);
	if (n == 0) {
		slist_next(list1) = NULL;
		return temp; }

	while (n > 0) {
		temp = list1;
		list1 = slist_next(list1);
		n--;
	}

	slist_next(temp) = slist_next(list1);
	slist_next(list1) = NULL;
	slist_destroy(list1);
	return first;
}

int slist_contains(SList *list1, int data) {
	if (list1 == NULL) return 0;
	while (slist_next(list1) != NULL) {
		if (slist_data(list1) == data)
			return 1;
		list1 = slist_next(list1);
	}
	if (slist_data(list1) == data) return 1;
	return 0;
}

unsigned int slist_index(SList *list, const int data) {
	if (list == NULL) return -1;
	int i = 0;
	while (slist_next(list) != NULL) {
		if (slist_data(list) == data)
			return i;
		list = slist_next(list);
		i++;
	}
	if (slist_data(list) == data) return i;
	return -1;	
}

SList *slist_intersect(SList *l1, SList *l2) {
	SList *new = NULL;
	SList *aux = l1;
	
	if (l1 == NULL || l2 == NULL) return NULL;

	while (aux != NULL) {
		if (!(slist_contains(new, slist_data(aux))) && slist_contains(l2, slist_data(aux)))
			new = slist_append(new, slist_data(aux));
			
		aux = slist_next(aux);
	}
	return new;
}
