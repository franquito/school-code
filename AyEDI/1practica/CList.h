#ifndef __CLIST_H__
#define __CLIST_H__

#include<stdio.h>
//#include<tuvieja con manaos>
#include<stdlib.h>

#define clist_data(l)       (l)->data
#define clist_next(l)       (l)->next

typedef void (*VisitorFuncInt) (int data, void *extra_data);

typedef struct _CList {
	int data;
	struct _CList *next;
} CList;

CList *clist_append(CList *list, int data);

void clist_foreach(CList *list, VisitorFuncInt func, void *data);

#endif
