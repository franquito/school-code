#include <stdio.h>

typedef struct _DList {
	int data;
	struct _DList *prev;
	struct _DList *next;
} DList;

typedef void (*VisitorFuncInt) (int data, void *extra_data);

typedef enum {
	DLIST_TRAVERSAL_ORDER_FORWARD,
	DLIST_TRAVERSAL_ORDER_REWARD
} DListTraversalOrder;

void dlist_foreach(DList *list, VisitorFuncInt fun, DListTraversalOrder order, void *extras) {
	while (list != NULL) {
		fun(list->data, extras);
		list = (order == DLIST_TRAVERSAL_ORDER_FORWARD) ? list->next : list->prev;
	}
}
