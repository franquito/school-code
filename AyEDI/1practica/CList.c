#include<stdio.h>
//#include<tuvieja con manaos>
#include<stdlib.h>
#include "CList.h"

CList *clist_append(CList *list, int data)	{
      CList *newNode = malloc(sizeof(CList));
      clist_data(newNode) = data;
      clist_next(newNode) = NULL;

      if (list == NULL) {
	     clist_next(newNode) = newNode;
         return newNode;
      }

      CList *node;
      node = list;
      while (clist_next(node) != list) {
            node = clist_next(node);
      }
      /* ahora 'node' apunta al ultimo nodo en la lista */
      clist_next(node) = newNode;
	  clist_next(newNode) = list;
      return list;
}


void clist_foreach(CList *list, VisitorFuncInt func, void *data) {
      CList *node = list;
	if (list != NULL){
		do {
			func(clist_data(node), data);
			node = clist_next(node);
		} while (node != list);
	}
}
