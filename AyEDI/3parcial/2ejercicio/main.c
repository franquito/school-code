#include <stdio.h>

int min(int n, int s) {
  if (n < s)
    return n;
  return s;
}

int solve(int tipos[], int length, int importe) {
  int table[length+1][importe+1];
  int col, row;

#define INF 1e9
  //casos base. table[0] will represent using 0 coins
  table[0][0]=0;//no necesito monedas
  for (col = 1; col < importe+1; col++)
      table[0][col] = INF;//imposible


  for(col = 1; col < importe + 1; col++) {
    for (row = 1; row <= length; row++) {
      if (tipos[row-1] > col)
        table[row][col] = table[row-1][col];
      else if (tipos[row-1] <= col)
        table[row][col] = min(table[row-1][col], table[row][col-tipos[row-1]]+1);
    }
  }
  //~ for(row=0; row < length; row++) {
     //~ for(col=0; col < importe +1 ; col++)
       //~ printf("%d|",table[row][col]);
     //~ printf("\n");
  //~ }
  return table[length][importe];
}

int main(int argc, char *argv[]) {
  int tipos[] = {1, 2, 6, 8};
  printf("%d", solve(tipos, 4, 12));
  return 0;
}
