#include <stdio.h>
#include <stdlib.h>

#define Inf 100000000
#define Size 4

int values[Size][Size];

typedef struct _Punto {
  int x;
  int y;
} Punto;

int posible(Punto from, Punto to) {
  return to.y > abs(to.x - from.x);
}

int min3(int m1, int m2, int m3) {
  int m = m1;
  if (m2 < m) m = m2;
  if (m3 < m) m = m3;
  return m;
}

int solve(Punto from, Punto to) {
  // Check the basic case where from == to
  if (from.y == to.y) return values[from.y][from.x];

  int table[Size][to.y];
  table[from.y][from.x] = values[from.y][from.x];

  int col, row;
  for (row = from.y+1; row <= to.y; row++)
    for (col = 0; col < Size; col++) {
      Punto curr = {col, row};
      if (posible(from, curr)) {
        int cmp1 = Inf, cmp2 = Inf, cmp3 = Inf;
        Punto down = {col, row-1};
        Punto down_left = {col-1, row-1};
        Punto down_right = {col+1, row-1};

        if (posible(from, down))
          cmp1 = table[down.y][down.x];
        if (posible(from, down_left))
          cmp2 = table[down_left.y][down_left.x];
        if (posible(from, down_right))
          cmp3 = table[down_right.y][down_right.x];

        printf("Minimum: %d, Current: %d\n", min3(cmp1, cmp2, cmp3), values[row][col]);
        table[row][col] = min3(cmp1, cmp2, cmp3) + values[row][col];
      }
    }

  return table[to.y][to.x];
}

int main() {
  values[0][0] = 5;
  values[0][1] = 7;
  values[0][2] = 9;
  values[0][3] = 2;
  values[1][0] = 2;
  values[1][1] = 3;
  values[1][2] = 20;
  values[1][3] = 1;
  values[2][0] = 16;
  values[2][1] = 7;
  values[2][2] = 8;
  values[2][3] = 15;
  values[3][0] = 2;
  values[3][1] = 1;
  values[3][2] = 7;
  values[3][3] = 6;

  Punto from = {2, 1};
  Punto to = {0, 3};
  int s = solve(from, to);
  printf("%d\n", s);
}
