#include <stdio.h>
#include <stdlib.h>

typedef int _ChessBoard[8][8];
typedef _ChessBoard* ChessBoard;

ChessBoard init_match() {
  ChessBoard cb = malloc(sizeof(ChessBoard));
  int row; int col;
  for (row = 0; row < 9; row++)
    for (col = 0; col < 9; col++)
      (*cb)[row][col] = 0;

  return cb;
}

int can_put_queen(ChessBoard cb, int row, int col) {
  if (*(cb)[row][col] == 0) {
    return 1;
  } else return 0;
}

ChessBoard put_queen(ChessBoard cb, int row, int col) {
  int _col; int _row;

  for (_col = 0; _col < 8; _col++) {
    (*cb)[row][_col] = 1;
  }
  for (_row = row, _col = col; _row < 8 && _col < 8; _row++, _col++)
    (*cb)[_row][_col] = 1;
  for (_row = row, _col = col; _row < 8 && _col > -1; _row++, _col--)
    (*cb)[_row][_col] = 1;
  for (_row = row; _row < 8; _row++)
    (*cb)[_row][col] = 1;
  (*cb)[row][col] = 2;

  return cb;
}

ChessBoard solve(ChessBoard cb, int row) {
  int col;

  if (row == 8) {
    printf("Lo encontréee\n");
    return ;
  }

  for (col = 0; col < 7; col++) {
    if (can_put_queen(cb, row, col)) {
      cb = put_queen(cb, row, col);
      solve(cb, row++);
    }
  }
}

void print_table(ChessBoard cb) {
  int i, j;
  printf("\n");
  for(i=7; i > -1; i--) {
     for(j=0; j < 8; j++)
       printf("%d",(*cb)[i][j]);
     printf("\n");
  }
}

int main(int argc, char *argv[]) {
  /* code */
  ChessBoard cb = init_match();
  /* solve(cb, 0); */
  cb = put_queen(cb, 0, 0);
  cb = put_queen(cb, 1, 2);
  cb = put_queen(cb, 2, 4);
  cb = put_queen(cb, 3, 1);
  print_table(cb);
  return 0;
}
