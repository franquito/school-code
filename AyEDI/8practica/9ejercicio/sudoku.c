#include <stdio.h>
#include <stdlib.h>

int s[9][9];

void fill_sudoku() {
  s[0][1] = 2;
  s[1][0] = 5;
  s[2][1] = 3;

  s[1][3] = 1;
  s[1][4] = 4;
  s[1][5] = 3;
  s[2][4] = 9;

  s[0][8] = 3;
  s[1][7] = 9;
  s[1][8] = 2;
  s[2][7] = 8;
  s[2][8] = 6;

  s[3][0] = 8;
  s[3][1] = 4;
  s[4][0] = 7;
  s[5][1] = 9;

  s[3][3] = 7;
  s[5][3] = 4;
  s[5][4] = 3;
  s[5][5] = 5;
  s[4][5] = 8;

  s[4][6] = 2;
  s[4][8] = 4;

  s[7][0] = 4;
  s[6][1] = 7;
  s[7][2] = 8;

  s[6][3] = 6;
  s[8][4] = 7;
  s[7][5] = 9;

  s[8][6] = 5;
  s[6][7] = 4;
}

int feasible(int row, int col, int value) {
  //printf("Feasible with row %d, col %d, value %d\n", row, col, value);
  int i;
  // Check horizontal-right values.
  for (i = 0; i < 9; i++)
    if (s[row][i] == value)
      return 0;
  // Check vertical-upper values.
  for (i = 0; i < 9; i++)
    if (s[i][col] == value)
      return 0;
  // Check the 3x3 container box.
  int init_col, init_row, j;
  for (init_col = 0; init_col+3 <= col; init_col = init_col+3);
  for (init_row = 0; init_row+3 <= row; init_row = init_row+3);
  for (i = init_row; i < init_row+3; i++)
    for (j = init_col; j < init_col+3; j++)
      if (s[i][j] == value)
        return 0;

  return 1;
}

void print_sudoku() {
  int i, j;
  printf("\n");
  for(i=8; i > -1; i--) {
     for(j=0; j < 9; j++) {
       if (j % 3 == 0)
        printf("|");
       printf("%d",s[i][j]);
     }
     if (i % 3 == 0)
        printf("\n------------");
     printf("\n");
  }
}

void solve_sudoku(int row, int col) {
  if (row == 9) {
    print_sudoku();
    return; }

  if (s[row][col] != 0) {
    if (col == 8)
      solve_sudoku(row+1, 0);
    else
      solve_sudoku(row, col+1);

    return ;
  }

  int i = 1;
  for (; i < 10; i++) {
    //printf("Trying with %d\n", i);
    if (feasible(row, col, i)) {
      //printf("Adding row %d, col %d, value %d\n", row, col, i);
      s[row][col] = i;
      //print_sudoku();
      if (col == 8)
        solve_sudoku(row+1, 0);
      else
        solve_sudoku(row, col+1);
    }
    s[row][col] = 0;
  }
}

int main(int argc, char *argv[]) {
  fill_sudoku();
  solve_sudoku(0, 0);
  //printf("%d", feasible(8, 3, 9));
  return 0;
}
//http://www.sudoku-online.org/
//<sudoku>
//	<size>3</size>
//	<field>
//		<index>4</index>
//		<value>7</value>
//	</field>
//	<field>
//		<index>6</index>
//		<value>5</value>
//	</field>
//	<field>
//		<index>9</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>11</index>
//		<value>8</value>
//	</field>
//	<field>
//		<index>14</index>
//		<value>9</value>
//	</field>
//	<field>
//		<index>19</index>
//		<value>7</value>
//	</field>
//	<field>
//		<index>21</index>
//		<value>6</value>
//	</field>
//	<field>
//		<index>25</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>28</index>
//		<value>9</value>
//	</field>
//	<field>
//		<index>30</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>31</index>
//		<value>3</value>
//	</field>
//	<field>
//		<index>32</index>
//		<value>5</value>
//	</field>
//	<field>
//		<index>36</index>
//		<value>7</value>
//	</field>
//	<field>
//		<index>41</index>
//		<value>8</value>
//	</field>
//	<field>
//		<index>42</index>
//		<value>2</value>
//	</field>
//	<field>
//		<index>44</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>45</index>
//		<value>8</value>
//	</field>
//	<field>
//		<index>46</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>48</index>
//		<value>7</value>
//	</field>
//	<field>
//		<index>55</index>
//		<value>3</value>
//	</field>
//	<field>
//		<index>58</index>
//		<value>9</value>
//	</field>
//	<field>
//		<index>61</index>
//		<value>8</value>
//	</field>
//	<field>
//		<index>62</index>
//		<value>6</value>
//	</field>
//	<field>
//		<index>63</index>
//		<value>5</value>
//	</field>
//	<field>
//		<index>66</index>
//		<value>1</value>
//	</field>
//	<field>
//		<index>67</index>
//		<value>4</value>
//	</field>
//	<field>
//		<index>68</index>
//		<value>3</value>
//	</field>
//	<field>
//		<index>70</index>
//		<value>9</value>
//	</field>
//	<field>
//		<index>71</index>
//		<value>2</value>
//	</field>
//	<field>
//		<index>73</index>
//		<value>2</value>
//	</field>
//	<field>
//		<index>80</index>
//		<value>3</value>
//	</field>
//</sudoku>
