#include <stdlib.h>
#include <stdio.h>

int max(int n, int p) {
  if (n <= p) return p;
  return n;
}

int *best_two(int *arr, int sz) {
  int *two = malloc(sizeof(int)*2);

  // Asco..
  if (sz < 4) {
    if (sz == 2) {
      if (arr[0] > arr[1]) {
        two[0] = arr[0];
        two[1] = arr[1];
      } else {
        two[0] = arr[1];
        two[1] = arr[0];
      }
    } else {
      if (arr[0] >= arr[1] && arr[0] >= arr[2]) {
        two[0] = arr[0];
        two[1] = max(arr[1], arr[2]);
      }
      if (arr[1] >= arr[0] && arr[1] >= arr[2]) {
        two[0] = arr[1];
        two[1] = max(arr[0], arr[2]);
      }
      if (arr[2] >= arr[0] && arr[2] >= arr[1]) {
        two[0] = arr[2];
        two[1] = max(arr[0], arr[1]);
      }
    }
    return two;
  }

  int *first_half  = best_two(arr, sz/2);
  int *second_half = best_two(arr + sz/2, sz - sz/2);

  if (first_half[0] > second_half[0]) {
    two[0] = first_half[0];
    if (first_half[1] > second_half[0])
      two[1] = first_half[1];
    else
      two[1] = second_half[0];
  } else {
    two[0] = second_half[0];
    if (second_half[1] > first_half[0])
      two[1] = second_half[1];
    else
      two[1] = first_half[0];
  }
  free(first_half);
  free(second_half);

  return two;
}

int main() {
  int arr[] = {9, 4, 5, 5, 1, 0, 99};
  int *two = best_two(arr, sizeof(arr)/sizeof(int));
  printf("%d", two[1]);

}
