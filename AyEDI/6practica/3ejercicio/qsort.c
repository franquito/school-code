#include <stdio.h>

typedef enum _PivotPosition {
  Aleatorio,
  Ultimo,
  Primero,
  Mediana
} PivotPosition;

void swap(int *p, int *q) {
  int t = *p;
  *p = *q;
  *q = t;
}

int particionar(int arr[], int init, int last, PivotPosition pos) {
  int pivot_i,
      pivot,
      l, r;

  switch (pos) {
    case Ultimo:
      pivot_i = last;
      break;
    case Primero:
      pivot_i = init;
      break;
    case Mediana:
      pivot_i = (last - init)/2 + init;
      break;
    case Aleatorio:
      pivot_i = init + (rand() % (last + 1 - init));
      break;
  }

  pivot = arr[pivot_i];
  l = init;
  r = last;

  while (l < r) {
    while (arr[l] <= pivot && l<r)
      l++;
    while (arr[r] > pivot)
      r--;

    if (l < r) {
      if (l == pivot_i) pivot_i = r;
      else if (r == pivot_i) pivot_i = l;
      swap(&arr[l], &arr[r]); }

  }
  swap(&arr[pivot_i], &arr[r]);

  printf("Pivot is in position: %d\n", r);

  return r;
}

void qsort(int data[], int init, int last, PivotPosition pos) {
  int pivot;
  if (init < last) {
    pivot = particionar(data, init, last, pos);
    qsort(data, init, pivot - 1, pos);
    qsort(data, pivot + 1, last, pos);
  }
}
