#include <stdio.h>
#include "qsort.c"

int main(int argc, char *argv[]) {
  int p[] = {9, 1, 7, 4, 3, 2, 1};
  
  PivotPosition mediana = Primero;
  qsort(p, 0, 6, mediana);

  int i;
  for (i = 0; i < 7; i++)
    printf("%d ", p[i]);
  puts("");

  return 0;
}
