#include <stdio.h>

void swap(int *p, int *q) {
  int t = *p;
  *p = *q;
  *q = t;
}

int particionar(int arr[], int init, int last) {
  int pivot = arr[init],
      l = init,
      r = last;

  while (l < r) {
    while (arr[l] <= pivot && l<r)
      l++;
    while (arr[r] > pivot)
      r--;

    if (l < r)
      swap(&arr[l], &arr[r]);

  }
  swap(&arr[init], &arr[r]);

  return r;
}

void qsort(int data[], int init, int last) {
  int pivot;
  if (init < last) {
    pivot = particionar(data, init, last);
    qsort(data, init, pivot - 1);
    qsort(data, pivot + 1, last);
  }
}
