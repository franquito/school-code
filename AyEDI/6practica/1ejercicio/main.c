#include <stdio.h>
#include "qsort.c"

int main(int argc, char *argv[]) {
  int p[] = {9, 1, 7, 4, 3, 2, 1},
      length = sizeof(p)/sizeof(int);

  qsort(p, 0, length-1);

  /* Show ordered array. */
  int i;
  for (i = 0; i < length; i++)
    printf("%d ", p[i]);
  puts("");

  return 0;
}
