#include <stdio.h>

void swap(int *p, int *q) {
  int t = *p;
  *p = *q;
  *q = t;
}

int particionar(int arr[], int init, int last) {
  int i = init,
      store = init;

  while (i < last) {
    if (arr[i] <= arr[last]) {
      swap(&arr[i], &arr[store]);
      store++;
    }
    i++;
  }
  swap(&arr[store], &arr[last]);
  printf("%d\n", store);
  return store;
}

/* int main(int argc, char *argv[]) */
/* { */
/*   int arr[] = {10,9,5,3}; */
/*   particionar(arr, 0, 3); */
/*   int i; */
/*   for (i = 0; i < 4; i++) */
/*     printf("%d ", arr[i]); */
/*   puts(""); */
/*   return 0; */
/* } */
