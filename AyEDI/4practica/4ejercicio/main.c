#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "hashtable.c"

unsigned int hash(void *key, unsigned int size) {
  char *string = key;

  /* String -> Nat */
  unsigned int sum = 0;
  int i = 0;
  for (; string[i] != '\0'; i++)
    sum += (int)(string[i])*pow(128,(strlen(string)-i-1));

  /* Nat -> Nat */
  return floor(size*(sum*0.5-floor(sum*0.5)));
}

int eq(void *c1, void *c2) {
  int *p1 = c1;
  int *p2 = c2;
  if (*p1 == *p2)
    return 1;
  return 0;
}

int str_cmp (void *s1, void *s2) {
  char *c1 = (char *)s1;
  char *c2 = (char *)s2;
  return strcmp(c1, c2);
}

int main(void) {
  char hola[5] = "Hola";
  int i = 21;
  char chau[5] = "Chau";
  int j = 27;
  Hashtable *ht = hashtable_new(10, hash, str_cmp);

  printf("Pointer to hola: %p\n", hola);
  hashtable_insert(ht, hola, &i);
  hashtable_insert(ht, chau, &j);

  printf("Hello: %d\n", *(int *)hashtable_lookup(ht, hola));

  hashtable_delete(ht, hola);

  hashtable_destroy(ht);

  return 0;
}
