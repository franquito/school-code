#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__

typedef unsigned int (*HashFunc)(void *, unsigned int size);

typedef int (*EqualsFunc)(void *c1, void *c2);

typedef struct _Hashbucket {
  void *key;
  void *data;
} Hashbucket;

typedef struct _Hashtable {
  Hashbucket *table;
  int nelems, size;
  HashFunc hash;
  EqualsFunc equal;
} Hashtable;

Hashtable *hashtable_new(int size, HashFunc hash, EqualsFunc equal);

void hashtable_insert(Hashtable *ht, void *key, void *data);

void *hashtable_lookup(Hashtable *ht, void *key);

void hashtable_delete(Hashtable *ht, void *key);

void hashtable_destroy(Hashtable *ht);

#endif /* __HASHTABLE_H__ */
