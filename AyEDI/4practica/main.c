#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

unsigned int hash(void *key) {
  int *p = key;
  return *p % 10;
}

int eq(void *c1, void *c2) {
  int *p1 = c1;
  int *p2 = c2;
  if (*p1 == *p2)
    return 1;
  return 0;
}

//Asumo string length > 0
unsigned int stringtonat(char string[]) {
  unsigned int sum = 0;
  int i = 0;
  while (string[i] != '\0') {

  }
}

int main(void) {
  int x = 42, y = 42, z = 3;
  Hashtable *ht = hashtable_new(10, hash, eq);

  hashtable_insert(ht, &x, &z);

  printf("z : %d\n", *((int *)hashtable_lookup(ht, &x)));
  printf("z : %d\n", *((int *)hashtable_lookup(ht, &y)));

  hashtable_delete(ht, &x);

  hashtable_destroy(ht);

  return 0;
}
