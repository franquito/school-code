#include <stdlib.h>
#include "hashtable.h"

Hashtable *hashtable_new(int size, HashFunc hash, EqualsFunc equal) {
  unsigned int idx;
  Hashtable *ht = malloc(sizeof(Hashtable));

  ht->hash = hash;
  ht->equal = equal;
  ht->size = size;
  ht->table = malloc(sizeof(Hashbucket)*size);
  ht->nelems = 0;

  for (idx = 0; idx < size; idx++) {
    ht->table[idx].key = NULL;
    ht->table[idx].data = NULL;
    ht->table[idx].next = NULL;
  }

  return ht;
}

void hashtable_insert(Hashtable *ht, void *key, void *data) {
  //hashea dependiendo de la dirección de memoria.
  unsigned int idx = ht->hash(key, ht->size);
  Hashbucket *hb_aux;
  Hashbucket *hb_new;

  printf("Will save on %d\n", idx);

  if (ht->table[idx].key == NULL) {
    ht->nelems++;
    ht->table[idx].key = key;
    ht->table[idx].data = data;
  } else {
    hb_aux = &(ht->table[idx]);
    while (hb_aux->next != NULL)
      hb_aux = hb_aux->next;
    hb_new = malloc(sizeof(Hashbucket));
    hb_new->key = key;
    hb_new->data = data;
    hb_new->next = NULL;
    hb_aux->next = hb_new;
  }
}

void *hashtable_lookup(Hashtable *ht, void *key) {
  unsigned int idx = ht->hash(key, ht->size);

  Hashbucket *hb;
  for (hb = &(ht->table[idx]); hb != NULL; hb = hb->next) {
    printf("hb->key %s\n", hb->key);
    printf("key %s\n", key);
    printf("equals? %d\n", ht->equal(hb->key, key));
    if (ht->equal(hb->key, key))
      return hb->data;
  }
  return NULL;
  /* if (!ht->equal(ht->table[idx].key, key)) { */
  /*   printf("Return null\n"); */
  /*   return NULL; */
  /* } */

  /* return ht->table[idx].data; */
}

void hashtable_delete(Hashtable *ht, void *key) {
  unsigned int idx = ht->hash(key, ht->size);

  if (ht->table[idx].key != NULL)
    ht->nelems--;

  ht->table[idx].key = NULL;
  ht->table[idx].data = NULL;
  Hashbucket *hb = ht->table[idx].next;
  ht->table[idx].next = NULL;

  Hashbucket *aux;
  while (hb != NULL) {
    aux = hb;
    hb = hb->next;
    free(aux);
  }

}

void hashtable_destroy(Hashtable *ht) {
  Hashbucket *hb;
  int i;
  for (i = 0; i < ht->nelems; i++) {
    hb = &(ht->table[i]);
    hashtable_delete(ht, hb->key);
  }
  free(ht->table);
  free(ht);
}
