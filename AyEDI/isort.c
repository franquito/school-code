#include <stdlib.h>
#include <stdio.h>
#include "isort.h"
#include "SList.h"

void swap(int *p, int *q) {
  int t = *p;
  *p = *q;
  *q = t;
}

void isort(int data[], int sz) {
	int i, j;
	for (i=1; i<sz; ++i) {
		j=i;
		for(; j>0 && data[j]<data[j-1]; --j) 
			swap (data+j, data+(j-1));
	}
}


SList* isortl (SList *l) {
	SList *sorted = (SList *) malloc(sizeof(SList));
	SList *aux;
	sorted = NULL;
	int cont;
	
	while(l!=NULL) {
		cont=0;
		aux=sorted;
		for(; aux!= NULL && aux->data < l->data; aux=aux->next)
			cont++;
		sorted = slist_insert(sorted, cont, l->data);
		l= slist_remove(l, 0);
	}
	return sorted;
}
