#include "AStack.c"
#include <stdio.h>

int main(int argc, char *argv[]) {
  AStack *s = astack_create(8);
  astack_push(s, 9);
  astack_push(s, 5);
  astack_push(s, 5);
  astack_push(s, 1);
  astack_reverse(s);
  printf("El numero de elementos es %d\n", s->nelems);
  astack_print(s);
  astack_pop(s);
  astack_print(s);

  return 0;
}
