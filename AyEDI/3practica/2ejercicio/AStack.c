#include "AStack.h"
#include <stdio.h>
#include <stdlib.h>

AStack *astack_create(int size) {
  AStack *new = malloc(sizeof(AStack));
  new->data = malloc(sizeof(int)*size);
  new->nelems = 0;
  new->size = size;

  return new;
}

int astack_top(AStack *s) {
  return s->data[s->nelems-1];
}

void astack_push(AStack *s, int data) {
  if (s->nelems == s->size) {
    // Basically copy s on n, with a bigger size
    AStack *n = malloc(sizeof(s->size*s->size));
    n->size = s->size*s->size;
    int i;
    for (i = 0; i < s->nelems; i++)
      n->data[i] = s->data[i];
    n->data[s->nelems] = data;
    n->nelems = s->nelems++;
    astack_destroy(s);
    *s = *n;
  }
  s->data[s->nelems] = data;
  s->nelems++;
}

void astack_pop(AStack *s) {
  s->nelems--;
}

void astack_reverse(AStack *s) {
  AStack *n = astack_create(s->size);
  while (s->nelems != 0) {
    astack_push(n, astack_top(s));
    astack_pop(s);
  }
  astack_destroy(s);
  *s = *n;
}

void astack_print(AStack *s) {
  int i;
  for (i = s->nelems-1; i >= 0; i--)
    printf("%d ", s->data[i]);
  printf("\n");
}

void astack_destroy(AStack *s) {
  free(s);
}
