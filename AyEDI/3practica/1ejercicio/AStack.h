#define MAX 1000

typedef struct _AStack {
  int data[MAX];
  int nelems;
} AStack;

AStack *astack_create();

int astack_top(AStack *s);

void astack_push(AStack *s, int data);

void astack_pop(AStack *s);

void astack_reverse(AStack *s);

void astack_print(AStack *s);

void astack_destroy(AStack *s);
