#include "AStack.h"
#include <stdio.h>
#include <stdlib.h>

AStack *astack_create() {
  AStack *new = malloc(sizeof(AStack));
  new->nelems = 0;

  return new;
}

int astack_top(AStack *s) {
  return s->data[s->nelems-1];
}

void astack_push(AStack *s, int data) {
  s->data[s->nelems] = data;
  s->nelems++;
}

void astack_pop(AStack *s) {
  s->nelems--;
}

void astack_reverse(AStack *s) {
  AStack *n = astack_create();
  while (s->nelems != 0) {
    astack_push(n, astack_top(s));
    astack_pop(s);
  }
  astack_destroy(s);
  *s = *n;
}

void astack_print(AStack *s) {
  int i;
  for (i = s->nelems-1; i >= 0; i--)
    printf("%d ", s->data[i]);
  printf("\n");
}

void astack_destroy(AStack *s) {
  free(s);
}
