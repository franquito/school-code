#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "SLStackNode.h"


int main(){
	SLStack s=slstack_create();
	char line[1024];
	line[0]=' ';//simplifies cases
	fgets(line+1, sizeof(line)-1, stdin);
	int i;
	for(i=0; line[i]; i++){
		if(!isdigit(line[i]) && isdigit(line[i+1])){//beggining of number
			int v;
      // Extract a number i.e: '314pepe' -> 314
			sscanf(line+i+1, "%d", &v);
			s=slstack_push(s, v);
		}
		else if(line[i]=='+' || line[i]=='-' ||line[i]=='*'){
			int b=slstack_top(s);
			s=slstack_pop(s);
			int a=slstack_top(s);
			s=slstack_pop(s);
			switch(line[i]){
			case '+':
				s=slstack_push(s, a+b); break;
			case '-':
				s=slstack_push(s, a-b); break;
			case '*':
				s=slstack_push(s, a*b); break;
			}
		}
	}
	printf("%d\n", slstack_top(s));
	return 0;
}
