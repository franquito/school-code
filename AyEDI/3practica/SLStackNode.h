typedef struct _SLStackNode {
  int data;
  struct _SLStackNode *next;
} SLStackNode;

typedef SLStackNode *SLStack; // SLStack es un puntero a SLStackNode
                              // Leerlo haciendo un circulo desde
                              // SLStack, en sentido del reloj.

SLStack slstack_create();

int slstack_top(SLStack slstack);

SLStack slstack_push(SLStack slstack, int data);

SLStack slstack_pop(SLStack slstack);

