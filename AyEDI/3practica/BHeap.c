#include <stdio.h>
#include <stdlib.h>

void swap(int *p, int *q) {
  int t = *p;
  *p = *q;
  *q = t;
}

typedef struct _BHeap {
  int data[300];
  int nelems;
} BHeap;

BHeap *bheap_create() {
  BHeap *h = malloc(sizeof(BHeap));
  h->nelems = 0;

  return h;
}

int bheap_minimum(BHeap *h) {
  return h->data[0];
}

void bheap_erase_minimum(BHeap *h) {
  h->data[0] = h->data[h->nelems-1];
  h->nelems--;
  
  int index = 1;
  int *toSwap;
  int left, right;
  while (index < h->nelems + 1) {
    left = index*2-1;
    right = index*2;
    if (right >= h->nelems) {
      swap(&(h->data[index-1]), &(h->data[left]));
      index = left;
    } else if (h->data[right] > h->data[left]) {
      swap(&(h->data[index-1]), &(h->data[left]));
      index = left;
    } else if (h->data[right] < h->data[left]) {
      swap(&(h->data[index-1]), &(h->data[right]));
      index = right;
    }
  }
}

void bheap_insert(BHeap *h, int data) {
  h->data[h->nelems] = data;
  h->nelems++;

  int index = h->nelems;
  int parent = index/2;

  while (h->data[parent-1] > h->data[index-1]) {
    // @TODO: Falta terminarrrr.. algún día
  }
}

int main(int argc, char *argv[]) {
  BHeap *h = bheap_create();
  return 0;
}
