#include <stdlib.h>
#include <stdio.h>
#include "AQueue.h"

AQueue *queue_create() {
  AQueue *new = malloc(sizeof(AQueue));
  new->back=1;
  new->front=0;
  return new;
}

int aqueue_front(AQueue *queue) {
  int front = queue->front;
  return queue->data[front];
}
#define circular(x) (((x)+MAX_QUEUE)%MAX_QUEUE)
void aqueue_enqueue(AQueue *queue, int data) {
  queue->back=circular(queue->back-1);
  queue->data[queue->back] = data;
}

void aqueue_dequeue(AQueue *queue) {
  queue->front=circular(queue->front-1);
}

void aqueue_print(AQueue *queue) {
  int i = queue->back;
  printf("print: ");
  for (; i != circular(queue->front+1); i=circular(i+1)) {
    printf("%d ", queue->data[i]);
  }
  printf("\n");
}

void aqueue_destroy(AQueue *queue) {
  free(queue);
}
