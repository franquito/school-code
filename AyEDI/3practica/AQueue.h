#define MAX_QUEUE 30

typedef struct _AQueue {
  int data[MAX_QUEUE];
  int front, back;
} AQueue;

AQueue *queue_create();

int aqueue_front(AQueue *queue);

void aqueue_enqueue(AQueue *queue, int data);

void aqueue_dequeue(AQueue *queue);

void aqueue_print(AQueue *queue);

void aqueue_destroy(AQueue *queue);
