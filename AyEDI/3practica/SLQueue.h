typedef struct _SLQueueNode {
  int data;
  struct _SLQueueNode *next;
} SLQueueNode;

typedef SLQueueNode *SLQueue;

SLQueue slqueue_create();

int slqueue_front(SLQueue slqueue);

SLQueue slqueue_enqueue(SLQueue slqueue, int data);

SLQueue slqueue_dequeue(SLQueue slqueue);

void slqueue_print(SLQueue slqueue);

void slqueue_destroy(SLQueue slqueue);
