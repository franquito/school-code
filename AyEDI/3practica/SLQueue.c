#include <stdlib.h>
#include <stdio.h>
#include "SLQueue.h"

SLQueue slqueue_create() {
  return NULL;
}

int slqueue_front(SLQueue slqueue) {
  if (slqueue->next == NULL)
    return slqueue->data;
  return slqueue_front(slqueue->next);
}

SLQueue slqueue_enqueue(SLQueue slqueue, int data) {
  SLQueue new = malloc(sizeof(SLQueueNode));
  new->data = data;
  new->next = slqueue;
  return new;
}

SLQueue slqueue_dequeue(SLQueue slqueue) {
  if (slqueue->next == NULL) {
    free(slqueue);
    return NULL;
  }
  slqueue->next = slqueue_dequeue(slqueue->next);
  return slqueue;
}

void slqueue_print(SLQueue slqueue) {
  printf("slqueue print: ");
  while (slqueue != NULL) {
    printf("%d ", slqueue->data);
    slqueue = slqueue->next;
  }
  printf("\n");
}

void slqueue_destroy(SLQueue slqueue) {
  SLQueue aux;
  if (slqueue->next == NULL)
    free(slqueue);
  while (slqueue->next != NULL) {
    aux = slqueue->next;
    free(slqueue);
  }
  free(aux);
}
