#include <stdlib.h>
#include "NPI.h"

NPI npi_create() {
  return NULL;
  NPI s = malloc(sizeof(NPI));
  return s;
}

void *npi_top(NPI slstack) {
  return slstack->data;
}

NPI npi_push(NPI slstack, void *data) {
  NPI new = malloc(sizeof(NPINode));
  new->data = data;
  new->next = slstack;
  return new;
}

NPI npi_pop(NPI slstack) {
  if (slstack != NULL)
    return slstack->next;
  return NULL;
}
