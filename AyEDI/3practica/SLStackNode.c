#include <stdlib.h>
#include "SLStackNode.h"

SLStack slstack_create() {
  return NULL;
  SLStack s = malloc(sizeof(SLStackNode));
  return s;
}

int slstack_top(SLStack slstack) {
  return slstack->data;
}

SLStack slstack_push(SLStack slstack, int data) {
  SLStack new = malloc(sizeof(SLStackNode));
  new->data = data;
  new->next = slstack;
  return new;
}

SLStack slstack_pop(SLStack slstack) {
  if (slstack != NULL)
    return slstack->next;
  return NULL;
}
