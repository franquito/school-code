#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"
#include "SLStackNode.h"
#include "SList.h"
#include "AQueue.h"
#include "SLQueue.h"

typedef struct _foos {
  int x, y;
} foos;

int main(int argc, char *argv[]) {
  AStack *stack = astack_create();
  printf("Elementos del Stack: %d\n", stack->last);

  astack_push(stack, 4);
  astack_push(stack, 7);
  astack_push(stack, 1);

  printf("El elemento visible del stack es: %d\n", astack_top(stack));

  astack_pop(stack);

  stack = astack_reverse(stack);
  printf("%d\n", stack->last);
  printf("El primer elemento del reversed stack es: %d\n", astack_top(stack));

  printf("Lista de elementos: \n");
  astack_print(stack);

  printf("=====Ejercicio 3=====\n");
  SLStack sls = slstack_create();
  sls = slstack_push(sls, 31);
  sls = slstack_push(sls, 1);
  sls = slstack_push(sls, 11);
  sls = slstack_pop(sls);
  printf("%d\n", slstack_top(sls));

  printf("=====Ejercicio 4=====\n");
	SList *list = NULL;
	list = slist_append(list, 1);
	list = slist_append(list, 2);
	list = slist_append(list, 3);
  list = slist_reverse(list);
  printf("El primer elemento de la lista alvesre es: %d\n", list->data);

  printf("=====Ejercicio 5=====\n");
  // La interfaz te la debo
  printf("Viaje de ida\n");
  printf("=====Ejercicio 6=====\n");
  AQueue *q = queue_create();
  aqueue_enqueue(q, 1);
  aqueue_enqueue(q, 9);
  aqueue_enqueue(q, 5);
  aqueue_dequeue(q);
  printf("%d\n", aqueue_front(q));
  printf("=====Ejercicio 7=====\n");
  SLQueue s = slqueue_create();
  s = slqueue_enqueue(s, 1);
  s = slqueue_enqueue(s, 9);
  s = slqueue_enqueue(s, 5);
  s = slqueue_dequeue(s);
  slqueue_print(s);
  slqueue_destroy(s);

  foos f = {1, 2};
  printf("Something\n");
}

int main(int argc, char *argv[]) {
  /* code */
  return 0;
}
