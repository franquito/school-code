typedef struct _NPI {
  void *data;
  int size;
  struct _NPI *next;
} NPI_link;

typedef NPI_link *NPI;

NPI npi_create();

void *npi_top(NPI npi);

NPI npi_push(NPI npi, void *data);

NPI npi_pop(NPI npi);
