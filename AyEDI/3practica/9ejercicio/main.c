#include <stdio.h>
#include "BHeap.c"

int main(int argc, char *argv[]) {
  BHeap *h = bheap_create();
  bheap_insert(h, 3);
  bheap_insert(h, 5);
  bheap_insert(h, 4);
  bheap_insert(h, 6);
  bheap_insert(h, 9);

  bheap_erase_minimum(h);

  bheap_print(h);
  return 0;
}
