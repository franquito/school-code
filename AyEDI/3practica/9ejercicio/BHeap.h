typedef struct _BHeap {
  int data[100];
  int nelems;
} BHeap;

BHeap * bheap_create();

int bheap_minimum(BHeap *h);

void bheap_erase_minimum(BHeap *h);

void bheap_insert(BHeap *h, int data);

void bheap_print(BHeap *h);

void swap(int *a, int *b);
