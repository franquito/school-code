#include <stdlib.h>
#include <stdio.h>
#include "BHeap.h"

BHeap * bheap_create() {
  BHeap *new = malloc(sizeof(BHeap));
  new->nelems = 0;
  return new;
}

int bheap_minimum(BHeap *h) {
  return h->data[0];
}

void bheap_erase_minimum(BHeap *h) {
  h->data[0] = h->data[h->nelems-1];
  h->nelems--;

  int curr = 0, left = 1, right = 2;

  while (left < h->nelems && (h->data[curr] > h->data[left]
          || h->data[curr] > h->data[right])) {

    if (right >= h->nelems) {
      swap(&(h->data[left]), &(h->data[curr]));
      curr = left; }
    else if (h->data[left] > h->data[right]) {
      swap(&(h->data[right]), &(h->data[curr]));
      curr = right; }
    else {
      swap(&(h->data[left]), &(h->data[curr]));
      curr = left; }

    left = (curr+1) * 2;
    right = (curr+1) * 2 + 1;
  }
}

void bheap_insert(BHeap *h, int data) {
  h->data[h->nelems] = data;
  h->nelems++;

  int curr = h->nelems;
  while ((h->data[curr/2-1] > data) && (curr-1 != 0)) {
    swap(&(h->data[curr-1]), &(h->data[curr/2 - 1]));
    curr = curr/2;
  }
}

void bheap_print(BHeap *h) {
  int i = 0;
  for (; i < h->nelems; i++) {
    printf("%d ", h->data[i]);
  }
  printf("\n");
}

void swap(int *a, int *b) {
  int temp;
 
  temp = *b;
  *b   = *a;
  *a   = temp;
}
