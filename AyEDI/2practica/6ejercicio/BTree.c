#include <stdlib.h>
#include "BTree.h"

BTree *btree_create(int data, BTree *left, BTree *right) {
  BTree *newNode = malloc(sizeof(BTree));
  btree_data(newNode) = data;
  btree_left(newNode) = left;
  btree_right(newNode) = right;
  return newNode;
}

void  btree_destroy(BTree *node) {
  if (node != NULL) {
    btree_destroy(btree_left(node));
    btree_destroy(btree_right(node));
    free(node);
  }
}

void  btree_foreach(BTree *tree, VisitorFuncInt visit, void *extra_data) {
  if (tree != NULL) {
    visit(btree_data(tree), extra_data);
    btree_foreach(btree_left(tree), visit, extra_data);
    btree_foreach(btree_right(tree), visit, extra_data);
  }
}

BTree *mirror(BTree *tree) {
  BTree *aux;
  if (tree == NULL)
    return NULL;
  else {
    aux = tree->left;
    tree->left = mirror(tree->right);
    tree->right = aux;
    return tree;
  }
}
