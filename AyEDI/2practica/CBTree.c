#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "CBTree.h"

CBTree *cbtree_new(int niveles) {
	int size = pow(2, niveles);
	CBTree *new = malloc(sizeof(CBTree));
	int *arr = malloc(sizeof(int)*size);
	new->length = 0;
	new->array = arr;
	return new;
}

void cbtree_destroy(CBTree *tree) {
	free(tree->array);
	free(tree);
}

CBTree *cbtree_insert(CBTree *tree, int data) {
	*(tree->array + tree->length) = data;
	tree->length++;
	return tree;
}

int cbtree_nelements(CBTree *tree) {
	return tree->length; }

void cbtree_foreach(CBTree *tree, VisitorFuncInt visit, void *extra_data) {
	int i = 0;
	for (; i < tree->length; i++) {
		visit(*(tree->array + i), extra_data);
	}
}
