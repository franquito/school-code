struct _CBTree {
  int *data;
  int used;
};
typedef struct _CBTree * CBTree;

CBTree cbtree_new(int levels);

void cbtree_insert(CBTree tree, int data);
