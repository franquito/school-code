#include <stdlib.h>
#include <stdio.h>
#include "CBTree.h"
#include <math.h>

CBTree cbtree_new(int levels) {
  CBTree new = malloc(sizeof(CBTree));
  new->data = malloc(sizeof(int)*(int)(pow(2,levels)-1));
  new->used = 0;
  return new;
}

void cbtree_insert(CBTree tree, int data) {
  *((tree->data)+(tree->used)) = data;
  tree->used++;
}
