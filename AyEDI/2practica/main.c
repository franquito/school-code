#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "BTree.h"
#include "BSTree.h"
#include "CBTree.h"

static void print_int (int data, void *extra_data) {
	printf("%d ", data);
}

int main(int argc, char *argv[]) {
	BTree *ll = btree_create(1, NULL, NULL);
	BTree *l = btree_create(2, ll, NULL);
	BTree *r = btree_create(3, NULL, NULL);
	BTree *root = btree_create(4, l, r);

	btree_foreach(root, print_int, NULL);
	puts("");
	printf("Checkeando mirror: %d\n", mirror(root)->right->right->data);
	//btree_destroy(root);

	printf("El árbol suma: %d\n", btree_sum(root));
	printf("El árbol pesa: %d\n", btree_weight(root));
	printf("La altura del árbol es: %d\n", btree_height(root));

	BSTree *myBSTree = bstree_insert(NULL, 20);
	myBSTree = bstree_insert(myBSTree, 15);
	myBSTree = bstree_insert(myBSTree, 16);
	bstree_foreach(myBSTree, print_int, NULL);
	printf("\n");
	printf("Contiene el 16? %d \n", bstree_contains(myBSTree, 16));
	printf("Tiene %d elementos \n", bstree_nelements(myBSTree));
	printf("La altura es %d\n", bstree_height(myBSTree));

	RTree *sarasa = rtree_create(24);
	sarasa = rtree_addChild(sarasa, 15);
	/* printf("%d\n", sarasa->children->data); */

	CBTree *q = cbtree_new(3);
	q = cbtree_insert(q, 4);
	q = cbtree_insert(q, 2);
	printf("Tiene %d elementos.\n", cbtree_nelements(q));
	cbtree_foreach(q, print_int, NULL);
	printf("\n");

	/* cbtree_destroy(q); */
	return 0;
}
