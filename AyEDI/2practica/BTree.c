#include <stdlib.h>
#include "BTree.h"
#include "SList.h"

int max (int p1, int p2) {
	return (p1>p2) ? p1 : p2; }

BTree *btree_create(int data, BTree *left, BTree *right)
{
	BTree *newNode = malloc(sizeof(BTree));
	btree_data(newNode) = data;
	btree_left(newNode) = left;
	btree_right(newNode) = right;
	return newNode;
}

void  btree_destroy(BTree *node)
{
	if (node != NULL) {
		btree_destroy(btree_left(node));
		btree_destroy(btree_right(node));
		free(node);
	}
}

void btree_foreach(BTree *node, VisitorFuncInt visit, void *extra_data) {
	if (node != NULL) {
		visit(btree_data(node), extra_data);
		btree_foreach(btree_left(node), visit, extra_data);
		btree_foreach(btree_right(node), visit, extra_data);
	}
}

int btree_sum(BTree *tree) {
	if (tree == NULL) return 0;
	return btree_data(tree) + btree_sum(btree_left(tree)) + btree_sum(btree_right(tree));
}

int btree_weight(BTree *tree) {
	if (tree == NULL) return 0;
	return 1 + btree_weight(btree_left(tree)) + btree_weight(btree_right(tree));
}

int btree_height(BTree *tree) {
	if (tree == NULL) return 0;
	return max(1+btree_height(btree_left(tree)), 1+btree_height(btree_right(tree)));
}

SBTree *btree_downsize(SBTree *tree) {
	//Asumendo que uso un puntero para data. Y tree != NULL
	downsize(tree, tree->data);
	return tree;
}

void downsize(SBTree *tr, int *min) {
	if (tr == NULL) return ;
	if (*(tr->data) < *min)
		*min = *(tr->data);
	tr->data = min;
	return ;
}

BTree *mirror(BTree *tree) {
	if (tree == NULL) return NULL;
	if (tree->right == NULL && tree->left == NULL)
		return tree;
	if (tree->right != NULL || tree->left != NULL) {
		BTree *aux;
		aux = tree->right;
		tree->right = tree->left;
		tree->left = aux;
		
		tree->left = mirror(tree->left);
		tree->right = mirror(tree->right); }
	return tree;
}

RTree *rtree_create(int data) {
	RTree *new = malloc(sizeof(RTree));
	new->data = data;
	new->children = NULL;
	return new;
}

RTree *rtree_addChild(RTree *tree, int data) {
	tree->children = slist_append(tree->children, data);
	return tree;
}
