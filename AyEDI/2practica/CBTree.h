#ifndef __CBTREE_H__
#define __CBTREE_H__

typedef void (*VisitorFuncInt) (int data, void *extra_data);

/**
 * Los campos son privados, y no deberian ser accedidos
 * desde el código cliente.
 */
typedef struct CBTree {
	int length;
	int *array;
} CBTree;

/**
 * Crea un nuevo nodo, en complejidad O(1).
 *
 * Nota: el árbol vacio se representa con un (BTree *) NULL.
 */
CBTree *cbtree_new(int niveles);
void cbtree_destroy(CBTree *tree);
CBTree *cbtree_insert(CBTree *tree, int data);
int cbtree_nelements(CBTree *tree);
void cbtree_foreach(CBTree *tree, VisitorFuncInt visit, void *extra_data);
// BTree *btree_create(int data, BTree *left, BTree *right);
// BSTree *cbstree_insert(BSTree *tree, int data);
// 
// int bstree_nelements(BSTree *tree);
// 
// int bstree_height(BSTree *tree);

/**
 * Destruccion del árbol.
 */
// void  btree_destroy(BTree *root);

/**
 * Recorrido del árbol, utilizando la funcion pasada.
 */
// void  bstree_foreach(BSTree *list, VisitorFuncInt visit, void *extra_data);

// int bstree_contains(BSTree *tree, int data);
#endif /* __BTREE_H__ */
