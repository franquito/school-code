#ifndef __BSTREE_H__
#define __BSTREE_H__

typedef void (*VisitorFuncInt) (int data, void *extra_data);

/**
 * Los campos son privados, y no deberian ser accedidos
 * desde el código cliente.
 */
typedef struct _BSTNode {
	int    data;
	struct _BSTNode *left;
	struct _BSTNode *right;
} BSTree;

#define bstree_data(l)       (l)->data
#define bstree_left(l)       (l)->left
#define bstree_right(l)      (l)->right

/**
 * Crea un nuevo nodo, en complejidad O(1).
 *
 * Nota: el árbol vacio se representa con un (BTree *) NULL.
 */
// BTree *btree_create(int data, BTree *left, BTree *right);
BSTree *bstree_insert(BSTree *tree, int data);

int bstree_nelements(BSTree *tree);

int bstree_height(BSTree *tree);

/**
 * Destruccion del árbol.
 */
// void  btree_destroy(BTree *root);

/**
 * Recorrido del árbol, utilizando la funcion pasada.
 */
void  bstree_foreach(BSTree *list, VisitorFuncInt visit, void *extra_data);

int bstree_contains(BSTree *tree, int data);
#endif /* __BTREE_H__ */

