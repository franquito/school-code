#include <stdlib.h>
#include "BSTree.h"

/* int max (int p1, int p2) { */
/* 	return (p1>p2) ? p1 : p2; } */

void bstree_foreach(BSTree *node, VisitorFuncInt visit, void *extra_data) {
	if (node != NULL) {
		visit(bstree_data(node), extra_data);
		bstree_foreach(bstree_left(node), visit, extra_data);
		bstree_foreach(bstree_right(node), visit, extra_data);
	}
}

BSTree *bstree_insert(BSTree *tree, int data) {
	if (tree == NULL) {
		BSTree *new = malloc(sizeof(BSTree));
		new->data = data;
		new->left = NULL;
		new->right = NULL;
		return new;
	}
	if (tree->data > data)
		tree->left = bstree_insert(tree->left, data);
	else
		tree->right = bstree_insert(tree->right, data);
	return tree;
}

int bstree_contains(BSTree *tree, int data) {
	if (tree == NULL) return 0;
	if (tree->data == data) return 1;
	return bstree_contains(tree->left, data) || bstree_contains(tree->right, data);
}

int bstree_nelements(BSTree *tree) {
	if (tree == NULL) return 0;
	return 1 + bstree_nelements(bstree_left(tree)) + bstree_nelements(bstree_right(tree));
}

int bstree_height(BSTree *tree) {
	if (tree == NULL) return 0;
	return max(1+bstree_height(bstree_left(tree)), 1+bstree_height(bstree_right(tree)));
}
