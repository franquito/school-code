#ifndef __BTREE_H__
#define __BTREE_H__

typedef void (*VisitorFuncInt) (int data, void *extra_data);

typedef enum {
	BTREE_TRAVERSAL_ORDER_IN,
	BTREE_TRAVERSAL_ORDER_PRE,
	BTREE_TRAVERSAL_ORDER_POST
} BTreeTraversalOrder;

/**
 * Los campos son privados, y no deberian ser accedidos
 * desde el código cliente.
 */
typedef struct _BTNode {
	int    data;
	struct _BTNode *left;
	struct _BTNode *right;
} BTree;

typedef struct _SBTNode {
	int    *data;
	struct _SBTNode *left;
	struct _SBTNode *right;
} SBTree;

typedef struct _RNode {
	int  data;
	void *children;
} RTree;

#define btree_data(l)       (l)->data
#define btree_left(l)       (l)->left
#define btree_right(l)      (l)->right

/**
 * Crea un nuevo nodo, en complejidad O(1).
 *
 * Nota: el árbol vacio se representa con un (BTree *) NULL.
 */
BTree *btree_create(int data, BTree *left, BTree *right);

/**
 * Destruccion del árbol.
 */
void  btree_destroy(BTree *root);

/**
 * Recorrido del árbol, utilizando la funcion pasada.
 */
void  btree_foreach(BTree *list, VisitorFuncInt visit, void *extra_data);

int btree_sum(BTree *tree);

int btree_weight(BTree *tree);

int btree_height(BTree *tree);

void downsize(SBTree *tr, int *min);
SBTree *btree_downsize(SBTree *tree);

BTree *mirror(BTree *tree);

RTree *rtree_create(int data);
RTree *rtree_addChild(RTree *tree, int data);
#endif /* __BTREE_H__ */
