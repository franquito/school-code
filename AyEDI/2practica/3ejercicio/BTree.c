#include <stdlib.h>
#include "BTree.h"

BTree *btree_create(int data, BTree *left, BTree *right) {
  BTree *newNode = malloc(sizeof(BTree));
  btree_data(newNode) = data;
  btree_left(newNode) = left;
  btree_right(newNode) = right;
  return newNode;
}

void  btree_destroy(BTree *node) {
  if (node != NULL) {
    btree_destroy(btree_left(node));
    btree_destroy(btree_right(node));
    free(node);
  }
}

void  btree_foreach(BTree *tree, BTreeTraversalOrder order,
                    VisitorFuncInt visit, void *extra_data) {
  if (tree != NULL) {
    if (order == BTREE_TRAVERSAL_ORDER_PRE) {
      visit(btree_data(tree), extra_data);
      btree_foreach(btree_left(tree), order, visit, extra_data);
      btree_foreach(btree_right(tree), order, visit, extra_data);
    }
    else if (order == BTREE_TRAVERSAL_ORDER_POST) {
      btree_foreach(btree_left(tree), order, visit, extra_data);
      btree_foreach(btree_right(tree), order, visit, extra_data);
      visit(btree_data(tree), extra_data);
    } else {
      btree_foreach(btree_left(tree), order, visit, extra_data);
      visit(btree_data(tree), extra_data);
      btree_foreach(btree_right(tree), order, visit, extra_data);
    }
  }
}
