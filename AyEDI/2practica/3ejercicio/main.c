#include <stdio.h>
#include "BTree.c"

void print_data(int data, void *extra) {
  printf("%d ", data);
}

int main(int argc, char *argv[]) {
  BTree *ll = btree_create(1, NULL, NULL);
  BTree *l = btree_create(2, ll, NULL);
  BTree *r = btree_create(3, NULL, NULL);
  BTree *root = btree_create(4, l, r);

  BTreeTraversalOrder in_order = BTREE_TRAVERSAL_ORDER_IN;

  printf("Recorrido en orden: \n");
  btree_foreach(root, in_order, print_data, NULL);
  puts("");
  return 0;
}
