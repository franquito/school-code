#include <stdio.h>
#include <stdlib.h>

enum palo { Basto, Copa, Oro, Espada };

struct carta {
	enum palo p;
	int n;
};

struct contacto {
	char nombre[21];
	char telefono[21];
	unsigned int edad;
};

struct punto {
	float x;
	float y;
};

struct punto medio(struct punto p1, struct punto p2) {
	struct punto m;
	m.x = (p1.x+p2.x)/2;
	m.y = (p1.y+p2.y)/2;
	return m;
}

struct carta azar(struct carta c[], int len) {
	
}
void roberto() {}

void setin(int *n) {
	if (*n == 0) *n = 1;
	else *n = 0;
}

void swap(int *n1, int *n2) {
	int aux = *n1;
	*n1 = *n2;
	*n2 = aux;
}

void xorswap(int *n1, int *n2) {
	*n1 = *n1 ^ *n2;
	*n2 = *n1 ^ *n2;
	*n1 = *n1 ^ *n2;
}

struct contacto crearcontacto(void) {
	struct contacto nuevo;
	char nombre[21], tel[21]; int edad;

	printf("Nombre del nuevo contacto\n");
	scanf("%s", nuevo.nombre);
	printf("Teléfono: \n");
	scanf("%s", nuevo.telefono);
	printf("Edad:\n");
	scanf("%d", &nuevo.edad);

	return nuevo;
}

char *getnewline(void) {
	char line[31];
	char *p;

	char c;
	int i = 0;
	while((c=getchar()) && c != '\n') {
		line[i] = c;
		i++;
	}
	line[i] = '\0';
	
	p = &line[0];
	return p;
}

void actualizaredad(struct contacto *c) {
	int edad;
	scanf("%d", &edad);
	(*c).edad = edad;
}

float cprom(struct contacto *c, int n) {
	int i = 0;
	float acum = 0;
	for (; i < n; i++) {
		printf("%d\n", (*(c+i)).edad);
		acum += (*(c+i)).edad;
	}
	return acum/n;
}

void setzerozero(int arr[]) {
	arr[0] = 0;
}

int main(void) {
	int i = 0;

	char q[] = {'a','b','c'};
	
	printf("%p\n", &q[0]);
	printf("%p\n", &q[1]);

	struct carta mazo[48];
	for(i = 0; i < 48; i++) {
		mazo[i].n = (i+1) % 12;
		switch (i/12) {
			case 0: mazo[i].p = Basto; break;
			case 1: mazo[i].p = Copa; break;
			case 2: mazo[i].p = Oro; break;
			case 3: mazo[i].p = Espada; break;
		}
	}

	int toZero[] = {1,2,3};
	setzerozero(toZero);
	printf("Ejercicio 6: %d\n", toZero[0]);

	char *ptr;
	ptr = malloc(100);
	printf("%p\n", ptr);

	/* struct contacto franco = crearcontacto(); */
	/* actualizaredad(&franco); */
	/* printf("%d\n", franco.edad); */

	int number = 12;
	int *pointer = &number;
	setin(pointer);
	printf("%d\n", *pointer);

	int q1 = 20; int q2 = 30;
	xorswap(&q1, &q2);
	printf("XOR Swap 20 y 30: %d\n", q1);

	/* char *cs = getnewline(); */
	/* printf("%c\n", *cs); */

	/* struct contacto personas[2] = {crearcontacto(), crearcontacto()}; */
	/* printf("%f\n", cprom(&personas[0], 2)); */

	char *g = malloc(100);
	/* free(g); */
	free(g);
}
//5 clockwise spiral c.
