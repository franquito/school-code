#ifndef __BSORTG_H__
#define __BSORTG_H__

typedef int (*CmpFunc)(void *, void *);

void bsortg (int data[], int sz, CmpFunc cmp);

#endif
