#include "SList.h"
#include <stdlib.h>

void ssort(int arr[], int sz) {
  int sorted = 0;
  int *min;
  int i = 0;
  while (sorted != sz) {
    min = &arr[sorted];

    for (i = sorted; i < sz; i++)
      if (*min > arr[i])
        min = &arr[i];

    swap(&arr[sorted], min);
    sorted++;
  }
}

void _rssort(int arr[], int from, int sz) {
  if (from == sz)
    return ;

  int i = 0;
  int *min;

  min = &arr[from];
  for (i = from; i < sz; i++) {
    if (*min > arr[i])
      min = &arr[i];
  }

  swap(&arr[from], min);
  from++;

  _rssort(arr, from, sz);
}

void rssort(int arr[], int sz) {
  _rssort(arr, 0, sz);
}

SList* ssortl(SList *list, int sz) {
  SList *new = NULL;
  SList *walker;
  int min;
  int index;
  int count;

  while (list != NULL) {
    walker = list;
    min = list->data;
    index = count = 0;

    while (walker != NULL) {
      if (walker->data < min) {
        min = walker->data;
        index = count;
      } 
      count++;
      walker = walker->next;
    }

    new = slist_append(new, min);
    list = slist_remove(list, index);
  }
  return new;
}
