#ifndef __ISORTL_H__
#define __ISORTL_H__

SList* isortl(SList *list);

void isort(int arr[], int size);

void swap(int *i, int *j);

#endif
