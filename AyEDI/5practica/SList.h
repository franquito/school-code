#ifndef __SLIST_H__
#define __SLIST_H__

typedef void (*VisitorFuncInt) (int data, void *extra_data);

/**
 * Los campos son privados, y no deberian ser accedidos
 * desde el código cliente.
 */
typedef struct _SList {
        int    data;
        struct _SList *next;
} SList;

#define slist_data(l)       (l)->data
#define slist_next(l)       (l)->next

/**
 * Agrega un elemento al final de la lista, en complejidad O(n).
 *
 * Nota: una lista vacia se representa con un (SList *) NULL.
 */
SList *slist_append(SList *list, int data);

/**
 * Agrega un elemento al inicio de la lista, en complejidad O(1).
 *
 * Nota: una lista vacia se representa con un (SList *) NULL.
 */
SList *slist_prepend(SList *list, int data);

/**
 * Destruccion de la lista.
 */
void  slist_destroy(SList *list);

/**
 * Recorrido de la lista, utilizando la funcion pasada.
 */
void slist_foreach(SList *list, VisitorFuncInt visit, void *extra_data);

/**
 * Tiene próximo elemento? Sólo para listas de 1+ elementos.
 */
int slist_has_next(SList *list);

// Tamaño de la lista.
int slist_length(SList *list);

// Concatena listas.
SList *slist_concat(SList *list1, SList *list2);

// Inserta dato en posición arbitraria. pos < tamaño de list1
SList *slist_insert(SList *list1, int data, int pos);

// Remover dato.
SList *slist_remove(SList *list1, int n);

// Elem está?
int slist_contains(SList *list1, int data);

// Devuelve el index
unsigned int slist_index(SList *list, const int data);

// Intersección.
SList *slist_intersect(SList *l1, SList *l2);

// Intersección 1.
SList *slist_insersect_custom(SList *l1, SList *l2, int fun(int d1, int d2));

#endif /* __SLIST_H__ */
