#include "SList.h"
#include "isortl.h"
#include <stdio.h>

SList* isortl(SList *list) {
  SList *walker;
  SList *new = NULL;
  int data;
  int count;

  if (list != NULL) {
    new = slist_append(new, list->data);
    list = slist_remove(list, 0); }

  while (list != NULL) {
    data = list->data;

    walker = new;
    count = 0;
    while ((walker != NULL) && (data > walker->data)) {
      walker = walker->next;
      count++; }
    new = slist_insert(new, data, count);

    list = list->next;
  }

  return new;
}

void isort(int arr[], int size) {
  int i;
  int j;

  //assert: size > 0
  for (i = 1; i < size; i++) {
    j = i;
    while (j > 0 && arr[j] < arr[j-1]) {
      swap(&arr[j], &arr[j-1]);
      j--;
    }
  }
}
