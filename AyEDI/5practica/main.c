#include <stdio.h>
#include "SList.h"
#include "isortl.h"
#include "bsort.h"
#include "ssortl.h"
#include "bsortg.h"

#define NELEMS(a) (sizeof((a))/sizeof(int))

int lessThan (void *val1, void *val2) {
  int *v1 = (int *)val1;
  int *v2 = (int *)val2;
  
  if (*v1 < *v2)
    return 1;
  return 0;
}



int are_permutable(int *one, int *two, int size) {
  isort(one, size);
  isort(two, size);

  int checked = 0;

  while (checked < size) {
    if (one[checked] != two[checked])
      return 0;
    checked++;
  }
  return 1;
}

int main(void) {
  int arr[] = { 2, 4, 1, 3, 7, 8, 6 };
  int orr[] = { 4, 2, 1, 3, 7, 8, 6 };
  int i;

  printf("%d\n", are_permutable(arr, orr, 7));
  /* bsort(arr, NELEMS(arr)); */
  /* rssort(arr, NELEMS(arr)); */
  /* bsortg(arr, NELEMS(arr), lessThan); */

  isort(arr, NELEMS(arr));
  for (i  = 0; i < NELEMS(arr); i++)
    printf("%d ", arr[i]);
  puts("");

  SList *s = slist_append(NULL, 2);
  s = slist_append(s, 4);
  s = slist_append(s, 1);
  s = slist_append(s, 3);
  s = slist_append(s, 7);
  s = slist_append(s, 8);
  s = slist_append(s, 6);

  /* s = ssortl(s, 7); */
  /* s = isortl(s); */
  /* printf("%d \n", s->next->next->next->next->data); */


  return 0;
}
